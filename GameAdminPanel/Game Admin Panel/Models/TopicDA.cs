﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Game_Admin_Panel
{
    public static class TopicDA
    {
        public static IEnumerable<Topic> GetTopicsData()
        {
            using (var entities = new Lasta_Entities())
            {
                var data = entities.Topics.Where(p => p.IsDeleted == false).OrderByDescending(o => o.CreatedOn).ToList();
                return data;
            }
        }

        public static long InsertTopic(EditableTopic topic)
        {
            using (var entities = new Lasta_Entities())
            {
                var newtopic = new Topic()
                {
                    TopicContent = topic.TopicContent,
                    CreatedOn = topic.CreatedOn,
                    IsDeleted = topic.IsDeleted
                };

                entities.Topics.Add(newtopic);
                entities.SaveChanges();
                return newtopic.ID;
            }
        }

        public static long UpdateTopic(EditableTopic topic)
        {
            using (var entities = new Lasta_Entities())
            {
                var existstopic = entities.Topics.FirstOrDefault(p => p.ID == topic.ID);
                if (existstopic != null)
                {
                    existstopic.TopicContent = topic.TopicContent;
                    existstopic.CreatedOn = topic.CreatedOn;
                    entities.SaveChanges();
                    return existstopic.ID;
                }
                return 0;
            }
        }

        public static long DeletedTopic(long ID)
        {
            using (var entities = new Lasta_Entities())
            {
                var existstopic = entities.Topics.FirstOrDefault(p => p.ID == ID);
                if (existstopic != null)
                {
                    existstopic.IsDeleted = true;
                    entities.SaveChanges();
                    return existstopic.ID;
                }
                return 0;
            }
        }

    }

    public class EditableTopic
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Topic Content is required")]
        public string TopicContent { get; set; }

        [Required(ErrorMessage = "CreatedOn is required")]
        public DateTime CreatedOn { get; set; }

        [Required(ErrorMessage = "IsDeleted is required")]
        public bool IsDeleted { get; set; }

    }
}