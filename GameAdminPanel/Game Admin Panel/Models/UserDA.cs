﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Game_Admin_Panel
{
    public static class UserDA
    {
        public static IEnumerable<User> GetUsersData()
        {
            using (var entities = new Lasta_Entities())
            {
                var data = entities.Users.Where(p => p.IsDeleted == false).OrderByDescending(o => o.CreatedOn).ToList();
                return data;
            }
        }

        public static IEnumerable<object> Top3forquestion(int questionid)
        {
            using (var entities = new Lasta_Entities())
            {
                var data = entities.Answers.Where(p => p.IsDeleted == false && p.QuestionID == questionid).Select(g => new { g.Question.Number, g.Question.QuestionContent, g.User.Fullname, g.User.Birthday, g.User.Address, g.User.PhoneNumber, g.User.Email, g.User.Username, g.User.CreatedOn, g.Score, g.Duration }).OrderBy(o => o.Duration).Take(3).ToList();

                return data;
            }
        }

        public static IEnumerable<object> GetInfoTotalPointUser(int take)
        {
            using (var entities = new Lasta_Entities())
            {
                var data = entities.Answers.Where(p => p.IsDeleted == false).GroupBy(g => new { g.User.Fullname, g.User.Birthday, g.User.Address, g.User.PhoneNumber, g.User.Email, g.User.Username, g.User.CreatedOn }).Select(c => new { c.Key.Fullname, c.Key.Birthday, c.Key.Address, c.Key.PhoneNumber, c.Key.Email, c.Key.Username, c.Key.CreatedOn, TotalScore = c.Sum(s => s.Score) }).OrderByDescending(o => o.TotalScore).Take(take).ToList();

                return data;
            }
        }

        public static IEnumerable<object> GetAllUserByTotalScore()
        {
            using (var entities = new Lasta_Entities())
            {
                var data = entities.Answers.Where(p => p.IsDeleted == false).GroupBy(g => new { g.User.Fullname, g.User.Birthday, g.User.Address, g.User.PhoneNumber, g.User.Email, g.User.Username, g.User.CreatedOn }).Select(c => new { c.Key.Fullname, c.Key.Birthday, c.Key.Address, c.Key.PhoneNumber, c.Key.Email, c.Key.Username, c.Key.CreatedOn, TotalScore = c.Sum(s => s.Score) }).OrderByDescending(o => o.TotalScore).ToList();

                return data;
            }
        }

        public static IEnumerable<object> GetUserByContest(int contestID)
        {
            using (var entities = new Lasta_Entities())
            {
                var data = entities.Answers.Where(p => p.IsDeleted == false && p.ContestDayID == contestID).GroupBy(g => new { g.User.Fullname, g.User.Birthday, g.User.Address, g.User.PhoneNumber, g.User.Email, g.User.Username, g.User.CreatedOn }).Select(c => new { c.Key.Fullname, c.Key.Birthday, c.Key.Address, c.Key.PhoneNumber, c.Key.Email, c.Key.Username, c.Key.CreatedOn, TotalScore = c.Sum(s => s.Score) }).OrderByDescending(o => o.TotalScore).ToList();

                return data;
            }
        }

        public static IEnumerable<object> GetUserByTopic(int topicid)
        {
            using (var entities = new Lasta_Entities())
            {
                var data = entities.Answers.Where(p => p.IsDeleted == false && p.Question.TopicID == topicid).GroupBy(g => new { g.User.Fullname, g.User.Birthday, g.User.Address, g.User.PhoneNumber, g.User.Email, g.User.Username, g.User.CreatedOn }).Select(c => new { c.Key.Fullname, c.Key.Birthday, c.Key.Address, c.Key.PhoneNumber, c.Key.Email, c.Key.Username, c.Key.CreatedOn, TotalScore = c.Sum(s => s.Score) }).OrderByDescending(o => o.TotalScore).ToList();

                return data;
            }
        }

        public static int CountUserByContest(int contestid)
        {
            using (var entities = new Lasta_Entities())
            {
                var count = (from p in entities.Users
                             join c in entities.Answers on p.ID equals c.UserID
                             where p.IsDeleted == false && c.IsDeleted == false && c.ContestDayID == contestid
                             select p).Distinct().Count();

                return count;
            }
        }

        public static long InsertUser(EditableUser user)
        {
            using (var entities = new Lasta_Entities())
            {
                var newuser = new User()
                {
                    Fullname = user.FullName,
                    Address = user.Address,
                    Birthday = user.Birthday,
                    CreatedOn = user.CreatedOn == null ? DateTime.Now.Date : user.CreatedOn,
                    Email = user.Email,
                    Password = user.Password,
                    Username = user.UserName,
                    UniqueIdentifier = Guid.NewGuid().ToString(),
                    IsDeleted = user.IsDeleted,
                    PhoneNumber = user.PhoneNumber
                };
                entities.Users.Add(newuser);
                entities.SaveChanges();
                return newuser.ID;
            }
        }

        public static long UpdateUser(EditableUser user)
        {
            using (var entities = new Lasta_Entities())
            {
                var edituser = entities.Users.FirstOrDefault(p => p.ID == user.ID);
                if (edituser != null)
                {
                    edituser.Fullname = user.FullName;
                    edituser.Address = user.Address;
                    edituser.Birthday = user.Birthday;
                    edituser.Email = user.Email;
                    edituser.CreatedOn = user.CreatedOn;
                    edituser.Password = user.Password;
                    edituser.Username = user.UserName;
                    edituser.PhoneNumber = user.PhoneNumber;
                    entities.SaveChanges();
                    return edituser.ID;
                }
                return 0;
            }
        }

        public static long DeletedUser(long id)
        {
            using (var entities = new Lasta_Entities())
            {
                var data = entities.Users.FirstOrDefault(p => p.ID == id);
                if (data != null)
                {
                    data.IsDeleted = true;
                    entities.SaveChanges();
                    return id;
                }
                return 0;
            }
        }
    }

    public class EditableUser
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "FullName is required")]
        [StringLength(1000, ErrorMessage = "Must be under 1000 characters")]
        public string FullName { get; set; }

        [DataType(DataType.DateTime, ErrorMessage = "Must be Date Type")]
        public DateTime Birthday { get; set; }

        public string Address { get; set; }

        [DataType(DataType.PhoneNumber, ErrorMessage = "Must be PhoneNumber format")]
        public string PhoneNumber { get; set; }

        [DataType(DataType.EmailAddress, ErrorMessage = "Must be Email format")]
        public string Email { get; set; }

        [Required(ErrorMessage = "User Name is required")]
        [StringLength(100, ErrorMessage = "Must be under 100 characters")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [StringLength(100, ErrorMessage = "Must be under 100 characters")]
        public string Password { get; set; }

        public string UniqueIdentifier { get; set; }

        [DataType(DataType.DateTime, ErrorMessage = "Must be Date Type")]
        public DateTime CreatedOn { get; set; }

        public bool IsDeleted { get; set; }

    }
}