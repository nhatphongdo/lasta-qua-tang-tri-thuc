﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Game_Admin_Panel
{
    public static class Enums
    {
        public static bool CheckLogin()
        {
            HttpCookie cookie = System.Web.HttpContext.Current.Request.Cookies.Get("LastaLoggedIn");
            if (cookie != null)
            {
                return true;
            }
            return false;
        }
    }
}