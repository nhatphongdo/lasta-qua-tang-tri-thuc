﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Game_Admin_Panel
{
    public static class AnswerDA
    {
        public static IEnumerable<Answer> GetAnswersData()
        {
            using (var entities = new Lasta_Entities())
            {
                var data = entities.Answers.Where(p => p.IsDeleted == false).OrderByDescending(o => o.CreatedOn).ToList();
                return data;
            }
        }

        public static int CountAnswerInContest(int contestid)
        {
            using (var entities = new Lasta_Entities())
            {
                var count = entities.Answers.Count(p => p.IsDeleted == false && p.ContestDayID == contestid);
                return count;
            }
        }

        public static long InsertAnswer(EditableAnswer ans)
        {
            using (var entities = new Lasta_Entities())
            {
                var newans = new Answer()
                {
                    QuestionID = ans.QuestionID,
                    UserID = ans.UserID,
                    AnswerNumber = ans.AnswerNumber,
                    ContestDayID = ans.ContestDayID,
                    StartTime = ans.StartTime,
                    Duration = ans.Duration,
                    Score = ans.Score,
                    CreatedOn = ans.CreatedOn,
                    IsDeleted = ans.IsDeleted
                };
                entities.Answers.Add(newans);
                entities.SaveChanges();
                return newans.ID;
            }
        }

        public static long UpdateAnswer(EditableAnswer ans)
        {
            using (var entities = new Lasta_Entities())
            {
                var existsans = entities.Answers.FirstOrDefault(p => p.ID == ans.ID);
                if (existsans != null)
                {
                    existsans.QuestionID = ans.QuestionID;
                    existsans.UserID = ans.UserID;
                    existsans.AnswerNumber = ans.AnswerNumber;
                    existsans.ContestDayID = ans.ContestDayID;
                    existsans.StartTime = ans.StartTime;
                    existsans.Duration = ans.Duration;
                    existsans.Score = ans.Score;

                    entities.SaveChanges();
                    return existsans.ID;
                }
                return 0;
            }
        }

        public static long DeletedAnswer(long ID)
        {
            using (var entities = new Lasta_Entities())
            {
                var existsans = entities.Answers.FirstOrDefault(p => p.ID == ID);
                if (existsans != null)
                {
                    existsans.IsDeleted = true;
                    entities.SaveChanges();
                    return existsans.ID;
                }
                return 0;
            }
        }
    }

    public class EditableAnswer
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Question is required")]
        public int QuestionID { get; set; }

        [Required(ErrorMessage = "User is required")]
        public long UserID { get; set; }

        [Required(ErrorMessage = "AnswerNumber is required")]
        public int AnswerNumber { get; set; }

        [Required(ErrorMessage = "ContestDay is required")]
        public int ContestDayID { get; set; }

        [Required(ErrorMessage = "StartTime is required")]
        public DateTime StartTime { get; set; }

        [Required(ErrorMessage = "Duration is required")]
        public int Duration { get; set; }

        [Required(ErrorMessage = "Score is required")]
        public int Score { get; set; }

        [Required(ErrorMessage = "CreatedOn is required")]
        public DateTime CreatedOn { get; set; }

        [Required(ErrorMessage = "IsDeleted is required")]
        public bool IsDeleted { get; set; }

    }
}