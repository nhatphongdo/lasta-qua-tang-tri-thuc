﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Game_Admin_Panel
{
    public static class PrizeDA
    {
        public static IEnumerable<Prize> GetPrizesData()
        {
            using (var entities = new Lasta_Entities())
            {
                var data = entities.Prizes.Where(p => p.IsDeleted == false).OrderByDescending(o => o.CreatedOn).ToList();
                return data;
            }
        }

        public static int CountPrizeByContest(int contestid)
        {
            using (var entities = new Lasta_Entities())
            {
                var data = entities.Prizes.Count(p => p.ContestDayID == contestid && p.IsDeleted == false);
                return data;
            }
        }

        public static IEnumerable<Prize> GetPrizeByContest(int contestid)
        {
            using (var entities = new Lasta_Entities())
            {
                var data = entities.Prizes.Where(p => p.ContestDayID == contestid && p.IsDeleted == false).OrderByDescending(p => p.CreatedOn).ToList();
                return data;
            }
        }

        public static long InsertPrize(EditablePrize prize)
        {
            using (var entities = new Lasta_Entities())
            {
                var newprize = new Prize()
                {
                    Name = prize.Name,
                    Description = prize.Description,
                    Image = prize.Image,
                    Sponsor = prize.Sponsor,
                    SponsorType = prize.SponsorType,
                    ContestDayID = prize.ContestDayID,
                    Type = prize.Type,
                    CreatedOn = prize.CreatedOn,
                    IsDeleted = prize.IsDeleted
                };

                entities.Prizes.Add(newprize);
                entities.SaveChanges();
                return newprize.ID;
            }
        }

        public static long UpdatePrize(EditablePrize prize)
        {
            using (var entities = new Lasta_Entities())
            {
                var existsprize = entities.Prizes.FirstOrDefault(p => p.ID == prize.ID);
                if (existsprize != null)
                {
                    existsprize.Name = prize.Name;
                    existsprize.Description = prize.Description;
                    existsprize.Image = prize.Image;
                    existsprize.Sponsor = prize.Sponsor;
                    existsprize.SponsorType = prize.SponsorType;
                    existsprize.ContestDayID = prize.ContestDayID;
                    existsprize.Type = prize.Type;
                    existsprize.CreatedOn = prize.CreatedOn;
                    entities.SaveChanges();
                    return existsprize.ID;
                }
                return 0;
            }
        }

        public static long DeletedPrize(long ID)
        {
            using (var entities = new Lasta_Entities())
            {
                var existsprize = entities.Prizes.FirstOrDefault(p => p.ID == ID);
                if (existsprize != null)
                {
                    existsprize.IsDeleted = true;
                    entities.SaveChanges();
                    return existsprize.ID;
                }
                return 0;
            }
        }
    }

    public class EditablePrize
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Image { get; set; }

        public string Sponsor { get; set; }

        [Required(ErrorMessage = "SponsorType is required")]
        public int SponsorType { get; set; }

        [Required(ErrorMessage = "ContestDay is required")]
        public int ContestDayID { get; set; }

        [Required(ErrorMessage = "Type is required")]
        public int Type { get; set; }

        [Required(ErrorMessage = "CreatedOn is required")]
        public DateTime CreatedOn { get; set; }

        [Required(ErrorMessage = "IsDeleted is required")]
        public bool IsDeleted { get; set; }

    }
}