﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Game_Admin_Panel
{
    public class ContestDayDA
    {
        public static IEnumerable<ContestDay> GetContestDaysData()
        {
            using (var entities = new Lasta_Entities())
            {
                var data = entities.ContestDays.Where(p => p.IsDeleted == false).OrderByDescending(o => o.StartTime).ToList();
                return data;
            }
        }

        public static IEnumerable<object> ManagerContestDaysData()
        {
            using (var entities = new Lasta_Entities())
            {
                var data = entities.Answers.Where(p => p.IsDeleted == false).GroupBy(p => new { p.ContestDay.StartTime, p.ContestDay.State, p.ContestDay.CreatedOn }).Select(g => new { g.Key.StartTime, g.Key.State, g.Key.CreatedOn, Totalplay = g.Count() }).Distinct().OrderByDescending(d => d.StartTime).ToList();
                return data;
            }
        }

        public static long InsertContestDay(EditableContestDay contest)
        {
            using (var entities = new Lasta_Entities())
            {
                var newcontest = new ContestDay()
                {
                    UniqueID = contest.UniqueID,
                    StartTime = contest.StartTime,
                    NumberOfQuestions = contest.NumberOfQuestions,
                    State = contest.State,
                    RemainTime = contest.RemainTime,
                    CreatedOn = contest.CreatedOn,
                    IsDeleted = contest.IsDeleted
                };
                entities.ContestDays.Add(newcontest);
                entities.SaveChanges();
                return newcontest.ID;
            }
        }

        public static long UpdateContestDay(EditableContestDay contest)
        {
            using (var entities = new Lasta_Entities())
            {
                var existscontest = entities.ContestDays.FirstOrDefault(p => p.ID == contest.ID);
                if (existscontest != null)
                {
                    existscontest.StartTime = contest.StartTime;
                    existscontest.NumberOfQuestions = contest.NumberOfQuestions;
                    existscontest.State = contest.State;
                    existscontest.RemainTime = contest.RemainTime;

                    entities.SaveChanges();
                    return existscontest.ID;
                }
                return 0;
            }
        }

        public static long DeletedContestDay(long ID)
        {
            using (var entities = new Lasta_Entities())
            {
                var existscontest = entities.ContestDays.FirstOrDefault(p => p.ID == ID);
                if (existscontest != null)
                {
                    existscontest.IsDeleted = true;
                    entities.SaveChanges();
                    return existscontest.ID;
                }
                return 0;
            }
        }
    }

    public class EditableContestDay
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "UniqueID is required")]
        public Guid UniqueID { get; set; }

        [Required(ErrorMessage = "StartTime is required")]
        public DateTime StartTime { get; set; }

        [Required(ErrorMessage = "NumberOfQuestions is required")]
        public int NumberOfQuestions { get; set; }

        [Required(ErrorMessage = "State is required")]
        public int State { get; set; }

        [Required(ErrorMessage = "RemainTime is required")]
        public int RemainTime { get; set; }

        [Required(ErrorMessage = "CreatedOn is required")]
        public DateTime CreatedOn { get; set; }

        [Required(ErrorMessage = "IsDeleted is required")]
        public bool IsDeleted { get; set; }

    }
}