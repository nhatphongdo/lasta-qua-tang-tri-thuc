﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Game_Admin_Panel
{
    public static class QuestionDA
    {
        public static IEnumerable<Question> GetQuestionsData()
        {
            using (var entities = new Lasta_Entities())
            {
                var data = entities.Questions.Where(p => p.IsDeleted == false).OrderByDescending(o => o.CreatedOn).ToList();
                return data;
            }
        }

        public static IEnumerable<Topic> GetTopicData()
        {
            using (var entities = new Lasta_Entities())
            {
                var data = entities.Topics.Where(p => p.IsDeleted == false).ToList();
                return data;
            }
        }

        public static IEnumerable<ContestDay> GetContestsData()
        {
            using (var entities = new Lasta_Entities())
            {
                var data = entities.ContestDays.Where(p => p.IsDeleted == false).ToList();
                return data;
            }
        }

        public static IEnumerable<Question> GetQuestionByContest(int contestid)
        {
            using (var entities = new Lasta_Entities())
            {
                var data = entities.Questions.Where(p => p.IsDeleted == false && p.ContestDayID == contestid);
                return data;
            }
        }

        public static int CountQuestionInContest(int contestid)
        {
            using (var entitites = new Lasta_Entities())
            {
                var count = entitites.Questions.Count(p => p.IsDeleted == false && p.ContestDayID == contestid);
                return count;
            }
        }

        public static long InsertQuestion(EditableQuestion ques)
        {
            using (var entities = new Lasta_Entities())
            {
                var newq = new Question()
                {
                    Number = ques.Number,
                    QuestionContent = ques.QuestionContent,
                    Answer1 = ques.Answer1,
                    Answer2 = ques.Answer2,
                    Answer3 = ques.Answer3,
                    Answer4 = ques.Answer4,
                    TopicID = ques.TopicID,
                    Difficulty = ques.Difficulty,
                    CorrectAnswer = ques.CorrectAnswer,
                    ContestDayID = ques.ContestDayID,
                    CountdownTime = ques.CountdownTime,
                    Score = ques.Score,
                    CreatedOn = ques.CreatedOn,
                    IsDeleted = ques.IsDeleted
                };
                entities.Questions.Add(newq);
                entities.SaveChanges();
                return newq.ID;
            }
        }

        public static long UpdateQuestion(EditableQuestion ques)
        {
            using (var entities = new Lasta_Entities())
            {
                var existsques = entities.Questions.FirstOrDefault(p => p.ID == ques.ID);
                if (existsques != null)
                {
                    existsques.Number = ques.Number;
                    existsques.QuestionContent = ques.QuestionContent;
                    existsques.Answer1 = ques.Answer1;
                    existsques.Answer2 = ques.Answer2;
                    existsques.Answer3 = ques.Answer3;
                    existsques.Answer4 = ques.Answer4;
                    existsques.TopicID = ques.TopicID;
                    existsques.Difficulty = ques.Difficulty;
                    existsques.CorrectAnswer = ques.CorrectAnswer;
                    existsques.ContestDayID = ques.ContestDayID;
                    existsques.CountdownTime = ques.CountdownTime;
                    existsques.Score = ques.Score;
                    entities.SaveChanges();
                    return existsques.ID;
                }
                return 0;
            }
        }

        public static long DeletedQuestion(long ID)
        {
            using (var entities = new Lasta_Entities())
            {
                var existsques = entities.Questions.FirstOrDefault(p => p.ID == ID);
                if (existsques != null)
                {
                    existsques.IsDeleted = true;
                    entities.SaveChanges();
                    return existsques.ID;
                }
                return 0;
            }
        }
    }

    public class EditableQuestion
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Number is required")]
        public int Number { get; set; }

        [Required(ErrorMessage = "Question Content is required")]
        public string QuestionContent { get; set; }

        public string Answer1 { get; set; }

        public string Answer2 { get; set; }

        public string Answer3 { get; set; }

        public string Answer4 { get; set; }

        [Required(ErrorMessage = "TopicID is required")]
        [DataType(DataType.Currency)]
        public int TopicID { get; set; }

        [Required(ErrorMessage = "Difficulty is required")]
        public int Difficulty { get; set; }

        [Required(ErrorMessage = "CorrectAnswer is required")]
        public int CorrectAnswer { get; set; }

        public int ContestDayID { get; set; }

        [Required(ErrorMessage = "CountdownTime is required")]
        public int CountdownTime { get; set; }

        [Required(ErrorMessage = "Score is required")]
        public int Score { get; set; }

        [Required(ErrorMessage = "CreatedOn is required")]
        public DateTime CreatedOn { get; set; }

        [Required(ErrorMessage = "IsDeleted is required")]
        public bool IsDeleted { get; set; }

    }
}