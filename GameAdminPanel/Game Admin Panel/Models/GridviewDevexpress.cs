﻿using DevExpress.Web.ASPxGridView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Game_Admin_Panel
{
    public class GridviewDevexpress
    {
        public const string
            ImageQueryKey = "DXImage",
            PageSizeSessionKey = "ed5e843d-cff7-47a7-815e-832923f7fb09",
            EditingModeSessionKey = "AA054892-1B4C-4158-96F7-894E1545C05C";

        public static GridViewEditingMode EditMode
        {
            get
            {
                if (HttpContext.Current.Session[EditingModeSessionKey] == null)
                    HttpContext.Current.Session[EditingModeSessionKey] = GridViewEditingMode.EditFormAndDisplayRow;
                return (GridViewEditingMode)HttpContext.Current.Session[EditingModeSessionKey];
            }
            set { HttpContext.Current.Session[EditingModeSessionKey] = value; }
        }
    }
}