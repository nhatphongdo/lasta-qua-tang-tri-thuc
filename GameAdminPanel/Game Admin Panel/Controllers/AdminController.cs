﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;

namespace Game_Admin_Panel.Controllers
{
    public class AdminController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        // =============================================TOP=================================================
        //====================================== Người chơi top 5 =============================
        public ActionResult LayersTop5()
        {
            if (!Enums.CheckLogin())
            {
                return RedirectToAction("Login", "Home");
            }
            var top5 = UserDA.GetInfoTotalPointUser(5);
            return View(top5);
        }
        public ActionResult PartialLayersTop5()
        {
            var top5 = UserDA.GetInfoTotalPointUser(5);
            return PartialView("PartialLayersTop5", top5);
        }
        // ===================================== Người chơi Top 10 ===========================
        public ActionResult LayersTop10()
        {
            if (!Enums.CheckLogin())
            {
                return RedirectToAction("Login", "Home");
            }
            var top10 = UserDA.GetInfoTotalPointUser(10);
            return View(top10);
        }
        public ActionResult PartialLayersTop10()
        {
            var top10 = UserDA.GetInfoTotalPointUser(10);
            return PartialView("PartialLayersTop10", top10);
        }
        // ===================================== Người chơi top 50 ===========================
        public ActionResult LayersTop50()
        {
            if (!Enums.CheckLogin())
            {
                return RedirectToAction("Login", "Home");
            }
            var top50 = UserDA.GetInfoTotalPointUser(50);
            return View(top50);
        }
        public ActionResult PartialLayersTop50()
        {
            var top50 = UserDA.GetInfoTotalPointUser(50);
            return PartialView("PartialLayersTop50", top50);
        }

        // ========================================Info User Follow ContestDay==============================
        public ActionResult AllInfoUser()
        {
            if (!Enums.CheckLogin())
            {
                return RedirectToAction("Login", "Home");
            }
            var data = UserDA.GetAllUserByTotalScore();
            return View(data);
        }

        public ActionResult AllInfoUserPartial()
        {
            var data = UserDA.GetAllUserByTotalScore();
            return PartialView("AllInfoUserPartial", data);
        }

        //========================Manager User By ContestDay==============================
        public ActionResult ManagerContestDay(FormCollection forms)
        {
            if (!Enums.CheckLogin())
            {
                return RedirectToAction("Login", "Home");
            }

            int contestid = 0;
            if (!int.TryParse(forms["ContestDayList"], out contestid))
            {
                contestid = 0;
            }
            using (var entities = new Lasta_Entities())
            {
                var contestdata = (from p in entities.ContestDays
                                   where p.IsDeleted == false
                                   select p).ToList();
                var result = contestdata.OrderByDescending(p => p.StartTime).Select(p => new { ID = p.ID, StartTime = p.StartTime.ToShortDateString() });
                ViewBag.Listcontestday = new SelectList(result, "ID", "StartTime", contestid);
                if (contestid == 0)
                {
                    contestid = contestdata.FirstOrDefault().ID;
                }
            }
            var data = UserDA.GetUserByContest(contestid);
            return View(data);
        }

        public ActionResult ManagerContestDayPartial(int contestid)
        {
            var data = UserDA.GetUserByContest(contestid);
            return PartialView("ManagerContestDayPartial", data);
        }

        //========================TODO: Manager ContestDay=============================
        public ActionResult PanelContestDay()
        {
            if (!Enums.CheckLogin())
            {
                return RedirectToAction("Login", "Home");
            }

            var data = ContestDayDA.GetContestDaysData();
            return View(data);
        }

        public ActionResult PanelContestDayPartial()
        {
            var data = ContestDayDA.GetContestDaysData();
            return PartialView("PanelContestDayPartial", data);
        }

        public ActionResult DetailInfoContestDay(int id)
        {
            if (!Enums.CheckLogin())
            {
                return RedirectToAction("Login", "Home");
            }

            if (id > 0)
            {
                //todo:total User in contest
                ViewBag.totaluser = UserDA.CountUserByContest(id);
                //todo:total Question in contest
                ViewBag.totalquestion = QuestionDA.CountQuestionInContest(id);
                //todo:total Answer in contest
                ViewBag.totalanswer = AnswerDA.CountAnswerInContest(id);
                //todo:ListPrize in contest
                var data = PrizeDA.GetPrizeByContest(id);

                return View(data);
            }
            return View();
        }

        public ActionResult PrizeByContestPartial(int id)
        {
            var data = PrizeDA.GetPrizeByContest(id);
            return PartialView("PrizeByContestPartial", data);
        }

        public ActionResult DetailUserContestDay()
        {
            if (!Enums.CheckLogin())
            {
                return RedirectToAction("Login", "Home");
            }

            return View();
        }

        public ActionResult DetailUserContestDayPartial()
        {
            return PartialView("DetailUserContestDayPartial");
        }

        // ==========================Thông tin chung cho tất cả người chơi (theo ngày) ===============================
        public ActionResult Infolayers()
        {
            if (!Enums.CheckLogin())
            {
                return RedirectToAction("Login", "Home");
            }

            using (var entities = new Lasta_Entities())
            {
                var data = (from u in entities.Users
                            join a in entities.Answers on u.ID equals a.UserID
                            join q in entities.Questions on a.QuestionID equals q.ID
                            join t in entities.Topics on q.TopicID equals t.ID
                            join con in entities.ContestDays on q.ContestDayID equals con.ID
                            join p in entities.Prizes on con.ID equals p.ContestDayID
                            where q.CorrectAnswer == 1
                            orderby a.StartTime ascending
                            //group a by a.UserID into gr
                            select new
                            {
                                Hoten = u.Fullname,
                                Ngaysinh = u.Birthday,
                                Diachi = u.Address,
                                SoDT = u.PhoneNumber,
                                Email = u.Email,
                                Cauhoiso = q.Number,
                                Noidungcauhoi = q.QuestionContent,
                                Dapandung = q.CorrectAnswer,
                                Chude = t.TopicContent,
                                Sodiemdatduoc = a.Score,
                                Trangthainguoichoi = con.State,
                                Ngaythamgia = con.CreatedOn,
                                Thoigianbatdautraloi = a.StartTime,
                                Thoigiantraloi = a.Duration,
                                Thoigianconlai = con.RemainTime

                            }).ToList();
                return View(data);
            }


        }

        public ActionResult PartialInfolayers()
        {
            using (var entities = new Lasta_Entities())
            {
                var data = (from u in entities.Users
                            join a in entities.Answers on u.ID equals a.UserID
                            join q in entities.Questions on a.QuestionID equals q.ID
                            join t in entities.Topics on q.TopicID equals t.ID
                            join con in entities.ContestDays on q.ContestDayID equals con.ID
                            join p in entities.Prizes on con.ID equals p.ContestDayID
                            where q.CorrectAnswer == 1
                            orderby a.StartTime ascending
                            select new
                            {
                                Hoten = u.Fullname,
                                Ngaysinh = u.Birthday,
                                Diachi = u.Address,
                                SoDT = u.PhoneNumber,
                                Email = u.Email,
                                Cauhoiso = q.Number,
                                Noidungcauhoi = q.QuestionContent,
                                Dapandung = q.CorrectAnswer,
                                Chude = t.TopicContent,
                                Sodiemdatduoc = a.Score,
                                Trangthainguoichoi = con.State,
                                Ngaythamgia = con.CreatedOn,
                                Thoigianbatdautraloi = a.StartTime,
                                Thoigiantraloi = a.Duration,
                                Thoigianconlai = con.RemainTime

                            }).ToList();
                return PartialView("PartialInfolayers", data);
            }


        }
        //======================================== Tổng điểm cho từng người chơi ================
        // show thông tin của người chơi đầy đủ : họ tên, địa chỉ, số DD, ngày sinh... trạng thái chơi, ngày chơi
        public ActionResult Sumscore()
        {
            using (var entities = new Lasta_Entities())
            {
                var data = (from u in entities.Users
                            join a in entities.Answers on u.ID equals a.UserID
                            join con in entities.ContestDays on a.ContestDayID equals con.ID
                            group a by a.UserID into g
                            select new
                            {
                                UserID = g.Key,
                                TotalScore = g.Sum(p => p.Score),

                            }).ToList();
                return View(data);
            }
        }

        public ActionResult PartialSumscore()
        {
            using (var entities = new Lasta_Entities())
            {
                var data = (from u in entities.Users
                            join a in entities.Answers on u.ID equals a.UserID
                            join con in entities.ContestDays on a.ContestDayID equals con.ID
                            group a by a.UserID into g
                            select new
                            {
                                UserID = g.Key,
                                TotalScore = g.Sum(p => p.Score),

                            }).ToList();
                return PartialView("PartialSumscore", data);
            }
        }
        //======================================= Người chơi Top 3 =============================
        public ActionResult LayersTop3()
        {
            using (var entities = new Lasta_Entities())
            {
                var data = (from u in entities.Users
                            join a in entities.Answers on u.ID equals a.UserID
                            join q in entities.Questions on a.QuestionID equals q.ID
                            join t in entities.Topics on q.TopicID equals t.ID
                            join con in entities.ContestDays on q.ContestDayID equals con.ID
                            join p in entities.Prizes on con.ID equals p.ContestDayID
                            where q.CorrectAnswer == 1
                            orderby a.StartTime ascending
                            select new
                            {
                                Hoten = u.Fullname,
                                Ngaysinh = u.Birthday,
                                Diachi = u.Address,
                                SoDT = u.PhoneNumber,
                                Email = u.Email,
                                Cauhoiso = q.Number,
                                Noidungcauhoi = q.QuestionContent,
                                Dapandung = q.CorrectAnswer,
                                Chude = t.TopicContent,
                                Sodiemdatduoc = a.Score,
                                Trangthainguoichoi = con.State,
                                Ngaythamgia = con.CreatedOn,
                                Thoigianbatdautraloi = a.StartTime,
                                Thoigiantraloi = a.Duration,
                                Thoigianconlai = con.RemainTime

                            }).Take(3).ToList();
                return View(data);
            }

        }
        public ActionResult PartialLayersTop3()
        {
            using (var entities = new Lasta_Entities())
            {
                var data = (from u in entities.Users
                            join a in entities.Answers on u.ID equals a.UserID
                            join q in entities.Questions on a.QuestionID equals q.ID
                            join t in entities.Topics on q.TopicID equals t.ID
                            join con in entities.ContestDays on q.ContestDayID equals con.ID
                            join p in entities.Prizes on con.ID equals p.ContestDayID
                            where q.CorrectAnswer == 1
                            orderby a.StartTime ascending
                            select new
                            {
                                Hoten = u.Fullname,
                                Ngaysinh = u.Birthday,
                                Diachi = u.Address,
                                SoDT = u.PhoneNumber,
                                Email = u.Email,
                                Cauhoiso = q.Number,
                                Noidungcauhoi = q.QuestionContent,
                                Dapandung = q.CorrectAnswer,
                                Chude = t.TopicContent,
                                Sodiemdatduoc = a.Score,
                                Trangthainguoichoi = con.State,
                                Ngaythamgia = con.CreatedOn,
                                Thoigianbatdautraloi = a.StartTime,
                                Thoigiantraloi = a.Duration,
                                Thoigianconlai = con.RemainTime

                            }).Take(3).ToList();
                return PartialView("PartialLayersTop3", data);
            }

        }

        //============================= Danh sách người chơi nhận quà trong kỳ. (có thể GroupBy theo ngày chơi hoặc kỳ)===============================
        public ActionResult LayersGetPrize()
        {
            using (var entities = new Lasta_Entities())
            {
                var data = (from u in entities.Users
                            join a in entities.Answers on u.ID equals a.UserID
                            join con in entities.ContestDays on a.ContestDayID equals con.ID
                            join p in entities.Prizes on con.ID equals p.ContestDayID
                            select new
                            {
                                Hoten = u.Fullname,
                                Ngaysinh = u.Birthday,
                                Diachi = u.Address,
                                SoDT = u.PhoneNumber,
                                Email = u.Email,
                                Ngaythamgia = con.CreatedOn,
                                Sodiemdatduoc = a.Score,
                                Tenqua = p.Name,
                                Mota = p.Description,
                                Hinhanh = p.Image

                            }
                    ).ToList();
                return View(data);
            }

        }
        public ActionResult PartialLayersGetPrize()
        {
            using (var entities = new Lasta_Entities())
            {
                var data = (from u in entities.Users
                            join a in entities.Answers on u.ID equals a.UserID
                            join con in entities.ContestDays on a.ContestDayID equals con.ID
                            join p in entities.Prizes on con.ID equals p.ContestDayID
                            select new
                            {
                                Hoten = u.Fullname,
                                Ngaysinh = u.Birthday,
                                Diachi = u.Address,
                                SoDT = u.PhoneNumber,
                                Email = u.Email,
                                Ngaythamgia = con.CreatedOn,
                                Sodiemdatduoc = a.Score,
                                Tenqua = p.Name,
                                Mota = p.Description,
                                Hinhanh = p.Image

                            }
                    ).ToList();
                return PartialView("PartialLayersGetPrize", data);
            }

        }

    }
}