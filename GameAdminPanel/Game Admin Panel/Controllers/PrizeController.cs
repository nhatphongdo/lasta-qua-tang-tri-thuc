﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Game_Admin_Panel;

namespace Game_Admin_Panel.Controllers
{
    public class PrizeController : Controller
    {
        private Lasta_Entities db = new Lasta_Entities();

        // GET: /Prize/
        public async Task<ActionResult> Index()
        {
            var prizes = db.Prizes.Include(p => p.ContestDay);
            return View(await prizes.ToListAsync());
        }

        // GET: /Prize/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Prize prize = await db.Prizes.FindAsync(id);
            if (prize == null)
            {
                return HttpNotFound();
            }
            return View(prize);
        }

        // GET: /Prize/Create
        public ActionResult Create()
        {
            ViewBag.ContestDayID = new SelectList(db.ContestDays, "ID", "ID");
            return View();
        }

        // POST: /Prize/Create
        // To protect from over posting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        // 
        // Example: public ActionResult Update([Bind(Include="ExampleProperty1,ExampleProperty2")] Model model)
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Prize prize)
        {
            if (ModelState.IsValid)
            {
                db.Prizes.Add(prize);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.ContestDayID = new SelectList(db.ContestDays, "ID", "ID", prize.ContestDayID);
            return View(prize);
        }

        // GET: /Prize/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Prize prize = await db.Prizes.FindAsync(id);
            if (prize == null)
            {
                return HttpNotFound();
            }
            ViewBag.ContestDayID = new SelectList(db.ContestDays, "ID", "ID", prize.ContestDayID);
            return View(prize);
        }

        // POST: /Prize/Edit/5
        // To protect from over posting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        // 
        // Example: public ActionResult Update([Bind(Include="ExampleProperty1,ExampleProperty2")] Model model)
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Prize prize)
        {
            if (ModelState.IsValid)
            {
                db.Entry(prize).State = System.Data.Entity.EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.ContestDayID = new SelectList(db.ContestDays, "ID", "ID", prize.ContestDayID);
            return View(prize);
        }

        // GET: /Prize/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Prize prize = await db.Prizes.FindAsync(id);
            if (prize == null)
            {
                return HttpNotFound();
            }
            return View(prize);
        }

        // POST: /Prize/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Prize prize = await db.Prizes.FindAsync(id);
            db.Prizes.Remove(prize);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
