﻿using System;

using System.Collections.Generic;
//using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Game_Admin_Panel;

namespace Game_Admin_Panel.Controllers
{
    public class AnswerController : Controller
    {
        private Lasta_Entities db = new Lasta_Entities();

        // GET: /Answer/
        public async Task<ActionResult> Index()
        {
            var answers = db.Answers.Include(a => a.ContestDay).Include(a => a.Question).Include(a => a.User);
            return View(await answers.ToListAsync());
        }

        // GET: /Answer/Details/5
        public async Task<ActionResult> Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Answer answer = await db.Answers.FindAsync(id);
            if (answer == null)
            {
                return HttpNotFound();
            }
            return View(answer);
        }

        // GET: /Answer/Create
        public ActionResult Create()
        {
            ViewBag.ContestDayID = new SelectList(db.ContestDays, "ID", "ID");
            ViewBag.QuestionID = new SelectList(db.Questions, "ID", "QuestionContent");
            ViewBag.UserID = new SelectList(db.Users, "ID", "Fullname");
            return View();
        }

        // POST: /Answer/Create
        // To protect from over posting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        // 
        // Example: public ActionResult Update([Bind(Include="ExampleProperty1,ExampleProperty2")] Model model)
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Answer answer)
        {
            if (ModelState.IsValid)
            {
                db.Answers.Add(answer);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.ContestDayID = new SelectList(db.ContestDays, "ID", "ID", answer.ContestDayID);
            ViewBag.QuestionID = new SelectList(db.Questions, "ID", "QuestionContent", answer.QuestionID);
            ViewBag.UserID = new SelectList(db.Users, "ID", "Fullname", answer.UserID);
            return View(answer);
        }

        // GET: /Answer/Edit/5
        public async Task<ActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Answer answer = await db.Answers.FindAsync(id);
            if (answer == null)
            {
                return HttpNotFound();
            }
            ViewBag.ContestDayID = new SelectList(db.ContestDays, "ID", "ID", answer.ContestDayID);
            ViewBag.QuestionID = new SelectList(db.Questions, "ID", "QuestionContent", answer.QuestionID);
            ViewBag.UserID = new SelectList(db.Users, "ID", "Fullname", answer.UserID);
            return View(answer);
        }

        // POST: /Answer/Edit/5
        // To protect from over posting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        // 
        // Example: public ActionResult Update([Bind(Include="ExampleProperty1,ExampleProperty2")] Model model)
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Answer answer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(answer).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.ContestDayID = new SelectList(db.ContestDays, "ID", "ID", answer.ContestDayID);
            ViewBag.QuestionID = new SelectList(db.Questions, "ID", "QuestionContent", answer.QuestionID);
            ViewBag.UserID = new SelectList(db.Users, "ID", "Fullname", answer.UserID);
            return View(answer);
        }

        // GET: /Answer/Delete/5
        public async Task<ActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Answer answer = await db.Answers.FindAsync(id);
            if (answer == null)
            {
                return HttpNotFound();
            }
            return View(answer);
        }

        // POST: /Answer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(long id)
        {
            Answer answer = await db.Answers.FindAsync(id);
            db.Answers.Remove(answer);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}