﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Game_Admin_Panel;

namespace Game_Admin_Panel.Controllers
{
    public class ContestDayController : Controller
    {
        private Lasta_Entities db = new Lasta_Entities();

        // GET: /ContestDay/
        public async Task<ActionResult> Index()
        {
            return View(await db.ContestDays.ToListAsync());
        }

        // GET: /ContestDay/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContestDay contestday = await db.ContestDays.FindAsync(id);
            if (contestday == null)
            {
                return HttpNotFound();
            }
            return View(contestday);
        }

        // GET: /ContestDay/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /ContestDay/Create
        // To protect from over posting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        // 
        // Example: public ActionResult Update([Bind(Include="ExampleProperty1,ExampleProperty2")] Model model)
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(ContestDay contestday)
        {
            if (ModelState.IsValid)
            {
                db.ContestDays.Add(contestday);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(contestday);
        }

        // GET: /ContestDay/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContestDay contestday = await db.ContestDays.FindAsync(id);
            if (contestday == null)
            {
                return HttpNotFound();
            }
            return View(contestday);
        }

        // POST: /ContestDay/Edit/5
        // To protect from over posting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        // 
        // Example: public ActionResult Update([Bind(Include="ExampleProperty1,ExampleProperty2")] Model model)
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(ContestDay contestday)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contestday).State = System.Data.Entity.EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(contestday);
        }

        // GET: /ContestDay/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContestDay contestday = await db.ContestDays.FindAsync(id);
            if (contestday == null)
            {
                return HttpNotFound();
            }
            return View(contestday);
        }

        // POST: /ContestDay/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ContestDay contestday = await db.ContestDays.FindAsync(id);
            db.ContestDays.Remove(contestday);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
