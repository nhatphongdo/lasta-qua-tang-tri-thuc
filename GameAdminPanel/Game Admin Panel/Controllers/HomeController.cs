using DevExpress.Web.ASPxGridView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Game_Admin_Panel;

namespace Game_Admin_Panel.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (!Enums.CheckLogin())
            {
                return RedirectToAction("Login");
            }
            using (var db = new Lasta_Entities())
            {
                //contestday hien tai
                int idcontestday = db.ContestDays.Where(cd => cd.IsDeleted == false).Max(cd => cd.ID);
                //cau hoi hien tai
                int cauhoihientai = db.Questions.Where(qu => qu.IsDeleted == false).Max(qu => qu.ID);
                //tong so nguoi tham gia tro choi            
                ViewBag.tongthamgia = db.Users.Count();

                //tong so luot tra loi dung tinh toi thoi diem hien tai
                ViewBag.dunghientai = db.Answers.Count(p => p.IsDeleted == false && p.AnswerNumber == p.Question.CorrectAnswer);

                //tong so luot nguoi tra loi sai tinh toi thoi diem hien tai
                ViewBag.saihientai = db.Answers.Count(p => p.IsDeleted == false && p.AnswerNumber != p.Question.CorrectAnswer);
                //tong luot nguoi tra loi tinh toi thoi diem hien tai
                ViewBag.hientai = db.Answers.Count(an => an.IsDeleted == false);

                //tong so nguoi choi  trong ngay
                var now = DateTime.Now.Date;
                ViewBag.tongthamgiangay = db.Users.Count(us => us.CreatedOn >= now && us.IsDeleted == false);
                //tong so luot tra loi dung trong ngay
                ViewBag.dungtrongngay = db.Answers.Count(p => p.IsDeleted == false && p.AnswerNumber == p.Question.CorrectAnswer && p.CreatedOn >= now);
                //tong so luot tra loi sai trong ngay
                ViewBag.saitrongngay = db.Answers.Count(p => p.IsDeleted == false && p.AnswerNumber != p.Question.CorrectAnswer && p.CreatedOn >= now);
                //tong so luot tra loi trong ngay
                ViewBag.trongngay = db.Answers.Count(an => an.CreatedOn >= now && an.IsDeleted == false);
            }
            return View();

        }

        public ActionResult Login()
        {
            HttpCookie cookie = HttpContext.Request.Cookies.Get("LastaLoggedIn");
            if (cookie != null)
            {
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpPost]
        public ActionResult Login(FormCollection forms)
        {
            if (forms["Password"] == "lasta123!@#")
            {
                HttpCookie cookie = new HttpCookie("LastaLoggedIn");
                HttpContext.Response.Cookies.Remove("LastaLoggedIn");
                HttpContext.Response.SetCookie(cookie);
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.ErrorMessage = "Wrong password!";
                HttpContext.Response.Cookies.Remove("LastaLoggedIn");
            }
            return View();
        }

        public ActionResult PanelManagerUser()
        {
            return View();
        }

        public ActionResult PanelManagerContest()
        {
            return View();
        }

        //User Manager
        public ActionResult EditUser()
        {
            if (!Enums.CheckLogin())
            {
                return RedirectToAction("Login");
            }
            return View(UserDA.GetUsersData());
        }

        [ValidateInput(false)]
        public ActionResult EditUserPartial()
        {
            return PartialView("EditUserPartial", UserDA.GetUsersData());
        }
        public ActionResult ChangeEditUserPartial(GridViewEditingMode editMode)
        {
            GridviewDevexpress.EditMode = editMode;
            return PartialView("EditUserPartial", UserDA.GetUsersData());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult EditUserAddNewPartial(EditableUser user)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    user.IsDeleted = false;
                    UserDA.InsertUser(user);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";

            return PartialView("EditUserPartial", UserDA.GetUsersData());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult EditUserUpdatePartial(EditableUser user)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    UserDA.UpdateUser(user);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";

            return PartialView("EditUserPartial", UserDA.GetUsersData());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult EditUserDeletePartial(long ID)
        {
            if (ID >= 0)
            {
                try
                {
                    UserDA.DeletedUser(ID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("EditUserPartial", UserDA.GetUsersData());
        }

        //Question
        public ActionResult EditQuestion()
        {
            if (!Enums.CheckLogin())
            {
                return RedirectToAction("Login");
            }
            return View(QuestionDA.GetQuestionsData());
        }

        [ValidateInput(false)]
        public ActionResult EditQuestionPartial()
        {
            return PartialView("EditQuestionPartial", QuestionDA.GetQuestionsData());
        }
        public ActionResult ChangeEditQuestionPartial(GridViewEditingMode editMode)
        {
            GridviewDevexpress.EditMode = editMode;
            return PartialView("EditQuestionPartial", QuestionDA.GetQuestionsData());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult EditQuestionAddNewPartial(EditableQuestion ques)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    QuestionDA.InsertQuestion(ques);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";

            return PartialView("EditQuestionPartial", QuestionDA.GetQuestionsData());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult EditQuestionUpdatePartial(EditableQuestion ques)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    QuestionDA.UpdateQuestion(ques);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";

            return PartialView("EditQuestionPartial", QuestionDA.GetQuestionsData());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult EditQuestionDeletePartial(long ID)
        {
            if (ID >= 0)
            {
                try
                {
                    QuestionDA.DeletedQuestion(ID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("EditQuestionPartial", QuestionDA.GetQuestionsData());
        }

        //Topic
        public ActionResult EditTopic()
        {
            if (!Enums.CheckLogin())
            {
                return RedirectToAction("Login");
            }
            return View(TopicDA.GetTopicsData());
        }

        [ValidateInput(false)]
        public ActionResult EditTopicPartial()
        {
            return PartialView("EditTopicPartial", TopicDA.GetTopicsData());
        }
        public ActionResult ChangeEditTopicPartial(GridViewEditingMode editMode)
        {
            GridviewDevexpress.EditMode = editMode;
            return PartialView("EditTopicPartial", TopicDA.GetTopicsData());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult EditTopicAddNewPartial(EditableTopic topic)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    topic.CreatedOn = DateTime.Now;
                    topic.IsDeleted = false;
                    TopicDA.InsertTopic(topic);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";

            return PartialView("EditTopicPartial", TopicDA.GetTopicsData());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult EditTopicUpdatePartial(EditableTopic topic)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    TopicDA.UpdateTopic(topic);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";

            return PartialView("EditTopicPartial", TopicDA.GetTopicsData());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult EditTopicDeletePartial(long ID)
        {
            if (ID >= 0)
            {
                try
                {
                    TopicDA.DeletedTopic(ID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("EditTopicPartial", TopicDA.GetTopicsData());
        }

        //Prize
        public ActionResult EditPrize()
        {
            if (!Enums.CheckLogin())
            {
                return RedirectToAction("Login");
            }
            return View(PrizeDA.GetPrizesData());
        }

        [ValidateInput(false)]
        public ActionResult EditPrizePartial()
        {
            return PartialView("EditPrizePartial", PrizeDA.GetPrizesData());
        }
        public ActionResult ChangeEditPrizePartial(GridViewEditingMode editMode)
        {
            GridviewDevexpress.EditMode = editMode;
            return PartialView("EditPrizePartial", PrizeDA.GetPrizesData());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult EditPrizeAddNewPartial(EditablePrize prize)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    prize.CreatedOn = DateTime.Now;
                    prize.IsDeleted = false;
                    PrizeDA.InsertPrize(prize);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";

            return PartialView("EditPrizePartial", PrizeDA.GetPrizesData());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult EditPrizeUpdatePartial(EditablePrize prize)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    PrizeDA.UpdatePrize(prize);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";

            return PartialView("EditPrizePartial", PrizeDA.GetPrizesData());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult EditPrizeDeletePartial(long ID)
        {
            if (ID >= 0)
            {
                try
                {
                    PrizeDA.DeletedPrize(ID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("EditPrizePartial", PrizeDA.GetPrizesData());
        }


        //Answer
        public ActionResult EditAnswer()
        {
            if (!Enums.CheckLogin())
            {
                return RedirectToAction("Login");
            }
            return View(AnswerDA.GetAnswersData());
        }

        [ValidateInput(false)]
        public ActionResult EditAnswerPartial()
        {
            return PartialView("EditAnswerPartial", AnswerDA.GetAnswersData());
        }
        public ActionResult ChangeEditAnswerPartial(GridViewEditingMode editMode)
        {
            GridviewDevexpress.EditMode = editMode;
            return PartialView("EditAnswerPartial", AnswerDA.GetAnswersData());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult EditAnswerAddNewPartial(EditableAnswer answer)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    answer.CreatedOn = DateTime.Now;
                    answer.IsDeleted = false;
                    AnswerDA.InsertAnswer(answer);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";

            return PartialView("EditAnswerPartial", AnswerDA.GetAnswersData());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult EditAnswerUpdatePartial(EditableAnswer answer)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    AnswerDA.UpdateAnswer(answer);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";

            return PartialView("EditAnswerPartial", AnswerDA.GetAnswersData());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult EditAnswerDeletePartial(long ID)
        {
            if (ID >= 0)
            {
                try
                {
                    AnswerDA.DeletedAnswer(ID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("EditAnswerPartial", AnswerDA.GetAnswersData());
        }


        //ContestDay
        public ActionResult EditContestDay()
        {
            if (!Enums.CheckLogin())
            {
                return RedirectToAction("Login");
            }
            return View(ContestDayDA.GetContestDaysData());
        }

        [ValidateInput(false)]
        public ActionResult EditContestDayPartial()
        {
            return PartialView("EditContestDayPartial", ContestDayDA.GetContestDaysData());
        }
        public ActionResult ChangeEditContestDayPartial(GridViewEditingMode editMode)
        {
            GridviewDevexpress.EditMode = editMode;
            return PartialView("EditContestDayPartial", ContestDayDA.GetContestDaysData());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult EditContestDayAddNewPartial(EditableContestDay contest)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    contest.UniqueID = Guid.NewGuid();
                    contest.CreatedOn = DateTime.Now;
                    contest.IsDeleted = false;
                    ContestDayDA.InsertContestDay(contest);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";

            return PartialView("EditContestDayPartial", ContestDayDA.GetContestDaysData());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult EditContestDayUpdatePartial(EditableContestDay contest)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ContestDayDA.UpdateContestDay(contest);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";

            return PartialView("EditContestDayPartial", ContestDayDA.GetContestDaysData());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult EditContestDayDeletePartial(long ID)
        {
            if (ID >= 0)
            {
                try
                {
                    ContestDayDA.DeletedContestDay(ID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("EditContestDayPartial", ContestDayDA.GetContestDaysData());
        }

    }
}