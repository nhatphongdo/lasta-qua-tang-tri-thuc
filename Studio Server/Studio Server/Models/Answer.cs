﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Studio_Server.Models
{
    public class Answer
    {
        public int ResultCode
        {
            get;
            set;
        }

        public string CorrectAnswer
        {
            get;
            set;
        }

        public long Score
        {
            get;
            set;
        }
    }
}