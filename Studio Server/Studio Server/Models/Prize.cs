﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Studio_Server.Models
{
    public class Prize
    {
        public string Name
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string Image
        {
            get;
            set;
        }

        public string Sponsor
        {
            get;
            set;
        }

        public int SponsorType
        {
            get;
            set;
        }

        public int Type
        {
            get;
            set;
        }
    }
}