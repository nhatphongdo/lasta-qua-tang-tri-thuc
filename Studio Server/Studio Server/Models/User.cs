﻿
namespace Studio_Server.Models
{
    public class User
    {
        public long ID
        {
            get;
            set;
        }

        public string Fullname
        {
            get;
            set;
        }

        public string Email
        {
            get;
            set;
        }

        public string PhoneNumber
        {
            get;
            set;
        }

        public string Address
        {
            get;
            set;
        }

        public bool? IsOptIn
        {
            get;
            set;
        }

        public long? Rank
        {
            get;
            set;
        }

        public string Seat
        {
            get;
            set;
        }

        public bool IsPlaying
        {
            get;
            set;
        }

        public int? Answer
        {
            get;
            set;
        }

        public double? Time
        {
            get;
            set;
        }

        public long? LastScore
        {
            get;
            set;
        }

        public long? Score
        {
            get;
            set;
        }

        public long? DayScore
        {
            get;
            set;
        }
    }
}