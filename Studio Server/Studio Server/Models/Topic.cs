﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Studio_Server.Models
{
    public class Topic
    {
        public string Name
        {
            get;
            set;
        }

        public int Count
        {
            get;
            set;
        }
    }
}