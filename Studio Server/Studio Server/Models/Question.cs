﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Studio_Server.Models
{
    public class Question
    {
        public int Number
        {
            get;
            set;
        }

        public string Content
        {
            get;
            set;
        }

        public string Answer1
        {
            get;
            set;
        }

        public string Answer2
        {
            get;
            set;
        }

        public string Answer3
        {
            get;
            set;
        }

        public string Answer4
        {
            get;
            set;
        }

        public string Hint
        {
            get;
            set;
        }

        public int Score
        {
            get;
            set;
        }

        public int CountdownTime
        {
            get;
            set;
        }

        public string Topic
        {
            get;
            set;
        }

        public int CorrectAnswer
        {
            get;
            set;
        }
    }
}