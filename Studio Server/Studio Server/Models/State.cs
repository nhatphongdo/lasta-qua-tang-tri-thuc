﻿
namespace Studio_Server.Models
{
    public class State
    {
        // Status:
        // 1x - 100x : status of each question (from 1-100)
        //      x = 0: choose topic
        //      x = 1: choose score
        //      x = 2: start
        //      x = 3: check
        // 100xx : prize list with xx is the prize number
        // 10100 : show summary page (top 10)
        // 10200 : wait for topic
        // 10300 : contest finished (top 50)
        public int Status
        {
            get;
            set;
        }

        public int RemainTime
        {
            get;
            set;
        }

        public int TotalQuestions
        {
            get;
            set;
        }
    }
}