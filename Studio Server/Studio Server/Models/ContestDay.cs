﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Studio_Server.Models
{
    public class ContestDay
    {
        public long StartTime
        {
            get;
            set;
        }

        public int NumberOfQuestions
        {
            get;
            set;
        }

        public int State
        {
            get;
            set;
        }

        public int Remain
        {
            get;
            set;
        }

        public Guid ContestKey
        {
            get;
            set;
        }
    }
}