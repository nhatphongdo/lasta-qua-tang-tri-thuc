﻿using System;

namespace Studio_Server.Models
{
    public class CacheItem
    {
        public string Name
        {
            get;
            set;
        }

        public object Value
        {
            get;
            set;
        }

        public DateTime CreatedOn
        {
            get;
            set;
        }

        public double ExpiredTime
        {
            get;
            set;
        }

        public bool IsExpired
        {
            get
            {
                return CreatedOn.AddMilliseconds(ExpiredTime) < DateTime.Now;
            }
        }
    }
}