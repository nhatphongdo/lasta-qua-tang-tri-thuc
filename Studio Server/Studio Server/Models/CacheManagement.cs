﻿using System;
using System.Collections.Generic;

namespace Studio_Server.Models
{
    public class CacheManagement
    {
        private static Dictionary<string, CacheItem> _caches;

        static CacheManagement()
        {
            _caches = new Dictionary<string, CacheItem>();
        }

        public static object Get(string name)
        {
            if (_caches.ContainsKey(name) && !_caches[name].IsExpired)
            {
                return _caches[name].Value;
            }

            return null;
        }

        public static void Set(string name, object value, double expires = 500)
        {
            var cacheItem = new CacheItem()
            {
                Name = name,
                Value = value,
                CreatedOn = DateTime.Now,
                ExpiredTime = expires
            };

            if (_caches.ContainsKey(name))
            {
                _caches[name] = cacheItem;
            }
            else
            {
                _caches.Add(name, cacheItem);
            }
        }
    }
}