﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Studio_Server.Models
{
    public class RegisteredUser
    {
        public long ID
        {
            get;
            set;
        }

        public int Error
        {
            get;
            set;
        }
    }
}