﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Studio_Server.SmsServiceReference {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://wslasta/", ConfigurationName="SmsServiceReference.checkcode")]
    public interface checkcode {
        
        // CODEGEN: Generating message contract since element name username from namespace  is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://wslasta/checkcode/checkRequest", ReplyAction="http://wslasta/checkcode/checkResponse")]
        Studio_Server.SmsServiceReference.checkResponse check(Studio_Server.SmsServiceReference.checkRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://wslasta/checkcode/checkRequest", ReplyAction="http://wslasta/checkcode/checkResponse")]
        System.Threading.Tasks.Task<Studio_Server.SmsServiceReference.checkResponse> checkAsync(Studio_Server.SmsServiceReference.checkRequest request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class checkRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="check", Namespace="http://wslasta/", Order=0)]
        public Studio_Server.SmsServiceReference.checkRequestBody Body;
        
        public checkRequest() {
        }
        
        public checkRequest(Studio_Server.SmsServiceReference.checkRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="")]
    public partial class checkRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string username;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string password;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string mathe;
        
        public checkRequestBody() {
        }
        
        public checkRequestBody(string username, string password, string mathe) {
            this.username = username;
            this.password = password;
            this.mathe = mathe;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class checkResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="checkResponse", Namespace="http://wslasta/", Order=0)]
        public Studio_Server.SmsServiceReference.checkResponseBody Body;
        
        public checkResponse() {
        }
        
        public checkResponse(Studio_Server.SmsServiceReference.checkResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="")]
    public partial class checkResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public bool @return;
        
        public checkResponseBody() {
        }
        
        public checkResponseBody(bool @return) {
            this.@return = @return;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface checkcodeChannel : Studio_Server.SmsServiceReference.checkcode, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class checkcodeClient : System.ServiceModel.ClientBase<Studio_Server.SmsServiceReference.checkcode>, Studio_Server.SmsServiceReference.checkcode {
        
        public checkcodeClient() {
        }
        
        public checkcodeClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public checkcodeClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public checkcodeClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public checkcodeClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Studio_Server.SmsServiceReference.checkResponse Studio_Server.SmsServiceReference.checkcode.check(Studio_Server.SmsServiceReference.checkRequest request) {
            return base.Channel.check(request);
        }
        
        public bool check(string username, string password, string mathe) {
            Studio_Server.SmsServiceReference.checkRequest inValue = new Studio_Server.SmsServiceReference.checkRequest();
            inValue.Body = new Studio_Server.SmsServiceReference.checkRequestBody();
            inValue.Body.username = username;
            inValue.Body.password = password;
            inValue.Body.mathe = mathe;
            Studio_Server.SmsServiceReference.checkResponse retVal = ((Studio_Server.SmsServiceReference.checkcode)(this)).check(inValue);
            return retVal.Body.@return;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<Studio_Server.SmsServiceReference.checkResponse> Studio_Server.SmsServiceReference.checkcode.checkAsync(Studio_Server.SmsServiceReference.checkRequest request) {
            return base.Channel.checkAsync(request);
        }
        
        public System.Threading.Tasks.Task<Studio_Server.SmsServiceReference.checkResponse> checkAsync(string username, string password, string mathe) {
            Studio_Server.SmsServiceReference.checkRequest inValue = new Studio_Server.SmsServiceReference.checkRequest();
            inValue.Body = new Studio_Server.SmsServiceReference.checkRequestBody();
            inValue.Body.username = username;
            inValue.Body.password = password;
            inValue.Body.mathe = mathe;
            return ((Studio_Server.SmsServiceReference.checkcode)(this)).checkAsync(inValue);
        }
    }
}
