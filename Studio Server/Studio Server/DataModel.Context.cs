﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Studio_Server
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Data.Objects.DataClasses;
    using System.Linq;

    public partial class Lasta_Entities : DbContext
    {
        public Lasta_Entities()
            : base("name=Lasta_Entities")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }

        public DbSet<Answer> Answers { get; set; }
        public DbSet<ContestDay> ContestDays { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Prize> Prizes { get; set; }
        public DbSet<HomeAnswer> HomeAnswers { get; set; }
        public DbSet<HomeUser> HomeUsers { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<OldUser> OldUsers { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<ExchangeScore> ExchangeScores { get; set; }
        public DbSet<TransferScore> TransferScores { get; set; }
        public DbSet<PushMessage> PushMessages { get; set; }
        public DbSet<DayScore> DayScores { get; set; }
        public DbSet<TotalScore> TotalScores { get; set; }

        public virtual int CopyDailyLeaderboard(Nullable<int> contestDayID)
        {
            var contestDayIDParameter = contestDayID.HasValue ?
                new ObjectParameter("ContestDayID", contestDayID) :
                new ObjectParameter("ContestDayID", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("CopyDailyLeaderboard", contestDayIDParameter);
        }

        public virtual int CopyTotalLeaderboard()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("CopyTotalLeaderboard");
        }

        public virtual int usp_generateIdentifier(Nullable<int> minLen, Nullable<int> maxLen, ObjectParameter @string)
        {
            var minLenParameter = minLen.HasValue ?
                new ObjectParameter("minLen", minLen) :
                new ObjectParameter("minLen", typeof(int));

            var maxLenParameter = maxLen.HasValue ?
                new ObjectParameter("maxLen", maxLen) :
                new ObjectParameter("maxLen", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("usp_generateIdentifier", minLenParameter, maxLenParameter, @string);
        }
    }
}
