﻿using System.Net;
using System.Net.Http;
using PushSharp;
using PushSharp.Android;
using PushSharp.Apple;
using System;
using System.Data.Entity.SqlServer;
using System.IO;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Studio_Server
{
    // Note: For instructions on enabling IIS7 classic mode, 
    // visit http://go.microsoft.com/fwlink/?LinkId=301868
    public class WebApiApplication : System.Web.HttpApplication
    {
        public static System.Timers.Timer ApplicationTimer = new System.Timers.Timer();
        private static bool isRunning;

        //Create our push services broker
        public static PushBroker pushNotifier = new PushBroker();

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //Registering the Apple Service and sending an iOS Notification
            var appleCert = File.ReadAllBytes(System.AppDomain.CurrentDomain.BaseDirectory + "CLTP_PushCertificates.p12");
            pushNotifier.RegisterAppleService(new ApplePushChannelSettings(true, appleCert, "vietdev123!@#"));

            //Registering the GCM Service and sending an Android Notification
            pushNotifier.RegisterGcmService(new GcmPushChannelSettings("AIzaSyBaSlBLoLxtwG-0_aXmz3nfWHE2irAQsDo"));

            pushNotifier.OnNotificationSent += pushNotifier_OnNotificationSent;
            pushNotifier.OnNotificationFailed += pushNotifier_OnNotificationFailed;

            // Start timer on specific time
            isRunning = false;
            ApplicationTimer.Interval = 15 * 60 * 1000; // Run 15 mins
            ApplicationTimer.Elapsed += ApplicationTimer_Elapsed;
            ApplicationTimer.Start();
        }

        void ApplicationTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (isRunning == true)
            {
                return;
            }

            //isRunning = true;

            //var request = WebRequest.Create("http://cunglatyphu.vn/api/homeusers/register/" + Guid.NewGuid().ToString());
            //request.GetResponse();

            //isRunning = false;

            using (var entities = new Lasta_Entities())
            {
                if ((DateTime.Now.Hour == 12 && DateTime.Now.Minute < 20) || (DateTime.Now.Hour == 18 && DateTime.Now.Minute < 20))
                {
                    // Summarize total scores
                    entities.CopyTotalLeaderboard();
                }

                isRunning = true;

                // Check and send notification in 30 minutes
                var notification = entities.Notifications.FirstOrDefault(n => !n.IsDeleted && n.IsPublished && SqlFunctions.DateDiff("mi", n.PublishDate, DateTime.Now) <= 180);
                if (notification == null)
                {
                    isRunning = false;
                    return;
                }

                var deviceKeys = entities.HomeUsers
                                    .Where(u => !u.PushMessages.Any(m => m.NotifyID == notification.ID) && u.Token != null)
                                    .Select(u => new { u.ID, u.Token })
                                    .ToList();

                foreach (var key in deviceKeys)
                {
                    if (key.Token == null)
                    {
                        continue;
                    }

                    var pushMessage = new PushMessage();
                    pushMessage.CreatedOn = DateTime.Now;
                    pushMessage.Message = "";
                    pushMessage.NotifyID = notification.ID;
                    pushMessage.UserID = key.ID;

                    entities.PushMessages.Add(pushMessage);
                    entities.SaveChanges();

                    if (key.Token.Length == 64)
                    {
                        pushNotifier.QueueNotification(new AppleNotification().ForDeviceToken(key.Token).WithAlert(notification.Message).WithTag(pushMessage.ID));
                    }
                    else if (key.Token.EndsWith("=="))
                    {
                        // Windows Phone
                    }
                    else
                    {
                        //Fluent construction of an Android GCM Notification
                        //IMPORTANT: For Android you MUST use your own RegistrationId here that gets generated within your Android app itself!
                        pushNotifier.QueueNotification(new GcmNotification().ForDeviceRegistrationId(key.Token).WithJson("{\"message\":\"" + notification.Message.Replace("\"", "\\\"") + "\"}").WithTag(pushMessage.ID));
                    }
                }

                isRunning = false;
            }
        }

        void pushNotifier_OnNotificationFailed(object sender, PushSharp.Core.INotification notification, Exception error)
        {
            System.Diagnostics.EventLog.WriteEntry("Application", error.Message);
        }

        void pushNotifier_OnNotificationSent(object sender, PushSharp.Core.INotification notification)
        {
            using (var entities = new Lasta_Entities())
            {
                var appleNotification = notification as PushSharp.Apple.AppleNotification;
                var key = "";
                var payload = "";
                long messageID = 0;
                if (appleNotification != null)
                {
                    key = appleNotification.DeviceToken;
                    payload = appleNotification.Payload.Alert.Body;
                    messageID = long.Parse(appleNotification.Tag.ToString());
                }
                var androidNotification = notification as PushSharp.Android.GcmNotification;
                if (androidNotification != null)
                {
                    key = androidNotification.RegistrationIds.FirstOrDefault();
                    payload = androidNotification.JsonData;
                    messageID = long.Parse(androidNotification.Tag.ToString());
                }

                var message = entities.PushMessages.FirstOrDefault(u => u.ID == messageID);
                if (message != null)
                {
                    message.Message = payload;
                    entities.SaveChanges();
                }
            }
        }

        protected void Application_End()
        {
            ApplicationTimer.Enabled = false;
            pushNotifier.StopAllServices();
        }
    }
}
