﻿using System.Web.Mvc;

namespace Studio_Server.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
