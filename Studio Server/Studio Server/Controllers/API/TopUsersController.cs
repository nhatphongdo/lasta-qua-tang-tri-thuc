﻿using Studio_Server.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Studio_Server.Controllers.API
{
    [RoutePrefix("api/topusers")]
    public class TopUsersController : ApiController
    {
        public class HomeUser
        {
            public long ID
            {
                get;
                set;
            }

            public long Score
            {
                get;
                set;
            }
        }

        // GET api/<controller>/{top}
        [HttpGet("{top}")]
        [ResponseType(typeof(List<Models.User>))]
        public async Task<IHttpActionResult> Get(int? top)
        {
            if (!top.HasValue)
            {
                // Default is 10
                top = 10;
            }

            using (var entities = new Lasta_Entities())
            {
                var users = await entities.Users
                    .Where(u => !u.IsDeleted)
                    .Select(UsersController.AsUserModel)
                    .OrderByDescending(u => u.Score)
                    .ThenBy(u => u.ID)
                    .Take(top.Value)
                    .ToListAsync();

                return Ok(users);
            }
        }

        // GET api/<controller>/{top}
        [HttpGet("home/{top}")]
        [ResponseType(typeof(List<HomeUser>))]
        public async Task<IHttpActionResult> GetAtHome(int? top)
        {
            if (!top.HasValue)
            {
                // Default is 10
                top = 10;
            }

            using (var entities = new Lasta_Entities())
            {
                var users = (List<HomeUser>)CacheManagement.Get(string.Format("Contest_Top_{0}", top.Value));

                if (users == null)
                {
                    users = entities.TotalScores
                        .Select(u => new HomeUser()
                        {
                            ID = u.UserID,
                            Score = u.TotalScore1
                        })
                        .OrderByDescending(u => u.Score)
                        .Take(top.Value)
                        .ToList();

                    CacheManagement.Set(string.Format("Contest_Top_{0}", top.Value), users, 2 * 60000); // Expires in 5 mins

                    return Ok(users);
                }
                else
                {
                    return Ok(users);
                }
            }
        }

        // GET api/<controller>/{top}
        [HttpGet("dailyhome/{top}")]
        [ResponseType(typeof(List<HomeUser>))]
        public async Task<IHttpActionResult> GetAtHomeDaily(int? top)
        {
            if (!top.HasValue)
            {
                // Default is 10
                top = 10;
            }

            using (var entities = new Lasta_Entities())
            {
                var startDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                var endDay = startDay.AddDays(1);

                var contest = entities.ContestDays.FirstOrDefault(c => !c.IsDeleted && c.StartTime >= startDay && c.StartTime < endDay);

                if (contest == null)
                {
                    contest = entities.ContestDays.OrderByDescending(c => c.StartTime).FirstOrDefault(c => !c.IsDeleted && c.StartTime < endDay);
                }

                var contestDayID = contest.ID;

                var users = (List<HomeUser>)CacheManagement.Get(string.Format("Contest_{0}_Top_Daily_{1}", contestDayID, top.Value));

                if (users == null)
                {
                    users = entities.DayScores
                        .Where(s => s.ContestDayID == contestDayID)
                        .Select(u => new HomeUser()
                        {
                            ID = u.UserID,
                            Score = u.TotalScore
                        })
                        .OrderByDescending(u => u.Score)
                        .Take(top.Value)
                        .ToList();
                    //users = entities.HomeUsers
                    //    .Where(u => !u.IsDeleted)
                    //    .Select(u => new HomeUser()
                    //    {
                    //        ID = u.ID,
                    //        Score = u.HomeAnswers.Where(a => !a.IsDeleted && a.Score > 0 && a.ContestDayID == contestDayID)
                    //                .GroupBy(a => a.QuestionID).Select(a => a.FirstOrDefault().Score).DefaultIfEmpty(0).Sum()
                    //    })
                    //    .OrderByDescending(u => u.Score)
                    //    .Take(top.Value)
                    //    .ToList();

                    CacheManagement.Set(string.Format("Contest_{0}_Top_Daily_{1}", contestDayID, top.Value), users, 2 * 60000); // Expires in 2 mins

                    return Ok(users);
                }
                else
                {
                    return Ok(users);
                }
            }
        }

        [HttpGet("playing/{top}")]
        [ResponseType(typeof(List<Models.User>))]
        public async Task<IHttpActionResult> GetPlaying(int? top)
        {
            if (!top.HasValue)
            {
                // Default is 10
                top = 10;
            }

            using (var entities = new Lasta_Entities())
            {
                // Get the current question
                var question = await entities.Questions
                    .Where(q => !q.IsDeleted && q.Number > 0 && q.ContestDay.StartTime.Day == DateTime.Now.Day && q.ContestDay.StartTime.Month == DateTime.Now.Month && q.ContestDay.StartTime.Year == DateTime.Now.Year)
                    .OrderByDescending(q => q.Number)
                    .FirstOrDefaultAsync();

                if (question == null)
                {
                    // Not have any question yet
                    var users = await entities.Users
                        .Where(u => !u.IsDeleted)
                        .OrderBy(u => u.Seat)
                        .Select(u => new Models.User
                        {
                            ID = u.ID,
                            Fullname = u.Fullname,
                            Answer = 0,
                            Time = 0,
                            LastScore = 0,
                            Score = u.Answers.Where(a => !a.IsDeleted && a.Score > 0 && a.Question.ContestDay.StartTime.Day == DateTime.Now.Day && a.Question.ContestDay.StartTime.Month == DateTime.Now.Month && a.Question.ContestDay.StartTime.Year == DateTime.Now.Year)
                                .GroupBy(a => a.QuestionID)
                                .Select(a => a.FirstOrDefault().Score).DefaultIfEmpty(0).Sum(),
                            IsPlaying = u.IsPlaying,
                            DayScore = u.Answers.Where(a => !a.IsDeleted && a.Score > 0 && a.Question.ContestDay.StartTime.Day == DateTime.Now.Day && a.Question.ContestDay.StartTime.Month == DateTime.Now.Month && a.Question.ContestDay.StartTime.Year == DateTime.Now.Year)
                                .GroupBy(a => a.QuestionID)
                                .Select(a => a.FirstOrDefault().Score).DefaultIfEmpty(0).Sum()
                        })
                        .Take(top.Value)
                        .ToListAsync();

                    return Ok(users);
                }
                else
                {
                    var users = await entities.Users
                        .Where(u => !u.IsDeleted)
                        .OrderBy(u => u.Seat)
                        .Select(u => new Models.User
                        {
                            ID = u.ID,
                            Fullname = u.Fullname,
                            Answer = u.Answers.Where(a => !a.IsDeleted && a.QuestionID == question.ID).FirstOrDefault().AnswerNumber,
                            Time = SqlFunctions.DateDiff("ms", u.Answers.Where(a => !a.IsDeleted && a.QuestionID == question.ID).FirstOrDefault().StartTime, u.Answers.Where(a => !a.IsDeleted && a.QuestionID == question.ID).FirstOrDefault().EndTime) / 1000.0,
                            Rank = u.Answers.Where(a => !a.IsDeleted && a.QuestionID == question.ID).FirstOrDefault().Duration,
                            LastScore = u.Answers.Where(a => !a.IsDeleted && a.QuestionID == question.ID).FirstOrDefault().Score,
                            Score = u.Answers.Where(a => !a.IsDeleted && a.Score > 0 && a.Question.ContestDay.StartTime.Day == DateTime.Now.Day && a.Question.ContestDay.StartTime.Month == DateTime.Now.Month && a.Question.ContestDay.StartTime.Year == DateTime.Now.Year)
                                .GroupBy(a => a.QuestionID)
                                .Select(a => a.FirstOrDefault().Score).DefaultIfEmpty(0).Sum(),
                            IsPlaying = u.IsPlaying,
                            DayScore = u.Answers.Where(a => !a.IsDeleted && a.Score > 0 && a.Question.ContestDay.StartTime.Day == DateTime.Now.Day && a.Question.ContestDay.StartTime.Month == DateTime.Now.Month && a.Question.ContestDay.StartTime.Year == DateTime.Now.Year)
                                .GroupBy(a => a.QuestionID)
                                .Select(a => a.FirstOrDefault().Score).DefaultIfEmpty(0).Sum()
                        })
                        .Take(top.Value)
                        .ToListAsync();

                    for (var i = 0; i < users.Count; i++)
                    {
                        if (users[i].Time.HasValue == false && users[i].Rank.HasValue == false)
                        {
                            users[i].Rank = 0;
                            continue;
                        }

                        double fraction = 0;
                        if (users[i].Time.Value < users[i].Rank.Value)
                        {
                            // Round diff
                            //fraction = Math.Ceiling(users[i].Rank.Value - users[i].Time.Value);
                            fraction = users[i].Time.Value - Math.Truncate(users[i].Time.Value);
                        }
                        else if (users[i].Time.Value > users[i].Rank.Value)
                        {
                            fraction = users[i].Time.Value - Math.Truncate(users[i].Time.Value);
                        }
                        users[i].Time = fraction + users[i].Rank;
                        users[i].Rank = 0;
                    }

                    return Ok(users);
                }
            }
        }
    }
}
