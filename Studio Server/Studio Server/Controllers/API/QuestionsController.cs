﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;


namespace Studio_Server.Controllers
{
    [RoutePrefix("api/questions")]
    public class QuestionsController : ApiController
    {
        // Typed lambda expression for Select() method. 
        private static readonly Expression<Func<Question, Models.Question>> AsQuestionModel =
            q => new Models.Question
            {
                Number = q.Number,
                Content = "",
                Answer1 = q.Answer1,
                Answer2 = q.Answer2,
                Answer3 = q.Answer3,
                Answer4 = q.Answer4,
                Hint = "",
                Score = q.Score,
                CountdownTime = q.CountdownTime,
                Topic = q.Topic.TopicContent,
                CorrectAnswer = 0
            };

        private static readonly Expression<Func<Question, Models.Question>> AsMCQuestionModel =
            q => new Models.Question
            {
                Number = q.Number,
                Content = q.QuestionContent,
                Answer1 = q.Answer1,
                Answer2 = q.Answer2,
                Answer3 = q.Answer3,
                Answer4 = q.Answer4,
                Hint = q.Hint,
                Score = q.Score,
                CountdownTime = q.CountdownTime,
                Topic = q.Topic.TopicContent,
                CorrectAnswer = q.CorrectAnswer
            };

        // GET api/<controller>
        [HttpGet("{contestID}")]
        [ResponseType(typeof(List<Models.Question>))]
        public async Task<IHttpActionResult> Get(Guid? contestID)
        {
            if (contestID.HasValue)
            {
                using (var entities = new Lasta_Entities())
                {
                    var questions = await entities.Questions
                        .Where(q => !q.IsDeleted && q.ContestDay.UniqueID == contestID.Value && !q.ContestDay.IsDeleted)
                        .Select(AsQuestionModel)
                        .ToListAsync();

                    return Ok(questions);
                }
            }

            return NotFound();
        }

        [HttpGet("{contestID}/{number}")]
        [ResponseType(typeof(Models.Question))]
        public async Task<IHttpActionResult> Get(Guid? contestID, int? number)
        {
            if (!number.HasValue)
            {
                number = 1;
            }

            if (contestID.HasValue)
            {
                using (var entities = new Lasta_Entities())
                {
                    var question = await entities.Questions
                        .Where(q => !q.IsDeleted && q.Number == number.Value && q.ContestDay.UniqueID == contestID.Value && !q.ContestDay.IsDeleted)
                        .Select(AsQuestionModel)
                        .FirstOrDefaultAsync();

                    if (question != null)
                    {
                        return Ok(question);
                    }
                }
            }

            return NotFound();
        }

        [HttpGet("{contestID}/{number}/mc/{secretCode}")]
        //[HttpGet("{contestID}/{number}/mc")]
        [ResponseType(typeof(Models.Question))]
        public async Task<IHttpActionResult> GetForMC(Guid? contestID, int? number, string secretCode)
        {
            if (secretCode != "CLTP_VietD3V_123")
            {
                return NotFound();
            }

            if (!number.HasValue)
            {
                number = 1;
            }

            if (contestID.HasValue)
            {
                using (var entities = new Lasta_Entities())
                {
                    var question = await entities.Questions
                        .Where(q => !q.IsDeleted && q.Number == number.Value && q.ContestDay.UniqueID == contestID.Value && !q.ContestDay.IsDeleted)
                        .Select(AsMCQuestionModel)
                        .FirstOrDefaultAsync();

                    if (question != null)
                    {
                        return Ok(question);
                    }
                }
            }

            return NotFound();
        }
    }
}