﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Studio_Server.Controllers.API
{
    [RoutePrefix("api/answer")]
    public class AnswerController : ApiController
    {
        [HttpGet("{id}/{userID}/{answerNo}/{startTime}/{endTime}/{time}/{score}")]
        [ResponseType(typeof(Models.Answer))]
        public async Task<IHttpActionResult> Get(Guid? id, long? userID, int? answerNo, long? startTime, long? endTime, int? time, int? score)
        {
            if (id.HasValue && userID.HasValue && answerNo.HasValue && startTime.HasValue && endTime.HasValue && time.HasValue && score.HasValue)
            {
                using (var entities = new Lasta_Entities())
                {
                    var startDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    var endDay = startDay.AddDays(1);

                    var contestDay = await entities.ContestDays
                        .Where(d => !d.IsDeleted && d.StartTime >= startDay && d.StartTime < endDay)
                        .FirstOrDefaultAsync();

                    if (contestDay != null)
                    {
                        // Get the current question
                        if ((contestDay.State >= 10 && contestDay.State < 10000) || contestDay.State >= 10400)
                        {
                            var questionNumber = contestDay.State >= 10400 ? contestDay.NumberOfQuestions + 1 : contestDay.State / 10;

                            var question = await entities.Questions
                                .Where(q => !q.IsDeleted && q.Number == questionNumber && q.ContestDayID == contestDay.ID)
                                .FirstOrDefaultAsync();

                            var status = contestDay.State % 10;
                            if ((status != 2 && contestDay.State != 10401) || time.Value >= 15)
                            {
                                // Question not yet start or ended
                                return Ok(new Models.Answer()
                                {
                                    ResultCode = 2,
                                    CorrectAnswer = "",
                                    Score = 0
                                });
                            }

                            var answer = await entities.Answers
                                .Where(a => a.UserID == userID.Value && a.ContestDayID == contestDay.ID && a.QuestionID == question.ID)
                                .FirstOrDefaultAsync();

                            if (answer != null)
                            {
                                // Already answered
                                return Ok(new Models.Answer()
                                {
                                    ResultCode = 3,
                                    CorrectAnswer = "",
                                    Score = 0
                                });
                            }

                            var from1970 = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                            var paramDate = from1970.AddMilliseconds(startTime.Value).ToLocalTime();
                            var paramEnd = from1970.AddMilliseconds(endTime.Value).ToLocalTime();

                            var duration = question.CountdownTime - contestDay.RemainTime;
                            var tempScore = (question.Score / question.CountdownTime) * contestDay.RemainTime;
                            tempScore = score.Value;

                            if (tempScore > question.Score)
                            {
                                tempScore = question.Score;
                            }

                            duration = (question.CountdownTime * (question.Score - tempScore) / question.Score);

                            if (answerNo.Value != question.CorrectAnswer)
                            {
                                tempScore = 0;
                            }

                            answer = new Answer()
                            {
                                AnswerNumber = answerNo.Value,
                                ContestDayID = contestDay.ID,
                                CreatedOn = DateTime.Now,
                                Duration = duration,
                                IsDeleted = false,
                                QuestionID = question.ID,
                                Score = tempScore,
                                StartTime = paramDate,
                                EndTime = paramEnd,
                                UserID = userID.Value
                            };
                            entities.Answers.Add(answer);
                            entities.SaveChanges();

                            return Ok(new Models.Answer()
                            {
                                ResultCode = 0,
                                CorrectAnswer = question.CorrectAnswer == 1 ? question.Answer1 : (question.CorrectAnswer == 2 ? question.Answer2 : (question.CorrectAnswer == 3 ? question.Answer3 : question.Answer4)),
                                Score = answer.Score
                            });
                        }
                        else
                        {
                            // Contest ended
                            return Ok(new Models.Answer()
                            {
                                ResultCode = 1,
                                CorrectAnswer = "",
                                Score = 0
                            });
                        }
                    }
                }
            }

            return NotFound();
        }

        [HttpGet("check/{userID}")]
        [ResponseType(typeof(Models.Answer))]
        public async Task<IHttpActionResult> Check(long? userID)
        {
            if (userID.HasValue)
            {
                using (var entities = new Lasta_Entities())
                {
                    var startDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    var endDay = startDay.AddDays(1);

                    var contestDay = await entities.ContestDays
                        .Where(d => !d.IsDeleted && d.StartTime >= startDay && d.StartTime < endDay)
                        .FirstOrDefaultAsync();

                    if (contestDay != null)
                    {
                        // Get the current question
                        if ((contestDay.State >= 10 && contestDay.State < 10000) || contestDay.State >= 10400)
                        {
                            var questionNumber = contestDay.State >= 10400 ? contestDay.NumberOfQuestions + 1 : contestDay.State / 10;

                            var question = await entities.Questions
                                .Where(q => !q.IsDeleted && q.Number == questionNumber && q.ContestDayID == contestDay.ID)
                                .FirstOrDefaultAsync();

                            var answer = await entities.Answers
                                .Where(a => a.UserID == userID.Value && a.ContestDayID == contestDay.ID && a.QuestionID == question.ID)
                                .FirstOrDefaultAsync();

                            if (answer != null)
                            {
                                return Ok(new Models.Answer()
                                {
                                    ResultCode = (answer.AnswerNumber == question.CorrectAnswer ? 1 : 0),
                                    CorrectAnswer = answer.AnswerNumber == 1 ? ("A. " + question.Answer1) : (answer.AnswerNumber == 2 ? ("B. " + question.Answer2) : (answer.AnswerNumber == 3 ? ("C. " + question.Answer3) : (answer.AnswerNumber == 4 ? ("D. " + question.Answer4) : ""))),
                                    Score = answer.Score
                                });
                            }
                            else
                            {
                                return NotFound();
                            }
                        }
                        else
                        {
                            return NotFound();
                        }
                    }
                    else
                    {
                        return NotFound();
                    }
                }
            }
            else
            {
                return NotFound();
            }
        }
    }
}