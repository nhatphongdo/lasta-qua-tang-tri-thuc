﻿using Studio_Server.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Studio_Server.Controllers
{
    [RoutePrefix("api/states")]
    public class StatesController : ApiController
    {
        // Typed lambda expression for Select() method. 
        private static readonly Expression<Func<ContestDay, Models.State>> AsStateModel =
            d => new Models.State
            {
                Status = d.State,
                RemainTime = d.RemainTime,
                TotalQuestions = d.NumberOfQuestions
            };

        [HttpGet("{time:long}")]
        [ResponseType(typeof(Models.State))]
        public async Task<IHttpActionResult> Get(long? time)
        {
            // Return the current state of contest by specific date
            // If there is no contest, return -1
            if (time.HasValue)
            {
                using (var entities = new Lasta_Entities())
                {
                    var paramDate = new DateTime(time.Value * 10000);
                    var startDay = new DateTime(paramDate.Year, paramDate.Month, paramDate.Day);
                    var endDay = startDay.AddDays(1);

                    var state = await entities.ContestDays
                        .Where(d => !d.IsDeleted && d.StartTime >= startDay && d.StartTime < endDay)
                        .Select(AsStateModel)
                        .FirstOrDefaultAsync();

                    return Ok(state);
                }
            }

            return NotFound();
        }

        [HttpGet("{id}")]
        [ResponseType(typeof(Models.State))]
        public async Task<IHttpActionResult> Get(Guid? id)
        {
            // Return the current state of contest by specific id
            // If there is no contest, return -1
            if (id.HasValue)
            {
                var state = (State)CacheManagement.Get(id.Value.ToString());
                if (state == null)
                {
                    using (var entities = new Lasta_Entities())
                    {
                        state = await entities.ContestDays
                            .Where(d => !d.IsDeleted && d.UniqueID == id.Value)
                            .Select(AsStateModel)
                            .FirstOrDefaultAsync();

                        CacheManagement.Set(id.Value.ToString(), state);

                        return Ok(state);
                    }
                }
                else
                {
                    return Ok(state);
                }
            }

            return NotFound();
        }
    }
}