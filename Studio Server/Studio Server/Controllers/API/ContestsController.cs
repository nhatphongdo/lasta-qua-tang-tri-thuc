﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Studio_Server.Controllers
{
    [RoutePrefix("api/contests")]
    public class ContestsController : ApiController
    {
        private static readonly DateTime beginTime = new DateTime(1970, 1, 1);
        // Typed lambda expression for Select() method. 
        private static readonly Expression<Func<ContestDay, Models.ContestDay>> AsContestDayModel =
            c => new Models.ContestDay
            {
                StartTime = DbFunctions.DiffSeconds(beginTime, c.StartTime) ?? 0,
                NumberOfQuestions = c.NumberOfQuestions,
                State = c.State,
                Remain = c.RemainTime,
                ContestKey = c.UniqueID
            };


        // GET api/<controller>/<id>
        [HttpGet("{id}")]
        [ResponseType(typeof(Models.ContestDay))]
        public async Task<IHttpActionResult> Get(Guid? id)
        {
            if (id.HasValue)
            {
                using (var entity = new Lasta_Entities())
                {
                    var contestDay = await entity.ContestDays
                        .Where(d => d.UniqueID == id.Value && !d.IsDeleted)
                        .Select(AsContestDayModel)
                        .FirstOrDefaultAsync();

                    if (contestDay != null)
                    {
                        return Ok(contestDay);
                    }
                }
            }

            return NotFound();
        }

        [HttpGet("{id}/prizes")]
        [ResponseType(typeof(List<Models.Prize>))]
        public async Task<IHttpActionResult> GetPrizes(Guid? id)
        {
            if (id.HasValue)
            {
                using (var entity = new Lasta_Entities())
                {
                    var prizes = await entity.Prizes
                        .Where(p => !p.ContestDay.IsDeleted && p.ContestDay.UniqueID == id.Value)
                        .Select(p => new Models.Prize()
                        {
                            Name = p.Name,
                            Description = p.Description,
                            Image = p.Image,
                            Sponsor = p.Sponsor,
                            SponsorType = p.SponsorType,
                            Type = p.Type
                        })
                        .ToListAsync();

                    return Ok(prizes);
                }
            }

            return NotFound();
        }

        [HttpGet("{id}/prizes/{number}")]
        [ResponseType(typeof(Models.Prize))]
        public async Task<IHttpActionResult> GetPrize(Guid? id, int? number)
        {
            if (id.HasValue)
            {
                if (!number.HasValue)
                {
                    number = 1;
                }

                using (var entity = new Lasta_Entities())
                {
                    var prize = await entity.Prizes
                        .Where(p => !p.ContestDay.IsDeleted && p.ContestDay.UniqueID == id.Value)
                        .OrderBy(p => p.ID)
                        .Skip(number.Value - 1).Take(1)
                        .Select(p => new Models.Prize()
                        {
                            Name = p.Name,
                            Description = p.Description,
                            Image = p.Image,
                            Sponsor = p.Sponsor,
                            SponsorType = p.SponsorType,
                            Type = p.Type
                        })
                        .FirstOrDefaultAsync();

                    return Ok(prize);
                }
            }

            return NotFound();
        }

        [HttpGet("{id}/topics")]
        [ResponseType(typeof(List<Models.Topic>))]
        public async Task<IHttpActionResult> Topics(Guid? id)
        {
            if (id.HasValue)
            {
                using (var entity = new Lasta_Entities())
                {
                    var topics = await entity.Questions
                        .Where(q => q.ContestDay.UniqueID == id.Value && !q.IsDeleted)
                        .Select(q => q.Topic.TopicContent)
                        .ToListAsync();

                    var result = await entity.Topics
                        .Where(t => !t.IsDeleted)
                        .Select(t => new Models.Topic
                        {
                            Name = t.TopicContent,
                            Count = 2
                        })
                        .ToListAsync();

                    foreach (var item in result)
                    {
                        item.Count = topics.Count(t => t == item.Name);
                    }

                    return Ok(result);
                }
            }

            return NotFound();
        }

        [HttpGet("today")]
        [ResponseType(typeof(Models.ContestDay))]
        public async Task<IHttpActionResult> Today()
        {
            using (var entity = new Lasta_Entities())
            {
                var now = DateTime.Now;
                var contestDay = await entity.ContestDays
                    .Where(d => d.StartTime.Year == now.Year && d.StartTime.Month == now.Month && d.StartTime.Day == now.Day &&
                        ((now.Hour * 3600 + now.Minute * 60 + now.Second) - (d.StartTime.Hour * 3600 + d.StartTime.Minute * 60 + d.StartTime.Second) >= 300) &&
                        !d.IsDeleted)
                    .Select(AsContestDayModel)
                    .FirstOrDefaultAsync();

                if (contestDay != null)
                {
                    return Ok(contestDay);
                }
                else
                {
                    return NotFound();
                }
            }
        }
    }
}