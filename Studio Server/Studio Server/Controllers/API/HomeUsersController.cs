﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Studio_Server.Controllers
{
    [RoutePrefix("api/homeusers")]
    public class HomeUsersController : ApiController
    {
        // GET api/<controller>
        [ResponseType(typeof(List<Models.User>))]
        public async Task<IHttpActionResult> Get()
        {
            using (var entities = new Lasta_Entities())
            {
                var contestDayID = entities.ContestDays.Where(c => c.StartTime <= DateTime.Now).OrderByDescending(c => c.StartTime).FirstOrDefault().ID;

                var users = await entities.HomeUsers
                    .Where(u => !u.IsDeleted)
                    .Select(u => new Models.User
                    {
                        ID = u.ID,
                        Fullname = u.Fullname,
                        Email = u.Email,
                        Score = u.HomeAnswers.Where(a => !a.IsDeleted && a.Score > 0).Select(a => (long)a.Score).DefaultIfEmpty(0).Sum() +
                                u.ExchangeScores.Where(s => s.IsValid).Select(s => (long)s.ExchangedScore).DefaultIfEmpty(0).Sum() +
                                entities.TransferScores.Where(s => s.ReceiverID == u.ID).Select(s => (long)s.SentScore).DefaultIfEmpty(0).Sum() -
                                entities.TransferScores.Where(s => s.SenderID == u.ID).Select(s => (long)s.SentScore).DefaultIfEmpty(0).Sum(),
                        DayScore = u.HomeAnswers.Where(a => !a.IsDeleted && a.Score > 0 && a.Question.ContestDayID == contestDayID).Select(a => (long)a.Score).DefaultIfEmpty(0).Sum()
                    })
                    .ToListAsync();

                return Ok(users);
            }
        }

        [HttpGet("search/{filter}")]
        [ResponseType(typeof(List<Models.User>))]
        public async Task<IHttpActionResult> Search(string filter)
        {
            using (var entities = new Lasta_Entities())
            {
                var contestDayID = entities.ContestDays.Where(c => c.StartTime <= DateTime.Now).OrderByDescending(c => c.StartTime).FirstOrDefault().ID;

                filter = Uri.UnescapeDataString(filter).Replace("==", "/").Replace("===", "+");

                var users = await entities.HomeUsers
                    .Where(u => !u.IsDeleted && (u.Fullname.ToLower().Contains(filter.ToLower()) || SqlFunctions.StringConvert((decimal)u.ID).Contains(filter)))
                    .Select(u => new Models.User
                    {
                        ID = u.ID,
                        Fullname = u.Fullname,
                        Email = u.Email,
                        Rank = entities.TotalScores.Where(s => s.UserID == u.ID).Select(s => s.Rank).FirstOrDefault(),
                        Score = entities.TotalScores.Where(s => s.UserID == u.ID).Select(s => s.TotalScore1).FirstOrDefault(),
                        DayScore = entities.DayScores.Where(s => s.ContestDayID == contestDayID && s.UserID == u.ID).Select(s => s.TotalScore).FirstOrDefault()
                    })
                    .ToListAsync();

                return Ok(users);
            }
        }

        [HttpGet("{id}")]
        public IHttpActionResult Get(long? id)
        {
            if (id.HasValue)
            {
                using (var entities = new Lasta_Entities())
                {
                    var startDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    var endDay = startDay.AddDays(1);

                    var contest = entities.ContestDays.FirstOrDefault(c => !c.IsDeleted && c.StartTime >= startDay && c.StartTime < endDay);

                    if (contest == null)
                    {
                        contest = entities.ContestDays.OrderByDescending(c => c.StartTime).FirstOrDefault(c => !c.IsDeleted && c.StartTime < endDay);
                    }
                    else
                    {
                        // Update daily leaderboard if needed
                        //var lastUpdateTime = entities.DayScores.OrderByDescending(s => s.UpdatedOn).Select(s => s.UpdatedOn).FirstOrDefault();
                        //if ((DateTime.Now.ToUniversalTime() - lastUpdateTime).TotalMinutes >= 1)
                        //{
                        //    entities.CopyDailyLeaderboard(contest.ID);
                        //}
                    }

                    var totalScore = entities.TotalScores.Where(s => s.UserID == id.Value).Select(s => s.TotalScore1).FirstOrDefault();
                    var rank = entities.TotalScores.Where(s => s.UserID == id.Value).Select(s => s.Rank).FirstOrDefault();

                    if (totalScore == 0)
                    {
                        rank = entities.HomeUsers.OrderByDescending(u => u.ID).Select(u => u.ID).FirstOrDefault();
                    }

                    // No contest or end contest
                    var contestDayID = contest == null ? 0 : contest.ID;

                    var user = entities.HomeUsers
                    .Where(u => u.ID == id.Value && !u.IsDeleted)
                    .Select(u => new
                    {
                        ID = u.ID,
                        Fullname = u.Fullname,
                        Email = u.Email,
                        PhoneNumber = u.PhoneNumber,
                        Address = u.Address,
                        IsOptIn = u.OptIn,
                        DayRank = entities.DayScores.Where(s => s.ContestDayID == contestDayID && s.UserID == id.Value).Select(s => s.Rank).FirstOrDefault(),
                        Rank = rank,
                        Score = totalScore,
                        DayScore = entities.DayScores.Where(s => s.ContestDayID == contestDayID && s.UserID == id.Value).Select(s => s.TotalScore).FirstOrDefault()
                    })
                    .FirstOrDefault();

                    if (user == null)
                    {
                        return NotFound();
                    }

                    return Ok(new
                    {
                        ID = user.ID,
                        Fullname = user.Fullname,
                        Email = user.Email,
                        PhoneNumber = user.PhoneNumber,
                        Address = user.Address,
                        IsOptIn = user.IsOptIn,
                        DayRank = user.DayScore > 0 ? user.DayRank : (contest.StartTime < DateTime.Now ? 307826 : 0),
                        Rank = user.Rank,
                        Score = user.Score,
                        DayScore = user.DayScore
                    });
                }
            }

            return NotFound();
        }

        [HttpGet("transfercode/{id}/{deviceID}")]
        public IHttpActionResult GetTransferCode(long? id, string deviceID)
        {
            if (id.HasValue && !string.IsNullOrEmpty(deviceID))
            {
                using (var entities = new Lasta_Entities())
                {
                    deviceID = Uri.UnescapeDataString(deviceID).Replace("==", "/").Replace("===", "+");
                    var user = entities.HomeUsers.FirstOrDefault(u => u.ID == id.Value && !u.IsDeleted && u.UniqueIdentifier.ToUpper() == deviceID.ToUpper());

                    if (user == null)
                    {
                        return NotFound();
                    }

                    if (string.IsNullOrEmpty(user.TransferCode))
                    {
                        var code = new ObjectParameter("string", typeof(string));
                        entities.usp_generateIdentifier(5, 5, code);
                        user.TransferCode = code.Value.ToString();
                        entities.SaveChanges();
                    }

                    return Ok(new
                    {
                        ID = user.ID,
                        TransferCode = user.TransferCode
                    });
                }
            }

            return NotFound();
        }

        [HttpGet("register/{deviceID}")]
        [ResponseType(typeof(Models.RegisteredUser))]
        public async Task<IHttpActionResult> Register(string deviceID)
        {
            using (var entities = new Lasta_Entities())
            {
                deviceID = Uri.UnescapeDataString(deviceID).Replace("==", "/").Replace("===", "+");

                var user = await entities.HomeUsers
                    .FirstOrDefaultAsync(u => u.UniqueIdentifier.ToUpper() == deviceID.ToUpper());

                if (user != null)
                {
                    // Exist user with same device id
                    return Ok(new Models.RegisteredUser()
                    {
                        ID = user.ID,
                        Error = -1
                    });
                }

                var transferCodeParam = new ObjectParameter("string", typeof(string));
                entities.usp_generateIdentifier(5, 5, transferCodeParam);

                user = new HomeUser()
                {
                    UniqueIdentifier = deviceID,
                    TransferCode = transferCodeParam.Value.ToString(),
                    IsDeleted = false,
                    CreatedOn = DateTime.Now
                };
                entities.HomeUsers.Add(user);
                var result = await entities.SaveChangesAsync();

                if (result == 1)
                {
                    // Has inserted row
                    return Ok(new Models.RegisteredUser()
                    {
                        ID = user.ID,
                        Error = 0
                    });
                }
                else
                {
                    // No inserted row
                    return Ok(new Models.RegisteredUser()
                    {
                        ID = 0,
                        Error = -2
                    });
                }
            }
        }

        [HttpGet("setToken/{deviceID}/{userID}/{token}")]
        [ResponseType(typeof(Models.RegisteredUser))]
        public async Task<IHttpActionResult> SetToken(string deviceID, long? userID, string token)
        {
            if (!string.IsNullOrEmpty(deviceID) && userID.HasValue && !string.IsNullOrEmpty(token))
            {
                using (var entities = new Lasta_Entities())
                {
                    deviceID = Uri.UnescapeDataString(deviceID).Replace("==", "/").Replace("===", "+");
                    token = Uri.UnescapeDataString(token).Replace("==", "/").Replace("===", "+");

                    var user = await entities.HomeUsers
                        .FirstOrDefaultAsync(u => u.UniqueIdentifier.ToUpper() == deviceID.ToUpper());

                    if (user == null)
                    {
                        // Exist user with same device id
                        return NotFound();
                    }

                    user.Token = token;
                    var result = await entities.SaveChangesAsync();

                    if (result == 1)
                    {
                        // Has inserted row
                        return Ok(new Models.RegisteredUser()
                        {
                            ID = user.ID,
                            Error = 0
                        });
                    }
                    else
                    {
                        // No inserted row
                        return Ok(new Models.RegisteredUser()
                        {
                            ID = 0,
                            Error = -2
                        });
                    }
                }
            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet("update/{deviceID}/{userID}/{fullname}/{phoneNumber}/{email}/{address}/{optIn}")]
        [ResponseType(typeof(Models.RegisteredUser))]
        public async Task<IHttpActionResult> Update(string deviceID, long? userID, string fullname, string phoneNumber, string email, string address, int optIn)
        {
            if (userID.HasValue && !string.IsNullOrEmpty(deviceID) && userID != 2575)
            {
                deviceID = Uri.UnescapeDataString(deviceID).Replace("==", "/").Replace("===", "+");
                email = Uri.UnescapeDataString(email).Replace("==", "/").Replace("===", "+");
                fullname = Uri.UnescapeDataString(fullname).Replace("==", "/").Replace("===", "+");
                address = Uri.UnescapeDataString(address).Replace("==", "/").Replace("===", "+");

                using (var entities = new Lasta_Entities())
                {
                    var user = await entities.HomeUsers
                        .Where(u => u.ID == userID.Value && u.UniqueIdentifier.ToLower() == deviceID.ToLower() && !u.IsDeleted)
                        .FirstOrDefaultAsync();

                    if (user == null)
                    {
                        return NotFound();
                    }

                    if (user.Fullname == fullname && user.PhoneNumber == phoneNumber && user.Email == email && user.Address == address && user.OptIn == (optIn == 1))
                    {
                        // Not change
                        return Ok(new Models.RegisteredUser()
                        {
                            ID = user.ID,
                            Error = 0
                        });
                    }

                    user.Fullname = fullname;
                    user.PhoneNumber = phoneNumber;
                    user.Email = email;
                    user.Address = address;
                    user.OptIn = optIn == 1;

                    var result = await entities.SaveChangesAsync();

                    if (result == 1)
                    {
                        return Ok(new Models.RegisteredUser()
                        {
                            ID = user.ID,
                            Error = 0
                        });
                    }
                    else
                    {
                        return Ok(new Models.RegisteredUser()
                        {
                            ID = user.ID,
                            Error = -1
                        });
                    }
                }
            }

            return NotFound();
        }
    }
}
