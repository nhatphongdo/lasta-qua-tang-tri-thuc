﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Studio_Server.Controllers
{
    [RoutePrefix("api/users")]
    public class UsersController : ApiController
    {
        // Typed lambda expression for Select() method. 
        public static readonly Expression<Func<User, Models.User>> AsUserModel =
            u => new Models.User
            {
                ID = u.ID,
                Fullname = u.Fullname,
                Email = u.Email,
                //Score = u.Answers.Where(a => !a.IsDeleted && a.Score > 0).Sum(a => (long)a.Score),
                Score = u.Answers.Where(a => !a.IsDeleted && a.Score > 0 && a.Question.ContestDay.StartTime.Day == DateTime.Now.Day && a.Question.ContestDay.StartTime.Month == DateTime.Now.Month && a.Question.ContestDay.StartTime.Year == DateTime.Now.Year)
                        .GroupBy(a => a.QuestionID)
                        .Select(a => a.FirstOrDefault().Score).DefaultIfEmpty(0).Sum(),
                DayScore = u.Answers.Where(a => !a.IsDeleted && a.Score > 0 && a.Question.ContestDay.StartTime.Day == DateTime.Now.Day && a.Question.ContestDay.StartTime.Month == DateTime.Now.Month && a.Question.ContestDay.StartTime.Year == DateTime.Now.Year)
                        .GroupBy(a => a.QuestionID)
                        .Select(a => a.FirstOrDefault().Score).DefaultIfEmpty(0).Sum()
            };


        // GET api/<controller>
        [ResponseType(typeof(List<Models.User>))]
        public async Task<IHttpActionResult> Get()
        {
            using (var entities = new Lasta_Entities())
            {
                var users = await entities.Users
                    .Where(u => !u.IsDeleted)
                    .Select(AsUserModel)
                    .ToListAsync();

                return Ok(users);
            }
        }

        [HttpGet("{id}")]
        [ResponseType(typeof(Models.User))]
        public async Task<IHttpActionResult> Get(long? id)
        {
            if (id.HasValue)
            {
                using (var entities = new Lasta_Entities())
                {
                    var user = await entities.Users
                        .Where(u => u.ID == id.Value && !u.IsDeleted)
                        .Select(AsUserModel)
                        .FirstOrDefaultAsync();

                    if (user == null)
                    {
                        return NotFound();
                    }

                    return Ok(user);
                }
            }

            return NotFound();
        }

        [HttpGet("search/{filter}")]
        [ResponseType(typeof(List<Models.User>))]
        public async Task<IHttpActionResult> Search(string filter)
        {
            using (var entities = new Lasta_Entities())
            {
                filter = Uri.UnescapeDataString(filter).Replace("==", "/").Replace("===", "+");

                var users = await entities.Users
                    .Where(u => !u.IsDeleted && (u.Fullname.ToLower().Contains(filter.ToLower()) || SqlFunctions.StringConvert((decimal)u.ID).Contains(filter)))
                    .Select(AsUserModel)
                    .ToListAsync();

                return Ok(users);
            }
        }

        [HttpGet("register/{deviceID}")]
        [ResponseType(typeof(Models.RegisteredUser))]
        public async Task<IHttpActionResult> Register(string deviceID)
        {
            using (var entities = new Lasta_Entities())
            {
                deviceID = Uri.UnescapeDataString(deviceID).Replace("==", "/").Replace("===", "+");

                var user = await entities.Users
                    .FirstOrDefaultAsync(u => u.UniqueIdentifier.ToUpper() == deviceID.ToUpper());

                if (user != null)
                {
                    // Exist user with same device id
                    return Ok(new Models.RegisteredUser()
                    {
                        ID = user.ID,
                        Error = -1
                    });
                }

                user = new User()
                {
                    UniqueIdentifier = deviceID,
                    IsDeleted = false,
                    CreatedOn = DateTime.Now
                };
                entities.Users.Add(user);
                var result = await entities.SaveChangesAsync();

                if (result == 1)
                {
                    // Has inserted row
                    return Ok(new Models.RegisteredUser()
                    {
                        ID = user.ID,
                        Error = 0
                    });
                }
                else
                {
                    // No inserted row
                    return Ok(new Models.RegisteredUser()
                    {
                        ID = 0,
                        Error = -2
                    });
                }
            }
        }
    }
}