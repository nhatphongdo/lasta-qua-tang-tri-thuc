﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Studio_Server.Controllers.API
{
    [RoutePrefix("api/common")]
    public class CommonController : ApiController
    {
        private static Random _randomizer = new Random((int)DateTime.Now.Ticks);

        // GET api/<controller>/version
        [HttpGet("version/{platform}")]
        [ResponseType(typeof(Setting))]
        public async Task<IHttpActionResult> Version(string platform)
        {
            using (var entity = new Lasta_Entities())
            {
                var setting = await entity.Settings
                    .Where(s => s.Name.ToLower() == "version_" + platform)
                    .FirstOrDefaultAsync();

                if (setting != null)
                {
                    return Ok(setting);
                }
                else
                {
                    return NotFound();
                }
            }
        }

        // GET api/<controller>/exchange/{code}/{userID}
        [HttpGet("exchange/{code}/{userID}")]
        public async Task<IHttpActionResult> Exchange(string code, long? userID)
        {
            if (userID.HasValue && !string.IsNullOrEmpty(code))
            {
                using (var entity = new Lasta_Entities())
                {
                    var user = await entity.HomeUsers
                        .Where(u => !u.IsDeleted && u.ID == userID.Value)
                        .FirstOrDefaultAsync();

                    if (user != null)
                    {
                        var exchange = await entity.ExchangeScores
                            .Where(e => e.ProductCode.ToLower() == code.ToLower())
                            .FirstOrDefaultAsync();

                        if (exchange != null)
                        {
                            // Already exchanged
                            return Ok(new
                            {
                                Error = 1
                            });
                        }
                        else
                        {
                            // Check code
                            var service = new SmsServiceReference.checkcodeClient();
                            var serviceResult = service.check("lasta", "!@#lasta", code);
                            if (serviceResult == false)
                            {
                                return Ok(new
                                {
                                    Error = 2
                                });
                            }

                            exchange = new ExchangeScore();
                            exchange.UserID = user.ID;
                            exchange.ProductCode = code;
                            exchange.ExchangeDate = DateTime.Now;
                            var seed = _randomizer.Next(1000);
                            var score = 0;
                            if (seed < 800)
                            {
                                score = 5;
                            }
                            else if (seed < 950)
                            {
                                score = 10;
                            }
                            else if (seed < 980)
                            {
                                score = 15;
                            }
                            else if (seed < 995)
                            {
                                score = 30;
                            }
                            else if (seed < 998)
                            {
                                score = 50;
                            }
                            else if (seed < 1000)
                            {
                                score = 100;
                            }
                            exchange.ExchangedScore = score;
                            exchange.IsValid = serviceResult;

                            entity.ExchangeScores.Add(exchange);
                            var result = await entity.SaveChangesAsync();

                            if (result == 1)
                            {
                                return Ok(new
                                {
                                    Error = 0,
                                    Score = score
                                });
                            }
                            else
                            {
                                return Ok(new
                                {
                                    Error = 3
                                });
                            }
                        }
                    }
                    else
                    {
                        return NotFound();
                    }
                }
            }
            else
            {
                return NotFound();
            }
        }

        // GET api/<controller>/transfer/{userID}/{deviceID}/{receiverID}/{score}/
        [HttpGet("transfer/{userID}/{deviceID}/{receiverID}/{receiverCode}/{score}")]
        public async Task<IHttpActionResult> Transfer(long? userID, string deviceID, long? receiverID, string receiverCode, int? score)
        {
            if (userID.HasValue && !string.IsNullOrEmpty(deviceID) && receiverID.HasValue && !string.IsNullOrEmpty(receiverCode) && score.HasValue)
            {
                deviceID = Uri.UnescapeDataString(deviceID).Replace("==", "/").Replace("===", "+");

                using (var entities = new Lasta_Entities())
                {
                    // Home to Home
                    var setting = await entities.Settings
                        .Where(s => s.Name.ToLower() == "allowtransfer")
                        .FirstOrDefaultAsync();

                    if (setting == null || setting.Value.ToLower() == "no")
                    {
                        // Not allow to transfer score
                        return Ok(new
                        {
                            Error = -1
                        });
                    }

                    var user = await entities.HomeUsers
                        .Where(u => u.ID == userID.Value && u.UniqueIdentifier.ToLower() == deviceID.ToLower() && !u.IsDeleted)
                        .FirstOrDefaultAsync();

                    if (user == null)
                    {
                        return NotFound();
                    }

                    var transferHistory = await entities.TransferScores
                        .Where(t => t.SenderID == user.ID && t.Type == 1)
                        .CountAsync();

                    if (transferHistory > 0)
                    {
                        // Already transfer
                        return Ok(new
                        {
                            Error = 1
                        });
                    }

                    transferHistory = await entities.TransferScores
                            .Where(t => t.ReceiverID == user.ID && t.Type == 1)
                            .CountAsync();

                    if (transferHistory > 0)
                    {
                        // Already received, not allow to transfer
                        return Ok(new
                        {
                            Error = 5
                        });
                    }

                    var receiver = await entities.HomeUsers
                        .Where(u => u.ID == receiverID.Value && u.TransferCode.ToLower() == receiverCode.ToLower() && !u.IsDeleted)
                        .FirstOrDefaultAsync();

                    if (receiver == null)
                    {
                        return NotFound();
                    }

                    var receiveHistory = await entities.TransferScores
                        .Where(t => t.ReceiverID == receiver.ID && t.Type == 1)
                        .CountAsync();

                    if (receiveHistory >= 5)
                    {
                        // Maximum of 5 transfers
                        return Ok(new
                        {
                            Error = 2
                        });
                    }

                    // Get the maximum score of this user
                    var maxScore = entities.HomeAnswers.Where(a => a.UserID == user.ID && !a.IsDeleted && a.Score > 0).Count() == 0 ? 0 :
                                        entities.HomeAnswers
                                        .Where(a => a.UserID == user.ID && !a.IsDeleted && a.Score > 0)
                                        .Sum(a => a.Score);

                    if (maxScore == 0 || score.Value > maxScore)
                    {
                        // No score to transfer
                        return Ok(new
                        {
                            Error = 4
                        });
                    }

                    if (score.Value > maxScore)
                    {
                        score = maxScore;
                    }

                    var transfer = new TransferScore();
                    transfer.ReceiverID = receiver.ID;
                    transfer.SenderID = user.ID;
                    transfer.SentDate = DateTime.Now;
                    transfer.SentScore = score.Value;
                    transfer.Type = 1;

                    entities.TransferScores.Add(transfer);
                    var result = await entities.SaveChangesAsync();

                    if (result == 1)
                    {
                        return Ok(new
                        {
                            Error = 0
                        });
                    }
                    else
                    {
                        return Ok(new
                        {
                            Error = 3
                        });
                    }
                }
            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet("message/{name}")]
        [ResponseType(typeof(Setting))]
        public async Task<IHttpActionResult> Message(string name)
        {
            using (var entity = new Lasta_Entities())
            {
                var setting = await entity.Settings
                    .Where(s => s.Name.ToLower() == "message_" + name)
                    .FirstOrDefaultAsync();

                if (setting != null)
                {
                    return Ok(setting);
                }
                else
                {
                    return NotFound();
                }
            }
        }

        [HttpGet("copy")]
        public IHttpActionResult Copy()
        {
            using (var entity = new Lasta_Entities())
            {
                try
                {
                    //var users = entity.HomeUsers
                    //    .Where(u => !u.IsDeleted)
                    //    .Select(u => new
                    //    {
                    //        UserID = u.ID,
                    //        TotalScore = u.HomeAnswers.Where(a => !a.IsDeleted && a.Score > 0).GroupBy(a => a.QuestionID).Select(a => a.FirstOrDefault().Score).DefaultIfEmpty(0).Sum()
                    //            + u.ExchangeScores.Where(s => s.IsValid).Select(s => s.ExchangedScore).DefaultIfEmpty(0).Sum()
                    //            + entity.TransferScores.Where(a => a.ReceiverID == u.ID && a.Type == 2).Select(a => a.SentScore).DefaultIfEmpty(0).Sum()
                    //            - entity.TransferScores.Where(a => a.SenderID == u.ID && a.Type == 2).Select(a => a.SentScore).DefaultIfEmpty(0).Sum()
                    //    })
                    //    .ToList();

                    //foreach (var user in users)
                    //{
                    //    var dayScore = entity.DayScores.FirstOrDefault(s => s.UserID == user.UserID);
                    //    if (dayScore != null)
                    //    {
                    //        if (dayScore.TotalScore < user.TotalScore)
                    //        {
                    //            dayScore.TotalScore = user.TotalScore;
                    //            dayScore.UpdatedOn = DateTime.Now;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        dayScore = new DayScore()
                    //        {
                    //            TotalScore = user.TotalScore,
                    //            UserID = user.UserID,
                    //            UpdatedOn = DateTime.Now
                    //        };

                    //        entity.DayScores.Add(dayScore);
                    //    }

                    //    entity.SaveChanges();
                    //}

                    entity.CopyTotalLeaderboard();

                    return Ok(new
                    {
                        Message = "Copy items successfully."
                    });
                }
                catch (Exception exc)
                {
                    return Ok(new
                    {
                        Message = exc.ToString(),
                        StackTrace = exc.StackTrace,
                    });
                }
            }
        }

        [HttpGet("count")]
        [ResponseType(typeof(Setting))]
        public IHttpActionResult Count()
        {
            using (var entity = new Lasta_Entities())
            {
                var playerCount = entity.HomeAnswers.GroupBy(a => a.UserID).Count();

                return Ok(new
                {
                    PlayerCount = playerCount
                });
            }
        }
    }
}
