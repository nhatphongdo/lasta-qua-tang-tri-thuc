﻿using System.Windows;
using Microsoft.Phone.Scheduler;
using Microsoft.Phone.Shell;
using System;
using System.Net;
using Newtonsoft.Json.Linq;
using System.IO;


namespace BillBackgroundAgent
{
    public class ScheduledAgent : ScheduledTaskAgent
    {
        private static volatile bool _classInitialized;

        /// <remarks>
        /// ScheduledAgent constructor, initializes the UnhandledException handler
        /// </remarks>
        public ScheduledAgent()
        {
            if (!_classInitialized)
            {
                _classInitialized = true;
                // Subscribe to the managed exception handler
                Deployment.Current.Dispatcher.BeginInvoke(delegate
                {
                    Application.Current.UnhandledException += ScheduledAgent_UnhandledException;
                });
            }
        }

        /// Code to execute on Unhandled Exceptions
        private void ScheduledAgent_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // An unhandled exception has occurred; break into the debugger
                System.Diagnostics.Debugger.Break();
            }
        }

        /// <summary>
        /// Agent that runs a scheduled task
        /// </summary>
        /// <param name="task">
        /// The invoked task
        /// </param>
        /// <remarks>
        /// This method is called when a periodic or resource intensive task is invoked
        /// </remarks>
        protected override void OnInvoke(ScheduledTask task)
        {
            if (task is PeriodicTask)
            {
                try
                {
                    Uri uri = new Uri("http://cunglatyphu.vn/api/common/message/wp_notification");
                    HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(uri);
                    request.BeginGetResponse(new AsyncCallback(ReadWebRequestCallback), request);
                }
                catch (Exception ex)
                {
                    Abort();
                }
            }
        }

        public void ReadWebRequestCallback(IAsyncResult callbackResult)
        {
            HttpWebRequest myRequest = (HttpWebRequest)callbackResult.AsyncState;
            using (HttpWebResponse myResponse = (HttpWebResponse)myRequest.EndGetResponse(callbackResult))
            {
                using (StreamReader httpwebStreamReader = new StreamReader(myResponse.GetResponseStream()))
                {
                    try
                    {
                        string results = httpwebStreamReader.ReadToEnd();
                        JObject obj = JObject.Parse(results);
                        if (obj["Name"] != null && obj["Value"] != null)
                        {
                            ShellToast toast = new ShellToast();
                            toast.Title = "Cùng Là Tỷ Phú";
                            toast.Content = (obj["Value"] ?? "").ToString();
                            toast.Show();
                        }
                    }
                    catch (WebException ex)
                    {
                    }
                }
            }
            NotifyComplete();
        }
    }
}