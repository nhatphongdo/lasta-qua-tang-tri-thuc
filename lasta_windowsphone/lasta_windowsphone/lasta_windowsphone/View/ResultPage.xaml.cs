﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Threading;
using Newtonsoft.Json.Linq;
using System.IO.IsolatedStorage;
using Microsoft.Xna.Framework.Media;

namespace lasta_windowsphone.View
{
    public partial class ResultPage : PhoneApplicationPage
    {
        private DispatcherTimer Timer;
        private bool isFailed = false;
        private bool isLoading = false;
        private bool isLoadQuestion = false;
        public long contestState = 0;
        public long userID;
        public string contestKey = "";
        //public long score = 0;
        //public string correctAnswer = "";
        public string answeredText = "";
        //public string prefix = "";
        IsolatedStorageSettings appData = IsolatedStorageSettings.ApplicationSettings;
        private int countTimeout;
        private int countDisconnect;
        private int countFail;
        private WebClient loadstate;
        private WebClient loadAnswer;

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            base.OnBackKeyPress(e);
            e.Cancel = true;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            GoogleAnalytics.EasyTracker.GetTracker().SendView("ResultPage");
            //remove Back Entry
            NavigationService.RemoveBackEntry();

            //string str_score = "";
            if (NavigationContext.QueryString.ContainsKey("contestkey"))
            {
                NavigationContext.QueryString.TryGetValue("contestkey", out contestKey);
            }
            //if (NavigationContext.QueryString.ContainsKey("correctanswer"))
            //{
            //    NavigationContext.QueryString.TryGetValue("correctanswer", out correctAnswer);
            //}
            //if (NavigationContext.QueryString.ContainsKey("score"))
            //{
            //    NavigationContext.QueryString.TryGetValue("score", out str_score);
            //}
            if (NavigationContext.QueryString.ContainsKey("answeredtext"))
            {
                NavigationContext.QueryString.TryGetValue("answeredtext", out answeredText);
            }
            //if (NavigationContext.QueryString.ContainsKey("prefix"))
            //{
            //    NavigationContext.QueryString.TryGetValue("prefix", out prefix);
            //}

            //if (!long.TryParse(str_score, out score))
            //{
            //    score = 0;
            //}

            if (!long.TryParse(appData[Enums.USERID_KEY].ToString(), out userID))
            {
                userID = 0;
            }

            LoadAnswer(userID);
            isFailed = false;
            isLoading = false;
            isLoadQuestion = false;
            countDisconnect = 0;
            countTimeout = 0;
            countFail = 0;
            //refresh in 2s
            //Update timer code init here.
            Timer = new DispatcherTimer();
            Timer.Interval = TimeSpan.FromMilliseconds(2000);
            Timer.Tick += new EventHandler(update_Tick);
            Timer.Start();

#if HOME_VERSION
            //UPDATE SCORE, DAY SCORE, RANK FOR HOME USER
            //Function.UpdateUserData();
#endif
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            MediaPlayer.Stop();
            base.OnNavigatedFrom(e);
            this.Timer.Stop();
        }

        public ResultPage()
        {
            InitializeComponent();
        }

        private void update_Tick(object sender, EventArgs e)
        {
            if (isLoadQuestion == true)
            {
                countTimeout += 1000;
                if (isLoading == true && countTimeout > Enums.timeout)
                {
                    loadstate.CancelAsync();
                    isLoading = false;
                }
                if (isLoading == false)
                {
                    countTimeout = 0;
                    loadState();
                }
            }
        }

        private void loadState()
        {
            if (!string.IsNullOrEmpty(contestKey))
            {
                isLoading = true;

                //Load Question
                loadstate = new WebClient();
                loadstate.DownloadStringCompleted += new DownloadStringCompletedEventHandler(loadstate_downloadstringcompleted);
                loadstate.DownloadStringAsync(new Uri(string.Format(Enums.STATE_URL, contestKey)));
            }
        }

        private void loadstate_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                JObject json = JObject.Parse(e.Result);
                contestState = long.Parse(json["Status"].ToString());
                processState();
                isLoading = false;
            }
            catch (WebException ex)
            {
                countDisconnect += 1;
                if (isFailed == false && countDisconnect >= Enums.totalDisconnect)
                {
                    MessageBoxResult m = MessageBox.Show("Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công.", Enums.ApplicationTitle, MessageBoxButton.OK);
                    if (m == MessageBoxResult.OK)
                    {
                        // reconnetion code in here
                    }
                    isFailed = true;
                }
                isLoading = false;
            }
        }

        private void processState()
        {
            if (contestState >= 10 && contestState < 10000)
            {
                if (contestState % 10 == 3)
                {
                    //nothing in here
                }
                else if (contestState % 10 == 2)
                {
                    if (!appData.Contains(Enums.QUESTION_NUMBER_KEY))
                    {
                        appData.Add(Enums.QUESTION_NUMBER_KEY, contestState / 10);
                    }
                    else
                    {
                        appData[Enums.QUESTION_NUMBER_KEY] = contestState / 10;
                    }
                    //Redirect to PlayPage
                    NavigationService.Navigate(new Uri(string.Format("/View/PlayPage.xaml?contestkey={0}&questnumb={1}&conteststate={2}", Uri.EscapeUriString(contestKey), Uri.EscapeUriString((contestState / 10).ToString()), Uri.EscapeUriString(contestState.ToString())), UriKind.Relative));
                }
                else
                {
                    if (!appData.Contains(Enums.QUESTION_NUMBER_KEY))
                    {
                        appData.Add(Enums.QUESTION_NUMBER_KEY, contestState / 10);
                    }
                    else
                    {
                        appData[Enums.QUESTION_NUMBER_KEY] = contestState / 10;
                    }
                    //Redirect to PlayInfoPage
                    NavigationService.Navigate(new Uri(string.Format("/View/PlayInfoPage.xaml?contestkey={0}&conteststate={1}&questnumb={2}", Uri.EscapeUriString(contestKey), Uri.EscapeUriString(contestState.ToString()), Uri.EscapeUriString((contestState / 10).ToString())), UriKind.Relative));
                }
            }
            else if (contestState == 10400)
            {
                var number_question = ((appData.Contains(Enums.NUMBER_QUESTIONS_KEY) ? int.Parse(appData[Enums.NUMBER_QUESTIONS_KEY].ToString()) + 1 : 0)).ToString();
                NavigationService.Navigate(new Uri(string.Format("/View/PlayInfoPage.xaml?contestkey={0}&questnumb={1}&conteststate={2}", contestKey, number_question, contestState.ToString()), UriKind.Relative));
            }
            else if (contestState == 10401)
            {
                // go to play page
                NavigationService.Navigate(new Uri(string.Format("/View/PlayPage.xaml?contestkey={0}&conteststate={1}", Uri.EscapeUriString(contestKey), Uri.EscapeUriString(contestState.ToString())), UriKind.Relative));

            }
            else if (contestState == 10000 || contestState == 10100 || contestState == 10200)
            {
                int quesNum = appData.Contains(Enums.QUESTION_NUMBER_KEY) ? int.Parse(appData[Enums.QUESTION_NUMBER_KEY].ToString()) : 0;
                if (quesNum > 0 && quesNum % 4 == 0)
                {
                    //Go to summary page
                    NavigationService.Navigate(new Uri(string.Format("/View/SummaryPage.xaml?contestkey={0}", Uri.EscapeUriString(contestKey)), UriKind.Relative));
                }
                else
                {
                    //go to infoPage
                    NavigationService.Navigate(new Uri(string.Format("/View/ContestInfoPage.xaml?contestkey={0}", Uri.EscapeUriString(contestKey)), UriKind.Relative));
                }
            }
            else if (contestState == 10300)
            {
                //Redirect to SummaryPage
                NavigationService.Navigate(new Uri(string.Format("/View/SummaryPage.xaml?contestkey={0}", Uri.EscapeUriString(contestKey)), UriKind.Relative));
            }
        }

        private void LoadAnswer(long userID)
        {
            if (!string.IsNullOrEmpty(contestKey))
            {
                loadAnswer = new WebClient();
                loadAnswer.DownloadStringCompleted += new DownloadStringCompletedEventHandler(LoadAnswer_downloadstringcompleted);
                loadAnswer.DownloadStringAsync(new Uri(string.Format(Enums.CHECK_ANSWER, userID)));
            }
        }

        private void LoadAnswer_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                JObject obj = JObject.Parse(e.Result);
                string answer = (obj["CorrectAnswer"] ?? "").ToString();
                int resultCode = Convert.ToInt32((obj["ResultCode"] ?? "0").ToString());
                int score = Convert.ToInt32((obj["Score"] ?? "0").ToString());

                if (!string.IsNullOrEmpty(answer))
                {
                    if (resultCode == 1)
                    {
                        SuccessAnswer(answer, score);
                    }
                    else
                    {
                        FailAnswer(answer);
                    }
                }
                else
                {
                    FailAnswer("");
                }
                isLoadQuestion = true;
            }
            catch (WebException ex)
            {
                if (((HttpWebResponse)ex.Response).StatusCode == HttpStatusCode.NotFound)
                {
                    //NotFound
                    if (!string.IsNullOrEmpty(answeredText))
                    {
                        FailAnswer("");
                        MessageBoxResult m = MessageBox.Show("Vui lòng kiểm tra kết nối mạng, hệ thống không nhận được kết quả bạn gởi về.", Enums.MessageBoxTitle, MessageBoxButton.OK);
                    }
                    else
                    {
                        FailAnswer("");
                    }
                    isLoadQuestion = true;
                }
                else
                {
                    countFail++;
                    if (countFail <= 2)
                    {
                        LoadAnswer(userID);
                    }
                    else
                    {
                        isLoadQuestion = true;
                    }
                }
            }
        }

        private void FailAnswer(string answer)
        {
            //Incorrect
            txtAnswerFalse.Text = answer;
            text_True_1.Visibility = Visibility.Collapsed;
            borderTrue.Visibility = Visibility.Collapsed;
            icon_True.Visibility = Visibility.Collapsed;
            txtTruePlus.Visibility = Visibility.Collapsed;
            txtScore.Visibility = Visibility.Collapsed;
            img_true.Visibility = Visibility.Collapsed;

            text_False_1.Visibility = Visibility.Visible;
            borderFalse.Visibility = Visibility.Visible;
            icon_False.Visibility = Visibility.Visible;
            SoundPlayer.PlayMediaBackgroundNoRep("Sad", SoundPlayer.faceSad);
        }

        private void SuccessAnswer(string answer, int score)
        {
            //Correct
            txtAnswerTrue.Text = answer;
            text_True_1.Visibility = Visibility.Visible;
            borderTrue.Visibility = Visibility.Visible;
            icon_True.Visibility = Visibility.Visible;
            txtTruePlus.Visibility = Visibility.Visible;
            txtScore.Visibility = Visibility.Visible;
            img_true.Visibility = Visibility.Visible;
            txtScore.Text = score.ToString();

            text_False_1.Visibility = Visibility.Collapsed;
            borderFalse.Visibility = Visibility.Collapsed;
            icon_False.Visibility = Visibility.Collapsed;
            SoundPlayer.PlayMediaBackgroundNoRep("Smile", SoundPlayer.faceSmile);
        }

    }
}