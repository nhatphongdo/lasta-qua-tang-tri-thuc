﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Devices;
using ZXing;
using System.Windows.Threading;
using System.Windows.Media.Imaging;
using System.Windows.Input;
using Newtonsoft.Json.Linq;
using System.IO.IsolatedStorage;
using System.Text.RegularExpressions;
using System.Windows.Media;
using Microsoft.Xna.Framework.Media;

namespace lasta_windowsphone.View
{
    public partial class QAcodePage : PhoneApplicationPage
    {
        private PhotoCamera _phoneCamera;
        private IBarcodeReader _barcodeReader;
        private DispatcherTimer _scanTimer;
        private WriteableBitmap _previewBuffer;
        private IsolatedStorageSettings appData = IsolatedStorageSettings.ApplicationSettings;
        private string contestLink = "";
        private DispatcherTimer Timer;
        private int countTimeout;
        private int countDisconnect;
        private bool isLoading, isFailed;
        private WebClient getstate;
        public string contestKey;

        public QAcodePage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            GoogleAnalytics.EasyTracker.GetTracker().SendView("QRCodePage");
            // Initialize the camera object
            _phoneCamera = new PhotoCamera();
            _phoneCamera.Initialized += cam_Initialized;
            _phoneCamera.AutoFocusCompleted += _phoneCamera_AutoFocusCompleted;

            CameraButtons.ShutterKeyHalfPressed += CameraButtons_ShutterKeyHalfPressed;

            //Display the camera feed in the UI
            viewfinderBrush.SetSource(_phoneCamera);


            // This timer will be used to scan the camera buffer every 250ms and scan for any barcodes
            _scanTimer = new DispatcherTimer();
            _scanTimer.Interval = TimeSpan.FromMilliseconds(250);
            _scanTimer.Tick += (o, arg) => ScanForBarcode();
            //viewfinderCanvas.Tap += new EventHandler<GestureEventArgs>(focus_Tapped);
            base.OnNavigatedTo(e);

            countTimeout = 0;
            countDisconnect = 0;
            isLoading = false;
            isFailed = false;
            //refresh in 2s
            //Update timer code init here.
            Timer = new DispatcherTimer();
            Timer.Interval = TimeSpan.FromMilliseconds(2000);
            Timer.Tick += new EventHandler(update_Tick);
            Timer.Start();
        }

        private void update_Tick(object sender, EventArgs e)
        {
            countTimeout += 1000;
            if (isLoading == true && countTimeout > Enums.timeout)
            {
                getstate.CancelAsync();
                isLoading = false;
            }

            if (isLoading == false)
            {
                countTimeout = 0;
                loadContest(Enums.CONTEST_TODAY_URL);
            }
        }

        void _phoneCamera_AutoFocusCompleted(object sender, CameraOperationCompletedEventArgs e)
        {
            Deployment.Current.Dispatcher.BeginInvoke(delegate()
            {
                focusBrackets.Visibility = Visibility.Collapsed;
            });
        }

        void focus_Tapped(object sender, Microsoft.Phone.Controls.GestureEventArgs e)
        {
            if (_phoneCamera != null)
            {
                if (_phoneCamera.IsFocusAtPointSupported == true)
                {
                    // Determine the location of the tap.
                    Point tapLocation = e.GetPosition(viewfinderCanvas);

                    // Position the focus brackets with the estimated offsets.
                    focusBrackets.SetValue(Canvas.LeftProperty, tapLocation.X - 30);
                    focusBrackets.SetValue(Canvas.TopProperty, tapLocation.Y - 28);

                    // Determine the focus point.
                    double focusXPercentage = tapLocation.X / viewfinderCanvas.ActualWidth;
                    double focusYPercentage = tapLocation.Y / viewfinderCanvas.ActualHeight;

                    // Show the focus brackets and focus at point.
                    focusBrackets.Visibility = Visibility.Visible;
                    _phoneCamera.FocusAtPoint(focusXPercentage, focusYPercentage);
                }
            }
        }

        void CameraButtons_ShutterKeyHalfPressed(object sender, EventArgs e)
        {
            _phoneCamera.Focus();
        }

        protected override void OnNavigatingFrom(System.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            //we're navigating away from this page, we won't be scanning any barcodes
            _scanTimer.Stop();
            Timer.Stop();
            if (_phoneCamera != null)
            {
                // Cleanup
                _phoneCamera.Dispose();
                _phoneCamera.Initialized -= cam_Initialized;
                CameraButtons.ShutterKeyHalfPressed -= CameraButtons_ShutterKeyHalfPressed;
            }
        }

        void cam_Initialized(object sender, Microsoft.Devices.CameraOperationCompletedEventArgs e)
        {
            if (e.Succeeded)
            {
                this.Dispatcher.BeginInvoke(delegate()
                {
                    _phoneCamera.FlashMode = FlashMode.Off;
                    _previewBuffer = new WriteableBitmap((int)_phoneCamera.PreviewResolution.Width, (int)_phoneCamera.PreviewResolution.Height);

                    _barcodeReader = new BarcodeReader();

                    // By default, BarcodeReader will scan every supported barcode type
                    // If we want to limit the type of barcodes our app can read, 
                    // we can do it by adding each format to this list object

                    //var supportedBarcodeFormats = new List<BarcodeFormat>();
                    //supportedBarcodeFormats.Add(BarcodeFormat.QR_CODE);
                    //supportedBarcodeFormats.Add(BarcodeFormat.DATA_MATRIX);
                    //_bcReader.PossibleFormats = supportedBarcodeFormats;

                    _barcodeReader.TryHarder = true;

                    _barcodeReader.ResultFound += _bcReader_ResultFound;
                    _scanTimer.Start();
                    //Load Contest Today
                    //loadContest(Enums.CONTEST_TODAY_URL);
                });
            }
            else
            {
                Dispatcher.BeginInvoke(() =>
                {
                    MessageBoxResult message = MessageBox.Show("Không thể khởi động camera trên thiết bị của bạn, xin vui lòng chọn OK chương trình sẽ kết nối tự động.", Enums.MessageBoxTitle, MessageBoxButton.OK);
                    if (message == MessageBoxResult.OK)
                    {
                        //Load Contest Today
                        //loadContest(Enums.CONTEST_TODAY_URL);
                    }
                });
            }
        }

        void _bcReader_ResultFound(Result obj)
        {
            string regexUrl = @"^http(s)?://([\w-]+.)+[\w-]+(/[\w- ./?%&=])?$";
            // If a new barcode is found, vibrate the device and display the barcode details in the UI
            if (!string.IsNullOrEmpty(obj.Text) && Regex.IsMatch(obj.Text, regexUrl))
            {
                loadContest(obj.Text);
            }
        }

        void loadContest(string stateUrl)
        {
            isLoading = true;
            Uri url;
            bool result = Uri.TryCreate(stateUrl, UriKind.Absolute, out url);
            if (result)
            {
                contestLink = stateUrl;
                WebClient getstate = new WebClient();
                getstate.DownloadStringCompleted += new DownloadStringCompletedEventHandler(getstate_downloadstringcompleted);
                getstate.DownloadStringAsync(url);
            }
        }

        void getstate_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                JObject json = JObject.Parse(e.Result);
                long numbques = long.Parse(json["NumberOfQuestions"].ToString());
                if (!appData.Contains(Enums.NUMBER_QUESTIONS_KEY))
                {
                    appData.Add(Enums.NUMBER_QUESTIONS_KEY, numbques);
                }
                else
                {
                    appData[Enums.NUMBER_QUESTIONS_KEY] = numbques;
                }

                long state = long.Parse(json["State"].ToString());

                string contest_key = json["ContestKey"].ToString();
                if (state >= 10 && state < 10000)
                {
                    if (state % 10 == 3)
                    {
                        //Go to Result page 
                        NavigationService.Navigate(new Uri(string.Format("/View/ResultPage.xaml?contestkey={0}", contest_key), UriKind.RelativeOrAbsolute));
                    }
                    else if (state % 10 == 2)
                    {
                        //go to question page
                        if (!appData.Contains(Enums.QUESTION_NUMBER_KEY))
                        {
                            appData.Add(Enums.QUESTION_NUMBER_KEY, state / 10);
                        }
                        else
                        {
                            appData[Enums.QUESTION_NUMBER_KEY] = state / 10;
                        }

                        long questnumb = state / 10;  // Divided by 10 to get the question number
                        int numbofquest = int.Parse(json["NumberOfQuestions"].ToString());
                        long remaintime = long.Parse(json["Remain"].ToString());
                        NavigationService.Navigate(new Uri(string.Format("/View/PlayPage.xaml?questnumb={0}&contestkey={1}&numbofquest={2}&conteststate={3}&remaintime={4}", Uri.EscapeUriString(questnumb.ToString()), Uri.EscapeUriString(contest_key), Uri.EscapeUriString(numbofquest.ToString()), Uri.EscapeUriString(state.ToString()), Uri.EscapeUriString(remaintime.ToString())), UriKind.Relative));
                    }
                    else
                    {
                        if (!appData.Contains(Enums.QUESTION_NUMBER_KEY))
                        {
                            appData.Add(Enums.QUESTION_NUMBER_KEY, state / 10);
                        }
                        else
                        {
                            appData[Enums.QUESTION_NUMBER_KEY] = state / 10;
                        }
                        // Go to question info Page
                        long questnumb = state / 10;  // Divided by 10 to get the question number
                        NavigationService.Navigate(new Uri(string.Format("/View/PlayInfoPage.xaml?questnumb={0}&contestkey={1}&conteststate={2}", Uri.EscapeUriString(questnumb.ToString()), Uri.EscapeUriString(contest_key), Uri.EscapeUriString(state.ToString())), UriKind.Relative));
                    }
                }
                else if (state == 10400)
                {
                    var number_question = ((appData.Contains(Enums.NUMBER_QUESTIONS_KEY) ? int.Parse(appData[Enums.NUMBER_QUESTIONS_KEY].ToString()) + 1 : 0)).ToString();
                    NavigationService.Navigate(new Uri(string.Format("/View/PlayInfoPage.xaml?contestkey={0}&questnumb={1}&conteststate={2}", contest_key, number_question, state.ToString()), UriKind.Relative));
                }
                else if (state == 10401)
                {
                    //go to question page
                    long questnumb = int.Parse(json["NumberOfQuestions"].ToString()) + 1;
                    int numbofquest = int.Parse(json["NumberOfQuestions"].ToString());
                    long remaintime = long.Parse(json["Remain"].ToString());
                    NavigationService.Navigate(new Uri(string.Format("/View/PlayPage.xaml?questnumb={0}&contestkey={1}&numbofquest={2}&conteststate={3}&remaintime={4}", Uri.EscapeUriString(questnumb.ToString()), Uri.EscapeUriString(contest_key), Uri.EscapeUriString(numbofquest.ToString()), Uri.EscapeUriString(state.ToString()), Uri.EscapeUriString(remaintime.ToString())), UriKind.Relative));
                }
                else if (state == 10402)
                {
                    //Go to Result page 
                    NavigationService.Navigate(new Uri(string.Format("/View/ResultPage.xaml?contestkey={0}", contest_key), UriKind.RelativeOrAbsolute));
                }
                else if (state == 10000 || state == 10100 || state == 10200)
                {
                    // go to info page
                    int quesNum = appData.Contains(Enums.QUESTION_NUMBER_KEY) ? int.Parse(appData[Enums.QUESTION_NUMBER_KEY].ToString()) : 0;
                    if (quesNum > 0 && quesNum % 4 == 0 && quesNum < 16)
                    {
                        //Go to summary page
                        NavigationService.Navigate(new Uri(string.Format("/View/SummaryPage.xaml?contestkey={0}", Uri.EscapeUriString(contest_key)), UriKind.Relative));
                    }
                    else
                    {
                        //go to infoPage
                        NavigationService.Navigate(new Uri(string.Format("/View/ContestInfoPage.xaml?contestkey={0}&firstload=true", Uri.EscapeUriString(contest_key)), UriKind.Relative));
                    }
                }
                else if (state == 10300)
                {
                    // go to finished page
                    NavigationService.Navigate(new Uri(string.Format("/View/SummaryPage.xaml?contestkey={0}", Uri.EscapeUriString(contest_key)), UriKind.Relative));
                }
                else
                {
                    // If state is not in contest, stay here or go back?
                    // go to menu page
                    NavigationService.Navigate(new Uri("/View/MenuPage.xaml", UriKind.Relative));
                }
                isLoading = false;
            }
            catch (WebException ex)
            {
                if (((HttpWebResponse)ex.Response).StatusCode == HttpStatusCode.NotFound)
                {
                    // Not Found (404)
                    if (contestLink.Equals(Enums.CONTEST_TODAY_URL))
                    {
                        if (isFailed == false)
                        {
                            isFailed = true;
                            // Load today contest failed then stay here for QR scanning
#if STUDIO_VERSION
            
                MessageBoxResult m = MessageBox.Show("Không có chương trình gameshow vào thời gian này. Vui lòng đón xem gameshow \"Cùng Là Tỷ Phú\" được truyền hình trực tiếp trên kênh truyền hình Let's Viet vào lúc 20h30 thứ bảy và chủ nhật hàng tuần.", Enums.ApplicationTitle, MessageBoxButton.OK);
            
#else
                            Function.GetGreenMessage();

#endif
                        }
                    }
                    else
                    {
                        countDisconnect += 1;
                        if (isFailed == false && countDisconnect >= Enums.totalDisconnect)
                        {
                            MessageBoxResult m = MessageBox.Show("Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công.", Enums.ApplicationTitle, MessageBoxButton.OK);
                            if (m == MessageBoxResult.OK)
                            {
                                // reconnetion code in here
                            }
                            isFailed = true;
                        }
                    }
                }
                isLoading = false;
            }
        }

        private void ScanForBarcode()
        {
            try
            {
                //grab a camera snapshot
                _phoneCamera.GetPreviewBufferArgb32(_previewBuffer.Pixels);
                _previewBuffer.Invalidate();

                //scan the captured snapshot for barcodes
                //if a barcode is found, the ResultFound event will fire
                _barcodeReader.Decode(_previewBuffer);
            }
            catch (Exception ex)
            { }
        }

        private void PhoneApplicationPage_OrientationChanged(object sender, OrientationChangedEventArgs e)
        {
            var transform = viewfinderCanvas.TransformToVisual(LayoutRoot1);
            var offset = transform.Transform(new Point(0, 0));
            if ((this.Orientation & PageOrientation.LandscapeLeft) == (PageOrientation.LandscapeLeft))
            {
                RotateTransform rotate = new RotateTransform();
                rotate.Angle = 0;
                rotate.CenterX = offset.X;
                rotate.CenterY = offset.Y;
                viewfinderCanvas.RenderTransform = rotate;
            }
            else
            {
                RotateTransform rotate = new RotateTransform();
                rotate.Angle = 180;
                rotate.CenterX = offset.X + 108;
                rotate.CenterY = offset.Y + 60;
                viewfinderCanvas.RenderTransform = rotate;
            }
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            var transform = viewfinderCanvas.TransformToVisual(LayoutRoot1);
            var offset = transform.Transform(new Point(0, 0));
            if ((this.Orientation & PageOrientation.LandscapeLeft) == (PageOrientation.LandscapeLeft))
            {
                RotateTransform rotate = new RotateTransform();
                rotate.Angle = 0;
                rotate.CenterX = offset.X;
                rotate.CenterY = offset.Y;
                viewfinderCanvas.RenderTransform = rotate;
            }
            else
            {
                RotateTransform rotate = new RotateTransform();
                rotate.Angle = 180;
                rotate.CenterX = offset.X + 108;
                rotate.CenterY = offset.Y + 60;
                viewfinderCanvas.RenderTransform = rotate;
            }
        }
    }
}