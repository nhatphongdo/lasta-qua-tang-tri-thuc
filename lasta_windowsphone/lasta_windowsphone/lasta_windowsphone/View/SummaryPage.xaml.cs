﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Threading;
using Newtonsoft.Json.Linq;
using System.IO.IsolatedStorage;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace lasta_windowsphone.View
{
    public partial class SummaryPage : PhoneApplicationPage
    {
        private DispatcherTimer Timer;
        private bool isFailed;
        private bool isLoading;
        private bool isLock;
        public long contestState;
        public string contestKey, number_question;
        IsolatedStorageSettings appData = IsolatedStorageSettings.ApplicationSettings;
        private SoundEffectInstance soundeffect;
        private int countTimeout;
        private int countDisconnect;
        private int top;
        private WebClient loadstate;

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            GoogleAnalytics.EasyTracker.GetTracker().SendView("SummaryPage");
            top = 50;
            panelSearch.Visibility = Visibility.Collapsed;
            panelTop.Margin = new Thickness(12, 15, 0, 10);
            //clear media player
            MediaPlayer.Stop();
            //Load
            //get querystring here...
            string str_conteststate = "";
            string isSearch = "";
            if (NavigationContext.QueryString.ContainsKey("contestkey"))
            {
                NavigationContext.QueryString.TryGetValue("contestkey", out contestKey);
            }
            if (NavigationContext.QueryString.ContainsKey("conteststate"))
            {
                NavigationContext.QueryString.TryGetValue("conteststate", out str_conteststate);
            }
            if (NavigationContext.QueryString.ContainsKey("search"))
            {
                NavigationContext.QueryString.TryGetValue("search", out isSearch);
            }
            if (!long.TryParse(str_conteststate, out contestState))
            {
                contestState = 0;
            }
            number_question = ((appData.Contains(Enums.NUMBER_QUESTIONS_KEY) ? int.Parse(appData[Enums.NUMBER_QUESTIONS_KEY].ToString()) + 1 : 0)).ToString();

            //Invisible total score top 50
            btTotalTop.Visibility = Visibility.Collapsed;
            if (isSearch.Equals("true"))
            {
                SoundPlayer.PlayMediaBackground("SummaryMenu", SoundPlayer.opening);
                panelSummary.boxDayIndex.Visibility = Visibility.Visible;
                panelSummary.boxTotalIndex.Visibility = Visibility.Visible;
                panelSummary.boxTotalScore.Visibility = Visibility.Visible;
                btTotalTop.Visibility = Visibility.Visible;
                //Update Info from server
                Function.UpdateUserData();
            }
            else
            {
                SoundPlayer.PlayMediaBackground("SummaryTop", SoundPlayer.background);
                //remove Back Entry
                NavigationService.RemoveBackEntry();
                int quesNum = appData.Contains(Enums.QUESTION_NUMBER_KEY) ? int.Parse(appData[Enums.QUESTION_NUMBER_KEY].ToString()) : 0;
                int numb_ques = 0;
                int.TryParse(number_question, out numb_ques);
                if (numb_ques > 0 && quesNum >= numb_ques)
                {
                    //Update Info from server
                    Function.UpdateUserData();
                    appData.Remove(Enums.QUESTION_NUMBER_KEY);
                    appData.Remove(Enums.NUMBER_QUESTIONS_KEY);
                    appData.Save();
                    panelSummary.boxDayIndex.Visibility = Visibility.Visible;
                    panelSummary.boxTotalIndex.Visibility = Visibility.Visible;
                    panelSummary.boxTotalScore.Visibility = Visibility.Visible;
                }
                else
                {
                    panelSummary.boxDayIndex.Visibility = Visibility.Collapsed;
                    panelSummary.boxTotalIndex.Visibility = Visibility.Collapsed;
                    panelSummary.boxTotalScore.Visibility = Visibility.Collapsed;
                }
            }
            panelSearch.Visibility = Visibility.Collapsed;
            panelTop.Margin = new Thickness(12, 15, 0, 10);
            TopUsers.Height = 170;

#if STUDIO_VERSION
            panelTop.Visibility = Visibility.Collapsed;
            panelButtonTop.Visibility = Visibility.Collapsed;
#endif

#if HOME_VERSION
            DayTopHomeUsers(top);
            ActiveButton("day");
#endif

            //Load Data for ListItem
            isFailed = false;
            isLoading = false;
            countTimeout = 0;
            countDisconnect = 0;
            isLock = false;
            Timer = new DispatcherTimer();
            Timer.Interval = TimeSpan.FromMilliseconds(2000);
            Timer.Tick += new EventHandler(update_Tick);
            Timer.Start();

            //UPDATE SCORE, DAY SCORE, RANK
            //Function.UpdateUserData();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            MediaPlayer.Stop();
            base.OnNavigatedFrom(e);
            //Exist
            this.Timer.Stop();
        }

        public SummaryPage()
        {
            InitializeComponent();
        }

        private void TotalTopHomeUsers(int top)
        {
            WebClient totalTop = new WebClient();
            totalTop.DownloadStringCompleted += new DownloadStringCompletedEventHandler(totalTop_downloadstringcompleted);
            totalTop.DownloadStringAsync(new Uri(string.Format(Enums.TOP_USER_URL, top)));
        }

        private void totalTop_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                var jsonArray = JArray.Parse(e.Result);
                List<RowData> users = getArrayData(jsonArray, top);
                this.TopUsers.ItemsSource = users;
            }
            catch (WebException ex)
            {
                if (isFailed == false)
                {
                    MessageBoxResult m = MessageBox.Show("Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công.", Enums.ApplicationTitle, MessageBoxButton.OK);
                    if (m == MessageBoxResult.OK)
                    {
                        // reconnetion code in here
                    }
                    isFailed = true;
                }
            }
            isLock = false;
        }

        private void DayTopHomeUsers(int top)
        {
            WebClient dayTop = new WebClient();
            dayTop.DownloadStringCompleted += new DownloadStringCompletedEventHandler(dayTop_downloadstringcompleted);
            dayTop.DownloadStringAsync(new Uri(string.Format(Enums.DAY_TOP_USER_URL, top)));
        }

        private void dayTop_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                var jsonArray = JArray.Parse(e.Result);
                List<RowData> users = getArrayData(jsonArray, top);
                this.TopUsers.ItemsSource = users;
            }
            catch (WebException ex)
            {
                if (isFailed == false)
                {
                    MessageBoxResult m = MessageBox.Show("Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công.", Enums.ApplicationTitle, MessageBoxButton.OK);
                    if (m == MessageBoxResult.OK)
                    {
                        // reconnetion code in here
                    }
                    isFailed = true;
                }
            }
            isLock = false;
        }

        private List<RowData> getArrayData(JArray jsonArray, int numb)
        {
            List<RowData> users = new List<RowData>();
            int i = 0;
            foreach (var item in jsonArray)
            {
                char[] charArray = new char[26];
                i++;

                //split TT
                if (i.ToString().Length == 1)
                {
                    charArray[0] = '0';
                    charArray[1] = i.ToString().ToCharArray()[0];
                }
                else
                {
                    for (int a = 0; a < 2; a++)
                    {
                        charArray[a] = i.ToString().ToCharArray()[a];
                    }
                }
                charArray[2] = ' ';
                charArray[3] = ' ';
                //split ID
                char[] idArray = item["ID"].ToString().ToCharArray();
                for (int a = 4; a < 12; a++)
                {
                    if (a > idArray.Length + 3)
                    {
                        charArray[a] = ' ';
                    }
                    else
                    {
                        charArray[a] = idArray[a - 4];
                    }
                }
                charArray[12] = ' ';
                charArray[13] = ' ';
                //split Score
                char[] scoreArray = item["Score"].ToString().ToCharArray();
                Array.Reverse(scoreArray);
                for (int a = 23; a > 13; a--)
                {
                    if (a <= 23 - scoreArray.Length)
                    {
                        charArray[a] = ' ';
                    }
                    else
                    {
                        charArray[a] = scoreArray[23 - a];
                    }
                }
                charArray[24] = ' ';
                charArray[25] = ' ';
                users.Add(new RowData(charArray[0], charArray[1], charArray[2], charArray[3], charArray[4], charArray[5], charArray[6], charArray[7], charArray[8], charArray[9], charArray[10], charArray[11], charArray[12], charArray[13], charArray[14], charArray[15], charArray[16], charArray[17], charArray[18], charArray[19], charArray[20], charArray[21], charArray[22], charArray[23], charArray[24], charArray[25]));
                if (i == numb)
                {
                    break;
                }
            }
            return users;
        }

        private void update_Tick(object sender, EventArgs e)
        {
            countTimeout += 1000;
            if (isLoading == true && countTimeout > Enums.timeout)
            {
                loadstate.CancelAsync();
                isLoading = false;
            }

            if (isLoading == false)
            {
                loadState();
            }
        }

        private void loadState()
        {
            if (!string.IsNullOrEmpty(contestKey))
            {
                isLoading = true;
                loadstate = new WebClient();
                loadstate.DownloadStringCompleted += new DownloadStringCompletedEventHandler(loadstate_downloadstringcompleted);
                loadstate.DownloadStringAsync(new Uri(string.Format(Enums.STATE_URL, contestKey)));
            }
        }

        private void loadstate_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                JObject json = JObject.Parse(e.Result);
                if (!long.TryParse(json["Status"].ToString(), out contestState))
                {
                    contestState = 0;
                }
                processState();
                isLoading = false;
            }
            catch (WebException ex)
            {
                countDisconnect += 1;
                if (isFailed == false && countDisconnect >= Enums.totalDisconnect)
                {
                    MessageBoxResult m = MessageBox.Show("Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công.", Enums.ApplicationTitle, MessageBoxButton.OK);
                    if (m == MessageBoxResult.OK)
                    {
                        // reconnetion code in here
                    }
                    isFailed = true;
                }
                isLoading = false;
            }
        }

        private void processState()
        {
            if (contestState >= 10 && contestState < 10000)
            {
                if (contestState % 10 == 3)
                {
                }
                else if (contestState % 10 == 2)
                {
                    //go to PlayPage
                    if (!appData.Contains(Enums.QUESTION_NUMBER_KEY))
                    {
                        appData.Add(Enums.QUESTION_NUMBER_KEY, contestState / 10);
                    }
                    else
                    {
                        appData[Enums.QUESTION_NUMBER_KEY] = contestState / 10;
                    }
                    NavigationService.Navigate(new Uri(string.Format("/View/PlayPage.xaml?questnumb={0}&contestkey={1}&conteststate={2}", contestState / 10, contestKey, contestState), UriKind.Relative));
                }
                else
                {
                    if (!appData.Contains(Enums.QUESTION_NUMBER_KEY))
                    {
                        appData.Add(Enums.QUESTION_NUMBER_KEY, contestState / 10);
                    }
                    else
                    {
                        appData[Enums.QUESTION_NUMBER_KEY] = contestState / 10;
                    }
                    //go to PlayInfoPage
                    NavigationService.Navigate(new Uri(string.Format("/View/PlayInfoPage.xaml?questnumb={0}&contestkey={1}&conteststate={2}", contestState / 10, contestKey, contestState), UriKind.Relative));
                }
            }
            else if (contestState == 10400)
            {
                NavigationService.Navigate(new Uri(string.Format("/View/PlayInfoPage.xaml?contestkey={0}&questnumb={1}&conteststate={2}", contestKey, number_question, contestState.ToString()), UriKind.Relative));
            }
            else if (contestState == 10401)
            {
                //Go to play page
                var number_question = ((appData.Contains(Enums.NUMBER_QUESTIONS_KEY) ? int.Parse(appData[Enums.NUMBER_QUESTIONS_KEY].ToString()) + 1 : 0)).ToString();
                //var number_question = 17;
                NavigationService.Navigate(new Uri(string.Format("/View/PlayPage.xaml?contestkey={0}&questnumb={1}&conteststate={2}", contestKey, number_question, contestState.ToString()), UriKind.Relative));

            }
            else if (contestState == 10000 || contestState == 10100 || contestState == 10200)
            {
                int quesNum = appData.Contains(Enums.QUESTION_NUMBER_KEY) ? int.Parse(appData[Enums.QUESTION_NUMBER_KEY].ToString()) : 0;
                if (quesNum > 0 && quesNum % 4 == 0)
                {
                }
                else
                {
                    //go to infoPage
                    NavigationService.Navigate(new Uri(string.Format("/View/ContestInfoPage.xaml?contestkey={0}", contestKey), UriKind.Relative));
                }
            }
        }

        private void btSearch_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            soundeffect = SoundPlayer.PlaySound(SoundPlayer.chooseAnswer);
            string filter = txtSearch.Text;
            //long id = 0;
            //if (!long.TryParse(filter, out id))
            //{
            //    id = 0;
            //}
            //if (id > 0)
            //{
            //    //search follow userId
            //    WebClient searchId = new WebClient();
            //    searchId.DownloadStringCompleted += new DownloadStringCompletedEventHandler(searchId_downloadstringcompleted);
            //    searchId.DownloadStringAsync(new Uri(string.Format(Enums.GET_USER_URL, id)));
            //}
            //else
            //{
            //search follow name
            WebClient searchName = new WebClient();
            searchName.DownloadStringCompleted += new DownloadStringCompletedEventHandler(searchName_downloadstringcompleted);
            searchName.DownloadStringAsync(new Uri(string.Format(Enums.SEARCH_BY_NAME, Uri.EscapeDataString(Function.ReplaceUrlString(filter)))));
            //}
        }

        private void searchId_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                var jsonArray = JArray.Parse(e.Result);
                List<RowData> users = getArrayData(jsonArray, top);
                this.TopUsers.ItemsSource = users;
            }
            catch (WebException ex)
            {
            }
        }

        private void searchName_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                var jsonArray = JArray.Parse(e.Result);
                List<RowData> users = getArrayData(jsonArray, top);
                this.TopUsers.ItemsSource = users;
            }
            catch (WebException ex)
            { }
        }

        private void btBack_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            soundeffect = SoundPlayer.PlaySound(SoundPlayer.chooseAnswer);
            NavigationService.Navigate(new Uri("/View/MenuPage.xaml", UriKind.Relative));
        }

        private void txtSearch_GotFocus(object sender, RoutedEventArgs e)
        {
            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri("/Assets/txtIdShare.png", UriKind.Relative));
            (sender as TextBox).Background = brush;
        }

        private void btDayTop_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (isLock == true)
            {
                return;
            }
            isLock = true;
            DayTopHomeUsers(top);
            ActiveButton("day");
        }

        private void btTotalTop_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (isLock == true)
            {
                return;
            }
            isLock = true;
            TotalTopHomeUsers(top);
            ActiveButton("total");
        }

        private void ActiveButton(string active)
        {
            var DayTopNormal = new Image
            {
                Source = new BitmapImage(new Uri("/Assets/btDay50UnActive.png", UriKind.Relative))
            };
            var TotalTopNormal = new Image
            {
                Source = new BitmapImage(new Uri("/Assets/btTotal50UnActive.png", UriKind.Relative))
            };
            btDayTop.Content = DayTopNormal;
            btTotalTop.Content = TotalTopNormal;
            if (active == "day")
            {
                var imageBrush = new Image
                {
                    Source = new BitmapImage(new Uri("/Assets/btDayTop50.png", UriKind.Relative))
                };
                btDayTop.Content = imageBrush;
            }
            else if (active == "total")
            {
                var imageBrush = new Image
                {
                    Source = new BitmapImage(new Uri("/Assets/btTotalTop50.png", UriKind.Relative))
                };
                btTotalTop.Content = imageBrush;
            }
        }

    }
}