﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace lasta_windowsphone.View
{
    public partial class MenuPage : PhoneApplicationPage
    {
        private SoundEffectInstance soundeffect;
        public MenuPage()
        {
            InitializeComponent();

#if STUDIO_VERSION
            btBilODeg.IsEnabled = false;
            btSharepoint.IsEnabled = false;
            btShare.IsEnabled = false;
            btUserInfo.IsEnabled = false;
#endif

        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            GoogleAnalytics.EasyTracker.GetTracker().SendView("MenuPage");
            string isload = "";
            //version app
            string currentVersion = Function.GetVersionNumber();
            txtVersion.Text = currentVersion;

            if (NavigationContext.QueryString.ContainsKey("firstload"))
            {
                NavigationContext.QueryString.TryGetValue("firstload", out isload);
            }
            //for set load music only one time
            if (isload == "true")
            {
            }
            SoundPlayer.PlayMediaBackground("menu", SoundPlayer.opening);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            MediaPlayer.Stop();
            if (soundeffect != null)
            {
                //soundeffect.Stop();
                //soundeffect.Dispose();
            }
        }

        private void btStart_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            soundeffect = SoundPlayer.PlaySound(SoundPlayer.chooseAnswer);
            NavigationService.Navigate(new Uri("/View/QRcodePage.xaml", UriKind.Relative));
        }

        private void btIndex_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            soundeffect = SoundPlayer.PlaySound(SoundPlayer.chooseAnswer);
            NavigationService.Navigate(new Uri("/View/SummaryPage.xaml?search=true", UriKind.Relative));
        }

        private void btUserInfo_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            soundeffect = SoundPlayer.PlaySound(SoundPlayer.chooseAnswer);
            NavigationService.Navigate(new Uri("/View/AccountPage.xaml", UriKind.Relative));
        }

        private void btTerm_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            soundeffect = SoundPlayer.PlaySound(SoundPlayer.chooseAnswer);
            NavigationService.Navigate(new Uri("/View/TermPage.xaml", UriKind.Relative));
        }

        private void btBilODeg_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            soundeffect = SoundPlayer.PlaySound(SoundPlayer.chooseAnswer);
            NavigationService.Navigate(new Uri("/View/BillOPage.xaml", UriKind.Relative));
        }

        private void btShare_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            soundeffect = SoundPlayer.PlaySound(SoundPlayer.chooseAnswer);
            NavigationService.Navigate(new Uri("/View/SharePage.xaml", UriKind.Relative));
        }

        private void btSharepoint_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            soundeffect = SoundPlayer.PlaySound(SoundPlayer.chooseAnswer);
            NavigationService.Navigate(new Uri("/View/SharePointPage.xaml", UriKind.Relative));
        }

    }
}