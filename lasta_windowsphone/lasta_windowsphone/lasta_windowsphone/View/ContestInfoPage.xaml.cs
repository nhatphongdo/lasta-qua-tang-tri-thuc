﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json.Linq;
using System.Windows.Threading;
using System.IO.IsolatedStorage;
using Microsoft.Xna.Framework.Media;

namespace lasta_windowsphone.View
{
    public partial class ContestInfoPage : PhoneApplicationPage
    {
        private DispatcherTimer Timer, slideImage;
        private bool isFailed;
        private bool isLoading;
        private long contestState;
        private string contestKey;
        private int imgIndex;
        private int totalPrize;
        IsolatedStorageSettings appData = IsolatedStorageSettings.ApplicationSettings;
        private int countTimeout;
        private int countDisconnect;
        private WebClient loadstate;

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            base.OnBackKeyPress(e);
            e.Cancel = true;
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            GoogleAnalytics.EasyTracker.GetTracker().SendView("ContestInfoPage");
            base.OnNavigatedTo(e);
            //remove Back Entry
            NavigationService.RemoveBackEntry();
            string firstload = "";
            //get querystring
            if (NavigationContext.QueryString.ContainsKey("contestkey"))
            {
                NavigationContext.QueryString.TryGetValue("contestkey", out contestKey);
            }
            if (NavigationContext.QueryString.ContainsKey("firstload"))
            {
                NavigationContext.QueryString.TryGetValue("firstload", out firstload);
            }
#if STUDIO_VERSION
            if (firstload == "true")
            {
                MessageBox.Show("Chương trình đã kết nối với gameshow thành công. Vui lòng chờ và theo dõi chương trình diễn ra trên truyền hình.", Enums.MessageBoxTitle, MessageBoxButton.OK);
            }
#else
            if (firstload == "true")
            {
                Function.GetGreenMessage();
            }
#endif

            isFailed = false;
            isLoading = false;
            imgIndex = 0;
            countTimeout = 0;
            countDisconnect = 0;
            pivotSlideImage.SelectedIndex = imgIndex;
            //remove image minishow Prize
            DateTime now = DateTime.Now;
            DateTime endMiniShow = new DateTime(2014, 5, 20, 0, 0, 0);
            if (now >= endMiniShow)
            {
                pivotSlideImage.Items.RemoveAt(1);
            }
            totalPrize = this.pivotSlideImage.Items.Count;

            //Refresh in 2s
            Timer = new DispatcherTimer();
            Timer.Interval = TimeSpan.FromMilliseconds(2000);
            Timer.Tick += new EventHandler(update_Tick);
            Timer.Start();

            slideImage = new DispatcherTimer();
            slideImage.Interval = TimeSpan.FromSeconds(5);
            slideImage.Tick += new EventHandler(update_slideImage);
            slideImage.Start();
            SoundPlayer.PlayMediaBackground("Contestinfo", SoundPlayer.background);

        }

        private void update_slideImage(object sender, EventArgs e)
        {
            imgIndex = pivotSlideImage.SelectedIndex;
            imgIndex++;
            if (imgIndex >= totalPrize)
            {
                imgIndex = 0;
            }
            pivotSlideImage.SelectedIndex = imgIndex;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            this.Timer.Stop();
            this.slideImage.Stop();
            MediaPlayer.Stop();
        }

        public ContestInfoPage()
        {
            InitializeComponent();
        }

        private void update_Tick(object sender, EventArgs e)
        {
            countTimeout += 1000;
            if (isLoading == true && countTimeout > Enums.timeout)
            {
                loadstate.CancelAsync();
                isLoading = false;
            }
            if (isLoading == false)
            {
                countTimeout = 0;
                loadState();
            }

        }

        private void loadState()
        {
            if (!string.IsNullOrEmpty(contestKey))
            {
                isLoading = true;
                loadstate = new WebClient();
                loadstate.DownloadStringCompleted += new DownloadStringCompletedEventHandler(loadstate_downloadstringcompleted);
                loadstate.DownloadStringAsync(new Uri(string.Format(Enums.STATE_URL, contestKey), UriKind.RelativeOrAbsolute));
            }
        }

        private void loadstate_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                JObject json = JObject.Parse(e.Result);
                contestState = long.Parse(json["Status"].ToString());
                processState();
                isLoading = false;
            }
            catch (WebException ex)
            {
                countDisconnect += 1;
                if (isFailed == false && countDisconnect >= Enums.totalDisconnect)
                {
                    MessageBoxResult m = MessageBox.Show("Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công.", Enums.ApplicationTitle, MessageBoxButton.OK);
                    if (m == MessageBoxResult.OK)
                    {
                        // reconnetion code in here
                    }
                    isFailed = true;
                }
                isLoading = false;
            }
        }

        private void processState()
        {
            if (contestState >= 10 && contestState < 10000)
            {
                if (contestState % 10 == 3)
                {
                    // Redirect to ResultPage
                    NavigationService.Navigate(new Uri(string.Format("/View/ResultPage.xaml?contestkey={0}", Uri.EscapeUriString(contestKey)), UriKind.Relative));
                }
                else if (contestState % 10 == 2)
                {
                    if (!appData.Contains(Enums.QUESTION_NUMBER_KEY))
                    {
                        appData.Add(Enums.QUESTION_NUMBER_KEY, contestState / 10);
                    }
                    else
                    {
                        appData[Enums.QUESTION_NUMBER_KEY] = contestState / 10;
                    }
                    // Redirect to PlayPage
                    NavigationService.Navigate(new Uri(string.Format("/View/PlayPage.xaml?contestkey={0}&questnumb={1}", Uri.EscapeUriString(contestKey), Uri.EscapeUriString((contestState / 10).ToString())), UriKind.Relative));
                }
                else
                {
                    if (!appData.Contains(Enums.QUESTION_NUMBER_KEY))
                    {
                        appData.Add(Enums.QUESTION_NUMBER_KEY, contestState / 10);
                    }
                    else
                    {
                        appData[Enums.QUESTION_NUMBER_KEY] = contestState / 10;
                    }
                    // Redirect to PlayInfoPage
                    NavigationService.Navigate(new Uri(string.Format("/View/PlayInfoPage.xaml?questnumb={0}&contestkey={1}&conteststate={2}", Uri.EscapeUriString((contestState / 10).ToString()), Uri.EscapeUriString(contestKey), Uri.EscapeUriString(contestState.ToString())), UriKind.Relative));
                }
            }
            else if (contestState == 10400)
            {
                var number_question = ((appData.Contains(Enums.NUMBER_QUESTIONS_KEY) ? int.Parse(appData[Enums.NUMBER_QUESTIONS_KEY].ToString()) + 1 : 0)).ToString();

                NavigationService.Navigate(new Uri(string.Format("/View/PlayInfoPage.xaml?contestkey={0}&questnumb={1}&conteststate={2}", contestKey, number_question, contestState.ToString()), UriKind.Relative));
            }
            else if (contestState == 10401)
            {
                // Redirect to PlayPage
                var number_question = ((appData.Contains(Enums.NUMBER_QUESTIONS_KEY) ? int.Parse(appData[Enums.NUMBER_QUESTIONS_KEY].ToString()) + 1 : 0)).ToString();

                NavigationService.Navigate(new Uri(string.Format("/View/PlayPage.xaml?contestkey={0}&questnumb={1}&conteststate={2}", contestKey, number_question, contestState.ToString()), UriKind.Relative));
            }
            else if (contestState == 10402)
            {
                // Redirect to ResultPage
                NavigationService.Navigate(new Uri(string.Format("/View/ResultPage.xaml?contestkey={0}", Uri.EscapeUriString(contestKey)), UriKind.Relative));
            }
            else if (contestState == 10300)
            {
                // Redirect to SummaryPage
                NavigationService.Navigate(new Uri(string.Format("/View/SummaryPage.xaml?contestkey={0}", Uri.EscapeUriString(contestKey)), UriKind.RelativeOrAbsolute));
            }
            else if (contestState == 10000 || contestState == 10100 || contestState == 10200)
            {
                int quesNum = appData.Contains(Enums.QUESTION_NUMBER_KEY) ? int.Parse(appData[Enums.QUESTION_NUMBER_KEY].ToString()) : 0;
                if (quesNum > 0 && quesNum % 4 == 0)
                {
                }
                else
                {
                    //go to infoPage
                    NavigationService.Navigate(new Uri(string.Format("/View/ContestInfoPage.xaml?contestkey={0}", contestKey), UriKind.Relative));
                }
            }
        }
    }
}