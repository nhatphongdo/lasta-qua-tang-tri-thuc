﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using System.IO.IsolatedStorage;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace lasta_windowsphone.View
{
    public partial class SharePage : PhoneApplicationPage
    {
        private SoundEffectInstance soundeffect;

        public SharePage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //To Page ~ PageLoad
            base.OnNavigatedTo(e);
            GoogleAnalytics.EasyTracker.GetTracker().SendView("SharePage");
            SoundPlayer.PlayMediaBackground("ExchangeScore", SoundPlayer.opening);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            //Out Page
            base.OnNavigatedFrom(e);
            MediaPlayer.Stop();
            //if (soundeffect != null)
            //{
            //    soundeffect.Stop();
            //    if (soundeffect.IsDisposed == false)
            //    {
            //        soundeffect.Dispose();
            //    }
            //}
        }

        private void Button_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            //todo: get total score
            IsolatedStorageSettings appData = IsolatedStorageSettings.ApplicationSettings;
            long score = 0;
            if (appData.Contains(Enums.USER_DAY_SCORE_KEY))
            {
                if (!long.TryParse(appData[Enums.USER_DAY_SCORE_KEY].ToString(), out score))
                {
                    score = 0;
                }
            }
            ShareLinkTask shareLinkTask = new ShareLinkTask();
            shareLinkTask.LinkUri = new Uri("http://cunglatyphu.com/", UriKind.Absolute);
            shareLinkTask.Message = string.Format("Mình vừa giành được {0} điểm trong GameShow truyền hình trực tiếp \"Cùng Là Tỷ Phú\" trên kênh truyền hình Let's Việt. Các bạn và mình chúng ta cùng nhau tham gia nhé!", score);
            shareLinkTask.Show();
            soundeffect = SoundPlayer.PlaySound(SoundPlayer.chooseAnswer);
        }

        private void btBack_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            soundeffect = SoundPlayer.PlaySound(SoundPlayer.chooseAnswer);
            NavigationService.Navigate(new Uri("/View/MenuPage.xaml", UriKind.Relative));
        }

        private void btInvite_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            ShareLinkTask shareLinkTask = new ShareLinkTask();
            shareLinkTask.LinkUri = new Uri("http://cunglatyphu.com/", UriKind.Absolute);
            shareLinkTask.Message = "\"Cùng Là Tỷ Phú\" sau 3 tháng, tham gia ngay !";
            shareLinkTask.Show();
            soundeffect = SoundPlayer.PlaySound(SoundPlayer.chooseAnswer);
        }


    }
}