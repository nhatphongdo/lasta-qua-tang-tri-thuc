﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json.Linq;
using System.IO.IsolatedStorage;
using System.Windows.Threading;
using System.ComponentModel;
using System.Threading;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace lasta_windowsphone.View
{
    public partial class SharePointPage : PhoneApplicationPage
    {
        IsolatedStorageSettings appData = IsolatedStorageSettings.ApplicationSettings;
        private string deviceId, ReceiverCode;
        private long userId, receiverId;
        private int score;
        private SoundEffectInstance soundeffect;

        public SharePointPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            GoogleAnalytics.EasyTracker.GetTracker().SendView("ShareScoreToFriendPage");
            SoundPlayer.PlayMediaBackground("ShareScore", SoundPlayer.opening);
            //To Page ~ PageLoad
            if (appData.Contains(Enums.USERID_KEY) && appData.Contains(Enums.DEVICEID_KEY))
            {
                userId = long.Parse(appData[Enums.USERID_KEY].ToString());
                deviceId = appData[Enums.DEVICEID_KEY].ToString();
            }

            //Load Term 
            webTerm.NavigateToString(@"
<html>
<head>
<style>
body{font-family:Arial;font-size:10px;}
p, h1, h2, h3, h4, h5, h6 {
	margin: 0;
	padding: 0;
}
.bold {
	font-weight: bold;
}
.red {
	color: red;
}
.italic {
	font-style: italic;
}
.underline {
	text-decoration: underline;
}
.odo {
	text-align: left;
}
</style>
</head>
<body style='margin:0 auto;color:#000; line-height:20px;'>
<span class='red bold'>Tặng điểm </span><br/>
Người chơi được phép tặng điểm cho nhau (có thể tặng hết số điểm đang có)<br/>
•	Chỉ được tặng 01 lần. Thời gian tặng điểm: bắt đầu lúc 20:30 ngày THTT chương trình 23 và kết thúc vào lúc 18:00 ngày THTT chương trình 24.<br/>
•	Người chơi chỉ được nhận điểm của tối đa 5 người chơi khác.<br/>
•	Người chơi nếu được nhận điểm từ người chơi khác sẽ không được quyền tặng điểm.<br/>
<span class='red bold italic underline'>Cách thức:</span><br/>
•	Vào Menu trên ứng dụng Cùng Là Tỷ Phú, chọn Tặng điểm.<br/>
•	Nhập ID và mã số của người nhận. <br/>
•	Nhập số điểm tặng.<br/>
•	Nhấn tặng điểm. Hoàn thành.<br/>
</body>
</html>

");
            base.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            //Out Page
            base.OnNavigatedFrom(e);
        }

        private void LoadData()
        {
            //todo: get info of users has been send point
            WebClient getUsers = new WebClient();
            getUsers.DownloadStringCompleted += new DownloadStringCompletedEventHandler(getUsers_downloadstringcompleted);
            getUsers.DownloadStringAsync(new Uri(string.Format(Enums.TRANSFER_SCORE_URL, userId, Uri.EscapeDataString(deviceId), receiverId, score)));
        }

        private void getUsers_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                JObject obj = JObject.Parse(e.Result);
                //todo: process result
            }
            catch (WebException ex)
            {
                MessageBoxResult m = MessageBox.Show("Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại.", Enums.ApplicationTitle, MessageBoxButton.OK);
                if (m == MessageBoxResult.OK)
                {
                    // reconnetion
                    LoadData();
                }
            }
        }

        private void btShareScore_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            //validate 
            soundeffect = SoundPlayer.PlaySound(SoundPlayer.chooseAnswer);
            if (!isNumber(txtScore.Text))
            {
                MessageBoxResult m = MessageBox.Show("Bạn nhập sai định dạng, điểm phải là một số.", Enums.ApplicationTitle, MessageBoxButton.OK);
                return;
            }
            if (!isNumber(txtReceiverId.Text))
            {
                MessageBoxResult m = MessageBox.Show("Bạn nhập sai ID", Enums.ApplicationTitle, MessageBoxButton.OK);
                return;
            }
            if (string.IsNullOrEmpty(txtReceiverCode.Text))
            {
                MessageBoxResult m = MessageBox.Show("Bạn chưa nhập mã số người nhận điểm.", Enums.ApplicationTitle, MessageBoxButton.OK);
                return;
            }

            score = int.Parse(txtScore.Text);
            receiverId = long.Parse(txtReceiverId.Text);
            ReceiverCode = txtReceiverCode.Text;
            WebClient shareScore = new WebClient();
            shareScore.DownloadStringCompleted += new DownloadStringCompletedEventHandler(shareScore_downloadstringcompleted);
            shareScore.DownloadStringAsync(new Uri(string.Format(Enums.TRANSFER_SCORE_URL, userId, Uri.EscapeDataString(Function.ReplaceUrlString(deviceId)), receiverId, Uri.EscapeDataString(ReceiverCode), score)));
        }

        private void shareScore_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                JObject obj = JObject.Parse(e.Result);
                int error = (int)obj["Error"];
                switch (error)
                {
                    case -1:
                        MessageBoxResult m1 = MessageBox.Show("Chưa đến thời gian cho phép tặng điểm. Thời gian tặng điểm diễn ra từ đầu chương trình 23 đến giữa chương trình 24.", Enums.ApplicationTitle, MessageBoxButton.OK);
                        break;
                    case 0:
                        //reload new score of user
                        if (appData.Contains(Enums.USER_SCORE_KEY))
                        {
                            long localScore = Convert.ToInt64(appData[Enums.USER_SCORE_KEY]);
                            appData[Enums.USER_SCORE_KEY] = localScore - score;
                            appData.Save();
                        }
                        MessageBoxResult m2 = MessageBox.Show("Bạn đã tặng điểm cho người nhận thành công.", Enums.ApplicationTitle, MessageBoxButton.OK);
                        if (m2 == MessageBoxResult.OK)
                        {
                            //refresh this page
                            NavigationService.Navigate(NavigationService.Source);
                        }
                        break;
                    case 1:
                        MessageBoxResult m3 = MessageBox.Show("Bạn đã tặng điểm cho người khác. Bạn không thể thực hiện tặng điểm quá 1 lần.", Enums.ApplicationTitle, MessageBoxButton.OK);
                        break;
                    case 2:
                        MessageBoxResult m4 = MessageBox.Show("Người nhận điểm đã nhận đủ tối đa 5 lần tặng.", Enums.ApplicationTitle, MessageBoxButton.OK);
                        break;
                    case 3:
                        MessageBoxResult m5 = MessageBox.Show("Chương trình xảy ra lỗi trong quá trình xử lý. Vui lòng thông báo tới BTC để được hướng dẫn xử lý và hỗ trợ.", Enums.ApplicationTitle, MessageBoxButton.OK);
                        break;
                    case 4:
                        MessageBoxResult m6 = MessageBox.Show("Hiện tại bạn không có đủ điểm để tặng cho người khác.", Enums.ApplicationTitle, MessageBoxButton.OK);
                        break;
                    case 5:
                        MessageBoxResult m7 = MessageBox.Show("Bạn không thể tặng điểm được vì đã nhận điểm từ người khác.", Enums.ApplicationTitle, MessageBoxButton.OK);
                        break;
                }

            }
            catch (WebException ex)
            {
                if (((HttpWebResponse)ex.Response).StatusCode == HttpStatusCode.NotFound)
                {
                    MessageBoxResult m8 = MessageBox.Show(string.Format("Không tìm thấy người chơi với mã số ID {0} và mã số nhận điểm {1}.", receiverId.ToString(), ReceiverCode), Enums.ApplicationTitle, MessageBoxButton.OK);
                }
                else
                {
                    MessageBoxResult m = MessageBox.Show("Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại.", Enums.ApplicationTitle, MessageBoxButton.OK);
                    if (m == MessageBoxResult.OK)
                    {
                        // reconnetion
                    }
                }
            }
        }

        private bool isNumber(string str)
        {
            double numb;
            return double.TryParse(str, out numb);
        }

        private void loadUser_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                JObject obj = JObject.Parse(e.Result);
                tbId.Text = obj["ID"].ToString();
                tbName.Text = Convert.ToString(obj["FullName"]);
                tbTotalScore.Text = Convert.ToString(obj["Score"]);
            }
            catch (WebException ex)
            {
                tbId.Text = "";
                tbName.Text = "";
                tbTotalScore.Text = "";
            }
        }

        private void btBack_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            soundeffect = SoundPlayer.PlaySound(SoundPlayer.chooseAnswer);
            NavigationService.Navigate(new Uri("/View/MenuPage.xaml", UriKind.Relative));
        }

        private void txtReceiverId_LostFocus(object sender, RoutedEventArgs e)
        {
            long userId = 0;
            if (long.TryParse(txtReceiverId.Text, out userId))
            {
                WebClient loadUser = new WebClient();
                loadUser.DownloadStringCompleted += new DownloadStringCompletedEventHandler(loadUser_downloadstringcompleted);
                loadUser.DownloadStringAsync(new Uri(string.Format(Enums.GET_USER_URL, userId)));
            }
        }

        private void txtReceiverId_GotFocus(object sender, RoutedEventArgs e)
        {
            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri("/Assets/txtIdShare.png", UriKind.Relative));
            (sender as TextBox).Background = brush;
        }

        private void txtScore_GotFocus(object sender, RoutedEventArgs e)
        {
            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri("/Assets/txtPointShare.png", UriKind.Relative));
            (sender as TextBox).Background = brush;
        }

        private void btTermShareScore_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            soundeffect = SoundPlayer.PlaySound(SoundPlayer.chooseAnswer);
            this.borTermShareScore.Visibility = Visibility.Collapsed;
            (sender as Button).Visibility = Visibility.Collapsed;
            gridShareScore.Visibility = Visibility.Visible;
        }

    }
}