﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using Newtonsoft.Json.Linq;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace lasta_windowsphone.View
{
    public partial class AccountPage : PhoneApplicationPage
    {

        private IsolatedStorageSettings appData = IsolatedStorageSettings.ApplicationSettings;
        private long userId;
        private string deviceId;
        private SoundEffectInstance soundeffect;

        public AccountPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            GoogleAnalytics.EasyTracker.GetTracker().SendView("AccountPage");
            SoundPlayer.PlayMediaBackground("AccountSound", SoundPlayer.opening);
            //To Page ~ PageLoad
            userId = 0;
            deviceId = "";
            if (appData.Contains(Enums.USERID_KEY) && appData.Contains(Enums.DEVICEID_KEY))
            {
                userId = long.Parse(appData[Enums.USERID_KEY].ToString());
                deviceId = appData[Enums.DEVICEID_KEY].ToString();
            }

            loadData(userId);
            base.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            //out Page
            base.OnNavigatedFrom(e);
            MediaPlayer.Stop();
        }

        private void loadData(long userId = 0)
        {
            WebClient loadUserInfo = new WebClient();
            loadUserInfo.DownloadStringCompleted += new DownloadStringCompletedEventHandler(loadData_downloadstringcompleted);
            loadUserInfo.DownloadStringAsync(new Uri(string.Format(Enums.GET_USER_URL, userId)));
        }

        private void loadData_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                JObject obj = JObject.Parse(e.Result);
                txtName.Text = Convert.ToString(obj["Fullname"]);
                txtPhoneNumber.Text = Convert.ToString(obj["PhoneNumber"]);
                txtAddress.Text = Convert.ToString(obj["Address"]);
                txtEmail.Text = Convert.ToString(obj["Email"]);
                bool isOpIn = false;
                if (!bool.TryParse(obj["IsOptIn"].ToString(), out isOpIn))
                {
                    isOpIn = false;
                }
                if (isOpIn == true)
                {
                    toggleSwitch.IsChecked = true;
                }
                else
                {
                    toggleSwitch.IsChecked = false;
                }
            }
            catch (WebException ex)
            {
                MessageBoxResult m = MessageBox.Show("Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại.", Enums.ApplicationTitle, MessageBoxButton.OK);
                if (m == MessageBoxResult.OK)
                {
                    // reconnetion
                    loadData(userId);
                }
            }
        }

        private void btFinish_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            soundeffect = SoundPlayer.PlaySound(SoundPlayer.chooseAnswer);
            //validate info
            if (string.IsNullOrEmpty(txtName.Text))
            {
                MessageBoxResult m = MessageBox.Show("Xin vui lòng nhập tên của bạn để cập nhật thông tin.", Enums.MessageBoxTitle, MessageBoxButton.OK);
                return;
            }
            if (string.IsNullOrEmpty(txtPhoneNumber.Text))
            {
                MessageBoxResult m = MessageBox.Show("Xin vui lòng nhập số điện thoại của bạn để cập nhật thông tin.", Enums.MessageBoxTitle, MessageBoxButton.OK);
                return;
            }
            if (string.IsNullOrEmpty(txtAddress.Text))
            {
                MessageBoxResult m = MessageBox.Show("Xin vui lòng nhập địa chỉ của bạn để cập nhật thông tin.", Enums.MessageBoxTitle, MessageBoxButton.OK);
                return;
            }
            if (string.IsNullOrEmpty(txtEmail.Text))
            {
                MessageBoxResult m = MessageBox.Show("Xin vui lòng nhập email của bạn để cập nhật thông tin.", Enums.MessageBoxTitle, MessageBoxButton.OK);
                return;
            }
            if (!Function.IsValidEmail(txtEmail.Text))
            {
                MessageBoxResult m = MessageBox.Show("Xin vui lòng nhập đúng định dạng email.", Enums.MessageBoxTitle, MessageBoxButton.OK);
                return;
            }
            string fullname = txtName.Text;
            string phone = txtPhoneNumber.Text;
            string email = txtEmail.Text;
            string address = txtAddress.Text;
            int isOpln = toggleSwitch.IsChecked == true ? 1 : 0;

            WebClient updateUserinfo = new WebClient();
            updateUserinfo.DownloadStringCompleted += new DownloadStringCompletedEventHandler(updateUserInfo_downloadstringcompleted);
            updateUserinfo.DownloadStringAsync(new Uri(string.Format(Enums.UPDATE_USER_INFO_URL, Uri.EscapeDataString(Function.ReplaceUrlString(deviceId)), userId, Uri.EscapeDataString(Function.ReplaceUrlString(fullname)), Uri.EscapeDataString(Function.ReplaceUrlString(phone)), Uri.EscapeDataString(Function.ReplaceUrlString(email)), Uri.EscapeDataString(Function.ReplaceUrlString(address)), isOpln)));

        }

        private void updateUserInfo_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                JObject obj = JObject.Parse(e.Result);
                int error = (int)obj["Error"];
                switch (error)
                {
                    case -1:
                        MessageBoxResult m1 = MessageBox.Show("Cập nhật thông tin thất bại. Vui lòng thông báo tới BTC để được hướng dẫn xử lý và hỗ trợ.", Enums.ApplicationTitle, MessageBoxButton.OK);
                        break;
                    case 0:
                        MessageBoxResult m2 = MessageBox.Show("Cập nhật thông tin thành công.", Enums.ApplicationTitle, MessageBoxButton.OK);
                        break;
                }
            }
            catch (WebException ex)
            {
                MessageBoxResult m = MessageBox.Show("Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại.", Enums.ApplicationTitle, MessageBoxButton.OK);
                if (m == MessageBoxResult.OK)
                {
                    // reconnetion
                    //NavigationService.Navigate(new Uri("/View/MenuPage.xaml", UriKind.Relative));
                }
            }
        }

        private void btBack_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            soundeffect = SoundPlayer.PlaySound(SoundPlayer.chooseAnswer);
            NavigationService.Navigate(new Uri("/View/MenuPage.xaml", UriKind.Relative));
        }

        private void txtName_GotFocus(object sender, RoutedEventArgs e)
        {
            BitmapImage image = new BitmapImage(new Uri("/Assets/box_setting_focus.png", UriKind.Relative));
            var brush = new ImageBrush();
            brush.ImageSource = image;
            gridName.Background = brush;
            (sender as TextBox).Background = new SolidColorBrush(Colors.Transparent);
        }

        private void txtName_LostFocus(object sender, RoutedEventArgs e)
        {
            BitmapImage image = new BitmapImage(new Uri("/Assets/box_setting.png", UriKind.Relative));
            var brush = new ImageBrush();
            brush.ImageSource = image;
            gridName.Background = brush;
        }

        private void txtPhoneNumber_GotFocus(object sender, RoutedEventArgs e)
        {
            BitmapImage image = new BitmapImage(new Uri("/Assets/box_setting_focus.png", UriKind.Relative));
            var brush = new ImageBrush();
            brush.ImageSource = image;
            gridPhoneNumber.Background = brush;
            (sender as TextBox).Background = new SolidColorBrush(Colors.Transparent);
        }

        private void txtPhoneNumber_LostFocus(object sender, RoutedEventArgs e)
        {
            BitmapImage image = new BitmapImage(new Uri("/Assets/box_setting.png", UriKind.Relative));
            var brush = new ImageBrush();
            brush.ImageSource = image;
            gridPhoneNumber.Background = brush;
        }

        private void txtAddress_GotFocus(object sender, RoutedEventArgs e)
        {
            BitmapImage image = new BitmapImage(new Uri("/Assets/box_setting_focus.png", UriKind.Relative));
            var brush = new ImageBrush();
            brush.ImageSource = image;
            gridAddress.Background = brush;
            (sender as TextBox).Background = new SolidColorBrush(Colors.Transparent);
        }

        private void txtAddress_LostFocus(object sender, RoutedEventArgs e)
        {
            BitmapImage image = new BitmapImage(new Uri("/Assets/box_setting.png", UriKind.Relative));
            var brush = new ImageBrush();
            brush.ImageSource = image;
            gridAddress.Background = brush;
        }

        private void txtEmail_GotFocus(object sender, RoutedEventArgs e)
        {
            BitmapImage image = new BitmapImage(new Uri("/Assets/box_setting_focus.png", UriKind.Relative));
            var brush = new ImageBrush();
            brush.ImageSource = image;
            gridEmail.Background = brush;
            (sender as TextBox).Background = new SolidColorBrush(Colors.Transparent);
        }

        private void txtEmail_LostFocus(object sender, RoutedEventArgs e)
        {
            BitmapImage image = new BitmapImage(new Uri("/Assets/box_setting.png", UriKind.Relative));
            var brush = new ImageBrush();
            brush.ImageSource = image;
            gridEmail.Background = brush;
        }

    }
}