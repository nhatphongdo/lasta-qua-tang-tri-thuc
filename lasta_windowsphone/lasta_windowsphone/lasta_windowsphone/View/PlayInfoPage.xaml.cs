﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Threading;
using Newtonsoft.Json.Linq;
using System.Windows.Media.Imaging;
using System.IO.IsolatedStorage;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace lasta_windowsphone.View
{
    public partial class PlayInfoPage : PhoneApplicationPage
    {
        private DispatcherTimer Timer;
        private bool isLoading;
        private bool isFailed;
        private bool isQuestionLoaded;
        private bool isLoadTopicSound;
        private bool isLoadScoreSound;
        public long questionScore;
        public int questionNumber = 0;
        public long contestState;
        public string contestKey = "";
        public string topic;
        public IsolatedStorageSettings appData = IsolatedStorageSettings.ApplicationSettings;
        private SoundEffectInstance soundeffect;
        private int countTimeout;
        private int countDisconnect;
        private WebClient loadquestion;
        private WebClient loadstate;

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            base.OnBackKeyPress(e);
            e.Cancel = true;
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            //remove Back Entry
            NavigationService.RemoveBackEntry();
            GoogleAnalytics.EasyTracker.GetTracker().SendView("PlayInfoPage");
            string str_questnumb = "", str_conteststate = "";
            //get querystring
            if (NavigationContext.QueryString.ContainsKey("contestkey"))
            {
                NavigationContext.QueryString.TryGetValue("contestkey", out contestKey);
            }
            if (NavigationContext.QueryString.ContainsKey("questnumb"))
            {
                NavigationContext.QueryString.TryGetValue("questnumb", out str_questnumb);
            }
            if (NavigationContext.QueryString.ContainsKey("conteststate"))
            {
                NavigationContext.QueryString.TryGetValue("conteststate", out str_conteststate);
            }
            if (!int.TryParse(str_questnumb, out questionNumber))
            {
                questionNumber = 0;
            }
            if (!long.TryParse(str_conteststate, out contestState))
            {
                contestState = 0;
            }


            isQuestionLoaded = false;
            loadQuestion(questionNumber);

            isFailed = false;
            isLoading = false;
            isLoadTopicSound = false;
            isLoadScoreSound = false;
            countTimeout = 0;
            countDisconnect = 0;
            //hide all label 
            imgTopic.Visibility = Visibility.Collapsed;
            txtScore.Visibility = Visibility.Collapsed;
            gridTitle.Visibility = Visibility.Collapsed;

            //Refresh in 2s
            Timer = new DispatcherTimer();
            Timer.Interval = TimeSpan.FromMilliseconds(2000);
            Timer.Tick += new EventHandler(update_Tick);
            Timer.Start();
            SoundPlayer.PlayMediaBackground("playInfo", SoundPlayer.background);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            this.Timer.Stop();
            MediaPlayer.Stop();
            if (soundeffect != null)
            {
                soundeffect.Stop();
                if (soundeffect.IsDisposed == false)
                {
                    soundeffect.Dispose();
                }
            }
        }

        public PlayInfoPage()
        {
            InitializeComponent();
        }

        private void loadQuestion(int number)
        {
            if (number > 0 && !string.IsNullOrEmpty(contestKey))
            {
                isLoading = true;
                loadquestion = new WebClient();
                loadquestion.DownloadStringCompleted += new DownloadStringCompletedEventHandler(loadquestion_downloadstringcompleted);
                loadquestion.DownloadStringAsync(new Uri(string.Format(Enums.QUESTION_URL, contestKey, number), UriKind.RelativeOrAbsolute));
            }
        }

        private void loadquestion_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                JObject json = JObject.Parse(e.Result);
                topic = json["Topic"].ToString();
                questionScore = long.Parse(json["Score"].ToString());
                processState();
                isLoading = false;
                isQuestionLoaded = true;
            }
            catch (WebException ex)
            {
                countDisconnect += 1;
                if (isFailed == false && countDisconnect >= Enums.totalDisconnect)
                {
                    MessageBoxResult m = MessageBox.Show("Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công.", Enums.ApplicationTitle, MessageBoxButton.OK);
                    if (m == MessageBoxResult.OK)
                    {
                        // reconnetion code in here
                    }
                    isFailed = true;
                }
                isLoading = false;
            }
        }

        private void loadState()
        {
            if (!string.IsNullOrEmpty(contestKey))
            {
                isLoading = true;

                loadstate = new WebClient();
                loadstate.DownloadStringCompleted += new DownloadStringCompletedEventHandler(loadstate_downloadstringcompleted);
                loadstate.DownloadStringAsync(new Uri(string.Format(Enums.STATE_URL, contestKey), UriKind.RelativeOrAbsolute));
            }
        }

        private void loadstate_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                JObject json = JObject.Parse(e.Result);
                contestState = long.Parse(json["Status"].ToString());
                isLoading = false;
                if (contestState % 10 == 1)
                {
                    loadQuestion(questionNumber);
                }
                else
                {
                    processState();
                }
            }
            catch (WebException ex)
            {
                countDisconnect += 1;
                if (isFailed == false && countDisconnect >= Enums.totalDisconnect)
                {
                    MessageBoxResult m = MessageBox.Show("Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công.", Enums.ApplicationTitle, MessageBoxButton.OK);
                    if (m == MessageBoxResult.OK)
                    {
                        // reconnetion code in here
                    }
                    isFailed = true;
                }
                isLoading = false;
            }
        }

        private void update_Tick(object sender, EventArgs e)
        {
            countTimeout += 1000;
            if (isLoading == true && countTimeout > Enums.timeout)
            {
                if (isQuestionLoaded == true)
                {
                    loadquestion.CancelAsync();
                    isQuestionLoaded = false;
                }
                else
                {
                    loadstate.CancelAsync();
                }
                isLoading = false;
            }

            if (isLoading == false)
            {
                countTimeout = 0;
                if (isQuestionLoaded == false)
                {
                    loadQuestion(questionNumber);
                }
                else
                {
                    loadState();
                }
            }
        }

        private void processState()
        {
            if (contestState >= 10 && contestState < 10000)
            {
                //Question
                if (contestState % 10 == 2)
                {
                    if (!appData.Contains(Enums.QUESTION_NUMBER_KEY))
                    {
                        appData.Add(Enums.QUESTION_NUMBER_KEY, contestState / 10);
                    }
                    else
                    {
                        appData[Enums.QUESTION_NUMBER_KEY] = contestState / 10;
                    }
                    //Redirect to PlayPage
                    NavigationService.Navigate(new Uri(string.Format("/View/PlayPage.xaml?contestkey={0}&questnumb={1}", Uri.EscapeUriString(contestKey), Uri.EscapeUriString((contestState / 10).ToString())), UriKind.Relative));
                }
                else if (contestState % 10 == 3)
                {
                    //Redirect to ResultPage
                    NavigationService.Navigate(new Uri(string.Format("/View/ResultPage.xaml?contestkey={0}", Uri.EscapeUriString(contestKey)), UriKind.Relative));
                }
                else if (contestState % 10 == 0)
                {
                    imgTopic.Visibility = Visibility.Visible;
                    gridTitle.Visibility = Visibility.Visible;
                    txtScore.Visibility = Visibility.Collapsed;
                    //Play Sound for TITLE 
                    if (isLoadTopicSound == false)
                    {
                        soundeffect = SoundPlayer.PlaySound(SoundPlayer.switchScreen);
                        isLoadTopicSound = true;
                    }

                    txtTitle.Text = string.Format("CHỦ ĐỀ CÂU {0}", questionNumber);
                    if (topic.Equals("Giải trí"))
                    {
                        BitmapImage bi1 = new BitmapImage(new Uri("/Assets/icon-giaitri.png", UriKind.Relative));
                        imgTopic.Source = bi1;
                    }
                    else if (topic.Equals("Địa lý"))
                    {
                        BitmapImage bi2 = new BitmapImage(new Uri("/Assets/icon-dialy.png", UriKind.Relative));
                        imgTopic.Source = bi2;
                    }
                    else if (topic.Equals("Sức khỏe"))
                    {
                        BitmapImage bi3 = new BitmapImage(new Uri("/Assets/icon-suckhoe.png", UriKind.Relative));
                        imgTopic.Source = bi3;
                    }
                    else if (topic.Equals("Lịch sử"))
                    {
                        BitmapImage bi4 = new BitmapImage(new Uri("/Assets/icon-lichsu.png", UriKind.Relative));
                        imgTopic.Source = bi4;
                    }
                    else if (topic.Equals("Thể thao"))
                    {
                        BitmapImage bi5 = new BitmapImage(new Uri("/Assets/icon-thethao.png", UriKind.Relative));
                        imgTopic.Source = bi5;
                    }
                    else if (topic.Equals("Văn hóa - Xã hội"))
                    {
                        BitmapImage bi6 = new BitmapImage(new Uri("/Assets/icon-vanhoaxahoi.png", UriKind.Relative));
                        imgTopic.Source = bi6;
                    }
                    else if (topic.Equals("Văn học"))
                    {
                        BitmapImage bi7 = new BitmapImage(new Uri("/Assets/icon-vanhoc.png", UriKind.Relative));
                        imgTopic.Source = bi7;
                    }
                    else if (topic.Equals("Khoa học - Công nghệ"))
                    {
                        BitmapImage bi8 = new BitmapImage(new Uri("/Assets/icon-khoahoccongnghe.png", UriKind.Relative));
                        imgTopic.Source = bi8;
                    }
                }
                else
                {
                    imgTopic.Visibility = Visibility.Collapsed;
                    //Play Sound for score
                    if (isLoadScoreSound == false)
                    {
                        soundeffect = SoundPlayer.PlaySound(SoundPlayer.switchScreen);
                        isLoadScoreSound = true;
                    }
                    txtTitle.Text = String.Format("SỐ ĐIỂM CÂU {0}", questionNumber);
                    txtScore.Text = questionScore.ToString();
                    txtScore.Visibility = Visibility.Visible;
                    gridTitle.Visibility = Visibility.Visible;
                }
            }
            else if (contestState == 10400)
            {
                txtTitle.Text = String.Format("SỐ ĐIỂM CÂU {0}", questionNumber);
                txtScore.Text = questionScore.ToString();
                txtScore.Visibility = Visibility.Visible;
                gridTitle.Visibility = Visibility.Visible;
                imgTopic.Visibility = Visibility.Collapsed;
            }
            else if (contestState == 10401)
            {
                //Go to play page
                var number_question = ((appData.Contains(Enums.NUMBER_QUESTIONS_KEY) ? int.Parse(appData[Enums.NUMBER_QUESTIONS_KEY].ToString()) + 1 : 0)).ToString();

                NavigationService.Navigate(new Uri(string.Format("/View/PlayPage.xaml?contestkey={0}&questnumb={1}&conteststate={2}", contestKey, number_question, contestState.ToString()), UriKind.Relative));

            }
            else if (contestState == 10402)
            { //Go to result page
                NavigationService.Navigate(new Uri(string.Format("/View/ResultPage.xaml?contestkey={0}", Uri.EscapeUriString(contestKey.ToString())), UriKind.Relative));
            }
            else if (contestState == 10000 || contestState == 10100 || contestState == 10200)
            {
                int quesNum = appData.Contains(Enums.QUESTION_NUMBER_KEY) ? int.Parse(appData[Enums.QUESTION_NUMBER_KEY].ToString()) : 0;
                if (quesNum > 0 && quesNum % 4 == 0)
                {
                    //Go to summary page
                    NavigationService.Navigate(new Uri(string.Format("/View/SummaryPage.xaml?contestkey={0}", Uri.EscapeUriString(contestKey)), UriKind.Relative));
                }
                else
                {
                    //go to infoPage
                    NavigationService.Navigate(new Uri(string.Format("/View/ContestInfoPage.xaml?contestkey={0}", Uri.EscapeUriString(contestKey)), UriKind.Relative));
                }
            }
            else if (contestState == 10300)
            {
                //Redirect to SummaryPage
                NavigationService.Navigate(new Uri(string.Format("/View/SummaryPage.xaml?contestkey={0}", Uri.EscapeUriString(contestKey)), UriKind.Relative));
            }
        }
    }
}