﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json.Linq;
using System.IO.IsolatedStorage;
using System.Windows.Threading;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;


namespace lasta_windowsphone.View
{
    public partial class PlayPage : PhoneApplicationPage
    {
        public int questionNumber = 0;
        public int numberOfQuestion = 0;
        public int answer = 0;
        public int answerTime = 0;
        private int countSubmitAnswer;
        private double progressbarWidth = 0;
        public long remainTime = 0;
        public double countDown = 0;
        public long contestState = 0;
        public long totalTime = 0, questionScore = 0;
        public long score = 0;
        private bool isLoading = false;
        private bool isFailed = false;
        private bool isQuestionLoaded = false;
        private bool isSubmittingAnswer = false;
        private bool isLocked = false;
        private bool isPlaySound;
        private DispatcherTimer Timer1, Timer2, TimerCountDown;
        public string answerA = "", answerB = "", answerC = "", answerD = "";
        public string contestKey = "";
        public string correctAnswer = "";
        public string topic = "";
        public string prefix = "";
        public DateTime? startAnswerTime;
        private IsolatedStorageSettings appData = IsolatedStorageSettings.ApplicationSettings;
        private SoundEffectInstance effectSound;
        private int countTimeout;
        private int countDisconnect;
        private WebClient loadquestion;
        private WebClient loadingQ;
        private WebClient getAnswer;

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            base.OnBackKeyPress(e);
            e.Cancel = true;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            GoogleAnalytics.EasyTracker.GetTracker().SendView("PlayPage");
            //remove Back Entry
            NavigationService.RemoveBackEntry();

            string str_questnumb = "", str_numbofquest = "", str_remaintime = "", str_conteststate = "";
            //get querystring
            if (this.NavigationContext.QueryString.ContainsKey("questnumb"))
            {
                this.NavigationContext.QueryString.TryGetValue("questnumb", out str_questnumb);
            }
            if (this.NavigationContext.QueryString.ContainsKey("numbofquest"))
            {
                this.NavigationContext.QueryString.TryGetValue("numbofquest", out str_numbofquest);
            }
            if (this.NavigationContext.QueryString.ContainsKey("remaintime"))
            {
                this.NavigationContext.QueryString.TryGetValue("remaintime", out str_remaintime);
            }
            if (this.NavigationContext.QueryString.ContainsKey("conteststate"))
            {
                this.NavigationContext.QueryString.TryGetValue("conteststate", out str_conteststate);
            }
            if (this.NavigationContext.QueryString.ContainsKey("contestkey"))
            {
                this.NavigationContext.QueryString.TryGetValue("contestkey", out contestKey);
            }

            if (!int.TryParse(str_questnumb, out questionNumber))
            {
                questionNumber = 0;
            }
            if (!int.TryParse(str_numbofquest, out numberOfQuestion))
            {
                numberOfQuestion = 0;
            }
            if (!long.TryParse(str_remaintime, out remainTime))
            {
                remainTime = 0;
            }
            if (!long.TryParse(str_conteststate, out contestState))
            {
                contestState = 0;
            }

            long userID = 0;
            if (!long.TryParse(appData[Enums.USERID_KEY].ToString(), out userID))
            {
                userID = 0;
            }

            disableAnswers();
            btAnswer_a.Content = "";
            btAnswer_b.Content = "";
            btAnswer_c.Content = "";
            btAnswer_d.Content = "";
            subjectImage.Visibility = Visibility.Collapsed;
            startAnswerTime = null;
            isSubmittingAnswer = false;
            isLocked = false;
            isQuestionLoaded = false;
            loadQuestion(questionNumber);
            isFailed = false;
            isLoading = false;
            isPlaySound = false;
            countDisconnect = 0;
            countTimeout = 0;
            countDown = 0;
            countSubmitAnswer = 0;
            progressbarWidth = progressBarImage.Width;
            //refresh in 0.5s
            //Update timer code init here.
            Timer1 = new DispatcherTimer();
            Timer1.Interval = TimeSpan.FromMilliseconds(500);
            Timer1.Tick += new EventHandler(update_Tick);
            Timer1.Start();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            this.Timer1.Stop();
            this.Timer2.Stop();
            this.TimerCountDown.Stop();
            if (effectSound != null)
            {
                effectSound.Stop();
                if (effectSound.IsDisposed == false)
                {
                    effectSound.Dispose();
                }
            }
        }

        public PlayPage()
        {
            InitializeComponent();

        }

        private void loadQuestion(int number)
        {
            if (number > 0 && !string.IsNullOrEmpty(contestKey))
            {
                isLoading = true;
                disableAnswers();
                //Load Question
                loadquestion = new WebClient();
                loadquestion.DownloadStringCompleted += new DownloadStringCompletedEventHandler(loadquestion_downloadstringcompleted);
                loadquestion.DownloadStringAsync(new Uri(string.Format(Enums.QUESTION_URL, contestKey, number)));
            }
        }

        private void loadquestion_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                JObject json = JObject.Parse(e.Result);
                topic = (string)json["Topic"];
                answerA = (string)json["Answer1"];
                answerB = (string)json["Answer2"];
                answerC = (string)json["Answer3"];
                answerD = (string)json["Answer4"];
                totalTime = long.Parse(json["CountdownTime"].ToString());
                questionScore = long.Parse(json["Score"].ToString());
                remainTime = long.Parse(json["CountdownTime"].ToString());
                //check string too large
                if (answerA.Length > 16 || answerB.Length > 16 || answerC.Length > 16 || answerD.Length > 16)
                {
                    UpdateStyleAnswer(18);
                }
                //process bar in here
                progressBarImage.Width = progressbarWidth / totalTime * remainTime;

                if (topic.Equals("Giải trí"))
                {
                    BitmapImage bi1 = new BitmapImage(new Uri("/Assets/icon-giaitri_smaller.png", UriKind.Relative));
                    subjectImage.Source = bi1;
                }
                else if (topic.Equals("Địa lý"))
                {
                    BitmapImage bi2 = new BitmapImage(new Uri("/Assets/icon-dialy_smaller.png", UriKind.Relative));
                    subjectImage.Source = bi2;
                }
                else if (topic.Equals("Sức khỏe"))
                {
                    BitmapImage bi3 = new BitmapImage(new Uri("/Assets/icon-suckhoe_smaller.png", UriKind.Relative));
                    subjectImage.Source = bi3;
                }
                else if (topic.Equals("Lịch sử"))
                {
                    BitmapImage bi4 = new BitmapImage(new Uri("/Assets/icon-lichsu_smaller.png", UriKind.Relative));
                    subjectImage.Source = bi4;
                }
                else if (topic.Equals("Thể thao"))
                {
                    BitmapImage bi5 = new BitmapImage(new Uri("/Assets/icon-thethao_smaller.png", UriKind.Relative));
                    subjectImage.Source = bi5;
                }
                else if (topic.Equals("Văn hóa - Xã hội"))
                {
                    BitmapImage bi6 = new BitmapImage(new Uri("/Assets/icon-vanhoaxahoi_smaller.png", UriKind.Relative));
                    subjectImage.Source = bi6;
                }
                else if (topic.Equals("Văn học"))
                {
                    BitmapImage bi7 = new BitmapImage(new Uri("/Assets/icon-vanhoc_smaller.png", UriKind.Relative));
                    subjectImage.Source = bi7;
                }
                else if (topic.Equals("Khoa học - Công nghệ"))
                {
                    BitmapImage bi8 = new BitmapImage(new Uri("/Assets/icon-khoahoccongnghe_smaller.png", UriKind.Relative));
                    subjectImage.Source = bi8;
                }
                if (subjectImage.Source != null)
                {
                    subjectImage.Visibility = Visibility.Visible;
                }

                btAnswer_a.Content = "A." + answerA;
                btAnswer_a.Foreground = new SolidColorBrush(Colors.Black);
                btAnswer_b.Content = "B." + answerB;
                btAnswer_b.Foreground = new SolidColorBrush(Colors.Black);
                btAnswer_c.Content = "C." + answerC;
                btAnswer_c.Foreground = new SolidColorBrush(Colors.Black);
                btAnswer_d.Content = "D." + answerD;
                btAnswer_d.Foreground = new SolidColorBrush(Colors.Black);

                isLoading = false;
                isQuestionLoaded = true;
            }
            catch (WebException ex)
            {
                countDisconnect += 1;
                if (isFailed == false && countDisconnect >= Enums.totalDisconnect)
                {
                    MessageBoxResult m = MessageBox.Show("Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công.", Enums.ApplicationTitle, MessageBoxButton.OK);
                    if (m == MessageBoxResult.OK)
                    {
                        // reconnetion code in here
                    }
                    isFailed = true;
                }
                isLoading = false;
            }
        }

        private void loadState1()
        {
            if (!string.IsNullOrEmpty(contestKey))
            {
                isLoading = true;
                //load Question
                loadingQ = new WebClient();
                loadingQ.DownloadStringCompleted += new DownloadStringCompletedEventHandler(loadState1_downloadstringcompleted);
                loadingQ.DownloadStringAsync(new Uri(string.Format(Enums.STATE_URL, contestKey)));
            }
        }

        private void loadState1_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                JObject json = JObject.Parse(e.Result);
                contestState = long.Parse(json["Status"].ToString());
                remainTime = long.Parse(json["RemainTime"].ToString());
                countDown = remainTime;

                if (isPlaySound == false && remainTime <= totalTime && isLocked == false)
                {
                    //start sound
                    SoundPlayer.PlayMediaBackgroundNoRep("countdownFinal", SoundPlayer.countDown);
                    isPlaySound = true;
                }
                //process bar
                progressBarImage.Width = progressbarWidth / totalTime * remainTime;
                if (isLocked == false)
                {
                    answerTime = (int)(totalTime - remainTime);
                    score = questionScore / totalTime * remainTime;
                    txtCountDown.Text = score.ToString();
                }
                processState();
                isLoading = false;
                Timer1.Stop();
                enableAnswers();
                startAnswerTime = DateTime.Now;
                //refresh in 1s

                TimerCountDown = new DispatcherTimer();
                TimerCountDown.Interval = TimeSpan.FromMilliseconds(1000);
                TimerCountDown.Tick += new EventHandler(updateCountDown_Tick);
                TimerCountDown.Start();

            }
            catch (WebException ex)
            {
                countDisconnect += 1;
                if (isFailed == false && countDisconnect >= Enums.totalDisconnect)
                {
                    MessageBoxResult m = MessageBox.Show("Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công.", Enums.ApplicationTitle, MessageBoxButton.OK);
                    if (m == MessageBoxResult.OK)
                    {
                        // reconnetion code in here
                    }
                    isFailed = true;
                }
                isLoading = false;
            }
        }

        private void loadState2()
        {
            if (!string.IsNullOrEmpty(contestKey))
            {
                isLoading = true;
                //load Question
                loadingQ = new WebClient();
                loadingQ.DownloadStringCompleted += new DownloadStringCompletedEventHandler(loadState2_downloadstringcompleted);
                loadingQ.DownloadStringAsync(new Uri(string.Format(Enums.STATE_URL, contestKey)));
            }
        }

        private void loadState2_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                JObject json = JObject.Parse(e.Result);
                contestState = long.Parse(json["Status"].ToString());
                remainTime = long.Parse(json["RemainTime"].ToString());

                processState();
                isLoading = false;
            }
            catch (WebException ex)
            {
                countDisconnect += 1;
                if (isFailed == false && countDisconnect >= Enums.totalDisconnect)
                {
                    MessageBoxResult m = MessageBox.Show("Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công.", Enums.ApplicationTitle, MessageBoxButton.OK);
                    if (m == MessageBoxResult.OK)
                    {
                        // reconnetion code in here
                    }
                    isFailed = true;
                }
                isLoading = false;
            }
        }

        private void update_Tick(object sender, EventArgs e)
        {
            countTimeout += 1000;
            if (isLoading == true && countTimeout > Enums.timeout)
            {
                if (isQuestionLoaded == false)
                {
                    loadquestion.CancelAsync();
                }
                else
                {
                    loadingQ.CancelAsync();
                }
                isLoading = false;
            }

            if (isLoading == false)
            {
                countTimeout = 0;
                if (isQuestionLoaded == false)
                {
                    loadQuestion(questionNumber);
                }
                else
                {
                    loadState1();
                }
            }
        }

        private void updateState_Tick(object sender, EventArgs e)
        {
            countTimeout += 1000;
            if (isLoading == true && countTimeout > Enums.timeout)
            {
                loadingQ.CancelAsync();
                isLoading = false;
            }

            if (isLoading == false)
            {
                countTimeout = 0;
                loadState2();
            }
        }

        private void updateCountDown_Tick(object sender, EventArgs e)
        {
            if (countDown > 0)
            {
                countDown--;
                progressBarImage.Width = progressbarWidth / totalTime * countDown;
                if (isLocked == false)
                {
                    answerTime = (int)(totalTime - countDown);
                    score = (long)(questionScore / totalTime * countDown);
                    txtCountDown.Text = score.ToString();
                }
            }
            else
            {
                MediaPlayer.Stop();
                isLocked = true;
                TimerCountDown.Stop();
                progressBarImage.Width = 0;
                //Load Timer2
                //refresh in 2s
                Timer2 = new DispatcherTimer();
                Timer2.Interval = TimeSpan.FromMilliseconds(2000);
                Timer2.Tick += new EventHandler(updateState_Tick);
                Timer2.Start();
            }
        }

        private void processState()
        {
            if (contestState >= 10 && contestState < 10000)
            {
                //Question
                long status = contestState % 10;
                if (status < 2)
                {
                    //not Start
                    startAnswerTime = null;
                }
                else if (status == 2)
                {
                    if (remainTime == 0)
                    {
                        if (isLocked == false)
                        {
                            //Not count down off sound
                            txtCountDown.Text = "0";
                            isLocked = true;
                        }
                    }
                    else
                    {

                    }
                }
                else if (status == 3)
                {
                    string answeredText = "";
                    if (answer == 1)
                    {
                        answeredText = answerA;
                    }
                    else if (answer == 2)
                    {
                        answeredText = answerB;
                    }
                    else if (answer == 3)
                    {
                        answeredText = answerC;
                    }
                    else if (answer == 4)
                    {
                        answeredText = answerD;
                    }

                    // Redirect to result page
                    NavigationService.Navigate(new Uri(string.Format("/View/ResultPage.xaml?contestkey={0}&correctanswer={1}&score={2}&answeredtext={3}&prefix={4}", Uri.EscapeUriString(contestKey), Uri.EscapeUriString(correctAnswer), Uri.EscapeUriString(score.ToString()), Uri.EscapeUriString(answeredText), Uri.EscapeUriString(prefix)), UriKind.Relative));
                }
            }
            else if (contestState == 10400)
            {
                //not start
                startAnswerTime = null;
            }
            else if (contestState == 10401)
            {
                if (remainTime == 0)
                {
                    if (isLocked == false)
                    {
                        txtCountDown.Text = "0";
                        isLocked = true;
                    }
                }
                else
                {
                }
            }
            else if (contestState == 10402)
            {
                //redirect to Result Page
                string answeredText = "";
                if (answer == 1)
                {
                    answeredText = answerA;
                }
                else if (answer == 2)
                {
                    answeredText = answerB;
                }
                else if (answer == 3)
                {
                    answeredText = answerC;
                }
                else if (answer == 4)
                {
                    answeredText = answerD;
                }

                // Redirect to result page
                NavigationService.Navigate(new Uri(string.Format("/View/ResultPage.xaml?contestkey={0}&correctanswer={1}&score={2}&answeredtext={3}&prefix={4}", Uri.EscapeUriString(contestKey), Uri.EscapeUriString(correctAnswer), Uri.EscapeUriString(score.ToString()), Uri.EscapeUriString(answeredText), Uri.EscapeUriString(prefix)), UriKind.Relative));
            }
            else if (contestState == 10000 || contestState == 10100 || contestState == 10200)
            {
                int quesNum = appData.Contains(Enums.QUESTION_NUMBER_KEY) ? int.Parse(appData[Enums.QUESTION_NUMBER_KEY].ToString()) : 0;
                if (quesNum > 0 && quesNum % 4 == 0)
                {
                    //Go to summary page
                    NavigationService.Navigate(new Uri(string.Format("/View/SummaryPage.xaml?contestkey={0}", Uri.EscapeUriString(contestKey)), UriKind.Relative));
                }
                else
                {
                    //go to infoPage
                    NavigationService.Navigate(new Uri(string.Format("/View/ContestInfoPage.xaml?contestkey={0}", Uri.EscapeUriString(contestKey)), UriKind.Relative));
                }
            }
            else if (contestState == 10300)
            {
                //SummaryPage
                NavigationService.Navigate(new Uri(string.Format("/View/SummaryPage.xaml?contestkey={0}", Uri.EscapeUriString(contestKey)), UriKind.Relative));
            }
        }

        private void disableAnswers()
        {
            btAnswer_a.IsEnabled = false;
            btAnswer_b.IsEnabled = false;
            btAnswer_c.IsEnabled = false;
            btAnswer_d.IsEnabled = false;
        }

        private void enableAnswers()
        {
            btAnswer_a.IsEnabled = true;
            btAnswer_b.IsEnabled = true;
            btAnswer_c.IsEnabled = true;
            btAnswer_d.IsEnabled = true;
        }

        private void btAnswer_a_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            effectSound = SoundPlayer.PlaySound(SoundPlayer.chooseAnswer);
            if (isLocked == true)
            {
                return;
            }

            btAnswer_a.Foreground = new SolidColorBrush(Colors.White);
            var imageBrush = new ImageBrush
            {
                ImageSource = new BitmapImage(new Uri("/Assets/box_a_hover.png", UriKind.Relative))
            };
            btAnswer_a.Background = imageBrush;
            isLocked = true;
            answer = 1;
            prefix = "A.";
            isSubmittingAnswer = true;
            submitAnswer();
        }

        private void btAnswer_b_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            effectSound = SoundPlayer.PlaySound(SoundPlayer.chooseAnswer);
            if (isLocked == true)
            {
                return;
            }

            btAnswer_b.Foreground = new SolidColorBrush(Colors.White);
            var imageBrush = new ImageBrush
            {
                ImageSource = new BitmapImage(new Uri("/Assets/box_b_hover.png", UriKind.Relative))
            };
            btAnswer_b.Background = imageBrush;
            isLocked = true;
            answer = 2;
            prefix = "B.";
            isSubmittingAnswer = true;
            submitAnswer();
        }

        private void btAnswer_c_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            effectSound = SoundPlayer.PlaySound(SoundPlayer.chooseAnswer);
            if (isLocked == true)
            {
                return;
            }

            btAnswer_c.Foreground = new SolidColorBrush(Colors.White);
            var imageBrush = new ImageBrush
            {
                ImageSource = new BitmapImage(new Uri("/Assets/box_c_hover.png", UriKind.Relative))
            };
            btAnswer_c.Background = imageBrush;
            isLocked = true;
            answer = 3;
            prefix = "C.";
            isSubmittingAnswer = true;
            submitAnswer();
        }

        private void btAnswer_d_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            effectSound = SoundPlayer.PlaySound(SoundPlayer.chooseAnswer);
            if (isLocked == true)
            {
                return;
            }

            btAnswer_d.Foreground = new SolidColorBrush(Colors.White);
            var imageBrush = new ImageBrush
            {
                ImageSource = new BitmapImage(new Uri("/Assets/box_d_hover.png", UriKind.Relative))
            };
            btAnswer_d.Background = imageBrush;
            isLocked = true;
            answer = 4;
            prefix = "D.";
            isSubmittingAnswer = true;
            submitAnswer();
        }

        private void submitAnswer()
        {
            if (startAnswerTime == null)
            {
                isSubmittingAnswer = false;
                return;
            }
            if (isSubmittingAnswer == false)
            {
                return;
            }
            if (countSubmitAnswer > 5)
            {
                return;
            }

            countSubmitAnswer++;
            long userID = long.Parse(appData[Enums.USERID_KEY].ToString());
            long startTime = (long)startAnswerTime.Value.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
            long endTime = (long)DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;

            getAnswer = new WebClient();
            getAnswer.DownloadStringCompleted += new DownloadStringCompletedEventHandler(getAnswer_downloadstringcompleted);
            getAnswer.DownloadStringAsync(new Uri(string.Format(Enums.ANSWER_URL, contestKey, userID, answer, startTime, endTime, answerTime, score), UriKind.RelativeOrAbsolute));
        }

        private void getAnswer_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                JObject json = JObject.Parse(e.Result);
                correctAnswer = json["CorrectAnswer"].ToString();
                int errorCode = int.Parse(json["ResultCode"].ToString());
                if (errorCode == 1)
                {
                    MessageBoxResult m = MessageBox.Show("Cuộc thi ngày hôm nay đã kết thúc. Hãy quay lại và tham gia vào ngày thi khác.", Enums.ApplicationTitle, MessageBoxButton.OK);
                    if (m == MessageBoxResult.OK)
                    {
                        // code after click
                    }
                }
                else if (errorCode == 2)
                {
                    MessageBoxResult m = MessageBox.Show("Câu hỏi chưa bắt đầu hoặc đã kết thúc. Hãy chờ câu hỏi bắt đầu hoặc chờ câu hỏi khác.", Enums.ApplicationTitle, MessageBoxButton.OK);
                    if (m == MessageBoxResult.OK)
                    {
                        // code after click
                    }
                }
                else if (errorCode == 3)
                {
                    MessageBoxResult m = MessageBox.Show("Bạn đã trả lời cho câu hỏi này rồi.", Enums.ApplicationTitle, MessageBoxButton.OK);
                    if (m == MessageBoxResult.OK)
                    {
                        // code after click
                    }
                }
                else
                {
                    ////UPDATE SCORE
                    long userScore = 0, userDayScore = 0;
                    if (appData.Contains(Enums.USER_SCORE_KEY) && appData.Contains(Enums.USER_DAY_SCORE_KEY))
                    {
                        if (!long.TryParse(appData[Enums.USER_SCORE_KEY].ToString(), out userScore))
                        {
                            userScore = 0;
                        }
                        if (!long.TryParse(appData[Enums.USER_DAY_SCORE_KEY].ToString(), out userDayScore))
                        {
                            userDayScore = 0;
                        }
                    }
                    else
                    {
                        userScore = 0;
                        userDayScore = 0;
                    }
                    if (!long.TryParse(json["Score"].ToString(), out score))
                    {
                        score = 0;
                    }
                    userScore += score;
                    userDayScore += score;
                    appData[Enums.USER_SCORE_KEY] = userScore;
                    appData[Enums.USER_DAY_SCORE_KEY] = userDayScore;
                    appData.Save();
                }
                isSubmittingAnswer = false;
            }
            catch (WebException ex)
            {
                if (isFailed == false)
                {
                    MessageBoxResult m = MessageBox.Show("Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công.", Enums.ApplicationTitle, MessageBoxButton.OK);
                    if (m == MessageBoxResult.OK)
                    {
                        // reconnetion code in here
                    }
                    isFailed = true;
                }
                submitAnswer();
            }
        }

        private void UpdateStyleAnswer(int fontsize)
        {
            btAnswer_a.FontSize = fontsize;
            btAnswer_b.FontSize = fontsize;
            btAnswer_c.FontSize = fontsize;
            btAnswer_d.FontSize = fontsize;
        }

    }
}