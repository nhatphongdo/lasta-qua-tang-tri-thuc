﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using Newtonsoft.Json.Linq;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace lasta_windowsphone.View
{
    public partial class BillOPage : PhoneApplicationPage
    {
        //Control.PopupControl popup;
        IsolatedStorageSettings appData = IsolatedStorageSettings.ApplicationSettings;
        long userId;
        private SoundEffectInstance soundeffect;

        public BillOPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            GoogleAnalytics.EasyTracker.GetTracker().SendView("BillOPage");
            SoundPlayer.PlayMediaBackground("ExchangeScore", SoundPlayer.opening);
            //popup = new Control.PopupControl();
            //popup.txtScore.Text = "xxx điểm";
            //popup.Show();
            //if ((this.Orientation & PageOrientation.LandscapeLeft) == (PageOrientation.LandscapeLeft))
            //{
            //    popup.LayoutPopup.RenderTransformOrigin = new Point(0.5, 0.5);
            //    popup.UpdateLayout();

            //}
            //else
            //{
            //    popup.LayoutPopup.RenderTransformOrigin = new Point(1.5, 1.5);
            //    popup.UpdateLayout();
            //}

            //To Page ~ PageLoad
            if (appData.Contains(Enums.USERID_KEY))
            {
                userId = long.Parse(appData[Enums.USERID_KEY].ToString());
            }

            webTerm.NavigateToString(@"
<html>
<head>
<style>
body{font-family:Arial;font-size:10px;}
p, h1, h2, h3, h4, h5, h6 {
	margin: 0;
	padding: 0;
}
.bold {
	font-weight: bold;
}
.red {
	color: red;
}
.italic {
	font-style: italic;
}
.underline {
	text-decoration: underline;
}
.odo {
	text-align: left;
}
</style>
</head>
<body style='margin:0 auto;color:#000; line-height:20px;'>
<div style='text-align:center;width:100%;'>
  <image src='http://vietdev.vn/images/title-tpkd.png' alt='logo' />
</div>
<h4 class='bold' style='color:red;'>GIỚI THIỆU CHUNG</h4>
<span class='bold'>Tên chương trình:</span>  XÉ NHÃN LIỀN TAY - TRÚNG NGAY TIỀN TỶ<br/>
<span class='bold'>Thời gian diễn ra chương trình:</span> Từ ngày 05/05 đến 31/07/2014<br/>
<span class='bold'>Phạm vi khuyến mại:</span>  Áp dụng  trên toàn quốc<br/>
<span class='bold'>Sản phẩm khuyến mại:</span> Là sản phẩm có nhãn khuyến mãi<br/>
<table class='odo italic' style='color:#0b8632;'>
  <tbody>
    <tr>
      <td>Trà xanh O&deg chai 500ml</td>
      <td></td>
      <td>Tăng lực Number 1 Dâu chai 330ml</td>
    </tr>
    <tr>
      <td>Trà xanh O&deg chai thủy tinh 240ml</td>
      <td></td>
      <td>Tăng Lực Number 1 Dâu chai thủy tinh 240ml</td>
    </tr>
    <tr>
      <td>Trà xanh O&deg ít  đường 500ml</td>
      <td></td>
      <td>Tăng Lực Number 1 Chanh chai 330ml</td>
    </tr>
    <tr>
      <td>Trà Thảo Mộc Dr Thanh chai 350ml, 500ml</td>
      <td></td>
      <td>Tăng Lực Number 1 Chanh chai thủy tinh 240ml</td>
    </tr>
    <tr>
      <td>Trà thảo mộc Dr.Thanh không đường 350ml</td>
      <td></td>
      <td>Sữa đậu nành Soya chai thủy tinh 240ml</td>
    </tr>
    <tr>
      <td>Tăng lực Number 1 chai 350ml</td>
      <td></td>
      <td>Number 1 Active Chanh Muối 500ml</td>
    </tr>
  </tbody>
</table>
<br/>
<h4 class='bold red'>GIẢI THƯỞNG</h4>
<span class='bold'>Cơ hội 1:</span> Xé nhãn trúng hàng triệu giải sảng khoái là sản phẩm cùng loại miễn phí<br/>
<span class='bold'>Cơ hội 2:</span> Nhắn tin mã số dưới nhãn sản phẩm để có cơ hội trúng thưởng bao gồm 01 giải đặc biệt một tỷ, 480 giải nhất  mỗi giải 01 điện thoại Nokia 630 hai sim, 4,992 giải nhì mỗi giải 1 nón bảo hiểm Dr.Thanh.<br/>
<br/>
<span class='red bold'>CÁCH THAM GIA</span><br/>
<span class='bold'>Cơ hội 1:</span> Khách hàng xé nhãn sản phẩm có bao bì khuyến mại. Xem nội dung bên trong nhãn sản phẩm để biết kết quả. Nếu trúng  thưởng một sản phẩm của THP chấp nhận đổi thưởng, đem nhãn tới các điểm đổi của THP để đổi chai miễn phí. <span class='bold'>Thời gian đổi sản phẩm miễn phí từ 00h00 ngày 05/05/2014 đến 24h ngày 15/08/2014</span><br/><br/>
<span class='bold'>Cơ hội 2:</span> Khách hàng tham gia bằng cách nhắn tin dãy mã số code dưới nhãn sản phẩm khuyến mại với cú pháp <span class='bold' style='color:#0daf4a'>KD < Dấu cách > < mã code >
</span> về tổng đài <span class='bold'>6020</span> ( mỗi tin nhắn 500 đ/tin) hoặc nhập mã số thông qua <a style='color:red' href='http://www.typhukhongdo.vn/'>www.typhukhongdo.vn</a> để tham gia chương trình. Hệ thống sẽ phản hồi xác nhận tham gia thành công cho khách hàng bằng một tin nhắn kèm theo mã code thứ hai vào số điện thoại đăng ký. Khách hàng phải lưu tin nhắn xác nhận tham gia để làm thủ tục nhận giải thưởng sau này. Xác định khách hàng trúng thưởng thông qua quay số dựa trên mã code thứ hai,  trực tiếp trên kênh truyền hình Let’s Viet (VTC9) vào lúc 20:30 đến 21:30 các ngày thứ 7 và Chủ Nhật hàng tuần kể từ ngày 17/05/2015 đến 03/08/2014. Riêng giải đặc biệt <span class='bold'>một tỷ đồng</span> sẽ được quay số kỹ thuật số trực tiếp vào ngày 03/08/2014. Khách hàng trúng thưởng được thông báo bằng tin nhắn tới số điện thoại đăng ký. Khách hàng trúng giải nhất và giải nhì sẽ được THP trao giải trực tiếp hoặc chuyển giải thưởng qua đường bưu điện. Khách hàng trúng giải đặc biệt sẽ được trao tại địa điểm do THP qui định. Quy định nhận giải thưởng áp dụng theo thể lệ công bố theo giấy phép khuyến mại của bộ thương mại. Người trúng thưởng chịu trách nhiệm chi trả các chi phí liên quan đến việc nhận giải thưởng bao gồm nhưng không loại trừ thuế thu nhập cá nhân. Thời gian khách hàng đổi các giải thưởng cơ hội 2 là từ khi có kết quả trúng thưởng đến 24h ngày 15/08/2014.<br/>
<br/>
<div style= 'margin:0 auto; padding:10px; width:100%; text-align: center'><span class='bold'>Chi tiết:</span> <a style='color:#fa1a1a' href='http://typhukhongdo.vn/'>http://typhukhongdo.vn/</a><br/></div>
<div style='height:30px; clear:both'></div>

<br/>
<div style='text-align:center;width:100%;'>
  <image src='http://vietdev.vn/images/02-typhukhongdo-withtext.png' alt='logo' />
</div>
</body>
</html>
");
            base.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            //Out Page
            base.OnNavigatedFrom(e);
            MediaPlayer.Stop();
            //if (soundeffect != null)
            //{
            //    soundeffect.Stop();
            //    if (soundeffect.IsDisposed == false)
            //    {
            //        soundeffect.Dispose();
            //    }
            //}
        }

        private void btReceiverScore_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            soundeffect = SoundPlayer.PlaySound(SoundPlayer.chooseAnswer);
            string code = txtCode.Text;
            WebClient sendCode = new WebClient();
            sendCode.DownloadStringCompleted += new DownloadStringCompletedEventHandler(sendCode_downloadstringcompleted);
            sendCode.DownloadStringAsync(new Uri(string.Format(Enums.EXCHANGE_SCORE_URL, Uri.EscapeDataString(Function.ReplaceUrlString(code)), userId)));
        }

        private void sendCode_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                JObject obj = JObject.Parse(e.Result);
                //todo: get result return and process it.
                int error = (int)obj["Error"];
                switch (error)
                {
                    case 0:
                        Function.UpdateUserData();
                        string score = Convert.ToString(obj["Score"]);
                        MessageBoxResult m = MessageBox.Show(string.Format("Bạn vừa nhận được {0} điểm", score), "CHÚC MỪNG", MessageBoxButton.OK);
                        break;
                    case 1:
                        MessageBoxResult m1 = MessageBox.Show("Mã số này đã được sử dụng xin vui lòng nhập mã số khác để cộng điểm.", Enums.ApplicationTitle, MessageBoxButton.OK);
                        break;
                    case 2:
                        MessageBoxResult m2 = MessageBox.Show("Mã số không hợp lệ hoặc không tồn tại, xin vui lòng nhập mã số khác.", Enums.ApplicationTitle, MessageBoxButton.OK);

                        break;
                    case 3:
                        MessageBoxResult m3 = MessageBox.Show("Chương trình xảy ra lỗi trong quá trình xử lý. Vui lòng thông báo tới BTC để được hướng dẫn xử lý và hỗ trợ.", Enums.ApplicationTitle, MessageBoxButton.OK);
                        break;
                }
            }
            catch (WebException ex)
            {
                MessageBoxResult m = MessageBox.Show("Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại.", Enums.ApplicationTitle, MessageBoxButton.OK);
                if (m == MessageBoxResult.OK)
                {
                    // reconnetion
                    NavigationService.Navigate(new Uri("/View/MenuPage.xaml", UriKind.Relative));
                }
            }
        }

        private void btTermBilO_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            soundeffect = SoundPlayer.PlaySound(SoundPlayer.chooseAnswer);
            borTermBilO.Visibility = Visibility.Collapsed;
            btTermBilO.Visibility = Visibility.Collapsed;
            groupGetCode.Visibility = Visibility.Visible;
        }

        private void btBack_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            soundeffect = SoundPlayer.PlaySound(SoundPlayer.chooseAnswer);
            NavigationService.Navigate(new Uri("/View/MenuPage.xaml", UriKind.Relative));
        }

        private void txtCode_GotFocus(object sender, RoutedEventArgs e)
        {
            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri("/Assets/txtBilO.png", UriKind.Relative));
            (sender as TextBox).Background = brush;
        }

        private void PhoneApplicationPage_OrientationChanged(object sender, OrientationChangedEventArgs e)
        {
            //if ((e.Orientation & PageOrientation.LandscapeLeft) == (PageOrientation.LandscapeLeft))
            //{
            //    RotateTransform rotate = new RotateTransform();
            //    rotate.Angle = 90;
            //    rotate.CenterX = 0;
            //    rotate.CenterY = 0;
            //    popup.RenderTransform = rotate;
            //    popup.LayoutPopup.RenderTransformOrigin = new Point(0.5, 0.5);
            //    popup.UpdateLayout();
            //}
            //else
            //{
            //    RotateTransform rotate = new RotateTransform();
            //    rotate.Angle = 270;
            //    rotate.CenterX = 0;
            //    rotate.CenterY = 0;
            //    popup.RenderTransform = rotate;
            //    popup.LayoutPopup.RenderTransformOrigin = new Point(0, 0);
            //    popup.UpdateLayout();
            //}
        }
    }
}