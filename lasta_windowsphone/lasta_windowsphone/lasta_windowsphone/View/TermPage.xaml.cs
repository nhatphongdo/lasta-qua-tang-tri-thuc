﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace lasta_windowsphone.View
{
    public partial class TermPage : PhoneApplicationPage
    {
        private SoundEffectInstance soundeffect;
        public TermPage()
        {
            InitializeComponent();
            GoogleAnalytics.EasyTracker.GetTracker().SendView("TermPage");
            webTerm.NavigateToString(@"
<html>
<head>
<style>
body{font-family:Arial;font-size:10px;}
p,h1,h2,h3,h4,h5,h6{margin:0; padding:0;}
 .bold
 {
 font-weight:bold;
 }
 .red
 {
 color:#fa1a1a;
 }
 .italic
 {
	font-style:italic;
 }
 .underline
 {
 text-decoration:underline;
 }
 .odo
 {
 text-align:left;
 }
</style>
</head>
<body style='margin:0 auto;color:#000; line-height:20px;'>
<span class='red bold'>“Cùng Là Tỷ Phú”</span> do Công ty Cổ phần Truyền thông Đa phương tiện Lát Sa Ta và Kênh truyền hình Let’s Viet sản xuất với sự tài trợ chính của Nhãn hàng Trà Xanh Không Độ và các nhãn hàng: Trà thảo mộc Dr Thanh, Nước tăng lực Number 1, Nước tăng lực Number 1 Chanh và Sữa đậu nành Soya Number 1. Chương trình được truyền hình trực tiếp vào lúc <span class='bold'>20 giờ 30 Thứ 7 và Chủ nhật</span> hàng tuần trên kênh <span class='bold'>Let’s Viet.</span>
<p><span class='red bold'>“Cùng Là Tỷ Phú”</span> do Công ty Cổ phần Truyền thông Đa phương tiện Lát Sa Ta và Kênh truyền hình Let’s Viet sản xuất với sự tài trợ chính của Nhãn hàng Trà Xanh Không Độ và các nhãn hàng: Trà thảo mộc Dr Thanh, Nước tăng lực Number 1, Nước tăng lực Number 1 Chanh và Sữa đậu nành Soya Number 1. Chương trình được truyền hình trực tiếp vào lúc <span class='bold'>20 giờ 30 Thứ 7 và Chủ nhật</span> hàng tuần trên kênh <span class='bold'>Let’s Viet.</span></p>
<p>Giai đoạn phát sóng chính thức ( 25 chương trình – chương trình số 25 là Gala ): bắt đầu từ ngày 17/05/2014<br/>
</p>
<br/>
<h4 class='red bold'>CẤU TRÚC CHƯƠNG TRÌNH</h4>
Chương trình có 2 vòng thi: <br/>
&nbsp;&nbsp;&nbsp;&nbsp;+Vòng 1: câu 1 – câu 16<br/>
&nbsp;&nbsp;&nbsp;&nbsp;+Vòng 2: câu 17 ( câu hỏi quyết định )<br/>

<span class='bold red'>• Vòng 1</span><br/>
Chương trình với sự tham gia của 50 thí sinh tại phim trường và tất cả khán giả xem truyền hình.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;+ Khán giả truyền hình có thể tham gia bất kỳ lúc nào trong thời gian chương trình phát sóng.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;+ Thí sinh tại phim trường phải tham gia từ câu đầu tiên đến câu 16.<br/>
- Có 16 câu hỏi trắc nghiệm a,b,c,d tương ứng với 8 chủ đề, thời gian trả lời là 15 giây/câu hỏi.<br/>
- Điểm sẽ giảm dần theo thời gian.<br/>
- 16 câu hỏi tương ứng với 16 thang điểm khác nhau: 30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330, 360, 390, 420, 450, 480. Thứ tự thang điểm sẽ được ngẫu nhiên trong các câu hỏi và các chương trình.<br/>
- Tất cả khán giả xem truyền hình tham gia chương trình sẽ được tích lũy điểm và cộng dồn vào vòng 2.<br/>

<span class='bold red'>• Vòng 2 – Câu 17</span><br/>
- Câu hỏi trắc nghiệm a, b, c, d. Thời gian trả lời là 15 giây với số điểm là 720 điểm.<br/>
- Điểm sẽ giảm dần theo thời gian.<br/>
<span class='bold'>- Người chiến thắng là người có đáp án đúng và nhanh nhất câu 17. Giải thưởng cho người thắng cuộc là 1 điện thoại Nokia Lumia 1520.</span><br/>
- Tổng điểm của 50 thí sinh đạt được tại phim trường sẽ được chuyển vào thiết bị cá nhân có mã ID tham gia chương tình.<br/>
- Tổng điểm của khán giả đạt được trong các ngày phát sóng sẽ được bảo lưu và cộng dồn cho đến khi kết thúc chương trình thứ 24.<br/>
<span class='bold underline italic'>Chú ý:</span><br/>
<span class='bold'>+ Khi có bảng xếp hạng cuối cùng, trong trường hợp nhiều người chơi có tổng điểm bằng nhau, BTC sẽ xét yếu tố thời gian ( tính đến mili giây ) của đáp án đúng ở câu hỏi cuối cùng để ưu tiên vị trí cao.</span><br/>
+ Vào ngày ghi hình, thí sinh tham gia tại phim trường không được đưa thiết bị cá nhân có mã ID của mình cho người khác sử dụng. Nếu BTC phát hiện, thí sinh vi phạm sẽ bị hủy tất cả số điểm đạt được trong ngày.<br/>
+ Tổng điểm cao nhất của 1 chương trình phát sóng là 4800 điểm.<br/>
+ Trường hợp người chơi đang tham gia chương trình bị mất điện thoại, điểm của người chơi sẽ không được chuyển vào bất kì thiết bị nào. Ban tổ chức sẽ không chịu trách nhiệm trong trường hợp này.<br/>
+ Ngoài ra, trong trường hợp bất khả kháng như thiên tai, cháy nổ hoặc cắt điện, đường truyền internet mất kết nối trên diện rộng,… thì chương trình sẽ tạm ngưng. Ban tổ chức sẽ chủ động thông báo thời gian phát sóng chương trình thay thế trên phương tiện truyền thông đại chúng. Giải thưởng và điểm của chương trình tạm ngưng phát sóng được tính như sau:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;a.   Giải thưởng được chuyển sang chương trình kế tiếp.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;b.   Điểm của người chơi tính đến thời điểm bị tạm dừng sẽ được bảo lưu và cộng dồn cho các chương trình tiếp theo. <br/>

<span class='red bold'>• Quyền trợ giúp: </span><br/>
<span class='bold'>1.	Tặng điểm</span><br/>
Người chơi được phép tặng điểm cho nhau (có thể tặng hết số điểm đang có)<br/>
•	Chỉ được tặng 01 lần. Thời gian tặng điểm: từ lúc bắt đầu phát sóng chương trình 23 và đến giữa chương trình 24 (kết thúc lúc 21:00 ngày phát sóng chương trình 24)<br/>
•	Người nhận chỉ được nhận tối đa điểm của 5 người chơi khác tặng.<br/>
<span class='bold italic'>Chú ý:</span> <span class='italic'>Người chơi nếu được nhận điểm từ người chơi khác sẽ không được quyền tặng điểm.</span><br/>
<span class='bold'>2.	Cộng điểm</span><br/>
Người chơi đang tham gia Game show “Cùng Là Tỷ Phú” khi mua sản phẩm trong chương trình khuyến mãi “Xé nhãn liền tay – Trúng ngay tiền tỷ”, sử dụng mã số code thứ hai và đăng nhập vào ứng dụng “Cùng Là Tỷ Phú” vào sẽ nhận điểm ngẫu nhiên với 6 giá trị: 5 điểm, 10 điểm, 15 điểm, 30 điểm, 50 điểm, 100 điểm. Thời gian đổi điểm bắt đầu từ 0 giờ ngày 17/05/14. Điểm đổi này chỉ được cộng vào tổng điểm cộng dồn, không tính trong điểm đạt được của mỗi chương trình phát sóng. <br/>
<br/>
<h4 class='red bold'>CÁCH THAM GIA CHƯƠNG TRÌNH </h4>
<span class='bold'>Để tham gia chương trình 'Cùng là Tỷ Phú', bạn hãy thực hiện: </span><br/>
<span class='bold red underline'> TRƯỚC 20:30 (GIỜ TRUYỀN HÌNH TRỰC TIẾP)</span><br/>
- Điện thoại thông minh hoặc máy tính bảng phải kết nối Internet.<br/>
- Tải ứng dụng “Cùng Là Tỷ Phú” trên các thiết bị điện thoại thông minh hay máy tính bảng có sử dụng hệ điều hành iOS, Android, Windows Phone.<br/>
<span class='italic bold'>Chi tiết:</span> <a href='http://cunglatyphu.com/cach-tai-phan-mem/'>http://cunglatyphu.com/cach-tai-phan-mem/</a><br/>
-  Xem kênh truyền hình Let's Viet.<br/>
<span class='bold red underline'> TỪ 20:30 ĐẾN 21:20</span><br/>
<span class='bold'>Bước 1:</span> Bấm biểu tượng 'Cùng là Tỷ Phú'.<br/>
<span class='bold'>Bước 2:</span> Bấm nút 'Bắt đầu'.<br/>
<span class='bold'>Bước 3:</span> Trên màn hình thiết bị hiển thị thông tin người chơi (số ID) và 1 khung camera. Màn hình sẽ tự động chuyển sang màn hình thông tin giải thưởng của chương trình.<br/>
<span class='bold'>Bước 4:</span> Người chơi không thao tác, màn hình thiết bị sẽ tự động thay đổi theo lời của người dẫn chương trình trên Tivi ở các nội dung: chọn chủ đề câu hỏi, xác định điểm của câu hỏi.<br/>
<span class='bold'>Bước 5:</span> Trả lời 01 câu hỏi, cách thao tác như sau:<br/>
Trên màn hình thiết bị chơi, không có nội dung câu hỏi, chỉ có chủ đề và số điểm.<br/>
- Khi MC nói '15 giây bắt đầu', người chơi sẽ bấm vào 1 trong 4 đáp án chọn. Người chơi trả lời càng nhanh thì điểm càng cao.<br/>
<span class='bold'>Bước 6:</span> Xác nhận đáp án đúng và điểm đạt được của 1 câu hỏi.<br/>
<span class='bold red underline'>TỪ 21:30 ĐẾN 21:40</span><br/>
<span class='bold'>Để tham gia quay số chương trình ' Xé nhãn liền tay – Trúng ngay tiền tỷ ',  từ ngày 05/05 đến 31/07/2014, bạn hãy mua bất kỳ sản phẩm sau đây:</span><br/>
<table class='odo italic' style='color:#0b8632;'>
  <tbody>
    <tr>
      <td>Trà xanh O&deg chai 500ml</td>
      <td></td>
      <td>Tăng lực Number 1 Dâu chai 330ml</td>
    </tr>
    <tr>
      <td>Trà xanh O&deg chai thủy tinh 240ml</td>
      <td></td>
      <td>Tăng Lực Number 1 Dâu chai thủy tinh 240ml</td>
    </tr>
    <tr>
      <td>Trà xanh O&deg ít  đường 500ml</td>
      <td></td>
      <td>Tăng Lực Number 1 Chanh chai 330ml</td>
    </tr>
    <tr>
      <td>Trà Thảo Mộc Dr Thanh chai 350ml, 500ml</td>
      <td></td>
      <td>Tăng Lực Number 1 Chanh chai thủy tinh 240ml</td>
    </tr>
    <tr>
      <td>Trà thảo mộc Dr.Thanh không đường 350ml</td>
      <td></td>
      <td>Sữa đậu nành Soya chai thủy tinh 240ml</td>
    </tr>
    <tr>
      <td>Tăng lực Number 1 chai 350ml</td>
      <td></td>
      <td>Number 1 Active Chanh Muối 500ml</td>
    </tr>
  </tbody>
</table>
Sau khi có  mã số code dưới nhãn sản phẩm khuyến mại, khách hàng sẽ nhắn tin với cú pháp <span class='bold' style='color:#0daf4a'>KD < Dấu cách >  < mã code > </span> về tổng đài <span class='bold;'>6020</span> ( mỗi tin nhắn 500 đ/tin nhắn) <span class='bold;'>hoặc</span> nhập mã số thông qua <a style='color:#fa3636' href='http://www.typhukhongdo.vn/'>www.typhukhongdo.vn</a> để tham gia chương trình. Hệ thống sẽ phản hồi xác nhận tham gia thành công cho khách hàng bằng một tin nhắn kèm theo mã code thứ hai vào số điện thoại đăng ký. <br/>
<br/>
<h4 class='red bold'>CƠ CẤU GIẢI THƯỞNG </h4>
<h5 class='red underline'>MÙA 1 – 25 CHƯƠNG TRÌNH: bắt đầu từ 17/5/2014 </h5>
<span class='bold'>1. Từ chương trình 1 – 24, cơ cấu giải thưởng mỗi chương trình:</span><br/>
<span class='bold'>a. Đối với thí sinh tại phim trường trong Phần 1 – CÙNG LÀ TỶ PHÚ</span><br/>
&nbsp;&nbsp;&nbsp;&nbsp;-	01 giải nhất: 1 điện thoại Nokia Lumia 1520 trị giá 13 triệu đồng cho người chiến thắng câu hỏi vòng 2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;-	03 giải nhì: trị giá 1 triệu đồng/giải cho 3 thí sinh còn lại tham gia vòng 2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;-	Quà tặng: 50 phần quà lưu niệm cho 50 thí sinh<br/>
<span class='italic'>(Trong trường hợp 4 thí sinh ở vòng 2 không có đáp án đúng thì giải thưởng Nokia Lumia 1520 sẽ được cộng dồn vào chương trình kế tiếp)</span><br/>
<span class='bold'>b. Đối với người chơi tại nhà trong Phần 1 – CÙNG LÀ TỶ PHÚ</span><br/>
&nbsp;&nbsp;&nbsp;&nbsp;- Top 10 người chơi cao nhất trong ngày: 1 điện thoại Nokia X trị giá 2,5 triệu đồng/giải<br/>
<span class='bold'>c. Đối với người tham gia quay số may mắn trong Phần 2 – TỶ PHÚ KHÔNG ĐỘ</span><br/>
&nbsp;&nbsp;&nbsp;&nbsp;-	20 giải điện thoại Nokia Lumia 630 <br/>
&nbsp;&nbsp;&nbsp;&nbsp;-	208 nón bảo hiểm sành điệu<br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span class='bold red'>- 01 giải đặc biệt 1 TỶ đồng ở chương trình 24</span><br/>
<span class='italic'>( Khách hàng nhận giải có trách nhiệm đóng 10% thuế thu nhập dựa trên giá trị giải thưởng khi nhận giải theo quy định của pháp luật )</span>
<span class='bold'>2.	Trong chương trình GALA thứ 25, cơ cấu giải thưởng mỗi chương trình:</span><br/>
<span class='bold'>a. Đối với thí sinh tại phim trường trong Phần 1 – CÙNG LÀ TỶ PHÚ</span><br/>
&nbsp;&nbsp;&nbsp;&nbsp;<span class='red bold'>-	01 giải đặc biệt: trị giá 1 TỶ đồng cho người chiến thắng</span><br/>
<span class='italic'>( Khách hàng nhận giải có trách nhiệm đóng 10% thuế thu nhập dựa trên giá trị giải thưởng khi nhận giải theo quy định của pháp luật )</span><br/>
&nbsp;&nbsp;&nbsp;&nbsp;-	01 giải nhất: 1 điện thoại Nokia Lumia 1520 trị giá 13 triệu đồng cho người trả lời đúng và nhanh sau người chiến thắng<br/>
&nbsp;&nbsp;&nbsp;&nbsp;-	02 giải nhì: 1 điện thoại Nokia Lumia 630 trị giá 3,5 triệu đồng/giải cho 2 thí sinh còn lại có đáp án đúng<br/>
&nbsp;&nbsp;&nbsp;&nbsp;-	50 giải khuyến khích: trị giá 1 triệu/giải cho 50 thí sinh<br/>
<span class='italic'>(Trong trường hợp 4 thí sinh ở vòng 2 không có đáp án đúng thì giải thưởng 1 Tỷ đồng trong chương trình Gala sẽ được đóng góp vào quỹ từ thiện)</span><br/>
<span class='bold'>b. Đối với người chơi tại nhà trong Phần 1 – CÙNG LÀ TỶ PHÚ</span><br/>
&nbsp;&nbsp;&nbsp;&nbsp;- Top 10 người chơi cao nhất trong ngày: 1 điện thoại Nokia X trị giá 2,5 triệu đồng/giải<br/>
<span class='bold italic underline'>Chú ý: Mọi chi phí liên quan cho việc nhận giải thưởng, khách hàng sẽ tự chịu trách nhiệm </span><br/>
<br/>
<h4 class='red bold'>QUY ĐỊNH THAM GIA CHƯƠNG TRÌNH </h4>
<span class='bold'>1.	Người chơi</span><br/>
- Người chơi có trách nhiệm tìm hiểu kỹ thể lệ của chương trình và tuân thủ tuyệt đối các quy định đã đề ra.<br/>
- Tất cả nhân viên Công ty Cổ phần Truyền thông Đa phương tiện Latsata  không được phép tham gia chương trình “Cùng Là Tỷ Phú”.<br/>
- Tất cả nhân viên Công ty TNHH TM – DV Tân Hiệp Phát không được phép tham gia chương trình “Tỷ Phú Không Độ”.<br/>
<span class='bold'>2.	Sở hữu thông tin</span><br/>
- Công ty Cổ phần Truyền thông Đa phương tiện Latsata  ( Ban tổ chức) là đại diện hợp pháp và duy nhất của chương trình, mọi quyết định của Ban tổ chức (bao gồm cả việc dừng sản xuất chương trình) là quyết định cuối cùng.<br/>
- Ban tổ chức có toàn quyền quyết định việc sử dụng thông tin và hình ảnh của thí sinh, và người chơi ở nhà trúng thưởng vào các hoạt động truyền thông trước, trong và sau cuộc thi, đồng thời không phải trả bất cứ khoản thù lao nào cho việc sử dụng những hình ảnh đó và không cần sự đồng ý của người trúng thưởng.<br/>
<span class='bold'>3.	Trả thưởng</span><br/>
- Tất cả người chơi phải đăng kí thông tin tài khoản trên ứng dụng “Cùng Là Tỷ Phú” để Ban tổ chức đảm bảo quyền lợi nhận giải thưởng. <br/>
- Khi đến nhận thưởng, người chơi phải mang theo bản chính và bản photo CMND và thiết bị đang chơi có ID  trong top 10 của chương trình.<br/>
- Trường hợp người nhận thưởng ở xa không thể đi được, BTC sẽ liên hệ và gửi giải thưởng qua đường bưu điện. <br/>
- Ở bất kì thời điểm nào, nếu Ban tổ chức phát hiện ra có sự gian lận, hoặc vi phạm luật chơi, giải thưởng sẽ bị giữ lại hoặc thu hồi, người chơi sẽ bị tước quyền nhận thưởng.<br/>
- Mọi chi phí liên quan cho việc nhận giải thưởng, khách hàng sẽ tự chịu trách nhiệm.<br/>
<span class='bold'>4.	Các quy định khác</span><br/>
- Ban tổ chức có quyền thay đổi về quy trình, kế hoạch và quy định vào bất kỳ lúc nào để đảm bảo việc giải quyết những phát sinh phù hợp với thực tế diễn biến của toàn bộ chương trình.<br/>
- Thí sinh phải có trang phục lịch sự, đẹp, phù hợp văn hóa, thuần phong mỹ tục của Việt Nam và đúng yêu cầu của BTC.<br/>
- Ban tổ chức sẽ không chịu trách nhiệm về những tổn thất về vật chất của thí sinh phát sinh chủ quan từ phía thí sinh trong quá trình vận chuyển hoặc đang tham dự chương trình.<br/>
- Ban tổ chức có quyền huỷ bỏ các ID ( mã số ) của người chơi nếu đối với các trường hợp: vi phạm thể lệ cuộc thi; thực hiện hành vi gian lận, thiếu trung thực khi tham gia chương trình; có hành động gây ảnh hưởng đến hình ảnh cá nhân cũng như hình ảnh chương trình.<br/>
<span class='bold'>Chi tiết:</span><a style='color:#fa1a1a' href='http://cunglatyphu.com/giai-thuong/'>http://cunglatyphu.com/giai-thuong/</a><br/>
<div style= 'margin:0 auto; padding:10px; width:100%; text-align: center'><span class='bold red'>Chi tiết:</span> <a style='color:#fa1a1a' href='http://cunglatyphu.com/'>http://cunglatyphu.com/</a><br/>
  <a style='color:#fa1a1a' href='www.facebook.com/cunglatyphu'>www.facebook.com/cunglatyphu</a></div>
<div style='height:30px; clear:both'></div>
<div style='text-align:center;width:100%;'>
  <image src='http://vietdev.vn/images/02-typhukhongdo-withtext.png' alt='logo' />
</div>
</body>
</html>

");
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            MediaPlayer.Stop();
            base.OnNavigatedFrom(e);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            SoundPlayer.PlayMediaBackground("Term", SoundPlayer.opening);
            base.OnNavigatedTo(e);
        }

        private void btBack_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            soundeffect = SoundPlayer.PlaySound(SoundPlayer.chooseAnswer);
            NavigationService.Navigate(new Uri("/View/MenuPage.xaml", UriKind.Relative));
        }
    }
}