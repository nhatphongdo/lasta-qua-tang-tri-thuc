﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace lasta_windowsphone
{
    public class RowTypeTemplateSelector : DataTemplateSelector
    {
        public DataTemplate even
        {
            get;
            set;
        }

        public DataTemplate odd
        {
            get;
            set;
        }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            lasta_windowsphone.User singleUser = item as lasta_windowsphone.User;
            if (singleUser != null)
            {
                if (singleUser.Type == "odd")
                {
                    return odd;
                }
                else
                {
                    return even;
                }
            }
            return base.SelectTemplate(item, container);
        }

    }


    public class RowData
    {
        public char CHAR1 { get; set; }
        public char CHAR2 { get; set; }
        public char CHAR3 { get; set; }
        public char CHAR4 { get; set; }
        public char CHAR5 { get; set; }
        public char CHAR6 { get; set; }
        public char CHAR7 { get; set; }
        public char CHAR8 { get; set; }
        public char CHAR9 { get; set; }
        public char CHAR10 { get; set; }
        public char CHAR11 { get; set; }
        public char CHAR12 { get; set; }
        public char CHAR13 { get; set; }
        public char CHAR14 { get; set; }
        public char CHAR15 { get; set; }
        public char CHAR16 { get; set; }
        public char CHAR17 { get; set; }
        public char CHAR18 { get; set; }
        public char CHAR19 { get; set; }
        public char CHAR20 { get; set; }
        public char CHAR21 { get; set; }
        public char CHAR22 { get; set; }
        public char CHAR23 { get; set; }
        public char CHAR24 { get; set; }
        public char CHAR25 { get; set; }
        public char CHAR26 { get; set; }

        public RowData(char char1, char char2, char char3, char char4, char char5, char char6, char char7, char char8, char char9, char char10, char char11, char char12, char char13, char char14, char char15, char char16, char char17, char char18, char char19, char char20, char char21, char char22, char char23, char char24, char char25, char char26)
        {
            this.CHAR1 = char1;
            this.CHAR2 = char2;
            this.CHAR3 = char3;
            this.CHAR4 = char4;
            this.CHAR5 = char5;
            this.CHAR6 = char6;
            this.CHAR7 = char7;
            this.CHAR8 = char8;
            this.CHAR9 = char9;
            this.CHAR10 = char10;
            this.CHAR11 = char11;
            this.CHAR12 = char12;
            this.CHAR13 = char13;
            this.CHAR14 = char14;
            this.CHAR15 = char15;
            this.CHAR16 = char16;
            this.CHAR17 = char17;
            this.CHAR18 = char18;
            this.CHAR19 = char19;
            this.CHAR20 = char20;
            this.CHAR21 = char21;
            this.CHAR22 = char22;
            this.CHAR23 = char23;
            this.CHAR24 = char24;
            this.CHAR25 = char25;
            this.CHAR26 = char26;
        }
    }
}
