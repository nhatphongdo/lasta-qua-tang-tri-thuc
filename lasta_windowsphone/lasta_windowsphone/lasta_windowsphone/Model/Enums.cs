﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lasta_windowsphone
{
    public static class Enums
    {
        public static string ApplicationTitle = "Cùng là tỷ phú";
        public static string MessageBoxTitle = "Cùng là tỷ phú";
        public static string USERID_KEY = "UserID";
        public static string USER_FULLNAME_KEY = "UserFullname";
        public static string USER_EMAIL_KEY = "UserEmail";
        public static string USER_SCORE_KEY = "UserScore";
        public static string DEVICEID_KEY = "DeviceID";
        public static string USER_DAY_SCORE_KEY = "UserDayScore";
        public static string NUMBER_QUESTIONS_KEY = "NumberOfQuestions";
        public static string QUESTION_NUMBER_KEY = "QuestionNumber";
        public static string RANK_KEY = "Rank";
        public static string DAY_RANK_KEY = "DayRank";
        public static string PLATFORM_WP = "wp";
        public static int timeout = 5000; //TimeOut miliseconds
        public static int totalDisconnect = 5;

#if STUDIO_VERSION
        //public static string ROOT_URL = "http://web.vietdev.vn/lasta/api/";
        public static string ROOT_URL = "http://192.168.1.2/api/";
        public static string ANSWER_URL = ROOT_URL + "answer/{0}/{1}/{2}/{3}/{4}/{5}/{6}";
        public static string REGISTER_USER_URL = ROOT_URL + "users/register/{0}";
        public static string GET_USER_URL = ROOT_URL + "users/{0}";
        public static string TOP_USER_URL = ROOT_URL + "topusers/{0}";
        public static string CHECK_ANSWER = ROOT_URL + "answer/check/{0}"; //Check answer by ID 0:ID

        //Fake Api
        public static string EXCHANGE_SCORE_URL = "";
        public static string TRANSFER_SCORE_URL = "";
        public static string UPDATE_USER_INFO_URL = "";
        public static string SETTOKEN_NOTIFICATION_URL = "";
        public static string SEARCH_BY_NAME = "";
        public static string DAY_TOP_USER_URL = "";
        public static string MESSAGE_GREETING = "";
        public static string GET_TRANSFER_CODE_URL = "";

#else
        public static string ROOT_URL = "http://cunglatyphu.vn/api/";
        //public static string ROOT_URL = "http://web.vietdev.vn/lasta/api/";
        public static string REGISTER_USER_URL = ROOT_URL + "homeusers/register/{0}"; // 0: string Device ID
        public static string ANSWER_URL = ROOT_URL + "homeanswer/{0}/{1}/{2}/{3}/{4}/{5}/{6}"; // 0:string ID from QR code, 1: long UserID, 2: int Dap an, 3: long Thoi gian bat dau tra loi, 4: long thời gian kết thúc, 5: int Thoi gian tra loi tinh bang giay, 6: int Diem so
        public static string GET_USER_URL = ROOT_URL + "homeusers/{0}"; //0: long User ID
        public static string TOP_USER_URL = ROOT_URL + "topusers/home/{0}"; // int 
        public static string DAY_TOP_USER_URL = ROOT_URL + "topusers/dailyhome/{0}"; // 0: Top  
        public static string EXCHANGE_SCORE_URL = ROOT_URL + "common/exchange/{0}/{1}"; //0:code, 1:userId
        public static string UPDATE_USER_INFO_URL = ROOT_URL + "homeusers/update/{0}/{1}/{2}/{3}/{4}/{5}/{6}"; //Update user info 0:deviceId, 1:userId, 2:fullName, 3:phoneNumber, 4:email, 5:address 6:bool is notification
        public static string SETTOKEN_NOTIFICATION_URL = ROOT_URL + "homeusers/setToken/{0}/{1}/{2}"; //Set token for home user 0:deviceId, 1:userId, 2:token
        public static string SEARCH_BY_NAME = ROOT_URL + "homeusers/search/{0}"; //Search in summary by fullName 0:FullName
        public static string CHECK_ANSWER = ROOT_URL + "homeanswer/check/{0}"; //Check answer by ID 0:ID
        public static string MESSAGE_GREETING = ROOT_URL + "common/message/greeting";
        public static string GET_TRANSFER_CODE_URL = ROOT_URL + "homeusers/transfercode/{0}/{1}"; // 0: userID, 1 DeviceID
        public static string TRANSFER_SCORE_URL = ROOT_URL + "common/transfer/{0}/{1}/{2}/{3}/{4}"; // 0: long UserID, 1: string DeviceID, 2: long receiverID, 3: receiver code(ma so nhan diem lay o link ), 4: long score

#endif
        public static string CONTEST_DAY_URL = ROOT_URL + "contests/{0}"; // 0: string ID from QR code 
        public static string CONTEST_DAY_PRIZES_URL = ROOT_URL + "contests/{0}/prizes"; // 0 : string ID from QR code
        public static string STATE_URL = ROOT_URL + "states/{0}"; // 0: string ID from QR code
        public static string QUESTION_URL = ROOT_URL + "questions/{0}/{1}"; // 0:string ID from QR code, 1: int STT cau hoi
        public static string CONTEST_TODAY_URL = ROOT_URL + "contests/today";
        public static string PLATFORM_URL = ROOT_URL + "common/version/{0}"; // check platform(windowsphone,android,ios) return version 0:platform
    }
}
