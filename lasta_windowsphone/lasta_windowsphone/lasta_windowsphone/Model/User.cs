﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lasta_windowsphone
{
    public class User
    {
        public string ID { get; set; }
        public string Fullname { get; set; }
        public string Score { get; set; }
        public string STT { get; set; }
        public string Type { get; set; }

        public User(string stt, string id, string fullname, string score, string type)
        {
            this.ID = id;
            this.Fullname = fullname;
            this.Score = score;
            this.STT = stt;
            this.Type = type;
        }
    }
}
