﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using System.Windows.Resources;

namespace lasta_windowsphone
{
    public static class SoundPlayer
    {
        public static string switchScreen = @"Assets/sounds/chuyen_canh.wav";
        public static string countDown = @"Assets/sounds/15s.mp3";
        public static string faceSad = @"Assets/sounds/Face_sad.mp3";
        public static string faceSmile = @"Assets/sounds/Face_Smile.mp3";
        public static string opening = @"Assets/sounds/opening_15s.mp3";
        public static string chooseAnswer = @"Assets/sounds/bam_dap_an_hoac_dong_ho.wav";
        public static string background = @"Assets/sounds/Backround_hoi_hop.mp3";

        private static SoundEffectInstance stopSound;
        public static SoundEffectInstance PlaySound(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                using (var stream = TitleContainer.OpenStream(path))
                {
                    if (stream != null)
                    {
                        var effect = SoundEffect.FromStream(stream);
                        stopSound = effect.CreateInstance();
                        FrameworkDispatcher.Update();
                        stopSound.Play();
                    }
                }
            }
            return stopSound;
        }

        public static void PlayMediaBackground(string name, string patch)
        {
            Song song;
            song = Song.FromUri(name, new Uri(patch, UriKind.Relative));
            FrameworkDispatcher.Update();
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(song);
        }

        public static void PlayMediaBackgroundNoRep(string name, string patch)
        {
            Song song;
            song = Song.FromUri(name, new Uri(patch, UriKind.Relative));
            FrameworkDispatcher.Update();
            MediaPlayer.IsRepeating = false;
            MediaPlayer.Play(song);
        }
    }
}
