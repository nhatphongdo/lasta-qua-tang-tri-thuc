﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.IO.IsolatedStorage;
using Newtonsoft.Json.Linq;
using System.Windows;
using System.Xml;
using System.Reflection;
using System.Windows.Threading;

namespace lasta_windowsphone
{
    public static class Function
    {
        public static bool IsValidEmail(string strIn)
        {
            // Return true if strIn is in valid e-mail format.
            return Regex.IsMatch(strIn,
                   @"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                   @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$");
        }

        public static string ReplaceUrlString(string str)
        {
            //Replace str
            return str.Replace("/", "==").Replace("+", "===");
        }

        public static void UpdateUserData()
        {
            IsolatedStorageSettings appData = IsolatedStorageSettings.ApplicationSettings;
            long userId = 0;
            if (appData.Contains(Enums.USERID_KEY))
            {
                userId = (long)appData[Enums.USERID_KEY];
            }
            if (userId > 0)
            {
                WebClient userInfo = new WebClient();
                userInfo.DownloadStringCompleted += new DownloadStringCompletedEventHandler(userInfo_downloadstringcompleted);
                userInfo.DownloadStringAsync(new Uri(string.Format(Enums.GET_USER_URL, userId)));
            }
        }

        private static void userInfo_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            IsolatedStorageSettings appData = IsolatedStorageSettings.ApplicationSettings;
            try
            {
                JObject obj = JObject.Parse(e.Result);
                if (appData.Keys.Count > 1)
                {
                    appData[Enums.USER_EMAIL_KEY] = obj["Email"] != null ? obj["Email"].ToString() : "";
                    appData[Enums.USER_FULLNAME_KEY] = obj["Fullname"] != null ? obj["Fullname"].ToString() : "";
                    appData[Enums.USER_SCORE_KEY] = obj["Score"] != null ? obj["Score"].ToString() : "0";
                    //appData[Enums.USER_DAY_SCORE_KEY] = obj["DayScore"] != null ? obj["DayScore"].ToString() : "0";
                    appData[Enums.RANK_KEY] = obj["Rank"] != null ? obj["Rank"].ToString() : "";
                    appData[Enums.DAY_RANK_KEY] = obj["DayRank"] != null ? obj["DayRank"].ToString() : "";
                    appData.Save();
                }
                else
                {
                    appData.Add(Enums.USER_EMAIL_KEY, obj["Email"] == null ? "" : obj["Email"].ToString());
                    appData.Add(Enums.USER_FULLNAME_KEY, obj["Fullname"] == null ? "" : obj["Fullname"].ToString());
                    appData.Add(Enums.USER_SCORE_KEY, obj["Score"] == null ? "0" : obj["Score"].ToString());
                    //appData.Add(Enums.USER_DAY_SCORE_KEY, obj["DayScore"] == null ? "0" : obj["DayScore"].ToString());
                    appData.Add(Enums.RANK_KEY, obj["Rank"] == null ? "0" : obj["Rank"].ToString());
                    appData.Add(Enums.DAY_RANK_KEY, obj["DayRank"] == null ? "0" : obj["DayRank"].ToString());
                    appData.Save();
                }
            }
            catch (WebException ex)
            {
            }
        }

        public static string GetVersionNumber()
        {
            var asm = Assembly.GetExecutingAssembly();
            var parts = asm.FullName.Split(',');
            return parts[1].Split('=')[1];
        }

        public static void GetGreenMessage()
        {
            WebClient greenMessage = new WebClient();
            greenMessage.DownloadStringCompleted += new DownloadStringCompletedEventHandler(greenmessage_downloadstringcompleted);
            greenMessage.DownloadStringAsync(new Uri(Enums.MESSAGE_GREETING));
        }

        private static void greenmessage_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                JObject obj = JObject.Parse(e.Result);
                string message = (obj["Value"] ?? "").ToString();
                MessageBoxResult result = MessageBox.Show(message, Enums.ApplicationTitle, MessageBoxButton.OK);
            }
            catch (WebException ex)
            {

            }
        }

    }

}
