﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Net.NetworkInformation;
using Newtonsoft.Json.Linq;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Notification;
using System.Text;
using Microsoft.Phone.Info;
using Microsoft.Phone.Scheduler;

namespace lasta_windowsphone
{
    public partial class MainPage : PhoneApplicationPage
    {
        IsolatedStorageSettings appstores = IsolatedStorageSettings.ApplicationSettings;
        string DeviceID = "";
        long userid = 0;
        private bool isCheckVersion, isRegister, isUpdateInfo;
        private PeriodicTask periodicTask;

        private string periodicTaskName = "NotificationTask";
        public bool agentsAreEnabled = true;

        // Constructor
        public MainPage()
        {
            InitializeComponent();
            GoogleAnalytics.EasyTracker.GetTracker().SendView("MainPage");
            isCheckVersion = false;
            isRegister = false;
            isUpdateInfo = false;
            if (!appstores.Contains(Enums.QUESTION_NUMBER_KEY))
            {
                appstores.Add(Enums.QUESTION_NUMBER_KEY, 0);
                appstores.Save();
            }
            else
            {
                appstores[Enums.QUESTION_NUMBER_KEY] = 0;
                appstores.Save();
            }

            Byte[] DeviceArrayID = (Byte[])Microsoft.Phone.Info.DeviceExtendedProperties.GetValue("DeviceUniqueId");
            DeviceID = Convert.ToBase64String(DeviceArrayID);

#if HOME_VERSION
            StartBackgroundAgent();
            checkVersion();

#elif STUDIO_VERSION
            //checkVersion();
            isCheckVersion = true;
            registerUser();
#endif
        }

        void registerUser()
        {
            //check internet connection
            if (NetworkInterface.GetIsNetworkAvailable())
            {
                //register user
                if (!appstores.Contains(Enums.DEVICEID_KEY))
                {
                    appstores.Add(Enums.DEVICEID_KEY, DeviceID);
                    appstores.Save();
                }
                WebClient webclient = new WebClient();
                webclient.DownloadStringCompleted += new DownloadStringCompletedEventHandler(webclient_downloadstringcompleted);
                webclient.DownloadStringAsync(new Uri(string.Format(Enums.REGISTER_USER_URL, Uri.EscapeDataString(Function.ReplaceUrlString(DeviceID)))));
            }
            else
            {
                btReconnect.Visibility = Visibility.Visible;
                isRegister = true;
                //MessageBoxResult m = MessageBox.Show("Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại.", Enums.ApplicationTitle, MessageBoxButton.OK);
                //if (m == MessageBoxResult.OK)
                //{
                //    // reconnetion
                //    registerUser();
                //}
            }
        }

        void webclient_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                JObject obj = JObject.Parse(e.Result);
                userid = (long)obj["ID"];
                isRegister = false;
                if ((int)obj["Error"] == -1)
                {
                    long checkUserId = 0;
                    if (appstores.Contains(Enums.USERID_KEY) && appstores[Enums.USERID_KEY] != null)
                    {
                        checkUserId = long.Parse(appstores[Enums.USERID_KEY].ToString());
                    }
                    else
                    {
                        checkUserId = 0;
                    }
                    if (checkUserId != userid)
                    {
                        //Exit user with the same device
                        MessageBoxResult m = MessageBox.Show(string.Format("Thiết bị đã được đăng ký trước đây với mã số '{0}'. Chương trình sẽ tự động kết nối lại với mã số này.", userid), Enums.MessageBoxTitle, MessageBoxButton.OK);
                        if (m == MessageBoxResult.OK)
                        {
                            appstores[Enums.USERID_KEY] = (long)obj["ID"];
                            if (!appstores.Contains(Enums.DEVICEID_KEY))
                            {
                                appstores.Add(Enums.DEVICEID_KEY, DeviceID);
                            }
                            appstores.Save();
                        }
                    }
                    updateUserInfo(userid);

                }
                else if ((int)obj["Error"] == -2)
                {
                    //Database Error
                    MessageBoxResult m = MessageBox.Show("Chương trình xảy ra lỗi trong quá trình xử lý. Vui lòng thông báo tới BTC để được hướng dẫn xử lý và hỗ trợ.", Enums.MessageBoxTitle, MessageBoxButton.OK);
                }
                else
                {
                    if (!appstores.Contains(Enums.USERID_KEY) && !appstores.Contains(Enums.DEVICEID_KEY))
                    {
                        appstores.Add(Enums.USERID_KEY, (long)obj["ID"]);
                        appstores.Add(Enums.DEVICEID_KEY, DeviceID);
                        appstores.Save();
                    }
                    else
                    {
                        appstores[Enums.USERID_KEY] = (long)obj["ID"];
                        appstores[Enums.DEVICEID_KEY] = DeviceID;
                        appstores.Save();
                    }
                    updateUserInfo(userid);
                }
            }
            catch (WebException ex)
            {
                //MessageBoxResult m = MessageBox.Show("Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại.", Enums.ApplicationTitle, MessageBoxButton.OK);
                //if (m == MessageBoxResult.OK)
                //{
                //    // reconnetion
                //    registerUser();
                //}
                btReconnect.Visibility = Visibility.Visible;
                isRegister = true;
            }
        }

        private void updateUserInfo(long userId)
        {
            WebClient webclient2 = new WebClient();
            webclient2.DownloadStringCompleted += new DownloadStringCompletedEventHandler(webclient2_downloadstringcompleted);
            webclient2.DownloadStringAsync(new Uri(string.Format(Enums.GET_USER_URL, userid)));
        }

        void webclient2_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                JObject obj = JObject.Parse(e.Result);
                isUpdateInfo = false;
                if (appstores.Keys.Count > 1)
                {
                    appstores[Enums.USER_EMAIL_KEY] = obj["Email"] != null ? obj["Email"].ToString() : "";
                    appstores[Enums.USER_FULLNAME_KEY] = obj["Fullname"] != null ? obj["Fullname"].ToString() : "";
                    appstores[Enums.USER_SCORE_KEY] = obj["Score"] != null ? obj["Score"].ToString() : "0";
                    appstores[Enums.USER_DAY_SCORE_KEY] = obj["DayScore"] != null ? obj["DayScore"].ToString() : "0";
                    appstores[Enums.RANK_KEY] = obj["Rank"] != null ? obj["Rank"].ToString() : "";
                    appstores[Enums.DAY_RANK_KEY] = obj["DayRank"] != null ? obj["DayRank"].ToString() : "";
                    appstores.Save();
                }
                else
                {
                    appstores.Add(Enums.USER_EMAIL_KEY, obj["Email"] == null ? "" : obj["Email"].ToString());
                    appstores.Add(Enums.USER_FULLNAME_KEY, obj["Fullname"] == null ? "" : obj["Fullname"].ToString());
                    appstores.Add(Enums.USER_SCORE_KEY, obj["Score"] == null ? "0" : obj["Score"].ToString());
                    appstores.Add(Enums.USER_DAY_SCORE_KEY, obj["DayScore"] == null ? "0" : obj["DayScore"].ToString());
                    appstores.Add(Enums.RANK_KEY, obj["Rank"] == null ? "" : obj["Rank"].ToString());
                    appstores.Add(Enums.DAY_RANK_KEY, obj["DayRank"] == null ? "" : obj["DayRank"].ToString());
                    appstores.Save();
                }
                //transfer to MenuPage
                NavigationService.Navigate(new Uri("/View/MenuPage.xaml?firstload=true", UriKind.RelativeOrAbsolute));

            }
            catch (WebException ex)
            {

                if (((HttpWebResponse)ex.Response).StatusCode == HttpStatusCode.NotFound)
                {
                    MessageBoxResult m = MessageBox.Show("Tài khoản này đã bị vô hiệu hóa. Vui lòng đăng ký lại tài khoản mới hoặc liên hệ với BTC để được hỗ trợ. CHÚ Ý: Tài khoản mới sẽ không còn lưu trữ các thông tin cá nhân và điểm số hiện tại!", Enums.MessageBoxTitle, MessageBoxButton.OK);
                    if (m == MessageBoxResult.OK)
                    {
                        //remove UserID on device
                        appstores.Remove(Enums.USERID_KEY);
                        appstores.Save();
                        // reconnetion
                        registerUser();
                    }
                }
                else
                {
                    btReconnect.Visibility = Visibility.Visible;
                    isUpdateInfo = true;
                }
            }
        }

        void PushChannel_ChannelUriUpdated(object sender, NotificationChannelUriEventArgs e)
        {
            Dispatcher.BeginInvoke(() =>
            {
#if HOME_VERSION
                if (appstores.Contains(Enums.USERID_KEY) && appstores.Contains(Enums.DEVICEID_KEY))
                {
                    long userId = long.Parse(appstores[Enums.USERID_KEY].ToString());
                    string deviceKey = appstores[Enums.DEVICEID_KEY].ToString();
                    try
                    {
                        WebClient updateToken = new WebClient();
                        updateToken.DownloadStringCompleted += new DownloadStringCompletedEventHandler(updateToken_downloadstringcompleted);
                        updateToken.DownloadStringAsync(new Uri(string.Format(Enums.SETTOKEN_NOTIFICATION_URL, Uri.EscapeDataString(Function.ReplaceUrlString(deviceKey)), userId.ToString(), Uri.EscapeDataString(Function.ReplaceUrlString(e.ChannelUri.ToString())))));
                    }
                    catch (WebException ex)
                    {
                        MessageBoxResult m = MessageBox.Show("Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại.", Enums.ApplicationTitle, MessageBoxButton.OK);
                        if (m == MessageBoxResult.OK)
                        {
                            // reconnetion
                        }
                    }
                }
#endif
                // Display the new URI for testing purposes.   Normally, the URI would be passed back to your web service at this point.
                //System.Diagnostics.Debug.WriteLine(e.ChannelUri.ToString());
                //MessageBox.Show(String.Format("Channel Uri is {0}",
                //    e.ChannelUri.ToString()));

            });
        }

        void PushChannel_ErrorOccurred(object sender, NotificationChannelErrorEventArgs e)
        {
            // Error handling logic for your particular application would be here.
            //Dispatcher.BeginInvoke(() =>
            //    MessageBox.Show(String.Format("A push notification {0} error occurred.  {1} ({2}) {3}",
            //        e.ErrorType, e.Message, e.ErrorCode, e.ErrorAdditionalData))
            //        );
        }

        void PushChannel_ShellToastNotificationReceived(object sender, NotificationEventArgs e)
        {
            StringBuilder message = new StringBuilder();
            string relativeUri = string.Empty;

            // Parse out the information that was part of the message.
            foreach (string key in e.Collection.Keys)
            {
                message.AppendFormat("{0}: {1}\n", key, e.Collection[key]);

                if (string.Compare(
                    key,
                    "wp:Param",
                    System.Globalization.CultureInfo.InvariantCulture,
                    System.Globalization.CompareOptions.IgnoreCase) == 0)
                {
                    relativeUri = e.Collection[key];
                }
            }

            // Display a dialog of all the fields in the toast.
            Dispatcher.BeginInvoke(() => MessageBox.Show(message.ToString()));

        }

        private void updateToken_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                JObject obj = JObject.Parse(e.Result);
                //todo: Return data for process
                int error = (int)obj["error"];
                long userId = (long)obj["ID"];
                switch (error)
                {
                    case -2:
                        break;
                    case 0:
                        break;
                }
            }
            catch (WebException ex)
            {
                //MessageBoxResult m = MessageBox.Show("Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại.", Enums.ApplicationTitle, MessageBoxButton.OK);
                //if (m == MessageBoxResult.OK)
                //{
                //    // reconnetion
                //}
            }
        }

        private void checkVersion()
        {
            WebClient checkver = new WebClient();
            checkver.DownloadStringCompleted += new DownloadStringCompletedEventHandler(checkver_downloadstringcompleted);
            checkver.DownloadStringAsync(new Uri(string.Format(Enums.PLATFORM_URL, Enums.PLATFORM_WP)));
        }

        private void checkver_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                JObject obj = JObject.Parse(e.Result);
                string version = (string)obj["Value"];
                //todo: get current version of wp app
                string currentVersion = Function.GetVersionNumber();
                isCheckVersion = false;
                if (!currentVersion.Equals(version))
                {
                    MessageBoxResult m = MessageBox.Show(string.Format("Ứng dụng \"Cùng Là Tỷ Phú\" đã được cập nhật lên phiên bản {0} bạn hãy lên Windowsphone Stores để cập nhật lại ứng dụng", version), Enums.MessageBoxTitle, MessageBoxButton.OK);
                    if (m == MessageBoxResult.OK)
                    {
                        //todo: process if version not update
                    }
                }
                else
                {
                    registerUser();
                }
            }
            catch (WebException ex)
            {
                if (isCheckVersion == false)
                {
                    MessageBoxResult m = MessageBox.Show("Hệ thống đang bảo trì, các bạn vui lòng quay lại sau.", Enums.ApplicationTitle, MessageBoxButton.OK);
                    if (m == MessageBoxResult.OK)
                    {
                        //todo: process if network invalid   
                    }
                }
                isCheckVersion = true;
                btReconnect.Visibility = Visibility.Visible;
            }
        }

        private void btReconnect_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
#if HOME_VERSION
            if (isCheckVersion == true)
            {
                checkVersion();
            }
            else if (isRegister == true)
            {
                registerUser();
            }
            else if (isUpdateInfo == true)
            {
                updateUserInfo(userid);
            }
            (sender as Button).Visibility = Visibility.Collapsed;
#else
            if (isRegister == true)
            {
                registerUser();
            }
            else if (isUpdateInfo == true)
            {
                updateUserInfo(userid);
            }
            (sender as Button).Visibility = Visibility.Collapsed;
#endif
        }

        private void StartBackgroundAgent()
        {

            // Variable for tracking enabled status of background agents for this app.
            agentsAreEnabled = true;

            // Obtain a reference to the period task, if one exists
            periodicTask = ScheduledActionService.Find(periodicTaskName) as PeriodicTask;

            // If the task already exists and background agents are enabled for the
            // application, you must remove the task and then add it again to update 
            // the schedule
            if (periodicTask != null)
            {
                RemoveAgent(periodicTaskName);
            }

            periodicTask = new PeriodicTask(periodicTaskName);

            periodicTask.Description = "This Notification background agent run as a periodic task.";

            // Place the call to Add in a try block in case the user has disabled agents.
            try
            {
                ScheduledActionService.Add(periodicTask);
                //ScheduledActionService.LaunchForTest(periodicTaskName, TimeSpan.FromSeconds(10));
            }
            catch (InvalidOperationException exception)
            {
                if (exception.Message.Contains("BNS Error: The action is disabled"))
                {
                    MessageBox.Show("Background agents for this application have been disabled by the user.");
                    agentsAreEnabled = false;
                }
                if (exception.Message.Contains("BNS Error: The maximum number of ScheduledActions of this type have already been added."))
                {
                    // No user action required. The system prompts the user when the hard limit of periodic tasks has been reached.
                }
            }
            catch (SchedulerServiceException)
            {
                // No user action required.
            }
        }

        private void RemoveAgent(string name)
        {
            try
            {
                ScheduledActionService.Remove(name);
            }
            catch (Exception)
            {
            }
        }

    }
}