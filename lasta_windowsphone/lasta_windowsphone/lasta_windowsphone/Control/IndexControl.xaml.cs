﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using Newtonsoft.Json.Linq;
using System.ComponentModel;

namespace lasta_windowsphone.Control
{
    public partial class IndexControl : UserControl
    {
        public IndexControl()
        {
            InitializeComponent();
            loadData();
        }

        private void loadData()
        {
            if (!DesignerProperties.IsInDesignTool)
            {
                IsolatedStorageSettings appData = IsolatedStorageSettings.ApplicationSettings;
                long userId = 0;
                if (appData.Contains(Enums.USERID_KEY))
                {
                    userId = long.Parse(appData[Enums.USERID_KEY].ToString());
                    txtControlUserID.Text = userId.ToString();
                    txtDayScore.Text = appData.Contains(Enums.USER_DAY_SCORE_KEY) ? (appData[Enums.USER_DAY_SCORE_KEY] ?? "0").ToString() : "0";
                    txtControlTotalScore.Text = appData.Contains(Enums.USER_SCORE_KEY) ? (appData[Enums.USER_SCORE_KEY] ?? "0").ToString() : "0";
                    txtControlIndexDay.Text = appData.Contains(Enums.DAY_RANK_KEY) ? (appData[Enums.DAY_RANK_KEY] ?? "0").ToString() : "0";
                    txtControlIndex.Text = appData.Contains(Enums.RANK_KEY) ? (appData[Enums.RANK_KEY] ?? "0").ToString() : "0";
                }
                //if (userId > 0)
                //{
                //    WebClient userInfo = new WebClient();
                //    userInfo.DownloadStringCompleted += new DownloadStringCompletedEventHandler(userInfo_downloadstringcompleted);
                //    userInfo.DownloadStringAsync(new Uri(string.Format(Enums.GET_USER_URL, userId)));
                //}
            }
        }

        //private void userInfo_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        //{
        //    if (!DesignerProperties.IsInDesignTool)
        //    {
        //        try
        //        {
        //            IsolatedStorageSettings appData = IsolatedStorageSettings.ApplicationSettings;
        //            JObject obj = JObject.Parse(e.Result);
        //            txtControlUserID.Text = Convert.ToString(obj["ID"]);
        //            txtControlTotalScore.Text = appData.Contains(Enums.USER_SCORE_KEY) ? appData[Enums.USER_SCORE_KEY].ToString() : "0";
        //            txtDayScore.Text = appData.Contains(Enums.USER_DAY_SCORE_KEY) ? appData[Enums.USER_DAY_SCORE_KEY].ToString() : "0";
        //            txtControlIndex.Text = Convert.ToString(obj["Rank"]);
        //        }
        //        catch (WebException ex)
        //        {

        //        }
        //    }
        //}
    }
}
