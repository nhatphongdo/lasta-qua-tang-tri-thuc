﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using System.ComponentModel;

namespace lasta_windowsphone.Control
{
    public partial class detailUserControl : UserControl
    {

        public detailUserControl()
        {
            InitializeComponent();
            loadData();
        }

        private void loadData()
        {
            if (!DesignerProperties.IsInDesignTool)
            {
                IsolatedStorageSettings appData = IsolatedStorageSettings.ApplicationSettings;
                long userId = 0;
                if (appData.Contains(Enums.USERID_KEY))
                {
                    userId = long.Parse(appData[Enums.USERID_KEY].ToString());
                    txtControlUserID.Text = userId.ToString();
                    txtDayScore.Text = appData.Contains(Enums.USER_DAY_SCORE_KEY) ? (appData[Enums.USER_DAY_SCORE_KEY] ?? "0").ToString() : "0";
                    //txtControlTotalScore.Text = appData.Contains(Enums.USER_SCORE_KEY) ? (appData[Enums.USER_SCORE_KEY] ?? "0").ToString() : "0";
                }
            }
        }
    }
}
