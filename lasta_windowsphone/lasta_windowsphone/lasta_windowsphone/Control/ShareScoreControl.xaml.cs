﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using System.ComponentModel;
using Newtonsoft.Json.Linq;

namespace lasta_windowsphone.Control
{
    public partial class ShareScoreControl : UserControl
    {
        public ShareScoreControl()
        {
            InitializeComponent();
            loadData();
        }

        private void loadData()
        {
            if (!DesignerProperties.IsInDesignTool)
            {
                IsolatedStorageSettings appData = IsolatedStorageSettings.ApplicationSettings;
                long userId = 0;
                if (appData.Contains(Enums.USERID_KEY))
                {
                    userId = long.Parse(appData[Enums.USERID_KEY].ToString());
                    txtControlUserID.Text = userId.ToString();
                    txtControlTotalScore.Text = appData.Contains(Enums.USER_SCORE_KEY) ? (appData[Enums.USER_SCORE_KEY] ?? "0").ToString() : "0";
                    txtControlTotalIndex.Text = appData.Contains(Enums.RANK_KEY) ? (appData[Enums.RANK_KEY] ?? "0").ToString() : "0";

                    if (userId > 0)
                    {
                        string DeviceID = "";
                        Byte[] DeviceArrayID = (Byte[])Microsoft.Phone.Info.DeviceExtendedProperties.GetValue("DeviceUniqueId");
                        DeviceID = Convert.ToBase64String(DeviceArrayID);
                        WebClient userInfo = new WebClient();
                        userInfo.DownloadStringCompleted += new DownloadStringCompletedEventHandler(GetReceiverCode_downloadstringcompleted);
                        userInfo.DownloadStringAsync(new Uri(string.Format(Enums.GET_TRANSFER_CODE_URL, userId, Uri.EscapeDataString(Function.ReplaceUrlString(DeviceID)))));
                    }
                }
            }
        }

        private void GetReceiverCode_downloadstringcompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            if (!DesignerProperties.IsInDesignTool)
            {
                try
                {
                    IsolatedStorageSettings appData = IsolatedStorageSettings.ApplicationSettings;
                    JObject obj = JObject.Parse(e.Result);
                    //txtControlUserID.Text = Convert.ToString(obj["ID"]);
                    txtControlReceiverCode.Text = Convert.ToString(obj["TransferCode"]);
                }
                catch (WebException ex)
                {

                }
            }
        }

    }
}
