﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace lasta_windowsphone.Control
{
    public partial class PopupControl : ChildWindow
    {
        public PopupControl()
        {
            InitializeComponent();
            closeButton.Click += (s, e) =>
            {
                this.DialogResult = true;
                this.Close();
            };
        }
    }
}
