package com.vietdev.network;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

public class ServiceClient {
	public static final boolean STUDIO_VERSION = false;

	public static final String BaseUrl = (STUDIO_VERSION ? "http://192.168.1.2/api/" : "http://cunglatyphu.vn/api/");
	public static final String CheckVersionUrl = BaseUrl + "common/version/android";
	public static final String ContestTodayUrl = BaseUrl + "contests/today";
	public static final String ContestDayUrl = BaseUrl + "contests/%s";
	public static final String ContestDayPrizeUrl = BaseUrl + "contests/%s/prizes";
	public static final String StateUrl = BaseUrl + "states/%s";
	public static final String QuestionUrl = BaseUrl + "questions/%s/%d";

	public static final String AnswerUrl = BaseUrl + (STUDIO_VERSION ? "answer/%s/%d/%d/%d/%d/%d/%d" : "homeanswer/%s/%d/%d/%d/%d/%d/%d");
	public static final String CheckAnswerUrl = BaseUrl + (STUDIO_VERSION ? "answer/check/%d" : "homeanswer/check/%d");
	public static final String RegisterUrl = BaseUrl + (STUDIO_VERSION ? "users/register/%s" : "homeusers/register/%s");
	public static final String GetUserUrl = BaseUrl + (STUDIO_VERSION ? "users/%d" : "homeusers/%d");
	public static final String TopUsersUrl = BaseUrl + (STUDIO_VERSION ? "topusers/%d" : "topusers/home/%d");
	public static final String TopDailyUsersUrl = BaseUrl + (STUDIO_VERSION ? "topusers/%d" : "topusers/dailyhome/%d");
	public static final String UpdateUserUrl = BaseUrl + (STUDIO_VERSION ? "users/update/%s/%d/%s/%s/%s/%s/%d" : "homeusers/update/%s/%d/%s/%s/%s/%s/%d");
	public static final String SearchUsersUrl = BaseUrl + (STUDIO_VERSION ? "users/search/%s" : "homeusers/search/%s");
	public static final String SetTokenUrl = BaseUrl + (STUDIO_VERSION ? "users/setToken/%s/%d/%s" : "homeusers/setToken/%s/%d/%s");

	public static final String ExchangeCodeUrl = BaseUrl + "common/exchange/%s/%d";
	public static final String TransferScoreUrl = BaseUrl + "common/transfer/%d/%s/%d/%d";

	public static final String USER_ID = "UserID";
	public static final String USER_FULLNAME = "UserFullname";
	public static final String USER_EMAIL = "UserEmail";
	public static final String USER_PHONE_NUMBER = "UserPhoneNumber";
	public static final String USER_ADDRESS = "UserAddress";
	public static final String USER_OPT_IN = "UserOptIn";
	public static final String USER_SCORE = "UserScore";
	public static final String USER_DAY_SCORE = "UserDayScore";
	public static final String USER_POSITION = "UserPosition";
	public static final String USER_DAY_POSITION = "UserDayPosition";
	public static final String CURRENT_QUESTION_NUMBER = "QuestionNumber";
	public static final String NUMBER_OF_QUESTIONS = "NumberOfQuestions";
	public static final String DEVICE_TOKEN = "DeviceToken";
	public static final String APP_VERSION = "AppVersion";

	private HttpClient client;
	private static ServiceClient instance;

	public static ServiceClient getInstance() {
		if (instance == null) {
			instance = new ServiceClient();
		}
		return instance;
	}

	private ServiceClient() {
		client = new DefaultHttpClient();
	}

	public static final int SC_CONNECTION_ERROR = -1;
	public static final int SC_CANCELED = -2;

	private enum HttpMethod {
		GET, POST
	}

	private class RequestTask extends AsyncTask<String, Integer, Integer> {
		String jsonString;

		private Handler successHandler;

		private Handler failureHandler;

		public RequestTask setHandlers(Handler successHandler, Handler failureHandler) {
			this.successHandler = successHandler;
			this.failureHandler = failureHandler;
			return this;
		}

		@Override
		protected Integer doInBackground(String... params) {
			String path = params[0];
			String httpMethod = params[1];
			HttpRequestBase request = null;
			if (httpMethod.equals(HttpMethod.GET.name())) {
				request = new HttpGet(path);
			}
			else if (httpMethod.equals(HttpMethod.POST.name())) {
				// request = new HttpPost(ForBossUtils.getBaseApi() + path);
				request = new HttpPost(path);
			}
			else {
				Log.e(this.getClass().getName(), "unsupported http method");
				return null;
			}

			HttpParams httpParameters = new BasicHttpParams();
			// Set the timeout in milliseconds until a connection is established.
			// The default value is zero, that means the timeout is not used.
			int timeoutConnection = 3000;
			HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
			// Set the default socket timeout (SO_TIMEOUT)
			// in milliseconds which is the timeout for waiting for data.
			int timeoutSocket = 5000;
			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
			request.setParams(httpParameters);

			jsonString = null;
			try {
				HttpResponse response = client.execute(request);
				if (isCancelled())
					return SC_CANCELED;
				jsonString = EntityUtils.toString(response.getEntity());
				return response.getStatusLine().getStatusCode();
			}
			catch (ClientProtocolException e) {
				Log.e(this.getClass().getName(), "http protocol error");
				return SC_CONNECTION_ERROR;
			}
			catch (IOException e) {
				Log.e(this.getClass().getName(), "connection abort");
				return SC_CONNECTION_ERROR;
			}
			catch (Exception e) {
				return SC_CONNECTION_ERROR;
			}
		}

		@Override
		protected void onPostExecute(Integer result) {
			Handler handler = null;
			if (result == HttpStatus.SC_OK) {
				handler = successHandler;
			}
			else {
				handler = failureHandler;
			}
			if (handler != null && !isCancelled()) {
				Message message = handler.obtainMessage();
				message.what = result;
				message.obj = jsonString;
				handler.sendMessage(message);
			}
		}
	}

	public AsyncTask getService(final String path, final Context context, final Handler successHandler, final Handler failureHandler) {
		if (!ClientUtils.isNetworkAvailable(context))
			return null;
		return (new RequestTask()).setHandlers(successHandler, failureHandler).execute(new String[] { path, HttpMethod.GET.name() });
	}

	public AsyncTask getJson(final String path, final Context context, final Handler finishHandler, final Handler failureHandler) {
		return getService(path, context, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				try {
					String jsonString = (String) msg.obj;
					JSONObject jsonObject = new JSONObject(jsonString);
					if (finishHandler != null) {
						Message responseMsg = finishHandler.obtainMessage();
						responseMsg.obj = jsonObject;
						finishHandler.sendMessage(responseMsg);
					}
				}
				catch (JSONException e) {
					Log.e(this.getClass().getName(), "Unable to parse json string", e);
				}
			}
		}, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				if (failureHandler != null)
					failureHandler.sendEmptyMessage(0);
			}
		});
	}

	public AsyncTask register(final String deviceID, final Context context, final Handler finishHandler, final Handler failureHandler) {
		String dID = deviceID;
		try {
			dID = URLEncoder.encode(deviceID.replace("/", "==").replace("+", "==="), "utf-8").replace("+", "%20");
		}
		catch (Exception exc) {

		}
		return getService(String.format(RegisterUrl, dID), context, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				try {
					String jsonString = (String) msg.obj;
					JSONObject jsonObject = new JSONObject(jsonString);
					if (finishHandler != null) {
						Message responseMsg = finishHandler.obtainMessage();
						responseMsg.obj = jsonObject;
						finishHandler.sendMessage(responseMsg);
					}
				}
				catch (JSONException e) {
					Log.e(this.getClass().getName(), "Unable to parse json string", e);
				}
			}
		}, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				if (failureHandler != null) {
					Message responseMsg = failureHandler.obtainMessage();
					responseMsg.what = msg.what;
					responseMsg.obj = msg.obj;
					failureHandler.sendMessage(responseMsg);
				}
			}
		});
	}

	public AsyncTask getUser(final long userID, final Context context, final Handler finishHandler, final Handler failureHandler) {
		return getService(String.format(GetUserUrl, userID), context, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				try {
					String jsonString = (String) msg.obj;
					JSONObject jsonObject = new JSONObject(jsonString);
					if (finishHandler != null) {
						Message responseMsg = finishHandler.obtainMessage();
						responseMsg.obj = jsonObject;
						finishHandler.sendMessage(responseMsg);
					}
				}
				catch (JSONException e) {
					Log.e(this.getClass().getName(), "Unable to parse json string", e);
				}
			}
		}, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				if (failureHandler != null) {
					Message responseMsg = failureHandler.obtainMessage();
					responseMsg.what = msg.what;
					responseMsg.obj = msg.obj;
					failureHandler.sendMessage(responseMsg);
				}
			}
		});
	}

	public AsyncTask getState(final String contestID, final Context context, final Handler finishHandler, final Handler failureHandler) {
		return getService(String.format(StateUrl, contestID), context, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				try {
					String jsonString = (String) msg.obj;
					JSONObject jsonObject = new JSONObject(jsonString);
					if (finishHandler != null) {
						Message responseMsg = finishHandler.obtainMessage();
						responseMsg.obj = jsonObject;
						finishHandler.sendMessage(responseMsg);
					}
				}
				catch (JSONException e) {
					Log.e(this.getClass().getName(), "Unable to parse json string", e);
				}
			}
		}, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				if (failureHandler != null) {
					Message responseMsg = failureHandler.obtainMessage();
					responseMsg.what = msg.what;
					responseMsg.obj = msg.obj;
					failureHandler.sendMessage(responseMsg);
				}
			}
		});
	}

	public AsyncTask getQuestion(final String contestID, final int questionNumber, final Context context, final Handler finishHandler, final Handler failureHandler) {
		return getService(String.format(QuestionUrl, contestID, questionNumber), context, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				try {
					String jsonString = (String) msg.obj;
					JSONObject jsonObject = new JSONObject(jsonString);

					if (finishHandler != null) {
						Message responseMsg = finishHandler.obtainMessage();
						responseMsg.obj = jsonObject;
						finishHandler.sendMessage(responseMsg);
					}
				}
				catch (JSONException e) {
					Log.e(this.getClass().getName(), "Unable to parse json string", e);
				}
			}
		}, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				if (failureHandler != null) {
					Message responseMsg = failureHandler.obtainMessage();
					responseMsg.what = msg.what;
					responseMsg.obj = msg.obj;
					failureHandler.sendMessage(responseMsg);
				}
			}
		});
	}

	public AsyncTask submitAnswer(final String contestID, final long userID, final int answer, final long startTime, final long endTime, final int answerTime, final long score, final Context context,
			final Handler finishHandler, final Handler failureHandler) {
		return getService(String.format(AnswerUrl, contestID, userID, answer, startTime, endTime, answerTime, score), context, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				try {
					String jsonString = (String) msg.obj;
					JSONObject jsonObject = new JSONObject(jsonString);
					if (finishHandler != null) {
						Message responseMsg = finishHandler.obtainMessage();
						responseMsg.obj = jsonObject;
						finishHandler.sendMessage(responseMsg);
					}
				}
				catch (JSONException e) {
					Log.e(this.getClass().getName(), "Unable to parse json string", e);
				}
			}
		}, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				if (failureHandler != null) {
					Message responseMsg = failureHandler.obtainMessage();
					responseMsg.what = msg.what;
					responseMsg.obj = msg.obj;
					failureHandler.sendMessage(responseMsg);
				}
			}
		});
	}

	public AsyncTask checkAnswer(final long userID, final Context context, final Handler finishHandler, final Handler failureHandler) {
		return getService(String.format(CheckAnswerUrl, userID), context, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				try {
					String jsonString = (String) msg.obj;
					JSONObject jsonObject = new JSONObject(jsonString);
					if (finishHandler != null) {
						Message responseMsg = finishHandler.obtainMessage();
						responseMsg.obj = jsonObject;
						finishHandler.sendMessage(responseMsg);
					}
				}
				catch (JSONException e) {
					Log.e(this.getClass().getName(), "Unable to parse json string", e);
				}
			}
		}, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				if (failureHandler != null) {
					Message responseMsg = failureHandler.obtainMessage();
					responseMsg.what = msg.what;
					responseMsg.obj = msg.obj;
					failureHandler.sendMessage(responseMsg);
				}
			}
		});
	}

	public AsyncTask getTopUsers(final int count, final Context context, final Handler finishHandler, final Handler failureHandler) {
		return getService(String.format(TopUsersUrl, count), context, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				try {
					String jsonString = (String) msg.obj;
					JSONArray jsonArray = new JSONArray(jsonString);
					if (finishHandler != null) {
						Message responseMsg = finishHandler.obtainMessage();
						responseMsg.obj = jsonArray;
						finishHandler.sendMessage(responseMsg);
					}
				}
				catch (JSONException e) {
					Log.e(this.getClass().getName(), "Unable to parse json string", e);
				}
			}
		}, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				if (failureHandler != null)
					failureHandler.sendEmptyMessage(0);
			}
		});
	}

	public AsyncTask getDailyTopUsers(final int count, final Context context, final Handler finishHandler, final Handler failureHandler) {
		return getService(String.format(TopDailyUsersUrl, count), context, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				try {
					String jsonString = (String) msg.obj;
					JSONArray jsonArray = new JSONArray(jsonString);
					if (finishHandler != null) {
						Message responseMsg = finishHandler.obtainMessage();
						responseMsg.obj = jsonArray;
						finishHandler.sendMessage(responseMsg);
					}
				}
				catch (JSONException e) {
					Log.e(this.getClass().getName(), "Unable to parse json string", e);
				}
			}
		}, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				if (failureHandler != null)
					failureHandler.sendEmptyMessage(0);
			}
		});
	}

	public AsyncTask searchUsers(final String filter, final Context context, final Handler finishHandler, final Handler failureHandler) {
		String f = filter;
		try {
			f = URLEncoder.encode(filter.replace("/", "==").replace("+", "==="), "utf-8").replace("+", "%20");
		}
		catch (Exception exc) {

		}
		return getService(String.format(SearchUsersUrl, f), context, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				try {
					String jsonString = (String) msg.obj;
					JSONArray jsonArray = new JSONArray(jsonString);
					if (finishHandler != null) {
						Message responseMsg = finishHandler.obtainMessage();
						responseMsg.obj = jsonArray;
						finishHandler.sendMessage(responseMsg);
					}
				}
				catch (JSONException e) {
					Log.e(this.getClass().getName(), "Unable to parse json string", e);
				}
			}
		}, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				if (failureHandler != null) {
					Message responseMsg = failureHandler.obtainMessage();
					responseMsg.what = msg.what;
					responseMsg.obj = msg.obj;
					failureHandler.sendMessage(responseMsg);
				}
			}
		});
	}

	public AsyncTask checkVersion(final Context context, final Handler finishHandler, final Handler failureHandler) {
		return getService(CheckVersionUrl, context, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				try {
					String jsonString = (String) msg.obj;
					JSONObject jsonObject = new JSONObject(jsonString);
					if (finishHandler != null) {
						Message responseMsg = finishHandler.obtainMessage();
						responseMsg.obj = jsonObject;
						finishHandler.sendMessage(responseMsg);
					}
				}
				catch (JSONException e) {
					Log.e(this.getClass().getName(), "Unable to parse json string", e);
				}
			}
		}, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				if (failureHandler != null) {
					Message responseMsg = failureHandler.obtainMessage();
					responseMsg.what = msg.what;
					responseMsg.obj = msg.obj;
					failureHandler.sendMessage(responseMsg);
				}
			}
		});
	}

	public AsyncTask setToken(final String token, final String deviceID, final long userID, final Context context, final Handler finishHandler, final Handler failureHandler) {
		String dID = deviceID;
		String t = token;
		try {
			dID = URLEncoder.encode(deviceID.replace("/", "==").replace("+", "==="), "utf-8").replace("+", "%20");
			t = URLEncoder.encode(token.replace("/", "==").replace("+", "==="), "utf-8").replace("+", "%20");
		}
		catch (Exception exc) {

		}
		return getService(String.format(SetTokenUrl, dID, userID, t), context, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				try {
					String jsonString = (String) msg.obj;
					JSONObject jsonObject = new JSONObject(jsonString);
					if (finishHandler != null) {
						Message responseMsg = finishHandler.obtainMessage();
						responseMsg.obj = jsonObject;
						finishHandler.sendMessage(responseMsg);
					}
				}
				catch (JSONException e) {
					Log.e(this.getClass().getName(), "Unable to parse json string", e);
				}
			}
		}, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				if (failureHandler != null) {
					Message responseMsg = failureHandler.obtainMessage();
					responseMsg.what = msg.what;
					responseMsg.obj = msg.obj;
					failureHandler.sendMessage(responseMsg);
				}
			}
		});
	}

	public AsyncTask exchangeCode(final String code, final long userID, final Context context, final Handler finishHandler, final Handler failureHandler) {
		return getService(String.format(ExchangeCodeUrl, code, userID), context, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				try {
					String jsonString = (String) msg.obj;
					JSONObject jsonObject = new JSONObject(jsonString);
					if (finishHandler != null) {
						Message responseMsg = finishHandler.obtainMessage();
						responseMsg.obj = jsonObject;
						finishHandler.sendMessage(responseMsg);
					}
				}
				catch (JSONException e) {
					Log.e(this.getClass().getName(), "Unable to parse json string", e);
				}
			}
		}, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				if (failureHandler != null) {
					Message responseMsg = failureHandler.obtainMessage();
					responseMsg.what = msg.what;
					responseMsg.obj = msg.obj;
					failureHandler.sendMessage(responseMsg);
				}
			}
		});
	}

	public AsyncTask transferCode(final long userID, final long receiverID, final long score, final String deviceID, final Context context, final Handler finishHandler, final Handler failureHandler) {
		String dID = deviceID;
		try {
			dID = URLEncoder.encode(deviceID.replace("/", "==").replace("+", "==="), "utf-8").replace("+", "%20");
		}
		catch (Exception exc) {

		}
		return getService(String.format(TransferScoreUrl, userID, dID, receiverID, score), context, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				try {
					String jsonString = (String) msg.obj;
					JSONObject jsonObject = new JSONObject(jsonString);
					if (finishHandler != null) {
						Message responseMsg = finishHandler.obtainMessage();
						responseMsg.obj = jsonObject;
						finishHandler.sendMessage(responseMsg);
					}
				}
				catch (JSONException e) {
					Log.e(this.getClass().getName(), "Unable to parse json string", e);
				}
			}
		}, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				if (failureHandler != null) {
					Message responseMsg = failureHandler.obtainMessage();
					responseMsg.what = msg.what;
					responseMsg.obj = msg.obj;
					failureHandler.sendMessage(responseMsg);
				}
			}
		});
	}

	public AsyncTask updateUser(final long userID, final String deviceID, final String fullname, final String phoneNumber, final String address, final String email, final int optIn,
			final Context context, final Handler finishHandler, final Handler failureHandler) {
		String dID = deviceID;
		String fn = fullname;
		String pn = phoneNumber;
		String a = address;
		String e = email;
		try {
			dID = URLEncoder.encode(deviceID.replace("/", "==").replace("+", "==="), "utf-8").replace("+", "%20");
			fn = URLEncoder.encode(fullname.replace("/", "==").replace("+", "==="), "utf-8").replace("+", "%20");
			pn = URLEncoder.encode(phoneNumber.replace("/", "==").replace("+", "==="), "utf-8").replace("+", "%20");
			a = URLEncoder.encode(address.replace("/", "==").replace("+", "==="), "utf-8").replace("+", "%20");
			e = URLEncoder.encode(email.replace("/", "==").replace("+", "==="), "utf-8").replace("+", "%20");
		}
		catch (Exception exc) {

		}
		
		return getService(String.format(UpdateUserUrl, dID, userID, fn, pn, e, a, optIn), context, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				try {
					String jsonString = (String) msg.obj;
					JSONObject jsonObject = new JSONObject(jsonString);
					if (finishHandler != null) {
						Message responseMsg = finishHandler.obtainMessage();
						responseMsg.obj = jsonObject;
						finishHandler.sendMessage(responseMsg);
					}
				}
				catch (JSONException e) {
					Log.e(this.getClass().getName(), "Unable to parse json string", e);
				}
			}
		}, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				if (failureHandler != null) {
					Message responseMsg = failureHandler.obtainMessage();
					responseMsg.what = msg.what;
					responseMsg.obj = msg.obj;
					failureHandler.sendMessage(responseMsg);
				}
			}
		});
	}

}
