package com.vietdev.network;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;

public class ClientUtils {
	public static SharedPreferences getSharedPreferences(Context context) {
		return context.getSharedPreferences("cunglatyphu", Activity.MODE_PRIVATE);
	}

	public static void alert(final Context context, final String message, final Handler finishHandler) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(message).setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				dialog.dismiss();
				if (finishHandler != null)
					finishHandler.sendEmptyMessage(0);
			}
		});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	public static boolean isNetworkAvailable(final Context context) {
		final ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		final NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}

	public static Tracker getTracker(final Context context) {
		GoogleAnalytics analytics = GoogleAnalytics.getInstance(context);
		Tracker t = analytics.newTracker("UA-9714747-7");
		
		return t;
	}
}
