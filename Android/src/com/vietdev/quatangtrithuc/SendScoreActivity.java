package com.vietdev.quatangtrithuc;

import java.io.InputStream;

import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.vietdev.cunglatyphu.R;
import com.vietdev.cunglatyphu.R.layout;
import com.vietdev.network.ClientUtils;
import com.vietdev.network.ServiceClient;
import com.vietdev.quatangtrithuc.util.SoundManager;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SendScoreActivity extends Activity {

	private RelativeLayout layoutInfo;
	private LinearLayout layoutSubmit;
	private EditText textSearch;
	private EditText textScore;
	private TextView labelUserID;
	private TextView labelFullname;
	private TextView labelScore;
	private Button butSubmit;

	private SendScoreActivity getContext() {
		return this;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// Remove notification bar
		// this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_send_score);

		// Get tracker.
		Tracker t = ClientUtils.getTracker(getContext());

		// Set screen name.
		// Where path is a String representing the screen name.
		t.setScreenName("Transfer Score Screen");

		// Send a screen view.
		t.send(new HitBuilders.AppViewBuilder().build());

		layoutInfo = (RelativeLayout) findViewById(R.id.layoutInfo);
		layoutSubmit = (LinearLayout) findViewById(R.id.layoutSubmit);
		textSearch = (EditText) findViewById(R.id.textSearchUser);
		textScore = (EditText) findViewById(R.id.textScore);
		labelUserID = (TextView) findViewById(R.id.textSearchUserID);
		labelFullname = (TextView) findViewById(R.id.textSearchFullname);
		labelScore = (TextView) findViewById(R.id.searchScore);

		final SharedPreferences prefs = ClientUtils.getSharedPreferences(this);
		long userID = prefs.getLong(ServiceClient.USER_ID, 0);
		long score = prefs.getLong(ServiceClient.USER_SCORE, 0);
		long position = prefs.getLong(ServiceClient.USER_POSITION, 0);

		TextView idLabel = (TextView) findViewById(R.id.idLabel);
		idLabel.setText("" + userID);
		TextView totalScoreLabel = (TextView) findViewById(R.id.totalScoreLabel);
		totalScoreLabel.setText("" + score);
		TextView positionLabel = (TextView) findViewById(R.id.positionLabel);
		positionLabel.setText("" + position);

		Button butBack = (Button) findViewById(R.id.butBack);
		butBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				SoundManager.getInstance(getContext()).playButton();
				finish();
			}
		});

		WebView webContent = (WebView) findViewById(R.id.webContent);
		try {
			InputStream inputStream = getResources().openRawResource(R.raw.tangdiem);
			byte[] data = new byte[inputStream.available()];
			inputStream.read(data);
			webContent.loadDataWithBaseURL("file:///android_asset/", new String(data), "text/html", "utf-8", null);
		}
		catch (Exception exc) {

		}

		Button butStart = (Button) findViewById(R.id.butChange);
		butStart.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				layoutInfo.setVisibility(View.INVISIBLE);
				layoutSubmit.setVisibility(View.VISIBLE);

				SoundManager.getInstance(getContext()).playButton();
			}
		});

		textSearch.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus && textSearch.getText().length() > 0) {
					long userID = Long.parseLong(textSearch.getText().toString());
					ServiceClient.getInstance().getUser(userID, getContext(), new Handler() {
						@Override
						public void handleMessage(Message msg) {
							try {
								JSONObject jsonObject = (JSONObject) msg.obj;

								labelUserID.setText(jsonObject.isNull("ID") ? "" : (jsonObject.getLong("ID") + ""));
								labelFullname.setText(jsonObject.isNull("Fullname") ? "" : jsonObject.getString("Fullname"));
								labelScore.setText(jsonObject.isNull("Score") ? "" : (jsonObject.getLong("Score") + ""));
							}
							catch (JSONException e) {
								Log.e(this.getClass().getName(), "Unable to parse json string", e);
							}
						}
					}, new Handler() {
						@Override
						public void handleMessage(Message msg) {
							// Alert error
							if (msg.what == HttpStatus.SC_NOT_FOUND) {
								labelUserID.setText("");
								labelFullname.setText("");
								labelScore.setText("");
								ClientUtils.alert(getContext(), "Không tìm thấy người chơi với mã số ID '" + textSearch.getText().toString() + "'.", null);
							}
							else {
								ClientUtils.alert(getContext(), "Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại.", null);
							}
						}
					});

				}
			}
		});

		butSubmit = (Button) findViewById(R.id.butSubmit);
		butSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SoundManager.getInstance(getContext()).playButton();

				if (textSearch.getText().length() == 0) {
					ClientUtils.alert(getContext(), "Vui lòng nhập mã số ID của người được nhận điểm.", null);
					return;
				}

				if (textScore.getText().length() == 0) {
					ClientUtils.alert(getContext(), "Vui lòng nhập số điểm muốn chuyển cho người nhận.", null);
					return;
				}

				final SharedPreferences prefs = ClientUtils.getSharedPreferences(getContext());
				long userID = prefs.getLong(ServiceClient.USER_ID, 0);
				long receiverID = Long.parseLong(textSearch.getText().toString());
				long score = Long.parseLong(textScore.getText().toString());
				String deviceID = Secure.getString(getContext().getContentResolver(), Secure.ANDROID_ID);

				butSubmit.setEnabled(false);
				ServiceClient.getInstance().transferCode(userID, receiverID, score, deviceID, getContext(), new Handler() {
					@Override
					public void handleMessage(Message msg) {
						try {
							JSONObject jsonObject = (JSONObject) msg.obj;

							if (jsonObject.getInt("Error") == -1) {
								ClientUtils.alert(getContext(), "Chưa đến thời gian cho phép tặng điểm. Thời gian tặng điểm diễn ra từ đầu chương trình 23 đến giữa chương trình 24.", null);
							}
							else if (jsonObject.getInt("Error") == 1) {
								ClientUtils.alert(getContext(), "Bạn đã tặng điểm cho người khác rồi. Bạn không thể thực hiện tặng điểm quá 1 lần.", null);
							}
							else if (jsonObject.getInt("Error") == 2) {
								ClientUtils.alert(getContext(), "Người nhận điểm tặng đã nhận đủ tối đa 5 lần tặng.", null);
							}
							else if (jsonObject.getInt("Error") == 3) {
								ClientUtils.alert(getContext(), "Chương trình xảy ra lỗi trong quá trình xử lý. Vui lòng thử lại hoặc thông báo tới BTC để được hướng dẫn xử lý và hỗ trợ.", null);
							}
							else if (jsonObject.getInt("Error") == 4) {
								ClientUtils.alert(getContext(), "Hiện tại bạn không có điểm để tặng cho người nhận.", null);
							}
							else {
								ClientUtils.alert(getContext(), "Bạn đã thực hiện việc tặng điểm cho người nhận thành công.", null);
							}
						}
						catch (JSONException e) {
							Log.e(this.getClass().getName(), "Unable to parse json string", e);
						}
						butSubmit.setEnabled(true);
					}
				}, new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// Alert error
						if (msg.what == HttpStatus.SC_NOT_FOUND) {
							labelUserID.setText("");
							labelFullname.setText("");
							labelScore.setText("");
							ClientUtils.alert(getContext(), "Không tìm thấy người nhận điểm hoặc thông tin bạn cung cấp không chính xác. Vui lòng kiểm tra thông tin và thử lại.", null);
						}
						else {
							ClientUtils.alert(getContext(), "Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại.", null);
						}
						butSubmit.setEnabled(true);
					}
				});
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();

		SoundManager.getInstance(getContext()).playOpenning();
	}

	@Override
	protected void onPause() {
		super.onPause();

		SoundManager.getInstance(getContext()).stopAudios();
	}

	@Override
	protected void onStop() {
		super.onStop();

		SoundManager.getInstance(getContext()).stopAudios();
	}
}
