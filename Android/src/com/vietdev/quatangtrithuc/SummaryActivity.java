package com.vietdev.quatangtrithuc;

import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.vietdev.cunglatyphu.R;
import com.vietdev.network.ClientUtils;
import com.vietdev.network.ServiceClient;
import com.vietdev.quatangtrithuc.util.SoundManager;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

/**
 * An example full-screen activity that shows and hides the system UI (i.e. status bar and navigation/system bar) with
 * user interaction.
 * 
 * @see SystemUiHider
 */
public class SummaryActivity extends Activity {

	public SummaryActivity getContext() {
		return this;
	}

	private Handler timerHandler = new Handler() {
		public void handleMessage(Message msg) {
			if (isLoading == false) {
				loadState();
			}
		}
	};

	public String contestKey;
	public long contestState;
	public boolean showFull;

	private EditText textSearch;
	private Button butSearch;

	private Button butTop50Day;
	private Button butTop50Total;

	private Timer updateTimer;
	private boolean isLoading;
	private boolean isFailed;
	private int networkFailedCount;

	private ListView leaderboardList;
	private LeaderboardAdapter leaderboardAdapter;
	
	private TextView idLabel;
	private TextView dayScoreLabel;
	private TextView dayPositionLabel;
	private TextView totalScoreLabel;
	private TextView positionLabel;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// Remove notification bar
		// this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_summary);

		// Get tracker.
		Tracker t = ClientUtils.getTracker(getContext());

		// Set screen name.
		// Where path is a String representing the screen name.
		t.setScreenName("Leaderboard screen");

		// Send a screen view.
		t.send(new HitBuilders.AppViewBuilder().build());

		isFailed = false;
		isLoading = false;
		networkFailedCount = 0;

		showFull = false;

		contestKey = getIntent().getExtras() == null ? "" : getIntent().getExtras().getString("ContestKey");
		contestState = getIntent().getExtras() == null ? 0 : getIntent().getExtras().getLong("ContestState");
		showFull = getIntent().getExtras() == null ? false : getIntent().getExtras().getBoolean("ShowFull");

		leaderboardList = (ListView) findViewById(R.id.leaderboardList);
		leaderboardAdapter = new LeaderboardAdapter(new JSONArray());
		leaderboardList.setAdapter(leaderboardAdapter);

		Button butBack = (Button) findViewById(R.id.butBack);
		butBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				SoundManager.getInstance(getContext()).playButton();
				finish();
			}
		});

		LinearLayout layoutSearch = (LinearLayout) findViewById(R.id.layoutSearch);

		if (showFull) {
			butBack.setVisibility(View.VISIBLE);

			final SharedPreferences prefs = ClientUtils.getSharedPreferences(this);
			Editor editor = prefs.edit();
			editor.remove(ServiceClient.CURRENT_QUESTION_NUMBER);
			editor.remove(ServiceClient.NUMBER_OF_QUESTIONS);
			editor.commit();
		}

		textSearch = (EditText) findViewById(R.id.textSearch);

		butSearch = (Button) findViewById(R.id.butSearch);
		butSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (textSearch.getText().length() == 0) {
					return;
				}

				butSearch.setEnabled(false);

				ServiceClient.getInstance().searchUsers(textSearch.getText().toString(), getContext(), new Handler() {
					@Override
					public void handleMessage(Message msg) {
						JSONArray jsonArray = (JSONArray) msg.obj;

						leaderboardAdapter.setData(jsonArray);
						if (leaderboardAdapter.getData().length() != 0) {
							leaderboardAdapter.notifyDataSetChanged();
						}

						butSearch.setEnabled(true);
					}
				}, new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// Alert error
						ClientUtils.alert(getContext(), "Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại.", null);

						butSearch.setEnabled(true);
					}
				});
			}
		});

		final SharedPreferences prefs = ClientUtils.getSharedPreferences(this);
		long userID = prefs.getLong(ServiceClient.USER_ID, 0);
		long dayScore = prefs.getLong(ServiceClient.USER_DAY_SCORE, 0);
		long dayPosition = prefs.getLong(ServiceClient.USER_DAY_POSITION, 0);
		long totalScore = prefs.getLong(ServiceClient.USER_SCORE, 0);
		long position = prefs.getLong(ServiceClient.USER_POSITION, 0);

		idLabel = (TextView) findViewById(R.id.idLabel);
		idLabel.setText("" + userID);
		dayScoreLabel = (TextView) findViewById(R.id.dayScoreLabel);
		dayScoreLabel.setText("" + dayScore);
		dayPositionLabel = (TextView) findViewById(R.id.dayPositionLabel);
		dayPositionLabel.setText("" + (dayPosition == 0 ? "Chờ xử lý" : dayPosition));
		totalScoreLabel = (TextView) findViewById(R.id.totalScoreLabel);
		totalScoreLabel.setText("" + totalScore);
		positionLabel = (TextView) findViewById(R.id.positionLabel);
		positionLabel.setText("" + (position == 0 ? "Chờ xử lý" : position));

		butTop50Day = (Button) findViewById(R.id.butTop50Day);
		butTop50Day.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				butTop50Day.setSelected(true);
				butTop50Total.setSelected(false);
				
				leaderboardAdapter.setData(null);
				leaderboardAdapter.notifyDataSetChanged();
				
				loadDailyTopUsers();
			}
		});

		butTop50Total = (Button) findViewById(R.id.butTop50Total);
		butTop50Total.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				butTop50Day.setSelected(false);
				butTop50Total.setSelected(true);

				leaderboardAdapter.setData(null);
				leaderboardAdapter.notifyDataSetChanged();
				
				loadTopUsers();
			}
		});

		getUserInfo(userID);
	}

	private void loadTopUsers() {
		ServiceClient.getInstance().getTopUsers(50, getContext(), new Handler() {
			@Override
			public void handleMessage(Message msg) {
				JSONArray jsonArray = (JSONArray) msg.obj;

				leaderboardAdapter.setData(jsonArray);
				leaderboardAdapter.notifyDataSetChanged();
			}
		}, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// Alert error
				ClientUtils.alert(getContext(), "Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại.", null);
			}
		});

	}

	private void loadDailyTopUsers() {
		ServiceClient.getInstance().getDailyTopUsers(50, getContext(), new Handler() {
			@Override
			public void handleMessage(Message msg) {
				JSONArray jsonArray = (JSONArray) msg.obj;

				leaderboardAdapter.setData(jsonArray);
				if (leaderboardAdapter.getData().length() != 0) {
					leaderboardAdapter.notifyDataSetChanged();
				}
			}
		}, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// Alert error
				ClientUtils.alert(getContext(), "Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại.", null);
			}
		});

	}

	private void getUserInfo(long userID) {
		// Get user information
		ServiceClient.getInstance().getUser(userID, getContext(), new Handler() {
			@Override
			public void handleMessage(Message msg) {
				try {
					JSONObject jsonObject = (JSONObject) msg.obj;

					final SharedPreferences innerPrefs = ClientUtils.getSharedPreferences(getContext());
					Editor editor = innerPrefs.edit();
					editor.putLong(ServiceClient.USER_SCORE, jsonObject.isNull("Score") ? 0 : jsonObject.getLong("Score"));
					editor.putLong(ServiceClient.USER_DAY_SCORE, jsonObject.isNull("DayScore") ? 0 : jsonObject.getLong("DayScore"));
					editor.putLong(ServiceClient.USER_POSITION, jsonObject.isNull("Rank") ? 0 : jsonObject.getLong("Rank"));
					editor.putLong(ServiceClient.USER_DAY_POSITION, jsonObject.isNull("DayRank") ? 0 : jsonObject.getLong("DayRank"));
					editor.commit();
					
					dayScoreLabel.setText("" + (jsonObject.isNull("DayScore") ? 0 : jsonObject.getLong("DayScore")));
					dayPositionLabel.setText("" + ((jsonObject.isNull("DayRank") ? 0 : jsonObject.getLong("DayRank")) == 0 ? "Chờ xử lý" : jsonObject.getLong("DayRank")));
					totalScoreLabel.setText("" + (jsonObject.isNull("Score") ? 0 : jsonObject.getLong("Score")));
					positionLabel.setText("" + ((jsonObject.isNull("Rank") ? 0 : jsonObject.getLong("Rank")) == 0 ? "Chờ xử lý" : jsonObject.getLong("Rank")));
					
					butTop50Day.performClick();
				}
				catch (JSONException e) {
					Log.e(this.getClass().getName(), "Unable to parse json string", e);
				}
			}
		}, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// Alert error
				if (msg.what == HttpStatus.SC_NOT_FOUND) {
				}
				else {
					ClientUtils.alert(getContext(), "Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại.", null);
				}
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (showFull == true) {
			SoundManager.getInstance(getContext()).playOpenning();
		}
		else {
			SoundManager.getInstance(getContext()).playBackground();
		}

		if (updateTimer == null) {
			updateTimer = new Timer();
		}

		isFailed = false;
		isLoading = false;

		if (showFull == false) {
			updateTimer.scheduleAtFixedRate(new TimerTask() {
				@Override
				public void run() {
					timerHandler.obtainMessage(1).sendToTarget();
				}
			}, 0, 2000);
		}

	}

	@Override
	protected void onPause() {
		super.onPause();

		SoundManager.getInstance(getContext()).stopAudios();
	}

	@Override
	protected void onStop() {
		super.onStop();

		SoundManager.getInstance(getContext()).stopAudios();

		if (updateTimer != null) {
			updateTimer.cancel();
			updateTimer = null;
		}
	}

	private void loadState() {
		if (this.isFinishing()) {
			return;
		}

		if (this.contestKey != "" && this.contestKey != null) {
			isLoading = true;

			ServiceClient.getInstance().getState(contestKey, getContext(), new Handler() {
				@Override
				public void handleMessage(Message msg) {
					try {
						JSONObject jsonObject = (JSONObject) msg.obj;

						contestState = jsonObject.getLong("Status");
						processState();
					}
					catch (JSONException e) {
						Log.e(this.getClass().getName(), "Unable to parse json string", e);
					}

					isLoading = false;
					networkFailedCount = 0;
				}
			}, new Handler() {
				@Override
				public void handleMessage(Message msg) {
					++networkFailedCount;
					// Alert error
					if (isFailed == false && networkFailedCount >= 5) {
						ClientUtils.alert(getContext(), "Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công.", null);
						isFailed = true;

						networkFailedCount = 0;
					}

					isLoading = false;
				}
			});
		}
	}

	private void processState() {
		if (this.isFinishing()) {
			return;
		}

		if (contestState >= 10 && contestState < 10000) {
			if (contestState % 10 == 3) {
			}
			else if (contestState % 10 == 2) {
				// Go to question page
				Intent intent = new Intent(getContext(), QuestionActivity.class);
				intent.putExtra("ContestKey", contestKey);
				intent.putExtra("QuestionNumber", (int) contestState / 10);
				intent.putExtra("ContestState", contestState);
				finish();
				startActivity(intent);
			}
			else {
				// Go to question info page
				Intent intent = new Intent(getContext(), QuestionInfoActivity.class);
				intent.putExtra("ContestKey", contestKey);
				intent.putExtra("QuestionNumber", (int) contestState / 10);
				intent.putExtra("ContestState", contestState);
				finish();
				startActivity(intent);
			}
		}
		else if (contestState == 10400) {
			final SharedPreferences prefs = ClientUtils.getSharedPreferences(this);

			Intent intent = new Intent(getContext(), QuestionInfoActivity.class);
			intent.putExtra("ContestKey", contestKey);
			intent.putExtra("QuestionNumber", prefs.getInt(ServiceClient.NUMBER_OF_QUESTIONS, 0) + 1);
			intent.putExtra("ContestState", contestState);
			finish();
			startActivity(intent);
		}
		else if (contestState == 10401) {
			// Go to question page
			final SharedPreferences prefs = ClientUtils.getSharedPreferences(this);

			Intent intent = new Intent(getContext(), QuestionActivity.class);
			intent.putExtra("ContestKey", contestKey);
			intent.putExtra("ContestState", contestState);
			intent.putExtra("QuestionNumber", prefs.getInt(ServiceClient.NUMBER_OF_QUESTIONS, 0) + 1);
			finish();
			startActivity(intent);

		}
		else if (contestState == 10000 || contestState == 10100 || contestState == 10200) {
			final SharedPreferences prefs = ClientUtils.getSharedPreferences(this);
			if (prefs.getInt(ServiceClient.CURRENT_QUESTION_NUMBER, 0) > 0 && prefs.getInt(ServiceClient.CURRENT_QUESTION_NUMBER, 0) % 4 == 0) {
			}
			else {
				// Go to info page
				Intent intent = new Intent(getContext(), PrizesActivity.class);
				intent.putExtra("ContestKey", contestKey);
				finish();
				startActivity(intent);
			}
		}
	}

	private class LeaderboardAdapter extends BaseAdapter {
		private JSONArray data;

		public JSONArray getData() {
			return data;
		}

		public void setData(JSONArray data) {
			this.data = data;
		}

		public LeaderboardAdapter(JSONArray data) {
			super();
			this.data = data;
		}

		@Override
		public int getCount() {
			if (data != null)
				return data.length();
			return 0;
		}

		@Override
		public Object getItem(int i) {
			if (data != null && i < data.length())
				try {
					return data.get(i);
				}
				catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			return null;
		}

		@Override
		public long getItemId(int i) {
			return i;
		}

		@Override
		public View getView(int pos, View view, ViewGroup container) {
			// create if needed
			if (view == null) {
				view = getLayoutInflater().inflate(R.layout.leaderboard_item, null);
			}

			// get item
			JSONObject record = (JSONObject) getItem(pos);

			try {
				TextView char01 = (TextView) view.findViewById(R.id.char01);
				TextView char02 = (TextView) view.findViewById(R.id.char02);
				TextView char03 = (TextView) view.findViewById(R.id.char03);
				TextView char04 = (TextView) view.findViewById(R.id.char04);
				TextView char05 = (TextView) view.findViewById(R.id.char05);
				TextView char06 = (TextView) view.findViewById(R.id.char06);
				TextView char07 = (TextView) view.findViewById(R.id.char07);
				TextView char08 = (TextView) view.findViewById(R.id.char08);
				TextView char09 = (TextView) view.findViewById(R.id.char09);
				TextView char10 = (TextView) view.findViewById(R.id.char10);
				TextView char11 = (TextView) view.findViewById(R.id.char11);
				TextView char12 = (TextView) view.findViewById(R.id.char12);
				TextView char13 = (TextView) view.findViewById(R.id.char13);
				TextView char14 = (TextView) view.findViewById(R.id.char14);
				TextView char15 = (TextView) view.findViewById(R.id.char15);
				TextView char16 = (TextView) view.findViewById(R.id.char16);
				TextView char17 = (TextView) view.findViewById(R.id.char17);
				TextView char18 = (TextView) view.findViewById(R.id.char18);
				TextView char19 = (TextView) view.findViewById(R.id.char19);
				TextView char20 = (TextView) view.findViewById(R.id.char20);
				TextView char21 = (TextView) view.findViewById(R.id.char21);
				TextView char22 = (TextView) view.findViewById(R.id.char22);
				TextView char23 = (TextView) view.findViewById(R.id.char23);
				TextView char24 = (TextView) view.findViewById(R.id.char24);

				// int width = leaderboardHeader.getWidth() / 24;
				// char01.setWidth(width);
				// char02.setWidth(width);
				// char03.setWidth(width);
				// char04.setWidth(width);
				// char05.setWidth(width);
				// char06.setWidth(width);
				// char07.setWidth(width);
				// char08.setWidth(width);
				// char09.setWidth(width);
				// char10.setWidth(width);
				// char11.setWidth(width);
				// char12.setWidth(width);
				// char13.setWidth(width);
				// char14.setWidth(width);
				// char15.setWidth(width);
				// char16.setWidth(width);
				// char17.setWidth(width);
				// char18.setWidth(width);
				// char19.setWidth(width);
				// char20.setWidth(width);
				// char21.setWidth(width);
				// char22.setWidth(width);
				// char23.setWidth(width);
				// char24.setWidth(width);

				String position = String.format("%02d", pos + 1);
				char01.setText(String.valueOf(position.charAt(position.length() - 2)));
				char02.setText(String.valueOf(position.charAt(position.length() - 1)));

				String id = String.format("%8d", record.getLong("ID"));
				char03.setText(String.valueOf(id.charAt(id.length() - 8)));
				char04.setText(String.valueOf(id.charAt(id.length() - 7)));
				char05.setText(String.valueOf(id.charAt(id.length() - 6)));
				char06.setText(String.valueOf(id.charAt(id.length() - 5)));
				char07.setText(String.valueOf(id.charAt(id.length() - 4)));
				char08.setText(String.valueOf(id.charAt(id.length() - 3)));
				char09.setText(String.valueOf(id.charAt(id.length() - 2)));
				char10.setText(String.valueOf(id.charAt(id.length() - 1)));

//				String dayScore = String.format("%6d", record.isNull("DayScore") ? 0 : record.getLong("DayScore"));
//				char11.setText(String.valueOf(dayScore.charAt(dayScore.length() - 6)));
//				char12.setText(String.valueOf(dayScore.charAt(dayScore.length() - 5)));
//				char13.setText(String.valueOf(dayScore.charAt(dayScore.length() - 4)));
//				char14.setText(String.valueOf(dayScore.charAt(dayScore.length() - 3)));
//				char15.setText(String.valueOf(dayScore.charAt(dayScore.length() - 2)));
//				char16.setText(String.valueOf(dayScore.charAt(dayScore.length() - 1)));

				String score = String.format("%8d", record.isNull("Score") ? 0 : record.getLong("Score"));
				char15.setText(String.valueOf(score.charAt(score.length() - 8)));
				char16.setText(String.valueOf(score.charAt(score.length() - 7)));
				char17.setText(String.valueOf(score.charAt(score.length() - 6)));
				char18.setText(String.valueOf(score.charAt(score.length() - 5)));
				char19.setText(String.valueOf(score.charAt(score.length() - 4)));
				char20.setText(String.valueOf(score.charAt(score.length() - 3)));
				char21.setText(String.valueOf(score.charAt(score.length() - 2)));
				char22.setText(String.valueOf(score.charAt(score.length() - 1)));
			}
			catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// return view
			view.setTag(record);

			return view;
		}
	}

}
