package com.vietdev.quatangtrithuc;

import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.vietdev.cunglatyphu.R;
import com.vietdev.network.ClientUtils;
import com.vietdev.network.ServiceClient;
import com.vietdev.quatangtrithuc.util.SoundManager;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.View.MeasureSpec;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;

/**
 * An example full-screen activity that shows and hides the system UI (i.e. status bar and navigation/system bar) with
 * user interaction.
 * 
 * @see SystemUiHider
 */
public class PrizesActivity extends Activity {

	private int direction;

	public PrizesActivity getContext() {
		return this;
	}

	private Handler timerHandler = new Handler() {
		public void handleMessage(Message msg) {
			if (isLoading == false) {
				loadState();
			}
		}
	};

	public String contestKey;
	public long contestState;

	private Timer updateTimer;
	private boolean isLoading;
	private boolean isFailed;
	private int networkFailedCount;

	private RelativeLayout prizeLayout;
	private CustomHorizontalScrollView layoutPrizeContainer;

	private Handler slideTimerHandler = new Handler() {
		public void handleMessage(Message msg) {
			prizeLayout.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
			int width = prizeLayout.getMeasuredWidth() / 7;

			if (direction == 1) {
				++activeItem;
			}
			else {
				--activeItem;
			}

			if (activeItem > 6) {
				direction = 0;
				activeItem = 5;
			}
			else if (activeItem < 0) {
				direction = 1;
				activeItem = 1;
			}

			if (direction == 1) {
				layoutPrizeContainer.smoothScrollTo(width * activeItem, 0);
			}
			else {
				layoutPrizeContainer.smoothScrollTo(width * activeItem, 0);
			}
		}
	};

	private Timer slideTimer;
	private int activeItem;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// Remove notification bar
		// this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_prizes);

		// Get tracker.
		Tracker t = ClientUtils.getTracker(getContext());

		// Set screen name.
		// Where path is a String representing the screen name.
		t.setScreenName("Contest info screen");

		// Send a screen view.
		t.send(new HitBuilders.AppViewBuilder().build());

		contestKey = getIntent().getExtras().getString("ContestKey");
		contestState = getIntent().getExtras().getInt("ContestState");

		final SharedPreferences prefs = ClientUtils.getSharedPreferences(this);
		long userID = prefs.getLong(ServiceClient.USER_ID, 0);
		long dayScore = prefs.getLong(ServiceClient.USER_DAY_SCORE, 0);
		long totalScore = prefs.getLong(ServiceClient.USER_SCORE, 0);

		TextView idLabel = (TextView) findViewById(R.id.idLabel);
		idLabel.setText("" + userID);
		TextView dayScoreLabel = (TextView) findViewById(R.id.dayScoreLabel);
		dayScoreLabel.setText("" + dayScore);
		TextView totalScoreLabel = (TextView) findViewById(R.id.totalScoreLabel);
		totalScoreLabel.setText("" + totalScore);

		networkFailedCount = 0;
		
		direction = 1;
		activeItem = 0;

		prizeLayout = (RelativeLayout) findViewById(R.id.layoutPrizeBox);
		prizeLayout.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		int width = prizeLayout.getMeasuredWidth();
		int height = prizeLayout.getMeasuredHeight();
		layoutPrizeContainer = new CustomHorizontalScrollView(getContext(), 7, width);

		prizeLayout.addView(layoutPrizeContainer);

		LinearLayout container = new LinearLayout(getContext());
		container.setLayoutParams(new LayoutParams(width, height));

		// Add images
		height = 2 * height / 3;
		ImageView image = new ImageView(getContext());
		image.setLayoutParams(new LayoutParams(width, height));
		image.setImageResource(R.drawable.prize_01);
		image.setScaleType(ScaleType.FIT_CENTER);
		container.addView(image);

		image = new ImageView(getContext());
		image.setLayoutParams(new LayoutParams(width, height));
		image.setImageResource(R.drawable.prize_02);
		image.setScaleType(ScaleType.FIT_CENTER);
		container.addView(image);

		image = new ImageView(getContext());
		image.setLayoutParams(new LayoutParams(width, height));
		image.setImageResource(R.drawable.prize_03);
		image.setScaleType(ScaleType.FIT_CENTER);
		container.addView(image);

		image = new ImageView(getContext());
		image.setLayoutParams(new LayoutParams(width, height));
		image.setImageResource(R.drawable.prize_04);
		image.setScaleType(ScaleType.FIT_CENTER);
		container.addView(image);

		image = new ImageView(getContext());
		image.setLayoutParams(new LayoutParams(width, height));
		image.setImageResource(R.drawable.prize_05);
		image.setScaleType(ScaleType.FIT_CENTER);
		container.addView(image);

		image = new ImageView(getContext());
		image.setLayoutParams(new LayoutParams(width, height));
		image.setImageResource(R.drawable.prize_06);
		image.setScaleType(ScaleType.FIT_CENTER);
		container.addView(image);

		image = new ImageView(getContext());
		image.setLayoutParams(new LayoutParams(width, height));
		image.setImageResource(R.drawable.prize_07);
		image.setScaleType(ScaleType.FIT_CENTER);
		container.addView(image);

		layoutPrizeContainer.addView(container);
	}

	@Override
	protected void onResume() {
		super.onResume();

		SoundManager.getInstance(getContext()).playBackground();

		if (updateTimer == null) {
			updateTimer = new Timer();
		}

		if (slideTimer == null) {
			slideTimer = new Timer();
		}

		isFailed = false;
		isLoading = false;
		updateTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				timerHandler.obtainMessage(1).sendToTarget();
			}
		}, 0, 2000);

		slideTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				slideTimerHandler.obtainMessage(1).sendToTarget();
			}
		}, 0, 5000);
	}

	@Override
	protected void onPause() {
		super.onPause();

		SoundManager.getInstance(getContext()).stopAudios();
	}

	@Override
	protected void onStop() {
		super.onStop();

		SoundManager.getInstance(getContext()).stopAudios();

		if (updateTimer != null) {
			updateTimer.cancel();
			updateTimer = null;
		}

		if (slideTimer != null) {
			slideTimer.cancel();
			slideTimer = null;
		}
	}

	private void loadState() {
		if (this.isFinishing()) {
			return;
		}

		if (this.contestKey != "" && this.contestKey != null) {
			isLoading = true;

			ServiceClient.getInstance().getState(contestKey, getContext(), new Handler() {
				@Override
				public void handleMessage(Message msg) {
					try {
						JSONObject jsonObject = (JSONObject) msg.obj;

						contestState = jsonObject.getInt("Status");
						processState();
					}
					catch (JSONException e) {
						Log.e(this.getClass().getName(), "Unable to parse json string", e);
					}

					isLoading = false;

					// Reset error
					networkFailedCount = 0;
				}
			}, new Handler() {
				@Override
				public void handleMessage(Message msg) {
					// Alert error
					++networkFailedCount;
					if (isFailed == false && networkFailedCount >= 5) {
						ClientUtils.alert(getContext(), "Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công.", null);
						isFailed = true;
						networkFailedCount = 0;
					}

					isLoading = false;
				}
			});

		}
	}

	private void processState() {
		if (this.isFinishing()) {
			return;
		}

		if (contestState >= 10 && contestState < 10000) {
			if (contestState % 10 == 3) {
				// Go to summary page
				Intent intent = new Intent(getContext(), ResultActivity.class);
				intent.putExtra("ContestKey", contestKey);

				finish();
				startActivity(intent);
			}
			else if (contestState % 10 == 2) {
				// Question
				Intent intent = new Intent(getContext(), QuestionActivity.class);
				intent.putExtra("ContestKey", contestKey);
				intent.putExtra("QuestionNumber", (int) contestState / 10);

				finish();
				startActivity(intent);
			}
			else {
				// Go to question info page
				Intent intent = new Intent(getContext(), QuestionInfoActivity.class);
				intent.putExtra("ContestKey", contestKey);
				intent.putExtra("QuestionNumber", (int) contestState / 10);
				intent.putExtra("ContestState", contestState);

				finish();
				startActivity(intent);
			}
		}
		else if (contestState == 10400) {
			Intent intent = new Intent(getContext(), QuestionInfoActivity.class);
			intent.putExtra("ContestKey", contestKey);
			intent.putExtra("QuestionNumber", (int) contestState / 10);
			intent.putExtra("ContestState", contestState);

			finish();
			startActivity(intent);			
		}
		else if (contestState == 10401) {
			// Go to question page
			final SharedPreferences prefs = ClientUtils.getSharedPreferences(this);

			Intent intent = new Intent(getContext(), QuestionActivity.class);
			intent.putExtra("ContestKey", contestKey);
			intent.putExtra("ContestState", contestState);
			intent.putExtra("QuestionNumber", prefs.getInt(ServiceClient.NUMBER_OF_QUESTIONS, 0) + 1);

			finish();
			startActivity(intent);
		}
		else if (contestState == 10402) {
			// Go to result page
			Intent intent = new Intent(getContext(), ResultActivity.class);
			intent.putExtra("ContestKey", contestKey);

			finish();
			startActivity(intent);
		}
		else if (contestState == 10300) {
			Intent intent = new Intent(getContext(), SummaryActivity.class);
			intent.putExtra("ContestKey", contestKey);
			intent.putExtra("ShowFull", true);

			finish();
			startActivity(intent);
		}
	}
}
