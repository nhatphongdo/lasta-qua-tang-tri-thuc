package com.vietdev.quatangtrithuc;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.vietdev.cunglatyphu.R;
import com.vietdev.cunglatyphu.R.layout;
import com.vietdev.network.ClientUtils;
import com.vietdev.network.ServiceClient;
import com.vietdev.quatangtrithuc.util.SoundManager;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ToggleButton;

public class SettingsActivity extends Activity {

	private EditText textFullname;
	private EditText textPhoneNumber;
	private EditText textAddress;
	private EditText textEmail;
	private ToggleButton buttonOptIn;
	private Button butSubmit;

	private SettingsActivity getContext() {
		return this;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// Remove notification bar
		// this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_settings);

		// Get tracker.
		Tracker t = ClientUtils.getTracker(getContext());

		// Set screen name.
		// Where path is a String representing the screen name.
		t.setScreenName("Settings screen");

		// Send a screen view.
		t.send(new HitBuilders.AppViewBuilder().build());

		textFullname = (EditText) findViewById(R.id.textFullname);
		textPhoneNumber = (EditText) findViewById(R.id.textPhoneNumber);
		textAddress = (EditText) findViewById(R.id.textAddress);
		textEmail = (EditText) findViewById(R.id.textEmail);
		buttonOptIn = (ToggleButton) findViewById(R.id.toggleOptIn);

		final SharedPreferences prefs = ClientUtils.getSharedPreferences(this);
		textFullname.setText(prefs.getString(ServiceClient.USER_FULLNAME, ""));
		textPhoneNumber.setText(prefs.getString(ServiceClient.USER_PHONE_NUMBER, ""));
		textAddress.setText(prefs.getString(ServiceClient.USER_ADDRESS, ""));
		textEmail.setText(prefs.getString(ServiceClient.USER_EMAIL, ""));
		buttonOptIn.setChecked(prefs.getBoolean(ServiceClient.USER_OPT_IN, true));

		Button butBack = (Button) findViewById(R.id.butBack);
		butBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				SoundManager.getInstance(getContext()).playButton();
				finish();
			}
		});

		butSubmit = (Button) findViewById(R.id.butSubmit);
		butSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				SoundManager.getInstance(getContext()).playButton();

				if (textFullname.getText().length() == 0) {
					ClientUtils.alert(getContext(), "Bạn chưa nhập Tên người chơi.", null);
					return;
				}
				if (textPhoneNumber.getText().length() == 0) {
					ClientUtils.alert(getContext(), "Bạn chưa nhập Số điện thoại.", null);
					return;
				}
				if (textAddress.getText().length() == 0) {
					ClientUtils.alert(getContext(), "Bạn chưa nhập Địa chỉ.", null);
					return;
				}
				if (textEmail.getText().length() == 0) {
					ClientUtils.alert(getContext(), "Bạn chưa nhập Địa chỉ email.", null);
					return;
				}

				butSubmit.setEnabled(false);

				long userID = prefs.getLong(ServiceClient.USER_ID, 0);
				String deviceID = Secure.getString(getContext().getContentResolver(), Secure.ANDROID_ID);
				
				String fullname = TextUtils.htmlEncode(textFullname.getText().toString());
				String phoneNumber = TextUtils.htmlEncode(textPhoneNumber.getText().toString());
				String address = TextUtils.htmlEncode(textAddress.getText().toString());

				ServiceClient.getInstance().updateUser(userID, deviceID, textFullname.getText().toString(), textPhoneNumber.getText().toString(), textAddress.getText().toString(),
						textEmail.getText().toString(), buttonOptIn.isChecked() ? 1 : 0, getContext(), new Handler() {
							@Override
							public void handleMessage(Message msg) {
								try {
									JSONObject jsonObject = (JSONObject) msg.obj;

									if (jsonObject.getInt("Error") == 0) {
										final SharedPreferences innerPrefs = ClientUtils.getSharedPreferences(getContext());
										Editor editor = innerPrefs.edit();
										editor.putString(ServiceClient.USER_FULLNAME, textFullname.getText().toString());
										editor.putString(ServiceClient.USER_PHONE_NUMBER, textPhoneNumber.getText().toString());
										editor.putString(ServiceClient.USER_ADDRESS, textAddress.getText().toString());
										editor.putString(ServiceClient.USER_EMAIL, textEmail.getText().toString());
										editor.commit();

										ClientUtils.alert(getContext(), "Bạn đã cập nhật thông tin cá nhân thành công.", null);
									}
									else {
										ClientUtils.alert(getContext(), "Chương trình xảy ra lỗi trong quá trình xử lý. Vui lòng thử lại hoặc thông báo tới BTC để được hướng dẫn xử lý và hỗ trợ.",
												null);
									}
								}
								catch (JSONException e) {
									Log.e(this.getClass().getName(), "Unable to parse json string", e);
								}

								butSubmit.setEnabled(true);
							}
						}, new Handler() {
							@Override
							public void handleMessage(Message msg) {
								// Alert error
								ClientUtils.alert(getContext(), "Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại.", null);

								butSubmit.setEnabled(true);
							}
						});
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();

		SoundManager.getInstance(getContext()).playOpenning();
	}

	@Override
	protected void onPause() {
		super.onPause();

		SoundManager.getInstance(getContext()).stopAudios();
	}

	@Override
	protected void onStop() {
		super.onStop();

		SoundManager.getInstance(getContext()).stopAudios();
	}
}
