package com.vietdev.quatangtrithuc;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.vietdev.cunglatyphu.R;
import com.vietdev.network.ClientUtils;
import com.vietdev.quatangtrithuc.util.SoundManager;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class ShareActivity extends Activity {

	private static final List<String> PERMISSIONS = Arrays.asList("publish_actions");
	private static final String PENDING_PUBLISH_KEY = "pendingPublishReauthorization";
	private boolean pendingPublishReauthorization = false;

	private ShareActivity getContext() {
		return this;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// Remove notification bar
		// this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_share);

		// Get tracker.
		Tracker t = ClientUtils.getTracker(getContext());

		// Set screen name.
		// Where path is a String representing the screen name.
		t.setScreenName("Sharing Screen");

		// Send a screen view.
		t.send(new HitBuilders.AppViewBuilder().build());

		Button butBack = (Button) findViewById(R.id.butBack);
		butBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				SoundManager.getInstance(getContext()).playButton();
				finish();
			}
		});

		Button butShare = (Button) findViewById(R.id.butShare);
		butShare.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				SoundManager.getInstance(getContext()).playButton();

				Session session = Session.getActiveSession();

				if (session != null && !session.isClosed()) {
					publishFeedDialog(session);
				}
				else {
					Session.openActiveSession(getContext(), true, callback);
				}
			}
		});

		Button butInvite = (Button) findViewById(R.id.butInvite);
		butInvite.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				SoundManager.getInstance(getContext()).playButton();

			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();

		SoundManager.getInstance(getContext()).playOpenning();
	}

	@Override
	protected void onPause() {
		super.onPause();

		SoundManager.getInstance(getContext()).stopAudios();
	}

	@Override
	protected void onStop() {
		super.onStop();

		SoundManager.getInstance(getContext()).stopAudios();
	}

	private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
		for (String string : subset) {
			if (!superset.contains(string)) {
				return false;
			}
		}
		return true;
	}

	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	private void onSessionStateChange(Session session, SessionState state, Exception exception) {
		if (state.isOpened()) {
			publishFeedDialog(session);
		}
	}

	private void publishFeedDialog(Session session) {
		// Check for publish permissions
		// List<String> permissions = session.getPermissions();
		// if (!isSubsetOf(PERMISSIONS, permissions)) {
		// pendingPublishReauthorization = true;
		// Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(getContext(),
		// PERMISSIONS);
		// session.requestNewPublishPermissions(newPermissionsRequest);
		// return;
		// }

		Bundle postParams = new Bundle();
		postParams.putString("name", "Facebook SDK for Android");
		postParams.putString("caption", "Build great social apps and get more installs.");
		postParams.putString("description", "The Facebook SDK for Android makes it easier and faster to develop Facebook integrated Android apps.");
		postParams.putString("link", "https://developers.facebook.com/android");
		postParams.putString("picture", "https://raw.github.com/fbsamples/ios-3.x-howtos/master/Images/iossdk_logo.png");

		Request.Callback callback = new Request.Callback() {
			@Override
			public void onCompleted(Response response) {
				JSONObject graphResponse = response.getGraphObject().getInnerJSONObject();
				String postId = null;
				try {
					postId = graphResponse.getString("id");
					Log.d("FB", postId);
				}
				catch (JSONException e) {
				}
				FacebookRequestError error = response.getError();
				if (error != null) {
					Toast.makeText(getContext().getApplicationContext(), error.getErrorMessage(), Toast.LENGTH_SHORT).show();
				}
				else {
					Toast.makeText(getContext().getApplicationContext(), postId, Toast.LENGTH_LONG).show();
				}
			}
		};

		Request request = new Request(session, "me/feed", postParams, HttpMethod.POST, callback);
		request.executeAndWait();
	}

}
