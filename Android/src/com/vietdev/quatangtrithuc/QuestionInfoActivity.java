package com.vietdev.quatangtrithuc;

import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.vietdev.cunglatyphu.R;
import com.vietdev.network.ClientUtils;
import com.vietdev.network.ServiceClient;
import com.vietdev.quatangtrithuc.util.SoundManager;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * An example full-screen activity that shows and hides the system UI (i.e. status bar and navigation/system bar) with
 * user interaction.
 * 
 * @see SystemUiHider
 */
public class QuestionInfoActivity extends Activity {

	public QuestionInfoActivity getContext() {
		return this;
	}

	private Handler timerHandler = new Handler() {
		public void handleMessage(Message msg) {
			if (isLoading == false) {
				if (isQuestionLoaded == false) {
					loadQuestion(questionNumber);
				}
				else {
					loadState();
				}
			}
		}
	};

	public String contestKey;
	public long contestState;

	public long questionScore;
	public int questionNumber;
	public String topic;
	public String answerA;
	public String answerB;
	public String answerC;
	public String answerD;
	public int totalTime;

	private Timer updateTimer;
	private boolean isLoading;
	private boolean isFailed;
	private boolean isQuestionLoaded;
	private int networkFailedCount;

	private boolean isLoadedTopic;
	private boolean isLoadedScore;

	private TextView topicTextTitle;
	private TextView scoreText;
	private ImageView topicImage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// Remove notification bar
		// this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_question_info);

		// Get tracker.
		Tracker t = ClientUtils.getTracker(getContext());

		// Set screen name.
		// Where path is a String representing the screen name.
		t.setScreenName("Question info screen");

		// Send a screen view.
		t.send(new HitBuilders.AppViewBuilder().build());

		networkFailedCount = 0;

		contestKey = getIntent().getExtras().getString("ContestKey");
		contestState = getIntent().getExtras().getLong("ContestState");
		questionNumber = getIntent().getExtras().getInt("QuestionNumber");

		topicTextTitle = (TextView) findViewById(R.id.topicTextTitle);
		scoreText = (TextView) findViewById(R.id.scoreText);
		topicImage = (ImageView) findViewById(R.id.topicImage);

		final SharedPreferences prefs = ClientUtils.getSharedPreferences(this);
		long userID = prefs.getLong(ServiceClient.USER_ID, 0);
		long dayScore = prefs.getLong(ServiceClient.USER_DAY_SCORE, 0);
		long totalScore = prefs.getLong(ServiceClient.USER_SCORE, 0);

		TextView idLabel = (TextView) findViewById(R.id.idLabel);
		idLabel.setText("" + userID);
		TextView dayScoreLabel = (TextView) findViewById(R.id.dayScoreLabel);
		dayScoreLabel.setText("" + dayScore);
		TextView totalScoreLabel = (TextView) findViewById(R.id.totalScoreLabel);
		totalScoreLabel.setText("" + totalScore);

		isLoadedScore = false;
		isLoadedTopic = false;

		isQuestionLoaded = false;
		loadQuestion(questionNumber);
	}

	@Override
	protected void onResume() {
		super.onResume();

		SoundManager.getInstance(getContext()).playBackground();

		if (updateTimer == null) {
			updateTimer = new Timer();
		}

		isFailed = false;
		isLoading = false;

		updateTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				timerHandler.obtainMessage(1).sendToTarget();
			}
		}, 0, 2000);
	}

	@Override
	protected void onPause() {
		super.onPause();

		SoundManager.getInstance(getContext()).stopAudios();
	}

	@Override
	protected void onStop() {
		super.onStop();

		if (updateTimer != null) {
			updateTimer.cancel();
			updateTimer = null;
		}
	}

	private void loadQuestion(int number) {
		if (this.contestKey != "" && this.contestKey != null && number > 0) {
			isLoading = true;

			ServiceClient.getInstance().getQuestion(contestKey, number, getContext(), new Handler() {
				@Override
				public void handleMessage(Message msg) {
					try {
						JSONObject jsonObject = (JSONObject) msg.obj;

						topic = jsonObject.getString("Topic");
						questionScore = jsonObject.getLong("Score");
						answerA = jsonObject.getString("Answer1");
						answerB = jsonObject.getString("Answer2");
						answerC = jsonObject.getString("Answer3");
						answerD = jsonObject.getString("Answer4");
						totalTime = jsonObject.getInt("CountdownTime");

						processState();

						isQuestionLoaded = true;
					}
					catch (JSONException e) {
						Log.e(this.getClass().getName(), "Unable to parse json string", e);
					}

					isLoading = false;
					networkFailedCount = 0;
				}
			}, new Handler() {
				@Override
				public void handleMessage(Message msg) {
					++networkFailedCount;
					// Alert error
					if (isFailed == false && networkFailedCount >= 5) {
						ClientUtils.alert(getContext(), "Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công.", null);
						isFailed = true;

						networkFailedCount = 0;
					}

					isLoading = false;
				}
			});

		}
	}

	private void loadState() {
		if (this.isFinishing()) {
			return;
		}

		if (this.contestKey != "" && this.contestKey != null) {
			isLoading = true;

			ServiceClient.getInstance().getState(contestKey, getContext(), new Handler() {
				@Override
				public void handleMessage(Message msg) {
					try {
						JSONObject jsonObject = (JSONObject) msg.obj;

						contestState = jsonObject.getLong("Status");

						isLoading = false;

						if (contestState % 10 == 1) {
							loadQuestion(questionNumber);
						}
						else {
							processState();
						}
					}
					catch (JSONException e) {
						Log.e(this.getClass().getName(), "Unable to parse json string", e);
					}

					isLoading = false;

					networkFailedCount = 0;
				}
			}, new Handler() {
				@Override
				public void handleMessage(Message msg) {
					++networkFailedCount;
					// Alert error
					if (isFailed == false && networkFailedCount >= 5) {
						ClientUtils.alert(getContext(), "Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công.", null);
						isFailed = true;
					}

					isLoading = false;
				}
			});

		}
	}

	private void processState() {
		if (this.isFinishing()) {
			return;
		}

		if (contestState >= 10 && contestState < 10000) {
			// Question
			if (contestState % 10 == 2) {
				final SharedPreferences prefs = ClientUtils.getSharedPreferences(getContext());
				Editor editor = prefs.edit();
				editor.putInt(ServiceClient.CURRENT_QUESTION_NUMBER, (int) contestState / 10);
				editor.commit();

				Intent intent = new Intent(getContext(), QuestionActivity.class);
				intent.putExtra("ContestKey", contestKey);
				intent.putExtra("ContestState", contestState);
				intent.putExtra("QuestionNumber", (int) contestState / 10);
				intent.putExtra("Topic", topic);
				intent.putExtra("AnswerA", answerA);
				intent.putExtra("AnswerB", answerB);
				intent.putExtra("AnswerC", answerC);
				intent.putExtra("AnswerD", answerD);
				intent.putExtra("QuestionScore", questionScore);
				intent.putExtra("TotalTime", totalTime);

				finish();
				startActivity(intent);
			}
			else if (contestState % 10 == 3) {
				// Go to summary page
				Intent intent = new Intent(getContext(), ResultActivity.class);
				intent.putExtra("ContestKey", contestKey);

				finish();
				startActivity(intent);
			}
			else if (contestState % 10 == 0) {
				topicImage.setVisibility(View.VISIBLE);
				scoreText.setVisibility(View.INVISIBLE);

				topicTextTitle.setText("CHỦ ĐỀ CỦA CÂU " + questionNumber);

				if (topic.equalsIgnoreCase("Sức khỏe")) {
					topicImage.setImageResource(R.drawable.suckhoe);
				}
				else if (topic.equalsIgnoreCase("Địa lý")) {
					topicImage.setImageResource(R.drawable.dialy);
				}
				else if (topic.equalsIgnoreCase("Giải trí")) {
					topicImage.setImageResource(R.drawable.giaitri);
				}
				else if (topic.equalsIgnoreCase("Lịch sử")) {
					topicImage.setImageResource(R.drawable.lichsu);
				}
				else if (topic.equalsIgnoreCase("Thể thao")) {
					topicImage.setImageResource(R.drawable.thethao);
				}
				else if (topic.equalsIgnoreCase("Văn hóa - Xã hội")) {
					topicImage.setImageResource(R.drawable.vanhoaxahoi);
				}
				else if (topic.equalsIgnoreCase("Văn học")) {
					topicImage.setImageResource(R.drawable.vanhoc);
				}
				else if (topic.equalsIgnoreCase("Khoa học - Công nghệ")) {
					topicImage.setImageResource(R.drawable.khoahoccongnghe);
				}

				if (isLoadedTopic == false) {
					SoundManager.getInstance(getContext()).playTransition();
					isLoadedTopic = true;
				}
			}
			else {
				topicImage.setVisibility(View.INVISIBLE);
				scoreText.setVisibility(View.VISIBLE);

				topicTextTitle.setText("ĐIỂM SỐ CỦA CÂU " + questionNumber);

				scoreText.setText("" + questionScore);

				if (isLoadedScore == false) {
					SoundManager.getInstance(getContext()).playTransition();
					isLoadedScore = true;
				}
			}
		}
		else if (contestState == 10400) {
			topicImage.setVisibility(View.INVISIBLE);
			scoreText.setVisibility(View.VISIBLE);

			topicTextTitle.setText("ĐIỂM SỐ CỦA CÂU " + questionNumber);

			scoreText.setText("" + questionScore);
		}
		else if (contestState == 10401) {
			// Go to question page
			final SharedPreferences prefs = ClientUtils.getSharedPreferences(getContext());

			Intent intent = new Intent(getContext(), QuestionActivity.class);
			intent.putExtra("ContestKey", contestKey);
			intent.putExtra("ContestState", contestState);
			intent.putExtra("QuestionNumber", prefs.getInt(ServiceClient.NUMBER_OF_QUESTIONS, 0) + 1);
			intent.putExtra("Topic", topic);
			intent.putExtra("AnswerA", answerA);
			intent.putExtra("AnswerB", answerB);
			intent.putExtra("AnswerC", answerC);
			intent.putExtra("AnswerD", answerD);
			intent.putExtra("TotalTime", totalTime);
			intent.putExtra("QuestionScore", questionScore);

			finish();
			startActivity(intent);
		}
		else if (contestState == 10402) {
			// Go to result page
			Intent intent = new Intent(getContext(), ResultActivity.class);
			intent.putExtra("ContestKey", contestKey);

			finish();
			startActivity(intent);
		}
		else if (contestState == 10000 || contestState == 10100 || contestState == 10200) {
			final SharedPreferences prefs = ClientUtils.getSharedPreferences(getContext());

			Intent intent;
			if (prefs.getInt(ServiceClient.CURRENT_QUESTION_NUMBER, 0) > 0 && prefs.getInt(ServiceClient.CURRENT_QUESTION_NUMBER, 0) % 4 == 0) {
				intent = new Intent(getContext(), SummaryActivity.class);
				intent.putExtra("ContestKey", contestKey);
			}
			else {
				// Info screen
				intent = new Intent(getContext(), PrizesActivity.class);
				intent.putExtra("ContestKey", contestKey);
			}

			finish();
			startActivity(intent);
		}
		else if (contestState == 10300) {
			Intent intent = new Intent(getContext(), SummaryActivity.class);
			intent.putExtra("ContestKey", contestKey);
			intent.putExtra("ShowFull", true);

			finish();
			startActivity(intent);
		}
	}
}
