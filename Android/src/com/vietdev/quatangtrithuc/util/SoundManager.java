package com.vietdev.quatangtrithuc.util;

import java.io.IOException;

import android.content.Context;
import android.media.MediaPlayer;

import com.vietdev.cunglatyphu.R;

public class SoundManager {

	private static SoundManager _instance;

	public static SoundManager getInstance(Context context) {
		if (_instance == null) {
			_instance = new SoundManager(context);
		}

		return _instance;
	}

	private Context _context;
	private MediaPlayer _openningPlayer;
	private MediaPlayer _backgroundPlayer;
	private MediaPlayer _buttonPlayer;
	private MediaPlayer _countDownPlayer;
	private MediaPlayer _sadPlayer;
	private MediaPlayer _smilePlayer;
	private MediaPlayer _transitionPlayer;

	public SoundManager(Context context) {
		_context = context;
	}

	public void playOpenning() {
		if (_openningPlayer != null) {
			_openningPlayer.stop();
		}
		if (_backgroundPlayer != null) {
			_backgroundPlayer.stop();
		}
		if (_buttonPlayer != null) {
			_buttonPlayer.stop();
		}
		if (_countDownPlayer != null) {
			_countDownPlayer.stop();
		}
		if (_sadPlayer != null) {
			_sadPlayer.stop();
		}
		if (_smilePlayer != null) {
			_smilePlayer.stop();
		}
		if (_transitionPlayer != null) {
			_transitionPlayer.stop();
		}

		_openningPlayer = MediaPlayer.create(_context, R.raw.openning);
		_openningPlayer.start();
	}

	public void playBackground() {
		if (_openningPlayer != null) {
			_openningPlayer.stop();
		}
		if (_backgroundPlayer != null) {
			_backgroundPlayer.stop();
		}
		if (_buttonPlayer != null) {
			_buttonPlayer.stop();
		}
		if (_countDownPlayer != null) {
			_countDownPlayer.stop();
		}
		if (_sadPlayer != null) {
			_sadPlayer.stop();
		}
		if (_smilePlayer != null) {
			_smilePlayer.stop();
		}
		if (_transitionPlayer != null) {
			_transitionPlayer.stop();
		}

		_backgroundPlayer = MediaPlayer.create(_context, R.raw.background);
		_backgroundPlayer.setLooping(true);
		_backgroundPlayer.start();
	}

	public void playButton() {
		if (_openningPlayer != null) {
			_openningPlayer.stop();
		}
		if (_backgroundPlayer != null) {
			_backgroundPlayer.stop();
		}
		if (_buttonPlayer != null) {
			_buttonPlayer.stop();
		}
		if (_countDownPlayer != null) {
			//_countDownPlayer.stop();
		}
		if (_sadPlayer != null) {
			_sadPlayer.stop();
		}
		if (_smilePlayer != null) {
			_smilePlayer.stop();
		}
		if (_transitionPlayer != null) {
			_transitionPlayer.stop();
		}

		_buttonPlayer = MediaPlayer.create(_context, R.raw.button_touched);
		_buttonPlayer.start();
	}

	public void playCountDown() {
		if (_openningPlayer != null) {
			_openningPlayer.stop();
		}
		if (_backgroundPlayer != null) {
			_backgroundPlayer.stop();
		}
		if (_buttonPlayer != null) {
			_buttonPlayer.stop();
		}
		if (_countDownPlayer != null) {
			_countDownPlayer.stop();
		}
		if (_sadPlayer != null) {
			_sadPlayer.stop();
		}
		if (_smilePlayer != null) {
			_smilePlayer.stop();
		}
		if (_transitionPlayer != null) {
			_transitionPlayer.stop();
		}

		_countDownPlayer = MediaPlayer.create(_context, R.raw.count_down);
		_countDownPlayer.start();
	}

	public void playSad() {
		if (_openningPlayer != null) {
			_openningPlayer.stop();
		}
		if (_backgroundPlayer != null) {
			_backgroundPlayer.stop();
		}
		if (_buttonPlayer != null) {
			_buttonPlayer.stop();
		}
		if (_countDownPlayer != null) {
			_countDownPlayer.stop();
		}
		if (_sadPlayer != null) {
			_sadPlayer.stop();
		}
		if (_smilePlayer != null) {
			_smilePlayer.stop();
		}
		if (_transitionPlayer != null) {
			_transitionPlayer.stop();
		}

		_sadPlayer = MediaPlayer.create(_context, R.raw.sad);
		_sadPlayer.start();

	}

	public void playSmile() {
		if (_openningPlayer != null) {
			_openningPlayer.stop();
		}
		if (_backgroundPlayer != null) {
			_backgroundPlayer.stop();
		}
		if (_buttonPlayer != null) {
			_buttonPlayer.stop();
		}
		if (_countDownPlayer != null) {
			_countDownPlayer.stop();
		}
		if (_sadPlayer != null) {
			_sadPlayer.stop();
		}
		if (_smilePlayer != null) {
			_smilePlayer.stop();
		}
		if (_transitionPlayer != null) {
			_transitionPlayer.stop();
		}

		_smilePlayer = MediaPlayer.create(_context, R.raw.smile);
		_smilePlayer.start();
	}

	public void playTransition() {
		if (_openningPlayer != null) {
			_openningPlayer.stop();
		}
		if (_backgroundPlayer != null) {
			_backgroundPlayer.stop();
		}
		if (_buttonPlayer != null) {
			_buttonPlayer.stop();
		}
		if (_countDownPlayer != null) {
			_countDownPlayer.stop();
		}
		if (_sadPlayer != null) {
			_sadPlayer.stop();
		}
		if (_smilePlayer != null) {
			_smilePlayer.stop();
		}
		if (_transitionPlayer != null) {
			_transitionPlayer.stop();
		}

		_transitionPlayer = MediaPlayer.create(_context, R.raw.transition);
		_transitionPlayer.start();
	}

	public void stopAudios() {
		if (_openningPlayer != null) {
			_openningPlayer.stop();
		}
		if (_backgroundPlayer != null) {
			_backgroundPlayer.stop();
		}
		if (_buttonPlayer != null) {
			_buttonPlayer.stop();
		}
		if (_countDownPlayer != null) {
			_countDownPlayer.stop();
		}
		if (_sadPlayer != null) {
			_sadPlayer.stop();
		}
		if (_smilePlayer != null) {
			_smilePlayer.stop();
		}
		if (_transitionPlayer != null) {
			_transitionPlayer.stop();
		}
	}
}
