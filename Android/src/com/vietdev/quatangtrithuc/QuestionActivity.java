package com.vietdev.quatangtrithuc;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.vietdev.cunglatyphu.R;
import com.vietdev.network.ClientUtils;
import com.vietdev.network.ServiceClient;
import com.vietdev.quatangtrithuc.util.SoundManager;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.View.MeasureSpec;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * An example full-screen activity that shows and hides the system UI (i.e. status bar and navigation/system bar) with
 * user interaction.
 * 
 * @see SystemUiHider
 */
public class QuestionActivity extends Activity {

	public QuestionActivity getContext() {
		return this;
	}

	private Handler timerHandler = new Handler() {
		public void handleMessage(Message msg) {
			if (isLoading == false) {
				if (isQuestionLoaded == false) {
					loadQuestion(questionNumber);
				}
				else {
					loadState();
				}
			}
		}
	};

	private Handler progressHandler = new Handler() {
		public void handleMessage(Message msg) {
			if (remainTime > 0) {
				if (updateTimer != null) {
					updateTimer.cancel();
				}

				isJoined = true;

				--progressTime;
				if (progressTime < 0) {
					progressTime = 0;
				}

				if (progressTime % 10 == 0) {
					--remainTime;
				}

				layoutProgress.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
				LayoutParams layout = progressImage.getLayoutParams();
				layout.width = (int) ((float) layoutProgress.getMeasuredWidth() / (totalTime * 10) * progressTime);
				progressImage.setLayoutParams(layout);

				processState();
			}
		}
	};

	private Handler stateHandler = new Handler() {
		public void handleMessage(Message msg) {
			if (isLoading == false && remainTime == 0 && isJoined == true) {
				loadState();
			}
		}
	};

	public String contestKey;
	public long contestState;

	public long questionScore;
	public int questionNumber;
	public String topic;
	public short numberOfQuestions;
	public int remainTime;
	public int totalTime;
	public String answerA;
	public String answerB;
	public String answerC;
	public String answerD;
	public int answer;
	public Date startAnswerTime;
	public String correctAnswer;
	public int answerTime;
	public long score;
	private int progressTime;
	private boolean isJoined;

	private Timer updateTimer;
	private Timer progressTimer;
	private Timer stateTimer;
	private boolean isLoading;
	private boolean isFailed;
	private int networkFailedCount;
	private boolean isQuestionLoaded;
	private boolean isSubmittingAnswer;
	private boolean isLocked;

	private FrameLayout layoutProgress;
	private ImageView progressImage;
	private TextView scoreText;
	private ImageView topicImage;
	private AutoResizeTextView answerALabel;
	private AutoResizeTextView answerBLabel;
	private AutoResizeTextView answerCLabel;
	private AutoResizeTextView answerDLabel;
	private ImageButton buttonAnswerA;
	private ImageButton buttonAnswerB;
	private ImageButton buttonAnswerC;
	private ImageButton buttonAnswerD;

	public QuestionActivity() {
		questionNumber = 0;
		contestKey = "";
		topic = "";
		remainTime = 0;
		progressTime = 0;
		isJoined = false;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// Remove notification bar
		// this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_question);

		// Get tracker.
		Tracker t = ClientUtils.getTracker(getContext());

		// Set screen name.
		// Where path is a String representing the screen name.
		t.setScreenName("Question Screen");

		// Send a screen view.
		t.send(new HitBuilders.AppViewBuilder().build());

		contestKey = getIntent().getExtras().getString("ContestKey");
		contestState = getIntent().getExtras().getLong("ContestState");
		questionNumber = getIntent().getExtras().getInt("QuestionNumber");
		topic = getIntent().getExtras().getString("Topic");
		answerA = getIntent().getExtras().getString("AnswerA");
		answerB = getIntent().getExtras().getString("AnswerB");
		answerC = getIntent().getExtras().getString("AnswerC");
		answerD = getIntent().getExtras().getString("AnswerD");
		totalTime = getIntent().getExtras().getInt("TotalTime");
		questionScore = getIntent().getExtras().getLong("QuestionScore");
		remainTime = getIntent().getExtras().getInt("RemainTime");
		numberOfQuestions = getIntent().getExtras().getShort("NumberOfQuestions");

		if (questionNumber == 0) {
			questionNumber = 17;
		}

		networkFailedCount = 0;

		startAnswerTime = null;

		isSubmittingAnswer = false;
		isLocked = false;

		layoutProgress = (FrameLayout) findViewById(R.id.layoutProgress);
		progressImage = (ImageView) findViewById(R.id.progressImage);
		scoreText = (TextView) findViewById(R.id.scoreText);
		topicImage = (ImageView) findViewById(R.id.topicImage);
		answerALabel = (AutoResizeTextView) findViewById(R.id.answerA);
		answerBLabel = (AutoResizeTextView) findViewById(R.id.answerB);
		answerCLabel = (AutoResizeTextView) findViewById(R.id.answerC);
		answerDLabel = (AutoResizeTextView) findViewById(R.id.answerD);
		buttonAnswerA = (ImageButton) findViewById(R.id.buttonAnswerA);
		buttonAnswerB = (ImageButton) findViewById(R.id.buttonAnswerB);
		buttonAnswerC = (ImageButton) findViewById(R.id.buttonAnswerC);
		buttonAnswerD = (ImageButton) findViewById(R.id.buttonAnswerD);

		buttonAnswerA.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isLocked == true) {
					return;
				}

				SoundManager.getInstance(getContext()).playButton();

				buttonAnswerA.setImageResource(R.drawable.bg_selected_answer);
				answerALabel.setTextColor(Color.WHITE);
				isLocked = true;
				answer = 1;
				isSubmittingAnswer = true;
				submitAnswer();
			}
		});

		buttonAnswerB.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isLocked == true) {
					return;
				}

				SoundManager.getInstance(getContext()).playButton();

				buttonAnswerB.setImageResource(R.drawable.bg_selected_answer);
				answerBLabel.setTextColor(Color.WHITE);
				isLocked = true;
				answer = 2;
				isSubmittingAnswer = true;
				submitAnswer();
			}
		});

		buttonAnswerC.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isLocked == true) {
					return;
				}

				SoundManager.getInstance(getContext()).playButton();

				buttonAnswerC.setImageResource(R.drawable.bg_selected_answer);
				answerCLabel.setTextColor(Color.WHITE);
				isLocked = true;
				answer = 3;
				isSubmittingAnswer = true;
				submitAnswer();
			}
		});

		buttonAnswerD.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isLocked == true) {
					return;
				}

				SoundManager.getInstance(getContext()).playButton();

				buttonAnswerD.setImageResource(R.drawable.bg_selected_answer);
				answerDLabel.setTextColor(Color.WHITE);
				isLocked = true;
				answer = 4;
				isSubmittingAnswer = true;
				submitAnswer();
			}
		});

		disableAnswers();
		answerALabel.setText("");
		answerBLabel.setText("");
		answerCLabel.setText("");
		answerDLabel.setText("");

		isFailed = false;
		isLoading = false;

		if (topic == null || topic.equalsIgnoreCase("") || questionScore == 0 || totalTime == 0) {
			isQuestionLoaded = false;
			loadQuestion(questionNumber);
		}
		else {
			topicImage.setVisibility(View.VISIBLE);
			if (topic.equalsIgnoreCase("Sức khỏe")) {
				topicImage.setImageResource(R.drawable.suckhoe);
			}
			else if (topic.equalsIgnoreCase("Địa lý")) {
				topicImage.setImageResource(R.drawable.dialy);
			}
			else if (topic.equalsIgnoreCase("Giải trí")) {
				topicImage.setImageResource(R.drawable.giaitri);
			}
			else if (topic.equalsIgnoreCase("Lịch sử")) {
				topicImage.setImageResource(R.drawable.lichsu);
			}
			else if (topic.equalsIgnoreCase("Thể thao")) {
				topicImage.setImageResource(R.drawable.thethao);
			}
			else if (topic.equalsIgnoreCase("Văn hóa - Xã hội")) {
				topicImage.setImageResource(R.drawable.vanhoaxahoi);
			}
			else if (topic.equalsIgnoreCase("Văn học")) {
				topicImage.setImageResource(R.drawable.vanhoc);
			}
			else if (topic.equalsIgnoreCase("Khoa học - Công nghệ")) {
				topicImage.setImageResource(R.drawable.khoahoccongnghe);
			}

			answerALabel.setText("A. " + answerA);
			answerALabel.setTextColor(Color.BLACK);

			answerBLabel.setText("B. " + answerB);
			answerBLabel.setTextColor(Color.BLACK);

			answerCLabel.setText("C. " + answerC);
			answerCLabel.setTextColor(Color.BLACK);

			answerDLabel.setText("D. " + answerD);
			answerDLabel.setTextColor(Color.BLACK);

			scoreText.setText("" + questionScore);

			isQuestionLoaded = true;
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (updateTimer == null) {
			updateTimer = new Timer();
		}

		if (progressTimer == null) {
			progressTimer = new Timer();
		}

		if (stateTimer == null) {
			stateTimer = new Timer();
		}

		isFailed = false;
		isLoading = false;

		updateTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				timerHandler.obtainMessage(1).sendToTarget();
			}
		}, 0, 500);

		progressTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				progressHandler.obtainMessage(1).sendToTarget();
			}
		}, 0, 100);

		stateTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				stateHandler.obtainMessage(1).sendToTarget();
			}
		}, 0, 2000);
	}

	@Override
	protected void onPause() {
		super.onPause();

		SoundManager.getInstance(getContext()).stopAudios();
	}

	@Override
	protected void onStop() {
		super.onStop();

		if (updateTimer != null) {
			updateTimer.cancel();
			updateTimer = null;
		}

		if (progressTimer != null) {
			progressTimer.cancel();
			progressTimer = null;
		}

		if (stateTimer != null) {
			stateTimer.cancel();
			stateTimer = null;
		}
	}

	private void loadQuestion(int number) {
		if (number > 0 && contestKey != "") {
			isLoading = true;

			// Load question
			ServiceClient.getInstance().getQuestion(contestKey, number, getContext(), new Handler() {
				@Override
				public void handleMessage(Message msg) {
					try {
						JSONObject jsonObject = (JSONObject) msg.obj;

						topic = jsonObject.getString("Topic");
						answerA = jsonObject.getString("Answer1");
						answerB = jsonObject.getString("Answer2");
						answerC = jsonObject.getString("Answer3");
						answerD = jsonObject.getString("Answer4");
						totalTime = jsonObject.getInt("CountdownTime");
						questionScore = jsonObject.getLong("Score");

						topicImage.setVisibility(View.VISIBLE);
						if (topic.equalsIgnoreCase("Sức khỏe")) {
							topicImage.setImageResource(R.drawable.suckhoe);
						}
						else if (topic.equalsIgnoreCase("Địa lý")) {
							topicImage.setImageResource(R.drawable.dialy);
						}
						else if (topic.equalsIgnoreCase("Giải trí")) {
							topicImage.setImageResource(R.drawable.giaitri);
						}
						else if (topic.equalsIgnoreCase("Lịch sử")) {
							topicImage.setImageResource(R.drawable.lichsu);
						}
						else if (topic.equalsIgnoreCase("Thể thao")) {
							topicImage.setImageResource(R.drawable.thethao);
						}
						else if (topic.equalsIgnoreCase("Văn hóa - Xã hội")) {
							topicImage.setImageResource(R.drawable.vanhoaxahoi);
						}
						else if (topic.equalsIgnoreCase("Văn học")) {
							topicImage.setImageResource(R.drawable.vanhoc);
						}
						else if (topic.equalsIgnoreCase("Khoa học - Công nghệ")) {
							topicImage.setImageResource(R.drawable.khoahoccongnghe);
						}

						answerALabel.setText("A. " + answerA);
						answerALabel.setTextColor(Color.BLACK);

						answerBLabel.setText("B. " + answerB);
						answerBLabel.setTextColor(Color.BLACK);

						answerCLabel.setText("C. " + answerC);
						answerCLabel.setTextColor(Color.BLACK);

						answerDLabel.setText("D. " + answerD);
						answerDLabel.setTextColor(Color.BLACK);

						scoreText.setText("" + questionScore);

						isQuestionLoaded = true;
					}
					catch (JSONException e) {
						Log.e(this.getClass().getName(), "Unable to parse json string", e);
					}

					isLoading = false;

					networkFailedCount = 0;
				}
			}, new Handler() {
				@Override
				public void handleMessage(Message msg) {
					// Alert error
					++networkFailedCount;
					if (isFailed == false && networkFailedCount >= 5) {
						ClientUtils.alert(getContext(), "Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công.", null);
						isFailed = true;
						networkFailedCount = 0;
					}

					isLoading = false;
				}
			});
		}
	}

	private void loadState() {
		if (this.isFinishing()) {
			return;
		}

		if (contestKey != "") {
			isLoading = true;

			// Load question
			ServiceClient.getInstance().getState(contestKey, getContext(), new Handler() {
				@Override
				public void handleMessage(Message msg) {
					try {
						JSONObject jsonObject = (JSONObject) msg.obj;

						contestState = jsonObject.getLong("Status");

						if (isJoined == false) {
							remainTime = jsonObject.getInt("RemainTime");
							progressTime = remainTime * 10;
						}

						layoutProgress.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
						LayoutParams layout = progressImage.getLayoutParams();
						layout.width = (int) ((float) layoutProgress.getMeasuredWidth() / (totalTime * 10) * progressTime);
						progressImage.setLayoutParams(layout);

						processState();
					}
					catch (JSONException e) {
						Log.e(this.getClass().getName(), "Unable to parse json string", e);
					}

					isLoading = false;

					// Reset error
					networkFailedCount = 0;
				}
			}, new Handler() {
				@Override
				public void handleMessage(Message msg) {
					++networkFailedCount;
					// Alert error
					if (isFailed == false && networkFailedCount >= 5) {
						ClientUtils.alert(getContext(), "Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công.", null);
						isFailed = true;
						networkFailedCount = 0;
					}

					isLoading = false;
				}
			});
		}
	}

	private void processState() {
		if (this.isFinishing()) {
			return;
		}

		if (contestState >= 10 && contestState < 10000) {
			// Question
			long status = contestState % 10;

			if (status < 2) {
				// Not start
				startAnswerTime = null;
			}
			else if (status == 2) {
				if (remainTime == 0) {
					if (isLocked == false) {
						scoreText.setText("0");
						isLocked = true;
						SoundManager.getInstance(getContext()).stopAudios();
					}
				}
				else {
					// Start counter
					if (startAnswerTime == null) {
						startAnswerTime = new Date();
						SoundManager.getInstance(getContext()).playCountDown();
					}

					enableAnswers();

					if (answer == 1) {
						answerALabel.setTextColor(Color.WHITE);
					}
					else if (answer == 2) {
						answerBLabel.setTextColor(Color.WHITE);
					}
					else if (answer == 3) {
						answerCLabel.setTextColor(Color.WHITE);
					}
					else if (answer == 4) {
						answerDLabel.setTextColor(Color.WHITE);
					}

					if (isLocked == false) {
						answerTime = totalTime - remainTime;
						score = questionScore / totalTime * remainTime;
						scoreText.setText("" + score);
					}
				}
			}
			else if (status == 3) {
				// Summary screen
				Intent intent = new Intent(getContext(), ResultActivity.class);
				intent.putExtra("ContestKey", contestKey);
				intent.putExtra("CorrectAnswer", correctAnswer);

				finish();
				startActivity(intent);
			}
		}
		else if (contestState == 10400) {
			// Not start
			startAnswerTime = null;
		}
		else if (contestState == 10401) {
			if (remainTime == 0) {
				if (isLocked == false) {
					scoreText.setText("0");
					isLocked = true;
					SoundManager.getInstance(getContext()).playCountDown();
				}
			}
			else {
				// Start counter
				if (startAnswerTime == null) {
					startAnswerTime = new Date();
				}

				enableAnswers();

				if (answer == 1) {
					answerALabel.setTextColor(Color.WHITE);
				}
				else if (answer == 2) {
					answerBLabel.setTextColor(Color.WHITE);
				}
				else if (answer == 3) {
					answerCLabel.setTextColor(Color.WHITE);
				}
				else if (answer == 4) {
					answerDLabel.setTextColor(Color.WHITE);
				}

				if (isLocked == false) {
					answerTime = totalTime - remainTime;
					score = questionScore / totalTime * remainTime;
					scoreText.setText("" + score);
				}
			}
		}
		else if (contestState == 10402) {
			// Result screen
			Intent intent = new Intent(getContext(), ResultActivity.class);
			intent.putExtra("ContestKey", contestKey);
			intent.putExtra("CorrectAnswer", correctAnswer);

			finish();
			startActivity(intent);
		}
		else if (contestState == 10000 || contestState == 10100 || contestState == 10200) {
			final SharedPreferences prefs = ClientUtils.getSharedPreferences(this);

			Intent intent;
			if (prefs.getInt(ServiceClient.CURRENT_QUESTION_NUMBER, 0) > 0 && prefs.getInt(ServiceClient.CURRENT_QUESTION_NUMBER, 0) % 4 == 0) {
				intent = new Intent(getContext(), SummaryActivity.class);
				intent.putExtra("ContestKey", contestKey);
			}
			else {
				// Info screen
				intent = new Intent(getContext(), PrizesActivity.class);
				intent.putExtra("ContestKey", contestKey);
			}
			
			finish();
			startActivity(intent);
		}
		else if (contestState == 10300) {
			Intent intent = new Intent(getContext(), SummaryActivity.class);
			intent.putExtra("ContestKey", contestKey);
			intent.putExtra("ShowFull", true);

			finish();
			startActivity(intent);
		}
	}

	private void disableAnswers() {
		buttonAnswerA.setEnabled(false);
		buttonAnswerB.setEnabled(false);
		buttonAnswerC.setEnabled(false);
		buttonAnswerD.setEnabled(false);
	}

	private void enableAnswers() {
		buttonAnswerA.setEnabled(true);
		buttonAnswerB.setEnabled(true);
		buttonAnswerC.setEnabled(true);
		buttonAnswerD.setEnabled(true);
	}

	private void submitAnswer() {
		if (startAnswerTime == null) {
			isSubmittingAnswer = false;
			return;
		}

		if (isSubmittingAnswer == false) {
			return;
		}

		final SharedPreferences prefs = ClientUtils.getSharedPreferences(this);
		long userID = prefs.getLong(ServiceClient.USER_ID, 0);

		long startTime = startAnswerTime.getTime();
		long endTime = (new Date()).getTime();

		ServiceClient.getInstance().submitAnswer(contestKey, userID, answer, startTime, endTime, answerTime, score, getContext(), new Handler() {
			@Override
			public void handleMessage(Message msg) {
				try {
					JSONObject jsonObject = (JSONObject) msg.obj;

					int errorCode = jsonObject.getInt("ResultCode");
					if (errorCode == 1) {
						ClientUtils.alert(getContext(), "Cuộc thi ngày hôm nay đã kết thúc. Hãy quay lại và tham gia vào ngày thi khác.", null);
					}
					else if (errorCode == 2) {
						ClientUtils.alert(getContext(), "Câu hỏi chưa bắt đầu hoặc đã kết thúc. Hãy chờ câu hỏi bắt đầu hoặc chờ câu hỏi khác.", null);
					}
					else if (errorCode == 3) {
						ClientUtils.alert(getContext(), "Bạn đã trả lời cho câu hỏi này rồi.", null);
					}
					else {
						long userScore = prefs.getLong(ServiceClient.USER_SCORE, 0);
						long userDayScore = prefs.getLong(ServiceClient.USER_DAY_SCORE, 0);
						score = jsonObject.getLong("Score");
						userScore += score;
						userDayScore += score;

						Editor editor = prefs.edit();
						editor.putLong(ServiceClient.USER_SCORE, userScore);
						editor.putLong(ServiceClient.USER_DAY_SCORE, userDayScore);
						editor.commit();
					}

					isSubmittingAnswer = false;
				}
				catch (JSONException e) {
					Log.e(this.getClass().getName(), "Unable to parse json string", e);
				}

				networkFailedCount = 0;
			}
		}, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// Alert error
				++networkFailedCount;
				if (isFailed == false && networkFailedCount >= 5) {
					ClientUtils.alert(getContext(), "Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công.", null);
					isFailed = true;
					networkFailedCount = 0;
				}

				isSubmittingAnswer = false;
				submitAnswer();
			}
		});
	}
}
