package com.vietdev.quatangtrithuc;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.vietdev.cunglatyphu.R;
import com.vietdev.network.ClientUtils;
import com.vietdev.network.ServiceClient;
import com.vietdev.quatangtrithuc.util.SoundManager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * An example full-screen activity that shows and hides the system UI (i.e. status bar and navigation/system bar) with
 * user interaction.
 * 
 * @see SystemUiHider
 */
public class MenuActivity extends Activity {

	private MenuActivity getContext() {
		return this;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// Remove notification bar
		// this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_menu);

		// Get tracker.
		Tracker t = ClientUtils.getTracker(getContext());

		// Set screen name.
		// Where path is a String representing the screen name.
		t.setScreenName("Menu Screen");

		// Send a screen view.
		t.send(new HitBuilders.AppViewBuilder().build());

		SoundManager.getInstance(getContext()).playOpenning();

		// Set action for menus
		Button butLeaderboard = (Button) findViewById(R.id.butLeaderboard);
		butLeaderboard.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// Open Leaderboard view
				SoundManager.getInstance(getContext()).playButton();

				Intent intent = new Intent(getContext(), SummaryActivity.class);
				intent.putExtra("ShowFull", true);
				startActivity(intent);
			}
		});

		Button butJoin = (Button) findViewById(R.id.butJoin);
		butJoin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// Open QR view
				SoundManager.getInstance(getContext()).playButton();

				startActivity(new Intent(getContext(), FakeCaptureActivity.class));
			}
		});

		Button butTerm = (Button) findViewById(R.id.butTerm);
		butTerm.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				SoundManager.getInstance(getContext()).playButton();

				startActivity(new Intent(getContext(), TermActivity.class));
			}

		});

		Button butChangeScore = (Button) findViewById(R.id.butChangeScore);
		butChangeScore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				SoundManager.getInstance(getContext()).playButton();

				if (ServiceClient.STUDIO_VERSION) {
					ClientUtils.alert(getContext(), "Chức năng này chỉ dành riêng cho phiên bản người chơi ở nhà.", null);
				}
				else {
					startActivity(new Intent(getContext(), ChangeScoreActivity.class));
				}
			}
		});

		Button butSendScore = (Button) findViewById(R.id.butSendScore);
		butSendScore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				SoundManager.getInstance(getContext()).playButton();

				if (ServiceClient.STUDIO_VERSION) {
					ClientUtils.alert(getContext(), "Chức năng này chỉ dành riêng cho phiên bản người chơi ở nhà.", null);
				}
				else {
					startActivity(new Intent(getContext(), SendScoreActivity.class));
				}
			}
		});

		Button butShare = (Button) findViewById(R.id.butShare);
		butShare.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				SoundManager.getInstance(getContext()).playButton();

				if (ServiceClient.STUDIO_VERSION) {
					ClientUtils.alert(getContext(), "Chức năng này chỉ dành riêng cho phiên bản người chơi ở nhà.", null);
				}
				else {
					startActivity(new Intent(getContext(), ShareActivity.class));
				}
			}
		});

		Button butSettings = (Button) findViewById(R.id.butSettings);
		butSettings.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				SoundManager.getInstance(getContext()).playButton();

				startActivity(new Intent(getContext(), SettingsActivity.class));
			}
		});
	}
}
