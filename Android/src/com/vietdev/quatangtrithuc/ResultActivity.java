package com.vietdev.quatangtrithuc;

import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.vietdev.cunglatyphu.R;
import com.vietdev.network.ClientUtils;
import com.vietdev.network.ServiceClient;
import com.vietdev.quatangtrithuc.util.SoundManager;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class ResultActivity extends Activity {

	public ResultActivity getContext() {
		return this;
	}

	private Handler timerHandler = new Handler() {
		public void handleMessage(Message msg) {
			if (isLoading == false) {
				if (isCheckingAnswer == true) {
					checkAnswer(userID);
				}
				else {
					loadState();
				}
			}
		}
	};

	public String contestKey;
	public long contestState;
	public long score;
	public long userID;
	public String answeredText;
	public String correctAnswer;
	public int correctAnswerNumber;

	private Timer updateTimer;
	private boolean isLoading;
	private boolean isFailed;
	private int networkFailedCount;
	private boolean isCheckingAnswer;

	private ImageView textTitle;
	private FrameLayout answerBackground;
	private TextView answerText;
	private ImageView textCongras;
	private ImageView iconEmoticon;
	private TextView scoreText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// Remove notification bar
		// this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_result);

		// Get tracker.
		Tracker t = ClientUtils.getTracker(getContext());

		// Set screen name.
		// Where path is a String representing the screen name.
		t.setScreenName("Result screen");

		// Send a screen view.
		t.send(new HitBuilders.AppViewBuilder().build());

		correctAnswer = "";

		contestKey = getIntent().getExtras().getString("ContestKey");
		contestState = getIntent().getExtras().getLong("ContestState");

		answerText = (TextView) findViewById(R.id.answerText);
		textTitle = (ImageView) findViewById(R.id.textTitle);
		answerBackground = (FrameLayout) findViewById(R.id.answerBackground);
		textCongras = (ImageView) findViewById(R.id.textCongras);
		iconEmoticon = (ImageView) findViewById(R.id.iconEmoticon);
		scoreText = (TextView) findViewById(R.id.scoreText);

		isFailed = false;
		networkFailedCount = 0;
		isCheckingAnswer = true;

		final SharedPreferences prefs = ClientUtils.getSharedPreferences(this);
		userID = prefs.getLong(ServiceClient.USER_ID, 0);
		checkAnswer(userID);
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (updateTimer == null) {
			updateTimer = new Timer();
		}

		isFailed = false;
		isLoading = false;

		updateTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				timerHandler.obtainMessage(1).sendToTarget();
			}
		}, 0, 2000);
	}

	@Override
	protected void onPause() {
		super.onPause();

		SoundManager.getInstance(getContext()).stopAudios();
	}

	@Override
	protected void onStop() {
		super.onStop();

		if (updateTimer != null) {
			updateTimer.cancel();
			updateTimer = null;
		}
	}

	private void loadState() {
		if (this.isFinishing()) {
			return;
		}

		if (this.contestKey != "" && this.contestKey != null) {
			isLoading = true;

			ServiceClient.getInstance().getState(contestKey, getContext(), new Handler() {
				@Override
				public void handleMessage(Message msg) {
					try {
						JSONObject jsonObject = (JSONObject) msg.obj;

						contestState = jsonObject.getLong("Status");
						processState();
					}
					catch (JSONException e) {
						Log.e(this.getClass().getName(), "Unable to parse json string", e);
					}

					isLoading = false;
					networkFailedCount = 0;
				}
			}, new Handler() {
				@Override
				public void handleMessage(Message msg) {
					// Alert error
					++networkFailedCount;
					if (isFailed == false && networkFailedCount >= 5) {
						ClientUtils.alert(getContext(), "Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công.", null);
						isFailed = true;
						networkFailedCount = 0;
					}

					isLoading = false;
				}
			});

		}
	}

	private void processState() {
		if (this.isFinishing()) {
			return;
		}

		if (contestState >= 10 && contestState < 10000) {
			if (contestState % 10 == 3) {
			}
			else if (contestState % 10 == 2) {
				// Go to question page
				// Question
				Intent intent = new Intent(getContext(), QuestionActivity.class);
				intent.putExtra("ContestKey", contestKey);
				intent.putExtra("QuestionNumber", (int) contestState / 10);
				intent.putExtra("ContestState", contestState);
				finish();
				startActivity(intent);
			}
			else {
				// Go to question info page
				Intent intent = new Intent(getContext(), QuestionInfoActivity.class);
				intent.putExtra("ContestKey", contestKey);
				intent.putExtra("QuestionNumber", (int) contestState / 10);
				intent.putExtra("ContestState", contestState);
				finish();
				startActivity(intent);
			}
		}
		else if (contestState == 10400) {
			final SharedPreferences prefs = ClientUtils.getSharedPreferences(this);
			
			Intent intent = new Intent(getContext(), QuestionInfoActivity.class);
			intent.putExtra("ContestKey", contestKey);
			intent.putExtra("QuestionNumber", prefs.getInt(ServiceClient.NUMBER_OF_QUESTIONS, 0) + 1);
			intent.putExtra("ContestState", contestState);
			finish();
			startActivity(intent);
		}
		else if (contestState == 10401) {
			// Go to question page
			final SharedPreferences prefs = ClientUtils.getSharedPreferences(this);

			Intent intent = new Intent(getContext(), QuestionActivity.class);
			intent.putExtra("ContestKey", contestKey);
			intent.putExtra("ContestState", contestState);
			intent.putExtra("QuestionNumber", prefs.getInt(ServiceClient.NUMBER_OF_QUESTIONS, 0) + 1);
			finish();
			startActivity(intent);
		}
		else if (contestState == 10000 || contestState == 10100 || contestState == 10200) {
			final SharedPreferences prefs = ClientUtils.getSharedPreferences(this);

			Intent intent;
			if (prefs.getInt(ServiceClient.CURRENT_QUESTION_NUMBER, 0) > 0 && prefs.getInt(ServiceClient.CURRENT_QUESTION_NUMBER, 0) % 4 == 0) {
				intent = new Intent(getContext(), SummaryActivity.class);
				intent.putExtra("ContestKey", contestKey);
			}
			else {
				// Go to info page
				intent = new Intent(getContext(), PrizesActivity.class);
				intent.putExtra("ContestKey", contestKey);
			}

			finish();
			startActivity(intent);
		}
		else if (contestState == 10300) {
			// Go to finished page
			Intent intent = new Intent(getContext(), SummaryActivity.class);
			intent.putExtra("ContestKey", contestKey);
			intent.putExtra("ShowFull", true);

			finish();
			startActivity(intent);
		}
	}

	private void checkAnswer(long userID) {
		isLoading = true;

		ServiceClient.getInstance().checkAnswer(userID, getContext(), new Handler() {
			@Override
			public void handleMessage(Message msg) {
				try {
					JSONObject jsonObject = (JSONObject) msg.obj;

					answerText.setText(jsonObject.getString("CorrectAnswer"));

					if (jsonObject.getInt("ResultCode") == 1) {
						SoundManager.getInstance(getContext()).playSmile();

						textTitle.setImageResource(R.drawable.text_right);
						answerBackground.setBackgroundResource(R.drawable.bg_correct_answer);
						iconEmoticon.setImageResource(R.drawable.icon_smile);

						textCongras.setVisibility(View.VISIBLE);
						scoreText.setVisibility(View.VISIBLE);
						scoreText.setText("+" + jsonObject.getInt("Score"));
					}
					else {
						// Wrong
						SoundManager.getInstance(getContext()).playSad();

						textTitle.setImageResource(R.drawable.text_wrong);
						answerBackground.setBackgroundResource(R.drawable.bg_wrong_answer);
						iconEmoticon.setImageResource(R.drawable.icon_sad);

						textCongras.setVisibility(View.INVISIBLE);
						scoreText.setVisibility(View.INVISIBLE);
					}
				}
				catch (JSONException e) {
					Log.e(this.getClass().getName(), "Unable to parse json string", e);
				}

				isLoading = false;
				isCheckingAnswer = false;

				networkFailedCount = 0;
			}
		}, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// Alert error
				if (msg.what == HttpStatus.SC_NOT_FOUND) {
					SoundManager.getInstance(getContext()).playSad();

					textTitle.setImageResource(R.drawable.text_wrong);
					answerBackground.setBackgroundResource(R.drawable.bg_wrong_answer);
					iconEmoticon.setImageResource(R.drawable.icon_sad);

					textCongras.setVisibility(View.INVISIBLE);
					scoreText.setVisibility(View.INVISIBLE);

					answerText.setText("");

					if (correctAnswer == null || correctAnswer == "") {
						// Empty string means no answer
					}
					else {
						ClientUtils.alert(getContext(), "Rất tiếc, chương trình gặp lỗi kết nối mạng khi gởi đáp án về hệ thống.", null);
					}

					isCheckingAnswer = false;
				}
				else {
					++networkFailedCount;

					if (isFailed == false && networkFailedCount >= 5) {
						ClientUtils.alert(getContext(), "Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công.", null);

						isFailed = true;
						networkFailedCount = 0;
					}
				}

				isLoading = false;
			}
		});
	}
}
