package com.vietdev.quatangtrithuc;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.vietdev.cunglatyphu.R;
import com.vietdev.network.ClientUtils;
import com.vietdev.network.ServiceClient;
import com.vietdev.quatangtrithuc.util.SoundManager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings.Secure;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;

/**
 * An example full-screen activity that shows and hides the system UI (i.e. status bar and navigation/system bar) with
 * user interaction.
 * 
 * @see SystemUiHider
 */
public class MainActivity extends Activity {

	private final static String SENDER_ID = "884135410837";

	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	/**
	 * // * Whether or not the system UI should be auto-hidden after {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds. //
	 */
	// private static final boolean AUTO_HIDE = true;
	//
	// /**
	// * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after user interaction before hiding the
	// system
	// * UI.
	// */
	// private static final int AUTO_HIDE_DELAY_MILLIS = 3000;
	//
	// /**
	// * If set, will toggle the system UI visibility upon interaction. Otherwise, will show the system UI visibility
	// upon
	// * interaction.
	// */
	// private static final boolean TOGGLE_ON_CLICK = true;
	//
	// /**
	// * The flags to pass to {@link SystemUiHider#getInstance}.
	// */
	// private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;
	//
	// /**
	// * The instance of the {@link SystemUiHider} for this activity.
	// */
	// private SystemUiHider mSystemUiHider;

	private Button butReconnect;

	private GoogleCloudMessaging gcm;
	private String regId;

	private MainActivity getContext() {
		return this;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// Remove notification bar
		// this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_main);

		// Get tracker.
		Tracker t = ClientUtils.getTracker(getContext());

		// Set screen name.
		// Where path is a String representing the screen name.
		t.setScreenName("Splash Screen");

		// Send a screen view.
		t.send(new HitBuilders.AppViewBuilder().build());

		butReconnect = (Button) findViewById(R.id.butReconnect);
		butReconnect.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				checkVersion();
			}
		});

		final SharedPreferences prefs = ClientUtils.getSharedPreferences(getContext());
		Editor editor = prefs.edit();
		editor.remove(ServiceClient.CURRENT_QUESTION_NUMBER);
		editor.remove(ServiceClient.NUMBER_OF_QUESTIONS);
		editor.commit();

		// try {
		//
		// PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
		//
		// for (Signature signature : info.signatures) {
		// MessageDigest md = MessageDigest.getInstance("SHA");
		// md.update(signature.toByteArray());
		// Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
		// }
		//
		// }
		// catch (NameNotFoundException e) {
		// Log.e("name not found", e.toString());
		// }
		// catch (NoSuchAlgorithmException e) {
		// Log.e("no such an algorithm", e.toString());
		// }

		// Check device for Play Services APK.
		if (checkPlayServices()) {
			// If this check succeeds, proceed with normal processing.
			// Otherwise, prompt user to get valid Play Services APK.
			gcm = GoogleCloudMessaging.getInstance(this);
			regId = getRegistrationId(getContext());

			if (regId.length() == 0) {
				registerInBackground();
			}
		}
		else {
			Log.d("GCM", "No valid Google Play Services APK found");
		}

		checkVersion();
		// final View controlsView = findViewById(R.id);
		// final View contentView = findViewById(R.id.fullscreen_content);

		// Set up an instance of SystemUiHider to control the system UI for
		// this activity.
		// mSystemUiHider = SystemUiHider.getInstance(this, contentView,
		// HIDER_FLAGS);
		// mSystemUiHider.setup();
		// mSystemUiHider
		// .setOnVisibilityChangeListener(new
		// SystemUiHider.OnVisibilityChangeListener() {
		// // Cached values.
		// int mControlsHeight;
		// int mShortAnimTime;
		//
		// @Override
		// @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
		// public void onVisibilityChange(boolean visible) {
		// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
		// // If the ViewPropertyAnimator API is available
		// // (Honeycomb MR2 and later), use it to animate the
		// // in-layout UI controls at the bottom of the
		// // screen.
		// if (mControlsHeight == 0) {
		// mControlsHeight = controlsView.getHeight();
		// }
		// if (mShortAnimTime == 0) {
		// mShortAnimTime = getResources().getInteger(
		// android.R.integer.config_shortAnimTime);
		// }
		// controlsView
		// .animate()
		// .translationY(visible ? 0 : mControlsHeight)
		// .setDuration(mShortAnimTime);
		// } else {
		// // If the ViewPropertyAnimator APIs aren't
		// // available, simply show or hide the in-layout UI
		// // controls.
		// controlsView.setVisibility(visible ? View.VISIBLE
		// : View.GONE);
		// }
		//
		// if (visible && AUTO_HIDE) {
		// // Schedule a hide().
		// delayedHide(AUTO_HIDE_DELAY_MILLIS);
		// }
		// }
		// });
		//
		// // Set up the user interaction to manually show or hide the system
		// UI.
		// contentView.setOnClickListener(new View.OnClickListener() {
		// @Override
		// public void onClick(View view) {
		// if (TOGGLE_ON_CLICK) {
		// mSystemUiHider.toggle();
		// } else {
		// mSystemUiHider.show();
		// }
		// }
		// });

		// Upon interacting with UI controls, delay any scheduled hide()
		// operations to prevent the jarring behavior of controls going away
		// while interacting with the UI.
		// findViewById(R.id.dummy_button).setOnTouchListener(mDelayHideTouchListener);
	}

	// @Override
	// protected void onPostCreate(Bundle savedInstanceState) {
	// super.onPostCreate(savedInstanceState);
	//
	// // Trigger the initial hide() shortly after the activity has been
	// // created, to briefly hint to the user that UI controls
	// // are available.
	// delayedHide(100);
	// }
	//
	// /**
	// * Touch listener to use for in-layout UI controls to delay hiding the
	// * system UI. This is to prevent the jarring behavior of controls going
	// away
	// * while interacting with activity UI.
	// */
	// View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener()
	// {
	// @Override
	// public boolean onTouch(View view, MotionEvent motionEvent) {
	// if (AUTO_HIDE) {
	// delayedHide(AUTO_HIDE_DELAY_MILLIS);
	// }
	// return false;
	// }
	// };
	//
	// Handler mHideHandler = new Handler();
	// Runnable mHideRunnable = new Runnable() {
	// @Override
	// public void run() {
	// mSystemUiHider.hide();
	// }
	// };
	//
	// /**
	// * Schedules a call to hide() in [delay] milliseconds, canceling any
	// * previously scheduled calls.
	// */
	// private void delayedHide(int delayMillis) {
	// mHideHandler.removeCallbacks(mHideRunnable);
	// mHideHandler.postDelayed(mHideRunnable, delayMillis);
	// }

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onResume() {
		super.onResume();

		butReconnect.setVisibility(View.VISIBLE);
		checkPlayServices();
	}

	private void checkVersion() {
		butReconnect.setVisibility(View.INVISIBLE);

		ServiceClient.getInstance().checkVersion(getContext(), new Handler() {
			@Override
			public void handleMessage(Message msg) {
				try {
					JSONObject jsonObject = (JSONObject) msg.obj;

					if (ServiceClient.STUDIO_VERSION) {
						registerUser();
					}
					else {
						String version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;

						if (!version.equalsIgnoreCase(jsonObject.getString("Value"))) {
							ClientUtils.alert(getContext(), "Bạn đang sử dụng phiên bản cũ của chương trình. Xin vui lòng cập nhật phiên bản mới trên kho ứng dụng để có thể tham gia cuộc thi.", null);
							return;
						}
						else {
							registerUser();
						}
					}
				}
				catch (NameNotFoundException e) {
					e.printStackTrace();
				}
				catch (JSONException e) {
					Log.e(this.getClass().getName(), "Unable to parse json string", e);
				}
			}
		}, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// Alert error
				ClientUtils.alert(getContext(), "Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại.", null);

				butReconnect.setVisibility(View.VISIBLE);
			}
		});
	}

	private void registerUser() {
		final SharedPreferences prefs = ClientUtils.getSharedPreferences(this);
		final long userID = prefs.getLong(ServiceClient.USER_ID, 0);

		// Register new user
		String deviceID = Secure.getString(getContext().getContentResolver(), Secure.ANDROID_ID);
		Log.i("Device ID", deviceID);

		ServiceClient.getInstance().register(deviceID, getContext(), new Handler() {
			@Override
			public void handleMessage(Message msg) {
				try {
					JSONObject jsonObject = (JSONObject) msg.obj;

					final SharedPreferences innerPrefs = ClientUtils.getSharedPreferences(getContext());
					Editor editor = innerPrefs.edit();
					if (jsonObject.getInt("Error") == -1 && jsonObject.getLong("ID") != userID) {
						// Exist user with the same device
						editor.putLong(ServiceClient.USER_ID, jsonObject.getLong("ID"));
						editor.commit();

						ClientUtils.alert(getContext(), "Thiết bị đã được đăng ký trước đây với mã số '" + jsonObject.getLong("ID")
								+ "'. Chương trình sẽ tự động kết nối lại với mã số này.", new Handler() {
							@Override
							public void handleMessage(Message msg) {
								getUserInfo();
							}
						});
					}
					else if (jsonObject.getInt("Error") == -2) {
						ClientUtils.alert(getContext(), "Chương trình xảy ra lỗi trong quá trình xử lý. Vui lòng thông báo tới BTC để được hướng dẫn xử lý và hỗ trợ.", null);
					}
					else {
						editor.putLong(ServiceClient.USER_ID, jsonObject.getLong("ID"));
						editor.commit();

						getUserInfo();
					}
				}
				catch (JSONException e) {
					Log.e(this.getClass().getName(), "Unable to parse json string", e);
				}
			}
		}, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// Alert error
				ClientUtils.alert(getContext(), "Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại.", null);

				butReconnect.setVisibility(View.VISIBLE);
			}
		});
	}

	private void getUserInfo() {
		final SharedPreferences prefs = ClientUtils.getSharedPreferences(this);
		long userID = prefs.getLong(ServiceClient.USER_ID, 0);

		// Get user information
		ServiceClient.getInstance().getUser(userID, getContext(), new Handler() {
			@Override
			public void handleMessage(Message msg) {
				try {
					JSONObject jsonObject = (JSONObject) msg.obj;

					final SharedPreferences innerPrefs = ClientUtils.getSharedPreferences(getContext());
					Editor editor = innerPrefs.edit();
					editor.putString(ServiceClient.USER_FULLNAME, jsonObject.isNull("Fullname") ? "" : jsonObject.getString("Fullname"));
					editor.putString(ServiceClient.USER_EMAIL, jsonObject.isNull("Email") ? "" : jsonObject.getString("Email"));
					editor.putString(ServiceClient.USER_PHONE_NUMBER, jsonObject.isNull("PhoneNumber") ? "" : jsonObject.getString("PhoneNumber"));
					editor.putString(ServiceClient.USER_ADDRESS, jsonObject.isNull("Address") ? "" : jsonObject.getString("Address"));
					editor.putLong(ServiceClient.USER_SCORE, jsonObject.isNull("Score") ? 0 : jsonObject.getLong("Score"));
					editor.putLong(ServiceClient.USER_DAY_SCORE, jsonObject.isNull("DayScore") ? 0 : jsonObject.getLong("DayScore"));
					editor.putLong(ServiceClient.USER_POSITION, jsonObject.isNull("Rank") ? 0 : jsonObject.getLong("Rank"));
					editor.putLong(ServiceClient.USER_DAY_POSITION, jsonObject.isNull("DayRank") ? 0 : jsonObject.getLong("DayRank"));
					editor.putBoolean(ServiceClient.USER_OPT_IN, jsonObject.isNull("IsOptIn") ? true : jsonObject.getBoolean("IsOptIn"));
					editor.remove(ServiceClient.CURRENT_QUESTION_NUMBER);
					editor.remove(ServiceClient.NUMBER_OF_QUESTIONS);
					editor.commit();

					if (!ServiceClient.STUDIO_VERSION) {
						registerToken();
					}

					finish();
					startActivity(new Intent(getContext(), MenuActivity.class));
				}
				catch (JSONException e) {
					Log.e(this.getClass().getName(), "Unable to parse json string", e);
				}
			}
		}, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// Alert error
				if (msg.what == HttpStatus.SC_NOT_FOUND) {
					final SharedPreferences innerPrefs = ClientUtils.getSharedPreferences(getContext());
					Editor editor = innerPrefs.edit();
					editor.remove(ServiceClient.USER_ID);
					editor.remove(ServiceClient.USER_FULLNAME);
					editor.remove(ServiceClient.USER_EMAIL);
					editor.remove(ServiceClient.USER_PHONE_NUMBER);
					editor.remove(ServiceClient.USER_ADDRESS);
					editor.remove(ServiceClient.USER_SCORE);
					editor.remove(ServiceClient.USER_DAY_SCORE);
					editor.remove(ServiceClient.USER_POSITION);
					editor.remove(ServiceClient.USER_DAY_POSITION);
					editor.remove(ServiceClient.USER_OPT_IN);
					editor.remove(ServiceClient.NUMBER_OF_QUESTIONS);
					editor.remove(ServiceClient.CURRENT_QUESTION_NUMBER);
					editor.commit();

					ClientUtils
							.alert(getContext(),
									"Tài khoản này đã bị vô hiệu hóa. Vui lòng đăng ký lại tài khoản mới hoặc liên hệ với BTC để được hỗ trợ. CHÚ Ý: tài khoản mới sẽ không còn lưu trữ các thông tin cá nhân và điểm số hiện tại!",
									null);
				}
				else {
					ClientUtils.alert(getContext(), "Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại.", null);
				}

				butReconnect.setVisibility(View.VISIBLE);
			}
		});
	}

	private void registerToken() {
		final SharedPreferences prefs = ClientUtils.getSharedPreferences(this);
		long userID = prefs.getLong(ServiceClient.USER_ID, 0);
		String token = prefs.getString(ServiceClient.DEVICE_TOKEN, "");
		String deviceID = Secure.getString(getContext().getContentResolver(), Secure.ANDROID_ID);

		if (token.length() == 0 || userID == 0) {
			return;
		}

		// Get user information
		ServiceClient.getInstance().setToken(token, deviceID, userID, getContext(), new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// try {
				// JSONObject jsonObject = (JSONObject) msg.obj;
				// }
				// catch (JSONException e) {
				// Log.e(this.getClass().getName(), "Unable to parse json string", e);
				// }
			}
		}, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				ClientUtils.alert(getContext(), "Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại.", null);
			}
		});
	}

	/**
	 * Check the device to make sure it has the Google Play Services APK. If it doesn't, display a dialog that allows
	 * users to download the APK from the Google Play Store or enable it in the device's system settings.
	 */
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
			}
			else {
				Log.i("GCM", "This device is not supported.");
			}
			return false;
		}
		return true;
	}

	/**
	 * Gets the current registration ID for application on GCM service.
	 * <p>
	 * If result is empty, the app needs to register.
	 * 
	 * @return registration ID, or empty string if there is no existing registration ID.
	 */
	private String getRegistrationId(Context context) {
		final SharedPreferences prefs = ClientUtils.getSharedPreferences(this);
		String registrationId = prefs.getString(ServiceClient.DEVICE_TOKEN, "");
		if (registrationId.length() == 0) {
			Log.i("GCM", "Registration not found.");
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = prefs.getInt(ServiceClient.APP_VERSION, Integer.MIN_VALUE);
		int currentVersion = 0;
		try {
			currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
		}
		catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (registeredVersion != currentVersion) {
			Log.i("GCM", "App version changed.");
			return "";
		}
		return registrationId;
	}

	/**
	 * Registers the application with GCM servers asynchronously.
	 * <p>
	 * Stores the registration ID and app versionCode in the application's shared preferences.
	 */
	private void registerInBackground() {
		new AsyncTask() {
			@Override
			protected Object doInBackground(Object... params) {
				String msg = "";
				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging.getInstance(getContext());
					}
					regId = gcm.register(SENDER_ID);
					msg = "Device registered, registration ID=" + regId;

					Log.d("GCM", msg);

					// You should send the registration ID to your server over HTTP,
					// so it can use GCM/HTTP or CCS to send messages to your app.
					// The request to your server should be authenticated if your app
					// is using accounts.
					final SharedPreferences prefs = ClientUtils.getSharedPreferences(getContext());
					Editor editor = prefs.edit();
					editor.putString(ServiceClient.DEVICE_TOKEN, regId);
					int currentVersion = 0;
					try {
						currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
					}
					catch (NameNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					editor.putInt(ServiceClient.APP_VERSION, currentVersion);
					editor.commit();

					registerToken();
				}
				catch (IOException ex) {
					msg = "Error :" + ex.getMessage();
					// If there is an error, don't just keep trying to register.
					// Require the user to click a button again, or perform
					// exponential back-off.
				}
				return msg;
			}
		}.execute(null, null, null);
	}
}
