package com.vietdev.quatangtrithuc;

import java.io.InputStream;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.vietdev.cunglatyphu.R;
import com.vietdev.cunglatyphu.R.layout;
import com.vietdev.network.ClientUtils;
import com.vietdev.quatangtrithuc.util.SoundManager;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;

public class TermActivity extends Activity {

	public TermActivity getContext() {
		return this;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// Remove notification bar
		// this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_term);

		// Get tracker.
		Tracker t = ClientUtils.getTracker(getContext());

		// Set screen name.
		// Where path is a String representing the screen name.
		t.setScreenName("T&C Screen");

		// Send a screen view.
		t.send(new HitBuilders.AppViewBuilder().build());

		Button backButton = (Button) findViewById(R.id.butBack);
		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				SoundManager.getInstance(getContext()).playButton();

				finish();
			}
		});

		WebView webContent = (WebView) findViewById(R.id.webContent);
		try {
			InputStream inputStream = getResources().openRawResource(R.raw.termcunglatyphu);
			byte[] data = new byte[inputStream.available()];
			inputStream.read(data);
			webContent.loadDataWithBaseURL("file:///android_asset/", new String(data), "text/html", "utf-8", null);
		}
		catch (Exception exc) {

		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		SoundManager.getInstance(getContext()).playOpenning();
	}

	@Override
	protected void onPause() {
		super.onPause();

		SoundManager.getInstance(getContext()).stopAudios();
	}

	@Override
	protected void onStop() {
		super.onStop();

		SoundManager.getInstance(getContext()).stopAudios();
	}
}
