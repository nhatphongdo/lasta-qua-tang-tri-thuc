package com.vietdev.quatangtrithuc;

import java.io.InputStream;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.vietdev.cunglatyphu.R;
import com.vietdev.network.ClientUtils;
import com.vietdev.network.ServiceClient;
import com.vietdev.quatangtrithuc.util.SoundManager;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ChangeScoreActivity extends Activity {

	private ChangeScoreActivity getContext() {
		return this;
	}

	private RelativeLayout infoView;
	private LinearLayout submitView;
	private RelativeLayout popupView;
	private EditText textScore;
	private TextView receivedScore;
	private Button butSubmit;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// Remove notification bar
		// this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_change_score);

		// Get tracker.
		Tracker t = ClientUtils.getTracker(getContext());

		// Set screen name.
		// Where path is a String representing the screen name.
		t.setScreenName("Ty Phu Khong Do Screen");

		// Send a screen view.
		t.send(new HitBuilders.AppViewBuilder().build());

		Button backButton = (Button) findViewById(R.id.butBack);
		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				SoundManager.getInstance(getContext()).playButton();
				finish();
			}
		});

		WebView webContent = (WebView) findViewById(R.id.webContent);
		try {
			InputStream inputStream = getResources().openRawResource(R.raw.termtyphu0do);
			byte[] data = new byte[inputStream.available()];
			inputStream.read(data);
			webContent.loadDataWithBaseURL("file:///android_asset/", new String(data), "text/html", "utf-8", null);
		}
		catch (Exception exc) {

		}

		infoView = (RelativeLayout) findViewById(R.id.layoutInfo);
		submitView = (LinearLayout) findViewById(R.id.layoutSubmit);
		popupView = (RelativeLayout) findViewById(R.id.popupView);
		textScore = (EditText) findViewById(R.id.textScore);
		receivedScore = (TextView) findViewById(R.id.labelScore);

		Button butStart = (Button) findViewById(R.id.butChange);
		butStart.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				infoView.setVisibility(View.INVISIBLE);
				submitView.setVisibility(View.VISIBLE);

				SoundManager.getInstance(getContext()).playButton();
			}
		});

		butSubmit = (Button) findViewById(R.id.butSubmit);
		butSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				SoundManager.getInstance(getContext()).playButton();

				if (textScore.getText().length() == 0) {
					ClientUtils.alert(getContext(), "Bạn chưa nhập Mã số quy đổi.", null);
					textScore.requestFocus();
					return;
				}

				butSubmit.setEnabled(false);

				final SharedPreferences prefs = ClientUtils.getSharedPreferences(getContext());
				long userID = prefs.getLong(ServiceClient.USER_ID, 0);

				ServiceClient.getInstance().exchangeCode(textScore.getText().toString(), userID, getContext(), new Handler() {
					@Override
					public void handleMessage(Message msg) {
						try {
							JSONObject jsonObject = (JSONObject) msg.obj;
							if (jsonObject.getInt("Error") == 0) {
								// Success
								popupView.setVisibility(View.VISIBLE);
								receivedScore.setText(jsonObject.getInt("Score") + " điểm");
							}
							else if (jsonObject.getInt("Error") == 1) {
								ClientUtils.alert(getContext(), "Mã số này đã được quy đổi rồi. Bạn không thể sử dụng lại để quy đổi điểm thưởng.", null);
							}
							else if (jsonObject.getInt("Error") == 2) {
								ClientUtils.alert(getContext(), "Mã số này không hợp lệ hoặc không chính xác. Vui lòng kiểm tra mã số và thử lại.", null);
							}
							else if (jsonObject.getInt("Error") == 3) {
								ClientUtils.alert(getContext(), "Chương trình xảy ra lỗi trong quá trình xử lý. Vui lòng thử lại hoặc thông báo tới BTC để được hướng dẫn xử lý và hỗ trợ.", null);
							}
						}
						catch (JSONException e) {
							Log.e(this.getClass().getName(), "Unable to parse json string", e);
						}

						butSubmit.setEnabled(true);
					}
				}, new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// Alert error
						ClientUtils.alert(getContext(), "Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại.", null);
						butSubmit.setEnabled(true);
					}
				});
			}
		});

		Button butPopupBack = (Button) findViewById(R.id.butPopupBack);
		butPopupBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				SoundManager.getInstance(getContext()).playButton();
				popupView.setVisibility(View.INVISIBLE);
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();

		SoundManager.getInstance(getContext()).playOpenning();
	}

	@Override
	protected void onPause() {
		super.onPause();

		SoundManager.getInstance(getContext()).stopAudios();
	}

	@Override
	protected void onStop() {
		super.onStop();

		SoundManager.getInstance(getContext()).stopAudios();
	}
}
