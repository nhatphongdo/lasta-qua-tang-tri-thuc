package com.vietdev.quatangtrithuc;

import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.vietdev.cunglatyphu.R;
import com.vietdev.network.ClientUtils;
import com.vietdev.network.ServiceClient;
import com.vietdev.quatangtrithuc.util.SoundManager;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class FakeCaptureActivity extends Activity {

	private Timer updateTimer;
	private boolean isLoading;
	private boolean autoFailed;
	private boolean alerted;

	private Handler timerHandler = new Handler() {
		public void handleMessage(Message msg) {
			if (isLoading == false && autoFailed == true) {
				loadContest(ServiceClient.ContestTodayUrl);
			}
		}
	};

	private FakeCaptureActivity getContext() {
		return this;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// Remove notification bar
		// this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_capture);

		// Get tracker.
		Tracker t = ClientUtils.getTracker(getContext());

		// Set screen name.
		// Where path is a String representing the screen name.
		t.setScreenName("QR Scanner Screen");

		// Send a screen view.
		t.send(new HitBuilders.AppViewBuilder().build());

		final SharedPreferences prefs = ClientUtils.getSharedPreferences(this);
		long userID = prefs.getLong(ServiceClient.USER_ID, 0);
		long dayScore = prefs.getLong(ServiceClient.USER_DAY_SCORE, 0);
		long totalScore = prefs.getLong(ServiceClient.USER_SCORE, 0);

		TextView idLabel = (TextView) findViewById(R.id.idLabel);
		idLabel.setText("" + userID);
		TextView dayScoreLabel = (TextView) findViewById(R.id.dayScoreLabel);
		dayScoreLabel.setText("" + dayScore);
		TextView totalScoreLabel = (TextView) findViewById(R.id.totalScoreLabel);
		totalScoreLabel.setText("" + totalScore);

		Button butBack = (Button) findViewById(R.id.butBack);
		butBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				SoundManager.getInstance(getContext()).playButton();
				finish();
			}
		});

		alerted = false;
	}

	@Override
	protected void onResume() {
		super.onResume();

		SoundManager.getInstance(getContext()).playBackground();

		if (updateTimer == null) {
			updateTimer = new Timer();
		}

		autoFailed = true;
		isLoading = false;
		updateTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				timerHandler.obtainMessage(1).sendToTarget();
			}
		}, 0, 1000);
	}

	@Override
	protected void onPause() {
		super.onPause();

		SoundManager.getInstance(getContext()).stopAudios();
	}

	@Override
	protected void onStop() {
		super.onStop();

		SoundManager.getInstance(getContext()).stopAudios();

		if (updateTimer != null) {
			updateTimer.cancel();
			updateTimer = null;
		}
	}

	private void loadContest(final String url) {
		if (this.isFinishing()) {
			return;
		}

		ServiceClient.getInstance().getService(url, this.getContext(), new Handler() {
			@Override
			public void handleMessage(Message msg) {
				try {
					String jsonString = (String) msg.obj;
					JSONObject jsonObject = new JSONObject(jsonString);

					SharedPreferences prefs = ClientUtils.getSharedPreferences(getContext());
					Editor editor = prefs.edit();
					editor.putInt(ServiceClient.NUMBER_OF_QUESTIONS, jsonObject.getInt("NumberOfQuestions"));
					editor.commit();

					long state = jsonObject.getLong("State");
					if (state >= 10 && state < 10000) {
						if (state % 10 == 3) {
							// Go to result page
							Intent intent = new Intent(getContext(), ResultActivity.class);
							intent.putExtra("ContestKey", jsonObject.getString("ContestKey"));

							finish();
							startActivity(intent);
						}
						else if (state % 10 == 2) {
							// Go to question page
							SharedPreferences innerPrefs = ClientUtils.getSharedPreferences(getContext());
							Editor innerEditor = innerPrefs.edit();
							innerEditor.putInt(ServiceClient.CURRENT_QUESTION_NUMBER, (int) state / 10);
							innerEditor.commit();

							Intent intent = new Intent(getContext(), QuestionActivity.class);
							intent.putExtra("QuestionNumber", (int) state / 10);
							intent.putExtra("ContestKey", jsonObject.getString("ContestKey"));
							intent.putExtra("NumberOfQuestions", jsonObject.getInt("NumberOfQuestions"));
							intent.putExtra("ContestState", state);
							intent.putExtra("RemainTime", jsonObject.getLong("Remain"));

							finish();
							startActivity(intent);
						}
						else {
							SharedPreferences innerPrefs = ClientUtils.getSharedPreferences(getContext());
							Editor innerEditor = innerPrefs.edit();
							innerEditor.putInt(ServiceClient.CURRENT_QUESTION_NUMBER, (int) state / 10);
							innerEditor.commit();

							// Go to question info page
							Intent intent = new Intent(getContext(), QuestionInfoActivity.class);
							intent.putExtra("QuestionNumber", (int) state / 10);
							intent.putExtra("ContestKey", jsonObject.getString("ContestKey"));
							intent.putExtra("ContestState", state);

							finish();
							startActivity(intent);
						}
					}
					else if (state == 10400) {
						SharedPreferences innerPrefs = ClientUtils.getSharedPreferences(getContext());
						Editor innerEditor = innerPrefs.edit();
						innerEditor.putInt(ServiceClient.CURRENT_QUESTION_NUMBER, jsonObject.getInt("NumberOfQuestions") + 1);
						innerEditor.commit();

						// Go to question info page
						Intent intent = new Intent(getContext(), QuestionInfoActivity.class);
						intent.putExtra("QuestionNumber", jsonObject.getInt("NumberOfQuestions") + 1);
						intent.putExtra("ContestKey", jsonObject.getString("ContestKey"));
						intent.putExtra("ContestState", state);

						finish();
						startActivity(intent);
					}
					else if (state == 10401) {
						SharedPreferences innerPrefs = ClientUtils.getSharedPreferences(getContext());
						Editor innerEditor = innerPrefs.edit();
						innerEditor.putInt(ServiceClient.CURRENT_QUESTION_NUMBER, jsonObject.getInt("NumberOfQuestions") + 1);
						innerEditor.commit();

						// Go to question page
						Intent intent = new Intent(getContext(), QuestionActivity.class);
						intent.putExtra("QuestionNumber", jsonObject.getInt("NumberOfQuestions") + 1);
						intent.putExtra("ContestKey", jsonObject.getString("ContestKey"));
						intent.putExtra("NumberOfQuestions", jsonObject.getInt("NumberOfQuestions"));
						intent.putExtra("ContestState", state);
						intent.putExtra("RemainTime", jsonObject.getLong("Remain"));

						finish();
						startActivity(intent);
					}
					else if (state == 10402) {
						// Go to result page
						Intent intent = new Intent(getContext(), ResultActivity.class);
						intent.putExtra("ContestKey", jsonObject.getString("ContestKey"));
						finish();
						startActivity(intent);
					}
					else if (state == 10000 || state == 10100 || state == 10200) {
						int currentQuestion = prefs.getInt(ServiceClient.CURRENT_QUESTION_NUMBER, 0);

						Intent intent;
						if (currentQuestion > 0 && currentQuestion % 4 == 0) {
							// Go to summary page
							intent = new Intent(getContext(), SummaryActivity.class);
							intent.putExtra("ContestKey", jsonObject.getString("ContestKey"));
						}
						else {
							// Go to info page
							intent = new Intent(getContext(), PrizesActivity.class);
							intent.putExtra("ContestKey", jsonObject.getString("ContestKey"));

							ClientUtils.alert(getContext(), "Chương trình đã kết nối với gameshow thành công. Vui lòng chờ và theo dõi chương trình diễn ra trên truyền hình.", null);
						}

						finish();
						startActivity(intent);
					}
					else if (state == 10300) {
						// Go to finished page
						// Intent intent = new Intent(getContext(), PrizesActivity.class);
						// intent.putExtra("ContestKey", jsonObject.getString("ContestKey"));
						finish();
						// startActivity(intent);
					}
					else {
						// If state is not in contest, stay here or go
						// back?
						// Go to menu page
						finish();
					}
				}
				catch (JSONException e) {
					Log.e(this.getClass().getName(), "Unable to parse json string", e);
				}
			}
		}, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// Back to menu page\
				if (msg.what == HttpStatus.SC_NOT_FOUND) {
					if (url.equalsIgnoreCase(ServiceClient.ContestTodayUrl)) {
						autoFailed = true;

						if (alerted == false) {
							ClientUtils
									.alert(getContext(),
											"Không có chương trình gameshow vào thời gian này. Vui lòng đón xem gameshow \"Cùng Là Tỷ Phú\" được truyền hình trực tiếp trên kênh truyền hình Let's Viet vào lúc 8h30 thứ bảy và chủ nhật hàng tuần.",
											null);
							alerted = true;
						}
					}
					else {
						finish();
					}
				}
				else {
					if (url.equalsIgnoreCase(ServiceClient.ContestTodayUrl)) {
						autoFailed = false;
					}
				}
			}
		});

	}

}
