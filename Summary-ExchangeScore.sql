/****** Script for SelectTopNRows command from SSMS  ******/
SELECT UserID, SUM(ExchangedScore) AS Score, 
	SUM(case when ExchangedScore=5 THEN 1 ELSE 0 END) AS Total05, 
	SUM(case when ExchangedScore=10 THEN 1 ELSE 0 END) AS Total10, 
	SUM(case when ExchangedScore=15 THEN 1 ELSE 0 END) AS Total15, 
	SUM(case when ExchangedScore=30 THEN 1 ELSE 0 END) AS Total30, 
	SUM(case when ExchangedScore=50 THEN 1 ELSE 0 END) AS Total50, 
	SUM(case when ExchangedScore=100 THEN 1 ELSE 0 END) AS Total100, 
	COUNT(ExchangedScore) AS Total
  FROM [CungLaTyPhu].[dbo].[ExchangeScore]
  GROUP BY UserID
  ORDER BY Score DESC
