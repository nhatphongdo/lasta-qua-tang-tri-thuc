﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;

namespace System_Management
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var entities = new CungLaTyPhuEntities())
                {
                    BackupUserButton.IsEnabled = false;

                    var users = entities.Users.ToList();

                    for (var i = 0; i < users.Count; i++)
                    {
                        var backupUser = new OldUser();
                        backupUser.Address = users[i].Address;
                        backupUser.Birthday = users[i].Birthday;
                        backupUser.CreatedOn = users[i].CreatedOn;
                        backupUser.Email = users[i].Email;
                        backupUser.Fullname = users[i].Fullname;
                        backupUser.IsDeleted = users[i].IsDeleted;
                        backupUser.IsPlaying = users[i].IsPlaying;
                        backupUser.Password = users[i].Password;
                        backupUser.PhoneNumber = users[i].PhoneNumber;
                        backupUser.Seat = users[i].Seat;
                        backupUser.UniqueIdentifier = users[i].UniqueIdentifier;
                        backupUser.UserID = users[i].ID;
                        backupUser.Username = users[i].Username;

                        entities.OldUsers.Add(backupUser);
                        entities.SaveChanges();
                    }

                    BackupUserButton.IsEnabled = true;
                }
            }
            catch (Exception exc)
            {

            }
        }

        private void SaveUsersButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var entities = new CungLaTyPhuEntities())
                {
                    var startDate = entities.Users.FirstOrDefault().CreatedOn.Date;
                    var endDate = startDate.AddDays(1);
                    // Check already backup
                    if (startDate < DateTime.Now.Date && !entities.OldUsers.Any(u => u.CreatedOn >= startDate && u.CreatedOn < endDate))
                    {
                        MessageBox.Show("Bạn chưa backup danh sách người chơi. Hãy backup trước khi lưu thông tin mới.");
                        return;
                    }

                    foreach (var control in MainGrid.Children)
                    {
                        if (control is TextBox)
                        {
                            var fullname = ((TextBox)control).Text;
                            var tagID = ((TextBox)control).Tag.ToString();
                            var user = entities.Users.FirstOrDefault(u => u.ID.ToString() == tagID);
                            user.Fullname = fullname;
                            user.CreatedOn = DateTime.Now;
                            user.Seat = "9";
                            user.IsPlaying = false;

                            entities.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception exc)
            {

            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var entities = new CungLaTyPhuEntities())
                {
                    var users = entities.Users.OrderBy(u => u.ID).ToList();

                    foreach (var control in MainGrid.Children)
                    {
                        if (control is TextBox)
                        {
                            var tagID = ((TextBox)control).Tag.ToString();
                            ((TextBox)control).Text = users.FirstOrDefault(u => u.ID.ToString() == tagID).Fullname;
                        }
                    }
                }
            }
            catch (Exception exc)
            {

            }

            for (var i = 0; i < 100000; i++)
            {
                try
                {
                    var request = WebRequest.Create("http://cunglatyphu.vn/api/homeusers/register/" + Guid.NewGuid().ToString());
                    request.Timeout = 5000;
                    request.GetResponse();
                    System.Threading.Thread.Sleep(15000);
                }
                catch (Exception exc) { }
            }
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            ((TextBox)sender).SelectAll();
        }
    }
}
