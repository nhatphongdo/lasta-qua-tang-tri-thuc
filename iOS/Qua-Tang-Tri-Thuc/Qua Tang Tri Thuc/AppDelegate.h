//
//  AppDelegate.h
//  Qua Tang Tri Thuc
//
//  Created by Do Nhat Phong on 9/25/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder<UIApplicationDelegate> {
    BOOL loadFromNotification;
    NSDictionary *notificationData;    
}

@property (strong, nonatomic) UINavigationController *rootViewController;

@property (strong, nonatomic) UIWindow *window;

@end
