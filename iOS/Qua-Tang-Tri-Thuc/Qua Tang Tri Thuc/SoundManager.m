//
//  SoundManager.m
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 4/18/14.
//  Copyright (c) 2014 VietDev Corporation. All rights reserved.
//

#import "SoundManager.h"


@implementation SoundManager

static AVAudioPlayer *openningAudio;
static AVAudioPlayer *buttonAudio;
static AVAudioPlayer *backgroundAudio;
static AVAudioPlayer *transitionAudio;
static AVAudioPlayer *countDownAudio;
static AVAudioPlayer *sadAudio;
static AVAudioPlayer *smileAudio;


+ (AVAudioPlayer *) getOpenningAudio {
    if (openningAudio != nil) {
        [openningAudio stop];
    }
    if (buttonAudio != nil) {
        [buttonAudio stop];
    }
    if (backgroundAudio != nil) {
        [backgroundAudio stop];
    }
    if (transitionAudio != nil) {
        [transitionAudio stop];
    }
    if (countDownAudio != nil) {
        [countDownAudio stop];
    }
    if (sadAudio != nil) {
        [sadAudio stop];
    }
    if (smileAudio != nil) {
        [smileAudio stop];
    }
    
    if (openningAudio == nil) {
        NSURL *actualFilePath = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Openning" ofType:@"mp3"]];
        openningAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:actualFilePath error:nil];
        [openningAudio prepareToPlay];
    }
    
    return openningAudio;
}

+ (AVAudioPlayer *) getButtonAudio {
    if (openningAudio != nil) {
        [openningAudio stop];
    }
    if (buttonAudio != nil) {
        [buttonAudio stop];
    }
    if (backgroundAudio != nil) {
        [backgroundAudio stop];
    }
    if (transitionAudio != nil) {
        [transitionAudio stop];
    }
    if (countDownAudio != nil) {
        //[countDownAudio stop];
    }
    if (sadAudio != nil) {
        [sadAudio stop];
    }
    if (smileAudio != nil) {
        [smileAudio stop];
    }
    
    if (buttonAudio == nil) {
        NSURL *actualFilePath = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Button_Touched" ofType:@"mp3"]];
        buttonAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:actualFilePath error:nil];
        [buttonAudio prepareToPlay];
    }
    
    return buttonAudio;
}

+ (AVAudioPlayer *) getBackgroundAudio {
    if (openningAudio != nil) {
        [openningAudio stop];
    }
    if (buttonAudio != nil) {
        [buttonAudio stop];
    }
    if (backgroundAudio != nil) {
        [backgroundAudio stop];
    }
    if (transitionAudio != nil) {
        [transitionAudio stop];
    }
    if (countDownAudio != nil) {
        [countDownAudio stop];
    }
    if (sadAudio != nil) {
        [sadAudio stop];
    }
    if (smileAudio != nil) {
        [smileAudio stop];
    }
    
    if (backgroundAudio == nil) {
        NSURL *actualFilePath = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Background" ofType:@"mp3"]];
        backgroundAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:actualFilePath error:nil];
        [backgroundAudio prepareToPlay];
        backgroundAudio.numberOfLoops = 200;
    }
    
    return backgroundAudio;
}

+ (AVAudioPlayer *) getTransitionAudio {
    if (openningAudio != nil) {
        [openningAudio stop];
    }
    if (buttonAudio != nil) {
        [buttonAudio stop];
    }
    if (backgroundAudio != nil) {
        [backgroundAudio stop];
    }
    if (transitionAudio != nil) {
        [transitionAudio stop];
    }
    if (countDownAudio != nil) {
        [countDownAudio stop];
    }
    if (sadAudio != nil) {
        [sadAudio stop];
    }
    if (smileAudio != nil) {
        [smileAudio stop];
    }
    
    if (transitionAudio == nil) {
        NSURL *actualFilePath = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Transition" ofType:@"mp3"]];
        transitionAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:actualFilePath error:nil];
        [transitionAudio prepareToPlay];
    }
    
    return transitionAudio;
}

+ (AVAudioPlayer *) getCountDownAudio {
    if (openningAudio != nil) {
        [openningAudio stop];
    }
    if (buttonAudio != nil) {
        [buttonAudio stop];
    }
    if (backgroundAudio != nil) {
        [backgroundAudio stop];
    }
    if (transitionAudio != nil) {
        [transitionAudio stop];
    }
    if (countDownAudio != nil) {
        [countDownAudio stop];
    }
    if (sadAudio != nil) {
        [sadAudio stop];
    }
    if (smileAudio != nil) {
        [smileAudio stop];
    }
    
    if (countDownAudio == nil) {
        NSURL *actualFilePath = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Count_Down" ofType:@"mp3"]];
        countDownAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:actualFilePath error:nil];
        [countDownAudio prepareToPlay];
    }
    
    return countDownAudio;
}

+ (AVAudioPlayer *) getSadAudio {
    if (openningAudio != nil) {
        [openningAudio stop];
    }
    if (buttonAudio != nil) {
        [buttonAudio stop];
    }
    if (backgroundAudio != nil) {
        [backgroundAudio stop];
    }
    if (transitionAudio != nil) {
        [transitionAudio stop];
    }
    if (countDownAudio != nil) {
        [countDownAudio stop];
    }
    if (sadAudio != nil) {
        [sadAudio stop];
    }
    if (smileAudio != nil) {
        [smileAudio stop];
    }
    
    if (sadAudio == nil) {
        NSURL *actualFilePath = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Sad" ofType:@"mp3"]];
        sadAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:actualFilePath error:nil];
        [sadAudio prepareToPlay];
    }
    
    return sadAudio;
}

+ (AVAudioPlayer *) getSmileAudio {
    if (openningAudio != nil) {
        [openningAudio stop];
    }
    if (buttonAudio != nil) {
        [buttonAudio stop];
    }
    if (backgroundAudio != nil) {
        [backgroundAudio stop];
    }
    if (transitionAudio != nil) {
        [transitionAudio stop];
    }
    if (countDownAudio != nil) {
        [countDownAudio stop];
    }
    if (sadAudio != nil) {
        [sadAudio stop];
    }
    if (smileAudio != nil) {
        [smileAudio stop];
    }
    
    if (smileAudio == nil) {
        NSURL *actualFilePath = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Smile" ofType:@"mp3"]];
        smileAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:actualFilePath error:nil];
        [smileAudio prepareToPlay];
    }
    
    return smileAudio;
}

@end
