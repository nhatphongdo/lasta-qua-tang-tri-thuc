//
//  Constants.h
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 10/14/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#ifndef cung_la_ty_phu_Constants_h
#define cung_la_ty_phu_Constants_h


#define USERID_KEY @"UserID"
#define USER_FULLNAME_KEY @"UserFullname"
#define USER_EMAIL_KEY @"UserEmail"
#define USER_PHONE_NUMBER_KEY @"UserPhoneNumber"
#define USER_ADDRESS_KEY @"UserAddress"
#define USER_OPT_IN_KEY @"UserOptIn"
#define USER_SCORE_KEY @"UserScore"
#define USER_DAY_SCORE_KEY @"UserDayScore"
#define USER_RANK_KEY @"UserRank"
#define USER_DAY_RANK_KEY @"UserDayRank"
#define NUMBER_QUESTIONS_KEY @"NumberOfQuestions"
#define DEVICEID_KEY @"DeviceID"
#define TOKEN_KEY @"TokenKey"
#define QUESTION_NUMBER_KEY @"QuestionNumber"


#define ApplicationTitle @"Cùng Là Tỷ Phú"



#ifdef STUDIO_VERSION
//    #define ROOT_URL @"http://web.vietdev.vn/lasta/api/"
    #define ROOT_URL @"http://192.168.1.2/api/"
    #define ANSWER_URL [ROOT_URL stringByAppendingString:@"answer/%@/%lld/%d/%lld/%lld/%d/%d"]
    #define CHECK_ANSWER_URL [ROOT_URL stringByAppendingString:@"answer/check/%lld"]
    #define REGISTER_USER_URL [ROOT_URL stringByAppendingString:@"users/register/%@"]
    #define GET_USER_URL [ROOT_URL stringByAppendingString:@"users/%lld"]
    #define TOP_USER_URL [ROOT_URL stringByAppendingString:@"topusers/%d"]
    #define TOP_DAILY_USER_URL [ROOT_URL stringByAppendingString:@"topusers/%d"]
    #define UPDATE_USER_URL [ROOT_URL stringByAppendingString:@"users/update/%@/%lld/%@/%@/%@/%@/%d"]
    #define SEARCH_USER_URL [ROOT_URL stringByAppendingString:@"users/search/%@"]
    #define SET_TOKEN_URL [ROOT_URL stringByAppendingString:@"users/setToken/%@/%lld/%@"]
#else
//    #define ROOT_URL @"http://web.vietdev.vn/lasta/api/"
    #define ROOT_URL @"http://cunglatyphu.vn/api/"
    #define ANSWER_URL [ROOT_URL stringByAppendingString:@"homeanswer/%@/%lld/%d/%lld/%lld/%d/%d"]
    #define CHECK_ANSWER_URL [ROOT_URL stringByAppendingString:@"homeanswer/check/%lld"]
    #define REGISTER_USER_URL [ROOT_URL stringByAppendingString:@"homeusers/register/%@"]
    #define GET_USER_URL [ROOT_URL stringByAppendingString:@"homeusers/%lld"]
    #define TOP_USER_URL [ROOT_URL stringByAppendingString:@"topusers/home/%d"]
    #define TOP_DAILY_USER_URL [ROOT_URL stringByAppendingString:@"topusers/dailyhome/%d"]
    #define UPDATE_USER_URL [ROOT_URL stringByAppendingString:@"homeusers/update/%@/%lld/%@/%@/%@/%@/%d"]
    #define SEARCH_USER_URL [ROOT_URL stringByAppendingString:@"homeusers/search/%@"]
    #define SET_TOKEN_URL [ROOT_URL stringByAppendingString:@"homeusers/setToken/%@/%lld/%@"]
#endif

#define CONTEST_TODAY_URL [ROOT_URL stringByAppendingString:@"contests/today"]
#define CONTEST_DAY_URL [ROOT_URL stringByAppendingString:@"contests/%@"]
#define CONTEST_DAY_PRIZES_URL [ROOT_URL stringByAppendingString:@"contests/%@/prizes"]
#define STATE_URL [ROOT_URL stringByAppendingString:@"states/%@"]
#define QUESTION_URL [ROOT_URL stringByAppendingString:@"questions/%@/%d"]
#define CHECK_VERSION_URL [ROOT_URL stringByAppendingString:@"common/version/ios"]
#define EXCHANGE_SCORE_URL [ROOT_URL stringByAppendingString:@"common/exchange/%@/%lld"]
#define TRANSFER_SCORE_URL [ROOT_URL stringByAppendingString:@"common/transfer/%lld/%@/%lld/%lld"]

#endif

