//
//  TimeoutHTTPRequestSerializer.m
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 4/17/14.
//  Copyright (c) 2014 VietDev Corporation. All rights reserved.
//

#import "TimeoutHTTPRequestSerializer.h"

@implementation TimeoutHTTPRequestSerializer

- (NSMutableURLRequest *)requestWithMethod:(NSString *)method
                                 URLString:(NSString *)URLString
                                parameters:(NSDictionary *)parameters
{
    NSMutableURLRequest *request = [super requestWithMethod:method URLString:URLString parameters:parameters];
    [request setTimeoutInterval:5];
    
    return request;
}

@end
