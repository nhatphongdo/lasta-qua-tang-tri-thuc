//
//  SoundManager.h
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 4/18/14.
//  Copyright (c) 2014 VietDev Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>


@interface SoundManager : NSObject

+ (AVAudioPlayer *) getOpenningAudio;
+ (AVAudioPlayer *) getButtonAudio;
+ (AVAudioPlayer *) getBackgroundAudio;
+ (AVAudioPlayer *) getTransitionAudio;
+ (AVAudioPlayer *) getCountDownAudio;
+ (AVAudioPlayer *) getSadAudio;
+ (AVAudioPlayer *) getSmileAudio;

@end
