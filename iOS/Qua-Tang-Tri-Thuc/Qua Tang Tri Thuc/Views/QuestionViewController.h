//
//  QuestionViewController.h
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 10/11/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"


@interface QuestionViewController : GAITrackedViewController {
    NSTimer *updateTimer;
    NSTimer *progressTimer;
    NSTimer *stateTimer;
    
    long progressTime;
    
    BOOL isLoading;
    BOOL isFailed;
    BOOL isQuestionLoaded;
    BOOL isSubmittingAnswer;
    BOOL isJoined;
    
    BOOL isLocked;
    
    int networkFailedCount;
}

@property (nonatomic) long totalTime;
@property (nonatomic) long long questionScore;
@property (nonatomic) int questionNumber;
@property (nonatomic) int numberOfQuestions;
@property (nonatomic) long contestState;
@property (nonatomic) long remainTime;
@property (nonatomic, strong) NSString *contestKey;
@property (nonatomic, strong) NSString *topic;
@property (nonatomic, strong) NSString *answerA;
@property (nonatomic, strong) NSString *answerB;
@property (nonatomic, strong) NSString *answerC;
@property (nonatomic, strong) NSString *answerD;
@property (nonatomic) int answer;
@property (nonatomic, strong) NSDate *startAnswerTime;
@property (nonatomic, strong) NSString *correctAnswer;
@property (nonatomic) int answerTime;
@property (nonatomic) long long score;

@property (strong, nonatomic) IBOutlet UIImageView *topicImage;
@property (strong, nonatomic) IBOutlet UIImageView *progressBarImage;
@property (strong, nonatomic) IBOutlet UILabel *scoreLabel;
@property (strong, nonatomic) IBOutlet UIButton *answerAButton;
@property (strong, nonatomic) IBOutlet UIImageView *logoAImage;
@property (strong, nonatomic) IBOutlet UILabel *answerALabel;
@property (strong, nonatomic) IBOutlet UIButton *answerBButton;
@property (strong, nonatomic) IBOutlet UIImageView *logoBImage;
@property (strong, nonatomic) IBOutlet UILabel *answerBLabel;
@property (strong, nonatomic) IBOutlet UIButton *answerCButton;
@property (strong, nonatomic) IBOutlet UIImageView *logoCImage;
@property (strong, nonatomic) IBOutlet UILabel *answerCLabel;
@property (strong, nonatomic) IBOutlet UIButton *answerDButton;
@property (strong, nonatomic) IBOutlet UIImageView *logoDImage;
@property (strong, nonatomic) IBOutlet UILabel *answerDLabel;
@property (strong, nonatomic) IBOutlet UIImageView *progressBarBackground;


- (IBAction)answerATouched:(id)sender;
- (IBAction)answerBTouched:(id)sender;
- (IBAction)answerCTouched:(id)sender;
- (IBAction)answerDTouched:(id)sender;

@end
