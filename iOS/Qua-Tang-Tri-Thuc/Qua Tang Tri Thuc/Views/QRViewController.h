//
//  QRViewController.h
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 10/11/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBarSDK.h"
#import "GAITrackedViewController.h"


@interface QRViewController : GAITrackedViewController<ZBarReaderDelegate> {
    ZBarReaderViewController *reader;
    BOOL autoFailed;
    NSTimer *updateTimer;
    BOOL isLoading;
    
    BOOL alerted;
}

@property (strong, nonatomic) IBOutlet UIView *qrScannerView;
@property (strong, nonatomic) IBOutlet UILabel *userIDLabel;
@property (strong, nonatomic) IBOutlet UILabel *dayScoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *totalScoreLabel;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
- (IBAction)backButtonTouched:(id)sender;

@end
