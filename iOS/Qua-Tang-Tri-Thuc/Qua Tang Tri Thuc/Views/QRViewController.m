//
//  QRViewController.m
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 10/11/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import "QRViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "Constants.h"
#import <AFNetworking/AFNetworking.h>
#import "MenuViewController.h"
#import "ContestInfoViewController.h"
#import "QuestionViewController.h"
#import "QuestionInfoViewController.h"
#import "SummaryViewController.h"
#import "ResultViewController.h"
#import "TimeoutHTTPRequestSerializer.h"
#import "SoundManager.h"


@interface QRViewController ()

@end

@implementation QRViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    long long userID = [[defaults objectForKey:USERID_KEY] longLongValue];
    long long userScore = [[defaults objectForKey:USER_SCORE_KEY] longLongValue];
    long long userDayScore = [[defaults objectForKey:USER_DAY_SCORE_KEY] longLongValue];

    self.userIDLabel.text = [NSString stringWithFormat:@"%lld", userID];
    self.dayScoreLabel.text = [NSString stringWithFormat:@"%lld", userDayScore];
    self.totalScoreLabel.text = [NSString stringWithFormat:@"%lld", userScore];
    
    reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    
    [reader.scanner setSymbology: 0
                          config: ZBAR_CFG_ENABLE
                              to: 0];
    [reader.scanner setSymbology: ZBAR_QRCODE
                          config: ZBAR_CFG_ENABLE
                              to: 1];
    
    reader.showsCameraControls = NO;  // for UIImagePickerController
    reader.showsZBarControls = NO;
    
    reader.readerView.frame = CGRectMake(0, 0, self.qrScannerView.frame.size.width, self.qrScannerView.frame.size.height);
    reader.readerView.zoom = 1.0;
    reader.cameraFlashMode = UIImagePickerControllerCameraFlashModeOff;
    reader.readerView.torchMode = AVCaptureTorchModeOff;
    reader.view = reader.readerView;

    [self addChildViewController:reader];
    [self.qrScannerView addSubview:reader.view];
    
    alerted = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.screenName = @"QR Scanner Screen";
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
//    [self loadContest:[NSString stringWithFormat:CONTEST_DAY_URL, @"f2e04240-6496-4c7c-abc7-1b8671c7bf63"]];
    autoFailed = YES;
    isLoading = NO;
    // Refresh in 1s
    if (updateTimer == nil) {
        updateTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(update:) userInfo:nil repeats:YES];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (updateTimer != nil) {
        [updateTimer invalidate];
        updateTimer = nil;
    }
}

- (void) update:(id) sender {
    if (isLoading == NO && autoFailed == YES) {
        [self loadContest:CONTEST_TODAY_URL];
    }
}

#pragma ZBarReader Delegate

- (void) imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary*) info
{
    id<NSFastEnumeration> results = [info objectForKey:ZBarReaderControllerResults];
    
    for (id item in results) {
        [self loadContest:((ZBarSymbol *)item).data];
    }
}


- (void) loadContest:(NSString *)contestLink {
    [self.loadingIndicator startAnimating];
    [reader.readerView stop];
    
    isLoading = YES;

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[TimeoutHTTPRequestSerializer serializer]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:contestLink parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:[JSON objectForKey:@"NumberOfQuestions"] forKey:NUMBER_QUESTIONS_KEY];
        [defaults synchronize];
        
        long state = [[JSON objectForKey:@"State"] longValue];
        if (state >= 10 && state < 10000) {
            if (state % 10 == 3) {
                // Go to result page
                ResultViewController *viewController = [[ResultViewController alloc] initWithNibName:@"ResultViewController" bundle:nil];
                viewController.contestKey = [JSON objectForKey:@"ContestKey"];
                [self.navigationController pushViewController:viewController animated:YES];
            }
            else if (state % 10 == 2) {
                // Go to question page
                [defaults setValue:[NSNumber numberWithInt:state / 10] forKey:QUESTION_NUMBER_KEY];
                [defaults synchronize];

                QuestionViewController *viewController = [[QuestionViewController alloc] initWithNibName:@"QuestionViewController" bundle:nil];
                viewController.questionNumber = state / 10; // Divided by 10 to get the question number
                viewController.contestKey = [JSON objectForKey:@"ContestKey"];
                viewController.numberOfQuestions = [[JSON objectForKey:@"NumberOfQuestions"] intValue];
                viewController.contestState = state;
                [self.navigationController pushViewController:viewController animated:YES];
            }
            else {
                // Go to question info page
                [defaults setValue:[NSNumber numberWithInt:state / 10] forKey:QUESTION_NUMBER_KEY];
                [defaults synchronize];

                QuestionInfoViewController *viewController = [[QuestionInfoViewController alloc] initWithNibName:@"QuestionInfoViewController" bundle:nil];
                viewController.questionNumber = state / 10; // Divided by 10 to get the question number
                viewController.contestKey = [JSON objectForKey:@"ContestKey"];
                viewController.contestState = state;
                [self.navigationController pushViewController:viewController animated:YES];
            }
        }
        else if (state == 10400) {
            [defaults setValue:[NSNumber numberWithInt:[[JSON objectForKey:@"NumberOfQuestions"] intValue] + 1] forKey:QUESTION_NUMBER_KEY];
            [defaults synchronize];
            
            QuestionInfoViewController *viewController = [[QuestionInfoViewController alloc] initWithNibName:@"QuestionInfoViewController" bundle:nil];
            viewController.questionNumber = [[JSON objectForKey:@"NumberOfQuestions"] intValue] + 1; // Divided by 10 to get the question number
            viewController.contestKey = [JSON objectForKey:@"ContestKey"];
            viewController.contestState = state;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else if (state == 10401) {
            // Go to question page
            [defaults setValue:[NSNumber numberWithInt:[[JSON objectForKey:@"NumberOfQuestions"] intValue] + 1] forKey:QUESTION_NUMBER_KEY];
            [defaults synchronize];

            QuestionViewController *viewController = [[QuestionViewController alloc] initWithNibName:@"QuestionViewController" bundle:nil];
            viewController.questionNumber = [[JSON objectForKey:@"NumberOfQuestions"] intValue] + 1; // Divided by 10 to get the question number
            viewController.contestKey = [JSON objectForKey:@"ContestKey"];
            viewController.numberOfQuestions = [[JSON objectForKey:@"NumberOfQuestions"] intValue];
            viewController.contestState = state;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else if (state == 10402) {
            // Go to result page
            ResultViewController *viewController = [[ResultViewController alloc] initWithNibName:@"ResultViewController" bundle:nil];
            viewController.contestKey = [JSON objectForKey:@"ContestKey"];
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else if (state == 10000 || state == 10100 || state == 10200) {
            int quesNum = [[defaults valueForKey:QUESTION_NUMBER_KEY] intValue];

            if (quesNum > 0 && quesNum % 4 == 0) {
                // Go to summary page
                SummaryViewController *viewController = [[SummaryViewController alloc] initWithNibName:@"SummaryViewController" bundle:nil];
                viewController.contestKey = [JSON objectForKey:@"ContestKey"];
                [self.navigationController pushViewController:viewController animated:YES];
            }
            else {
                // Go to info page
                ContestInfoViewController *viewController = [[ContestInfoViewController alloc] initWithNibName:@"ContestInfoViewController" bundle:nil];
                viewController.contestKey = [JSON objectForKey:@"ContestKey"];
                [self.navigationController pushViewController:viewController animated:YES];

                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                               message:@"Chương trình đã kết nối với gameshow thành công. Vui lòng chờ và theo dõi chương trình diễn ra trên truyền hình."
                                                              delegate:self
                                                     cancelButtonTitle:nil
                                                     otherButtonTitles:@"OK",nil];
                [alert show];
                alert = nil;
            }
        }
        else if (state == 10300) {
            // Go to menu page
            MenuViewController *viewController = [[MenuViewController alloc] initWithNibName:@"MenuViewController" bundle:nil];
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else {
            // If state is not in contest, stay here or go back?
            // Go to menu page
            MenuViewController *viewController = [[MenuViewController alloc] initWithNibName:@"MenuViewController" bundle:nil];
            [self.navigationController pushViewController:viewController animated:YES];
        }
                                                                                                   
        [self.loadingIndicator stopAnimating];
        isLoading = NO;
        [reader.readerView start];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (operation.response.statusCode == 404) {
            // Not found
            if ([contestLink isEqualToString:CONTEST_TODAY_URL]) {
                // Load today contest failed then stay here for QR scanning
                autoFailed = YES; //NO;
                
                // Show message
                if (alerted == NO) {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                                   message:@"Không có chương trình gameshow vào thời gian này. Vui lòng đón xem gameshow \"Cùng Là Tỷ Phú\" được truyền hình trực tiếp trên kênh truyền hình Let's Viet vào lúc 8h30 thứ bảy và chủ nhật hàng tuần."
                                                                  delegate:self
                                                         cancelButtonTitle:nil
                                                         otherButtonTitles:@"OK",nil];
                    [alert show];
                    alert = nil;
                    alerted = YES;
                }
            }
            else {
                // Go to menu page
                MenuViewController *viewController = [[MenuViewController alloc] initWithNibName:@"MenuViewController" bundle:nil];
                [self.navigationController pushViewController:viewController animated:YES];
            }
        }
        else {
            if ([contestLink isEqualToString:CONTEST_TODAY_URL]) {
                // Network error and auto-loading
                autoFailed = YES;
            }
        }
        
        [self.loadingIndicator stopAnimating];
        [reader.readerView start];
        isLoading = NO;
    }];
}

- (IBAction)backButtonTouched:(id)sender {
    [[SoundManager getButtonAudio] play];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
