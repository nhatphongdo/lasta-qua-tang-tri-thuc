//
//  TermViewController.m
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 4/15/14.
//  Copyright (c) 2014 VietDev Corporation. All rights reserved.
//

#import "ExchangeViewController.h"
#import "Constants.h"
#import <AFNetworking/AFNetworking.h>
#import "TimeoutHTTPRequestSerializer.h"
#import "SoundManager.h"


@interface ExchangeViewController ()

@end

@implementation ExchangeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

    [self.codeTextView setDelegate:self];
    
    NSURLRequest *urlReq = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"termTyPhuODo" ofType:@"html"]]];
    [self.contentWebView loadRequest:urlReq];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.screenName = @"Ty Phu Khong Do Screen";
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[SoundManager getOpenningAudio] play];
}

- (void)keyboardWillHide:(NSNotification *)aNotification
{
    // the keyboard is hiding reset the table's height
    NSTimeInterval animationDuration = [[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect frame = self.view.frame;
    frame.origin.y += 160;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];
}

- (void)keyboardWillShow:(NSNotification *)aNotification
{
    // the keyboard is showing so resize the table's height
    NSTimeInterval animationDuration = [[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect frame = self.view.frame;
    frame.origin.y -= 160;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.codeTextView) {
        [self exchangeButtonTouched:nil];
    }
    return YES;
}

- (IBAction)joinButtonTouched:(id)sender {
    // Hide info form
    [[SoundManager getButtonAudio] play];
    
    [self.popupView setHidden:YES];
    [self.infoView setHidden:YES];
    [self.submitView setHidden:NO];
}

- (IBAction)exchangeButtonTouched:(id)sender {
    [[SoundManager getButtonAudio] play];
    
    NSString *code = self.codeTextView.text;
    
    if (code.length == 0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                       message:@"Bạn chưa nhập Mã số quy đổi."
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
        
        [self.codeTextView becomeFirstResponder];
        return;
    }
    
    [self.exchangeButton setEnabled:NO];
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    long long userID = [[defaults objectForKey:USERID_KEY] longLongValue];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[TimeoutHTTPRequestSerializer serializer]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:[NSString stringWithFormat:EXCHANGE_SCORE_URL, code, userID] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
        
        NSLog(@"%@", JSON);
        
        if ([[JSON objectForKey:@"Error"] intValue] == 0) {
            // Show success popup
            [self.popupView setHidden:NO];
            [self.infoView setHidden:YES];
            [self.submitView setHidden:YES];
            
            [self.scoreLabel setText:[NSString stringWithFormat:@"%ld điểm", [[JSON objectForKey:@"Score"] longValue]]];
        }
        else if ([[JSON objectForKey:@"Error"] intValue] == 1) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Mã số này đã được quy đổi rồi. Bạn không thể sử dụng lại để quy đổi điểm thưởng."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            [alert show];
            alert = nil;
        }
        else if ([[JSON objectForKey:@"Error"] intValue] == 2) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Mã số này không hợp lệ hoặc không chính xác. Vui lòng kiểm tra mã số và thử lại."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            [alert show];
            alert = nil;
        }
        else if ([[JSON objectForKey:@"Error"] intValue] == 3) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Chương trình xảy ra lỗi trong quá trình xử lý. Vui lòng thử lại hoặc thông báo tới BTC để được hướng dẫn xử lý và hỗ trợ."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            [alert show];
            alert = nil;
        }
        
        [self.view endEditing:YES];
        [self.exchangeButton setEnabled:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@", error);
        // Network problem
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                       message:@"Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại."
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
        [self.exchangeButton setEnabled:YES];
    }];
}

- (IBAction)backButtonTouched:(id)sender {
    [[SoundManager getButtonAudio] play];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)popupBackButtonTouched:(id)sender {
    [[SoundManager getButtonAudio] play];    
    [self.popupView setHidden:YES];
    [self.infoView setHidden:YES];
    [self.submitView setHidden:NO];
}

@end
