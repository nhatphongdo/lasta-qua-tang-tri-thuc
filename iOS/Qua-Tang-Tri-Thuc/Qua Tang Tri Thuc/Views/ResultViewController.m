//
//  ResultViewController.m
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 10/11/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import "ResultViewController.h"
#import "Constants.h"
#import <AFNetworking/AFNetworking.h>
#import "ContestInfoViewController.h"
#import "ResultViewController.h"
#import "QuestionViewController.h"
#import "SummaryViewController.h"
#import "QuestionInfoViewController.h"
#import "TimeoutHTTPRequestSerializer.h"
#import "SoundManager.h"


@interface ResultViewController ()

@end

@implementation ResultViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.correctAnswer = @"";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    userID = [[defaults objectForKey:USERID_KEY] longLongValue];

    [self.rightImage setImage:nil];
    [self.smileIcon setImage:nil];
    [self.textTitle setImage:nil];
    [self.textCongras setHidden:YES];
    [self.currentScoreLabel setHidden:YES];
    self.answeredLabel.text = @"";

    isFailed = NO;
    networkFailedCount = 0;
    isCheckingAnswer = YES;
    
    [self checkAnswer:userID];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.screenName = @"Result Screen";
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (updateTimer != nil) {
        [updateTimer invalidate];
        updateTimer = nil;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (updateTimer == nil) {
        updateTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(update:) userInfo:nil repeats:YES];
    }
}

- (void) update:(id) sender {
    if (isLoading == NO) {
        if (isCheckingAnswer == YES) {
            [self checkAnswer:userID];
        }
        else {
            [self loadState];
        }
    }
}

- (void) loadState {
    if (![self.contestKey isEqualToString:@""]) {
        isLoading = YES;
        
        // Load question
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager setRequestSerializer:[TimeoutHTTPRequestSerializer serializer]];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        [manager GET:[NSString stringWithFormat:STATE_URL, self.contestKey] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
            self.contestState = [[JSON objectForKey:@"Status"] longValue];
            
            [self processState];
            isLoading = NO;
            
            networkFailedCount = 0;
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            // Network problem
            ++networkFailedCount;
            if (isFailed == NO && networkFailedCount >= 5) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                               message:@"Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công."
                                                              delegate:self
                                                     cancelButtonTitle:nil
                                                     otherButtonTitles:@"OK",nil];
                alert.tag = 2;
                [alert show];
                alert = nil;
                
                isFailed = YES;
                
                networkFailedCount = 0;
            }
            
            isLoading = NO;
        }];
    }
}

- (void) processState {
    if (self.contestState >= 10 && self.contestState < 10000) {
        if (self.contestState % 10 == 3) {
        }
        else if (self.contestState % 10 == 2) {
            // Go to question page
            NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
            [defaults setValue:[NSNumber numberWithInt:self.contestState / 10]  forKey:QUESTION_NUMBER_KEY];
            [defaults synchronize];

            QuestionViewController *viewController = [[QuestionViewController alloc] initWithNibName:@"QuestionViewController" bundle:nil];
            viewController.questionNumber = self.contestState / 10; // Divided by 10 to get the question number
            viewController.contestKey = self.contestKey;
            viewController.contestState = self.contestState;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else {
            // Go to question info page
            NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
            [defaults setValue:[NSNumber numberWithInt:self.contestState / 10]  forKey:QUESTION_NUMBER_KEY];
            [defaults synchronize];

            QuestionInfoViewController *viewController = [[QuestionInfoViewController alloc] initWithNibName:@"QuestionInfoViewController" bundle:nil];
            viewController.questionNumber = self.contestState / 10; // Divided by 10 to get the question number
            viewController.contestKey = self.contestKey;
            viewController.contestState = self.contestState;
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }
    else if (self.contestState == 10400) {
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        
        QuestionInfoViewController *viewController = [[QuestionInfoViewController alloc] initWithNibName:@"QuestionInfoViewController" bundle:nil];
        viewController.questionNumber = [[defaults objectForKey:NUMBER_QUESTIONS_KEY] intValue] + 1;
        viewController.contestKey = self.contestKey;
        viewController.contestState = self.contestState;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else if (self.contestState == 10401) {
        // Go to question page
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        
        QuestionViewController *viewController = [[QuestionViewController alloc] initWithNibName:@"QuestionViewController" bundle:nil];
        viewController.contestKey = self.contestKey;
        viewController.contestState = self.contestState;
        viewController.questionNumber = [[defaults objectForKey:NUMBER_QUESTIONS_KEY] intValue] + 1;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else if (self.contestState == 10000 || self.contestState == 10100 || self.contestState == 10200) {
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        int quesNum = [[defaults valueForKey:QUESTION_NUMBER_KEY] intValue];
        if (quesNum > 0 && quesNum % 4 == 0) {
            // Go to summary page
            SummaryViewController *viewController = [[SummaryViewController alloc] initWithNibName:@"SummaryViewController" bundle:nil];
            viewController.contestKey = self.contestKey;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else {
            // Go to info page
            ContestInfoViewController *viewController = [[ContestInfoViewController alloc] initWithNibName:@"ContestInfoViewController" bundle:nil];
            viewController.contestKey = self.contestKey;
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }
    else if (self.contestState == 10300) {
        // Go to finished page
        SummaryViewController *viewController = [[SummaryViewController alloc] initWithNibName:@"SummaryViewController" bundle:nil];
        viewController.contestKey = self.contestKey;
        viewController.showFull = YES;
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (void) checkAnswer:(long long)uID {
    isLoading = YES;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[TimeoutHTTPRequestSerializer serializer]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:[NSString stringWithFormat:CHECK_ANSWER_URL, uID] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
        
        self.answeredLabel.text = [NSString stringWithFormat:@"%@", [JSON objectForKey:@"CorrectAnswer"]];
        
        if ([[JSON objectForKey:@"ResultCode"] intValue] == 1) {
            // Correct
            [self.rightImage setImage:[UIImage imageNamed:@"bg_correct_answer.png"]];
            [self.smileIcon setImage:[UIImage imageNamed:@"icon_smile.png"]];
            [self.textTitle setImage:[UIImage imageNamed:@"text_right.png"]];
            [self.textCongras setHidden:NO];
            [self.currentScoreLabel setHidden:NO];
            self.currentScoreLabel.text = [NSString stringWithFormat:@"+%lld", [[JSON objectForKey:@"Score"] longLongValue]];
            
            [[SoundManager getSmileAudio] play];
        }
        else {
            // Wrong
            [self.rightImage setImage:[UIImage imageNamed:@"bg_wrong_answer.png"]];
            [self.smileIcon setImage:[UIImage imageNamed:@"icon_sad.png"]];
            [self.textTitle setImage:[UIImage imageNamed:@"text_wrong.png"]];
            [self.textCongras setHidden:YES];
            [self.currentScoreLabel setHidden:YES];
            
            [[SoundManager getSadAudio] play];
        }
        
        isLoading = NO;
        isCheckingAnswer = NO;
        
        networkFailedCount = 0;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // Network problem
        if (operation.response.statusCode == 404) {
            // Not found
            // Check sent answer
            [self.rightImage setImage:[UIImage imageNamed:@"bg_wrong_answer.png"]];
            [self.smileIcon setImage:[UIImage imageNamed:@"icon_sad.png"]];
            [self.textTitle setImage:[UIImage imageNamed:@"text_wrong.png"]];
            [self.textCongras setHidden:YES];
            [self.currentScoreLabel setHidden:YES];
            self.answeredLabel.text = @"";

            [[SoundManager getSadAudio] play];
            
            if (self.correctAnswer == NULL || [self.correctAnswer isEqualToString:@""]) {
                // Empty string means no answer
            }
            else {
                // Have string means network error
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                               message:@"Rất tiếc, chương trình gặp lỗi kết nối mạng khi gởi đáp án về hệ thống."
                                                              delegate:self
                                                     cancelButtonTitle:nil
                                                     otherButtonTitles:@"OK",nil];
                [alert show];
                alert = nil;
            }
            
            isCheckingAnswer = NO;
        }
        else {
            ++networkFailedCount;
            if (isFailed == NO && networkFailedCount >= 5) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                               message:@"Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công."
                                                              delegate:self
                                                     cancelButtonTitle:nil
                                                     otherButtonTitles:@"OK",nil];
                [alert show];
                alert = nil;
                
                isFailed = YES;
                
                networkFailedCount = 0;
            }
        }
        
        isLoading = NO;
    }];
}


@end
