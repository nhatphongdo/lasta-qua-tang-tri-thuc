//
//  ContestInfoViewController.h
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 10/11/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"


@interface ContestInfoViewController : GAITrackedViewController<UIGestureRecognizerDelegate> {
    NSTimer *updateTimer;
    BOOL isFailed;
    BOOL isLoading;

    NSTimer *slideTimer;
    int direction;
    
    int networkFailedCount;
}

@property (nonatomic) long contestState;
@property (nonatomic, strong) NSString *contestKey;

@property (strong, nonatomic) IBOutlet UILabel *userIDLabel;
@property (strong, nonatomic) IBOutlet UILabel *dayScoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *totalScoreLabel;
@property (strong, nonatomic) IBOutlet UIView *prizeView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageView;
@property (strong, nonatomic) IBOutlet UIView *prizeContainerView;

@end
