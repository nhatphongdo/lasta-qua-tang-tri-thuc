//
//  ShareViewController.h
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 4/15/14.
//  Copyright (c) 2014 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"


@interface ShareViewController : GAITrackedViewController

- (IBAction)backButtonTouched:(id)sender;
- (IBAction)shareButtonTouched:(id)sender;
- (IBAction)facebookButtonTouched:(id)sender;

@end
