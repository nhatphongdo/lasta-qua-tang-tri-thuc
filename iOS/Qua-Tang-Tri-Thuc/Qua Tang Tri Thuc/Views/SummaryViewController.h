//
//  SummaryViewController.h
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 10/11/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"


@interface SummaryViewController : GAITrackedViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate> {
    NSArray *users;
    NSInteger selectedRow;
    
    NSTimer *updateTimer;
    BOOL isFailed;
    BOOL isLoading;
    
    int networkFailedCount;
}

@property (nonatomic) long contestState;
@property (nonatomic, strong) NSString *contestKey;
@property (nonatomic) BOOL showFull;

@property (strong, nonatomic) IBOutlet UILabel *userIDLabel;
@property (strong, nonatomic) IBOutlet UILabel *totalScoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *dayScoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *dayPositionLabel;
@property (strong, nonatomic) IBOutlet UILabel *positionLabel;
@property (strong, nonatomic) IBOutlet UITableView *usersTableView;
@property (strong, nonatomic) IBOutlet UIButton *backButton;
@property (strong, nonatomic) IBOutlet UITextField *searchTextField;
@property (strong, nonatomic) IBOutlet UIButton *searchButton;
@property (strong, nonatomic) IBOutlet UIView *usersTableViewHeader;
@property (strong, nonatomic) IBOutlet UIButton *top50DayButton;
@property (strong, nonatomic) IBOutlet UIButton *top50TotalButton;

- (IBAction)backButtonTouched:(id)sender;
- (IBAction)searchButtonTouched:(id)sender;
- (IBAction)top50DayTouched:(id)sender;
- (IBAction)top50TotalTouched:(id)sender;

@end
