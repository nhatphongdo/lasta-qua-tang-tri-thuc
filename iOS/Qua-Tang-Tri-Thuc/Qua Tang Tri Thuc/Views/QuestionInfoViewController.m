//
//  QuestionInfoViewController.m
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 12/2/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import "QuestionInfoViewController.h"
#import "Constants.h"
#import <AFNetworking/AFNetworking.h>
#import "ContestInfoViewController.h"
#import "ResultViewController.h"
#import "QuestionViewController.h"
#import "SummaryViewController.h"
#import "TimeoutHTTPRequestSerializer.h"
#import "SoundManager.h"


@interface QuestionInfoViewController ()

@end

@implementation QuestionInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.questionNumber = 0;
        self.contestKey = @"";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    long long userID = [[defaults objectForKey:USERID_KEY] longLongValue];
    long long userScore = [[defaults objectForKey:USER_SCORE_KEY] longLongValue];
    long long userDayScore = [[defaults objectForKey:USER_DAY_SCORE_KEY] longLongValue];

    self.userIDLabel.text = [NSString stringWithFormat:@"%lld", userID];
    self.dayScoreLabel.text = [NSString stringWithFormat:@"%lld", userDayScore];
    self.totalScoreLabel.text = [NSString stringWithFormat:@"%lld", userScore];

    isLoadedScore = NO;
    isLoadedTopic = NO;
    
    isQuestionLoaded = NO;
    [self loadQuestion:self.questionNumber];
    
    isFailed = NO;
    isLoading = NO;
    
    networkFailedCount = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.screenName = @"Question Info Screen";
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (updateTimer != nil) {
        [updateTimer invalidate];
        updateTimer = nil;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[SoundManager getBackgroundAudio] play];

    if (updateTimer == nil) {
        updateTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(update:) userInfo:nil repeats:YES];
    }
}

- (void) loadQuestion:(int)number {
    if (number > 0 && ![self.contestKey isEqualToString:@""]) {
        isLoading = YES;
        
        // Load question
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager setRequestSerializer:[TimeoutHTTPRequestSerializer serializer]];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        [manager GET:[NSString stringWithFormat:QUESTION_URL, self.contestKey, number] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
            self.topic = [JSON objectForKey:@"Topic"];
            self.questionScore = [[JSON objectForKey:@"Score"] longLongValue];
            answerA =  [JSON objectForKey:@"Answer1"];
            answerB =  [JSON objectForKey:@"Answer2"];
            answerC =  [JSON objectForKey:@"Answer3"];
            answerD =  [JSON objectForKey:@"Answer4"];
            totalTime = [[JSON objectForKey:@"CountdownTime"] intValue];

            [self processState];
            
            isLoading = NO;
            isQuestionLoaded = YES;
            
            networkFailedCount = 0;
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            // Network problem
            ++networkFailedCount;
            if (isFailed == NO && networkFailedCount >= 5) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                               message:@"Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công."
                                                              delegate:self
                                                     cancelButtonTitle:nil
                                                     otherButtonTitles:@"OK",nil];
                alert.tag = 1;
                [alert show];
                alert = nil;
                
                isFailed = YES;
                networkFailedCount = 0;
            }
            
            isLoading = NO;
        }];
    }
}

- (void) loadState {
    if (![self.contestKey isEqualToString:@""]) {
        isLoading = YES;
        
        // Load question
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager setRequestSerializer:[TimeoutHTTPRequestSerializer serializer]];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        [manager GET:[NSString stringWithFormat:STATE_URL, self.contestKey] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
            
            self.contestState = [[JSON objectForKey:@"Status"] longValue];

            isLoading = NO;

            if (self.contestState % 10 == 1) {
                [self loadQuestion:self.questionNumber];
            }
            else {
                [self processState];
            }
            
            networkFailedCount = 0;
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            // Network problem
            if (isFailed == NO && networkFailedCount >= 5) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                               message:@"Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công."
                                                              delegate:self
                                                     cancelButtonTitle:nil
                                                     otherButtonTitles:@"OK",nil];
                alert.tag = 2;
                [alert show];
                alert = nil;
                
                isFailed = YES;
                networkFailedCount = 0;
            }
            
            isLoading = NO;
        }];
    }
}

- (void) update:(id) sender {
    if (isLoading == NO) {
        if (isQuestionLoaded == NO) {
            [self loadQuestion:self.questionNumber];
        }
        else {
            [self loadState];
        }
    }
}

- (void) processState {
    if (self.contestState >= 10 && self.contestState < 10000) {
        // Question
        if (self.contestState % 10 == 2) {
            NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
            [defaults setValue:[NSNumber numberWithInt:self.contestState / 10]  forKey:QUESTION_NUMBER_KEY];
            [defaults synchronize];

            QuestionViewController *questionView = [[QuestionViewController alloc] initWithNibName:@"QuestionViewController" bundle:nil];
            questionView.contestKey = self.contestKey;
            questionView.contestState = self.contestState;
            questionView.questionNumber = self.contestState / 10;
            questionView.topic = self.topic;
            questionView.questionScore = self.questionScore;
            questionView.answerA = answerA;
            questionView.answerB = answerB;
            questionView.answerC = answerC;
            questionView.answerD = answerD;
            questionView.totalTime = totalTime;
            
            [self.navigationController pushViewController:questionView animated:YES];
        }
        else if (self.contestState % 10 == 3) {
            // Go to summary page
            ResultViewController *viewController = [[ResultViewController alloc] initWithNibName:@"ResultViewController" bundle:nil];
            viewController.contestKey = self.contestKey;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else if (self.contestState % 10 == 0) {
            [self.topicImageView setHidden:NO];
            [self.scoreLabel setHidden:YES];
            
            self.topicTextLabel.text = [NSString stringWithFormat:@"CHỦ ĐỀ CỦA CÂU %d", self.questionNumber];
            
            if ([self.topic isEqualToString:@"Sức khỏe"]) {
                self.topicImageView.image = [UIImage imageNamed:@"suckhoe.png"];
            }
            else if ([self.topic isEqualToString:@"Địa lý"]) {
                self.topicImageView.image = [UIImage imageNamed:@"dialy.png"];
            }
            else if ([self.topic isEqualToString:@"Giải trí"]) {
                self.topicImageView.image = [UIImage imageNamed:@"giaitri.png"];
            }
            else if ([self.topic isEqualToString:@"Lịch sử"]) {
                self.topicImageView.image = [UIImage imageNamed:@"lichsu.png"];
            }
            else if ([self.topic isEqualToString:@"Thể thao"]) {
                self.topicImageView.image = [UIImage imageNamed:@"thethao.png"];
            }
            else if ([self.topic isEqualToString:@"Văn hóa - Xã hội"]) {
                self.topicImageView.image = [UIImage imageNamed:@"vanhoaxahoi.png"];
            }
            else if ([self.topic isEqualToString:@"Văn học"]) {
                self.topicImageView.image = [UIImage imageNamed:@"vanhoc.png"];
            }
            else if ([self.topic isEqualToString:@"Khoa học - Công nghệ"]) {
                self.topicImageView.image = [UIImage imageNamed:@"khoahoccongnghe.png"];
            }
            
            if (isLoadedTopic == NO) {
                [[SoundManager getTransitionAudio] play];
                isLoadedTopic = YES;
            }
        }
        else {
            [self.topicImageView setHidden:YES];
            [self.scoreLabel setHidden:NO];
            
            self.topicTextLabel.text = [NSString stringWithFormat:@"ĐIỂM SỐ CỦA CÂU %d", self.questionNumber];
            
            self.scoreLabel.text = [NSString stringWithFormat:@"%lld", self.questionScore];
            
            if (isLoadedScore == NO) {
                [[SoundManager getTransitionAudio] play];
                isLoadedScore = YES;
            }
        }
    }
    else if (self.contestState == 10400) {
        [self.topicImageView setHidden:YES];
        [self.scoreLabel setHidden:NO];
        
        self.topicTextLabel.text = [NSString stringWithFormat:@"ĐIỂM SỐ CỦA CÂU %d", self.questionNumber];
        
        self.scoreLabel.text = [NSString stringWithFormat:@"%lld", self.questionScore];
    }
    else if (self.contestState == 10401) {
        // Go to question page
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        
        QuestionViewController *viewController = [[QuestionViewController alloc] initWithNibName:@"QuestionViewController" bundle:nil];
        viewController.contestKey = self.contestKey;
        viewController.contestState = self.contestState;
        viewController.topic = self.topic;
        viewController.questionScore = self.questionScore;
        viewController.answerA = answerA;
        viewController.answerB = answerB;
        viewController.answerC = answerC;
        viewController.answerD = answerD;
        viewController.totalTime = totalTime;
        
        viewController.questionNumber = [[defaults objectForKey:NUMBER_QUESTIONS_KEY] intValue] + 1;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else if (self.contestState == 10402) {
        // Go to result page
        ResultViewController *viewController = [[ResultViewController alloc] initWithNibName:@"ResultViewController" bundle:nil];
        viewController.contestKey = self.contestKey;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else if (self.contestState == 10000 || self.contestState == 10100 || self.contestState == 10200) {
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        int quesNum = [[defaults valueForKey:QUESTION_NUMBER_KEY] intValue];
        if (quesNum > 0 && quesNum % 4 == 0) {
            // Go to summary page
            SummaryViewController *viewController = [[SummaryViewController alloc] initWithNibName:@"SummaryViewController" bundle:nil];
            viewController.contestKey = self.contestKey;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else {
            // Info screen
            ContestInfoViewController *infoView = [[ContestInfoViewController alloc] initWithNibName:@"ContestInfoViewController" bundle:nil];
            infoView.contestKey = self.contestKey;
            [self.navigationController pushViewController:infoView animated:YES];
        }
    }
    else if (self.contestState == 10300) {
        SummaryViewController *summaryView = [[SummaryViewController alloc] initWithNibName:@"SummaryViewController" bundle:nil];
        summaryView.contestKey = self.contestKey;
        summaryView.showFull = YES;
        [self.navigationController pushViewController:summaryView animated:YES];
    }
}


@end
