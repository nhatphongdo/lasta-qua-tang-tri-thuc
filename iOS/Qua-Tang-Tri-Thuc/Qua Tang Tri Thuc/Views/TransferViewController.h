//
//  TransferViewController.h
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 4/15/14.
//  Copyright (c) 2014 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"


@interface TransferViewController : GAITrackedViewController<UITextFieldDelegate, UIGestureRecognizerDelegate>

@property (strong, nonatomic) IBOutlet UILabel *userIDLabel;
@property (strong, nonatomic) IBOutlet UILabel *totalScoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *positionLabel;
@property (strong, nonatomic) IBOutlet UITextField *searchIDTextField;
@property (strong, nonatomic) IBOutlet UILabel *searchIDLabel;
@property (strong, nonatomic) IBOutlet UILabel *searchFullnameLabel;
@property (strong, nonatomic) IBOutlet UILabel *searchScoreLabel;
@property (strong, nonatomic) IBOutlet UITextField *transferScoreTextField;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UIView *infoView;
@property (strong, nonatomic) IBOutlet UIWebView *contentWebView;
@property (strong, nonatomic) IBOutlet UIButton *submitButton;

- (IBAction)transferScoreButtonTouched:(id)sender;
- (IBAction)joinButtonTouched:(id)sender;
- (IBAction)backButtonTouched:(id)sender;

@end
