//
//  QuestionViewController.m
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 10/11/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import "QuestionViewController.h"
#import "Constants.h"
#import <AFNetworking/AFNetworking.h>
#import "ResultViewController.h"
#import "ContestInfoViewController.h"
#import "SummaryViewController.h"
#import "TimeoutHTTPRequestSerializer.h"
#import "SoundManager.h"


@interface QuestionViewController ()

@end

@implementation QuestionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.questionNumber = 0;
        self.contestKey = @"";
        self.topic = @"";
        self.remainTime = 0;
        progressTime = 0;
        isJoined = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.progressBarImage.frame = CGRectMake(self.progressBarImage.frame.origin.x, self.progressBarImage.frame.origin.y,
                                             self.progressBarBackground.frame.size.width / 150.0 * 150.0,
                                             self.progressBarImage.frame.size.height);

    isSubmittingAnswer = NO;
    isLocked = NO;

    [self disableAnswers];
    self.answerALabel.text = @"";
    self.answerBLabel.text = @"";
    self.answerCLabel.text = @"";
    self.answerDLabel.text = @"";
    
    isQuestionLoaded = NO;
    
    if (self.questionNumber == 0) {
        self.questionNumber = 17;
    }
    
    self.startAnswerTime = nil;
    
    isFailed = NO;
    isLoading = NO;

    if ([self.topic isEqualToString:@""] || self.questionScore == 0 || self.totalTime == 0) {
        [self loadQuestion:self.questionNumber];
    }
    else {
        [self.topicImage setHidden:NO];
        if ([self.topic isEqualToString:@"Sức khỏe"]) {
            self.topicImage.image = [UIImage imageNamed:@"suckhoe.png"];
        }
        else if ([self.topic isEqualToString:@"Địa lý"]) {
            self.topicImage.image = [UIImage imageNamed:@"dialy.png"];
        }
        else if ([self.topic isEqualToString:@"Giải trí"]) {
            self.topicImage.image = [UIImage imageNamed:@"giaitri.png"];
        }
        else if ([self.topic isEqualToString:@"Lịch sử"]) {
            self.topicImage.image = [UIImage imageNamed:@"lichsu.png"];
        }
        else if ([self.topic isEqualToString:@"Thể thao"]) {
            self.topicImage.image = [UIImage imageNamed:@"thethao.png"];
        }
        else if ([self.topic isEqualToString:@"Văn hóa - Xã hội"]) {
            self.topicImage.image = [UIImage imageNamed:@"vanhoaxahoi.png"];
        }
        else if ([self.topic isEqualToString:@"Văn học"]) {
            self.topicImage.image = [UIImage imageNamed:@"vanhoc.png"];
        }
        else if ([self.topic isEqualToString:@"Khoa học - Công nghệ"]) {
            self.topicImage.image = [UIImage imageNamed:@"khoahoccongnghe.png"];
        }
        else {
            [self.topicImage setHidden:YES];
        }
        
        self.answerALabel.text = self.answerA;
        self.answerALabel.textColor = [UIColor blackColor];
        self.answerBLabel.text = self.answerB;
        self.answerBLabel.textColor = [UIColor blackColor];
        self.answerCLabel.text = self.answerC;
        self.answerCLabel.textColor = [UIColor blackColor];
        self.answerDLabel.text = self.answerD;
        self.answerDLabel.textColor = [UIColor blackColor];
        self.scoreLabel.text = [NSString stringWithFormat:@"%lld", self.questionScore];

        isQuestionLoaded = YES;
    }
    
    networkFailedCount = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.screenName = @"Question Screen";
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (updateTimer != nil) {
        [updateTimer invalidate];
        updateTimer = nil;
    }
    
    if (stateTimer != nil) {
        [stateTimer invalidate];
        stateTimer = nil;
    }
    
    if (progressTimer != nil) {
        [progressTimer invalidate];
        progressTimer = nil;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (updateTimer == nil) {
        updateTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(update:) userInfo:nil repeats:YES];
    }

    if (progressTimer == nil) {
        progressTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(progress:) userInfo:nil repeats:YES];
    }

    if (stateTimer == nil) {
        stateTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(updateState:) userInfo:nil repeats:YES];
    }
}

- (void) loadQuestion:(int)number {
    if (number > 0 && ![self.contestKey isEqualToString:@""]) {
        isLoading = YES;
        
        // Load question
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager setRequestSerializer:[TimeoutHTTPRequestSerializer serializer]];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        [manager GET:[NSString stringWithFormat:QUESTION_URL, self.contestKey, number] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
            self.topic = [JSON objectForKey:@"Topic"];
            self.answerA = [JSON objectForKey:@"Answer1"];
            self.answerB = [JSON objectForKey:@"Answer2"];
            self.answerC = [JSON objectForKey:@"Answer3"];
            self.answerD = [JSON objectForKey:@"Answer4"];
            self.totalTime = [[JSON objectForKey:@"CountdownTime"] longValue];
            self.questionScore = [[JSON objectForKey:@"Score"] longLongValue];
            
            [self.topicImage setHidden:NO];
            if ([self.topic isEqualToString:@"Sức khỏe"]) {
                self.topicImage.image = [UIImage imageNamed:@"suckhoe.png"];
            }
            else if ([self.topic isEqualToString:@"Địa lý"]) {
                self.topicImage.image = [UIImage imageNamed:@"dialy.png"];
            }
            else if ([self.topic isEqualToString:@"Giải trí"]) {
                self.topicImage.image = [UIImage imageNamed:@"giaitri.png"];
            }
            else if ([self.topic isEqualToString:@"Lịch sử"]) {
                self.topicImage.image = [UIImage imageNamed:@"lichsu.png"];
            }
            else if ([self.topic isEqualToString:@"Thể thao"]) {
                self.topicImage.image = [UIImage imageNamed:@"thethao.png"];
            }
            else if ([self.topic isEqualToString:@"Văn hóa - Xã hội"]) {
                self.topicImage.image = [UIImage imageNamed:@"vanhoaxahoi.png"];
            }
            else if ([self.topic isEqualToString:@"Văn học"]) {
                self.topicImage.image = [UIImage imageNamed:@"vanhoc.png"];
            }
            else if ([self.topic isEqualToString:@"Khoa học - Công nghệ"]) {
                self.topicImage.image = [UIImage imageNamed:@"khoahoccongnghe.png"];
            }
            else {
                [self.topicImage setHidden:YES];
            }
            
            self.answerALabel.text = self.answerA;
            self.answerALabel.textColor = [UIColor blackColor];
            self.answerBLabel.text = self.answerB;
            self.answerBLabel.textColor = [UIColor blackColor];
            self.answerCLabel.text = self.answerC;
            self.answerCLabel.textColor = [UIColor blackColor];
            self.answerDLabel.text = self.answerD;
            self.answerDLabel.textColor = [UIColor blackColor];
            self.scoreLabel.text = [NSString stringWithFormat:@"%lld", self.questionScore];

            isLoading = NO;
            isQuestionLoaded = YES;
            
            networkFailedCount = 0;
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            // Network problem
            ++networkFailedCount;
            if (isFailed == NO && networkFailedCount >= 5) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                               message:@"Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công."
                                                              delegate:self
                                                     cancelButtonTitle:nil
                                                     otherButtonTitles:@"OK",nil];
                alert.tag = 1;
                [alert show];
                alert = nil;

                isFailed = YES;
                
                networkFailedCount = 0;
            }
            
            isLoading = NO;
        }];
    }
}

- (void) loadState {
    if (![self.contestKey isEqualToString:@""]) {
        isLoading = YES;
        
        // Load question
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager setRequestSerializer:[TimeoutHTTPRequestSerializer serializer]];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        [manager GET:[NSString stringWithFormat:STATE_URL, self.contestKey] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
            self.contestState = [[JSON objectForKey:@"Status"] longValue];
            
            if (isJoined == NO) {
                // Load remain time if not join game
                self.remainTime = [[JSON objectForKey:@"RemainTime"] longValue];
                progressTime = self.remainTime * 10;
            }

            self.progressBarImage.frame = CGRectMake(self.progressBarImage.frame.origin.x, self.progressBarImage.frame.origin.y,
                                                     self.progressBarBackground.frame.size.width / (self.totalTime * 10.0) * progressTime,
                                                     self.progressBarImage.frame.size.height);

            [self processState];
            
            isLoading = NO;
            
            networkFailedCount = 0;
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            // Network problem
            ++networkFailedCount;
            if (isFailed == NO && networkFailedCount >= 5) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                               message:@"Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công."
                                                              delegate:self
                                                     cancelButtonTitle:nil
                                                     otherButtonTitles:@"OK",nil];
                alert.tag = 2;
                [alert show];
                alert = nil;
                
                isFailed = YES;
                
                networkFailedCount = 0;
            }
            
            isLoading = NO;
        }];
    }
}

- (void) update:(id) sender {
    if (isLoading == NO) {
        if (isQuestionLoaded == NO) {
            [self loadQuestion:self.questionNumber];
        }
        else if (self.remainTime == 0) {
            [self loadState];
        }
    }
}

- (void) updateState:(id) sender {
    if (isLoading == NO && self.remainTime == 0 && isJoined == YES) {
        [self loadState];
    }
}

- (void) progress:(id) sender {
    if (self.remainTime > 0) {
        if (updateTimer != nil && [updateTimer isValid]) {
            [updateTimer invalidate];
        }
        
        isJoined = YES;
        
        --progressTime;
        if (progressTime < 0) {
            progressTime = 0;
        }
        if (progressTime % 10 == 0) {
            --self.remainTime;
        }
        
        self.progressBarImage.frame = CGRectMake(self.progressBarImage.frame.origin.x, self.progressBarImage.frame.origin.y,
                                                 self.progressBarBackground.frame.size.width / (self.totalTime * 10.0) * progressTime,
                                                 self.progressBarImage.frame.size.height);
        
        [self processState];
    }
}

- (void) processState {
    if (self.contestState >= 10 && self.contestState < 10000) {
        // Question
        long status = self.contestState % 10;
        
        if (status < 2) {
            // Not start
            self.startAnswerTime = nil;
        }
        else if (status == 2) {
            if (self.remainTime == 0) {
                if (isLocked == NO) {
                    self.scoreLabel.text = @"0";
                    isLocked = YES;
                    [[SoundManager getCountDownAudio] stop];
                }
            }
            else {
                // Start counter
                if (self.startAnswerTime == nil) {
                    self.startAnswerTime = [NSDate date];
                    [[SoundManager getCountDownAudio] play];
                }
                
                //self.questionNumberLabel.text = [NSString stringWithFormat:@"%d", self.questionNumber];
                [self enableAnswers];
                
                if (self.answer == 1) {
                    self.answerALabel.textColor = [UIColor whiteColor];
                }
                else if (self.answer == 2) {
                    self.answerBLabel.textColor = [UIColor whiteColor];
                }
                else if (self.answer == 3) {
                    self.answerCLabel.textColor = [UIColor whiteColor];
                }
                else if (self.answer == 4) {
                    self.answerDLabel.textColor = [UIColor whiteColor];
                }
                
                if (isLocked == NO) {
                    self.answerTime = self.totalTime - self.remainTime;
                    self.score = self.questionScore / self.totalTime * self.remainTime;
                    self.scoreLabel.text = [NSString stringWithFormat:@"%lld", self.score];
                }
            }
        }
        else if (status == 3) {
            // Summary screen
            ResultViewController *resultView = [[ResultViewController alloc] initWithNibName:@"ResultViewController" bundle:nil];
            resultView.contestKey = self.contestKey;
            resultView.correctAnswer = self.correctAnswer;
            [self.navigationController pushViewController:resultView animated:YES];
        }
    }
    else if (self.contestState == 10400) {
        // Not start
        self.startAnswerTime = nil;
    }
    else if (self.contestState == 10401) {
        if (self.remainTime == 0) {
            if (isLocked == NO) {
                self.scoreLabel.text = @"0";
                isLocked = YES;
                [[SoundManager getCountDownAudio] stop];
            }
        }
        else {
            // Start counter
            if (self.startAnswerTime == nil) {
                self.startAnswerTime = [NSDate date];
            }
            
            //self.questionNumberLabel.text = [NSString stringWithFormat:@"%d", self.questionNumber];
            [self enableAnswers];

            if (self.answer == 1) {
                self.answerALabel.textColor = [UIColor whiteColor];
            }
            else if (self.answer == 2) {
                self.answerBLabel.textColor = [UIColor whiteColor];
            }
            else if (self.answer == 3) {
                self.answerCLabel.textColor = [UIColor whiteColor];
            }
            else if (self.answer == 4) {
                self.answerDLabel.textColor = [UIColor whiteColor];
            }
            
            if (isLocked == NO) {
                self.answerTime = self.totalTime - self.remainTime;
                self.score = self.questionScore / self.totalTime * self.remainTime;
                self.scoreLabel.text = [NSString stringWithFormat:@"%lld", self.score];
            }
        }
    }
    else if (self.contestState == 10402) {
        // Result screen
        ResultViewController *resultView = [[ResultViewController alloc] initWithNibName:@"ResultViewController" bundle:nil];
        resultView.contestKey = self.contestKey;
        resultView.correctAnswer = self.correctAnswer;
        [self.navigationController pushViewController:resultView animated:YES];
    }
    else if (self.contestState == 10000 || self.contestState == 10100 || self.contestState == 10200) {
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        int quesNum = [[defaults valueForKey:QUESTION_NUMBER_KEY] intValue];
        if (quesNum > 0 && quesNum % 4 == 0) {
            // Go to summary page
            SummaryViewController *viewController = [[SummaryViewController alloc] initWithNibName:@"SummaryViewController" bundle:nil];
            viewController.contestKey = self.contestKey;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else {
            // Info screen
            ContestInfoViewController *infoView = [[ContestInfoViewController alloc] initWithNibName:@"ContestInfoViewController" bundle:nil];
            infoView.contestKey = self.contestKey;
            [self.navigationController pushViewController:infoView animated:YES];
        }
    }
    else if (self.contestState == 10300) {
        SummaryViewController *summaryView = [[SummaryViewController alloc] initWithNibName:@"SummaryViewController" bundle:nil];
        summaryView.contestKey = self.contestKey;
        summaryView.showFull = YES;
        [self.navigationController pushViewController:summaryView animated:YES];
    }
}

- (void) disableAnswers {
    [self.answerAButton setEnabled:NO];
    [self.answerBButton setEnabled:NO];
    [self.answerCButton setEnabled:NO];
    [self.answerDButton setEnabled:NO];
}

- (void) enableAnswers {
    [self.answerAButton setEnabled:YES];
    [self.answerBButton setEnabled:YES];
    [self.answerCButton setEnabled:YES];
    [self.answerDButton setEnabled:YES];
}

- (IBAction)answerATouched:(id)sender {
    if (isLocked == YES) {
        return;
    }

    [[SoundManager getButtonAudio] play];

    [self.answerAButton setSelected:YES];
    self.answerALabel.textColor = [UIColor whiteColor];
    isLocked = YES;
    self.answer = 1;
    self.correctAnswer = self.answerALabel.text;
    isSubmittingAnswer = YES;
    [self submitAnswer];
}

- (IBAction)answerBTouched:(id)sender {
    if (isLocked == YES) {
        return;
    }

    [[SoundManager getButtonAudio] play];

    [self.answerBButton setSelected:YES];
    self.answerBLabel.textColor = [UIColor whiteColor];
    isLocked = YES;
    self.answer = 2;
    self.correctAnswer = self.answerBLabel.text;
    isSubmittingAnswer = YES;
    [self submitAnswer];
}

- (IBAction)answerCTouched:(id)sender {
    if (isLocked == YES) {
        return;
    }

    [[SoundManager getButtonAudio] play];

    [self.answerCButton setSelected:YES];
    self.answerCLabel.textColor = [UIColor whiteColor];
    isLocked = YES;
    self.answer = 3;
    self.correctAnswer = self.answerCLabel.text;
    isSubmittingAnswer = YES;
    [self submitAnswer];
}

- (IBAction)answerDTouched:(id)sender {
    if (isLocked == YES) {
        return;
    }

    [[SoundManager getButtonAudio] play];

    [self.answerDButton setSelected:YES];
    self.answerDLabel.textColor = [UIColor whiteColor];
    isLocked = YES;
    self.answer = 4;
    self.correctAnswer = self.answerDLabel.text;
    isSubmittingAnswer = YES;
    [self submitAnswer];
}

- (void) submitAnswer {
    if (self.startAnswerTime == nil) {
        isSubmittingAnswer = NO;
        return;
    }
    
    if (isSubmittingAnswer == NO) {
        return;
    }
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    long long userID = [[defaults objectForKey:USERID_KEY] longLongValue];

    long long startTime = (long long)([self.startAnswerTime timeIntervalSince1970] * 1000); // Milliseconds
    long long endTime = (long long)([[NSDate date] timeIntervalSince1970] * 1000);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[TimeoutHTTPRequestSerializer serializer]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:[NSString stringWithFormat:ANSWER_URL, self.contestKey, userID, self.answer, startTime, endTime, self.answerTime, self.score] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
        int errorCode = [[JSON objectForKey:@"ResultCode"] intValue];
        if (errorCode == 1) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Cuộc thi ngày hôm nay đã kết thúc. Hãy quay lại và tham gia vào ngày thi khác."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            alert.tag = 1;
            [alert show];
            alert = nil;
        }
        else if (errorCode == 2) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Câu hỏi chưa bắt đầu hoặc đã kết thúc. Hãy chờ câu hỏi bắt đầu hoặc chờ câu hỏi khác."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            alert.tag = 2;
            [alert show];
            alert = nil;
        }
        else if (errorCode == 3) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Bạn đã trả lời cho câu hỏi này rồi."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            alert.tag = 3;
            [alert show];
            alert = nil;
        }
        else {
            long long userScore = [[defaults objectForKey:USER_SCORE_KEY] longLongValue];
            long long userDayScore = [[defaults objectForKey:USER_DAY_SCORE_KEY] longLongValue];
            self.score = [[JSON objectForKey:@"Score"] longLongValue];
            userScore += self.score;
            userDayScore += self.score;
            [defaults setObject:[NSString stringWithFormat:@"%lld", userScore] forKey:USER_SCORE_KEY];
            [defaults setObject:[NSString stringWithFormat:@"%lld", userDayScore] forKey:USER_DAY_SCORE_KEY];
            [defaults synchronize];
        }
        
        isSubmittingAnswer = NO;
        
        networkFailedCount = 0;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // Network problem
        ++networkFailedCount;
        if (isFailed == NO && networkFailedCount >= 5) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            alert.tag = 4;
            [alert show];
            alert = nil;
            
            isFailed = YES;
            
            networkFailedCount = 0;
        }
        
        isSubmittingAnswer = NO;
        [self submitAnswer];
    }];
}

@end
