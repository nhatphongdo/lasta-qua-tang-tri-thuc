//
//  SettingsViewController.h
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 4/15/14.
//  Copyright (c) 2014 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"


@interface SettingsViewController : GAITrackedViewController<UITextFieldDelegate, UIGestureRecognizerDelegate>

@property (strong, nonatomic) IBOutlet UITextField *fullnameTextField;
@property (strong, nonatomic) IBOutlet UITextField *addressTextField;
@property (strong, nonatomic) IBOutlet UITextField *phoneTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UISwitch *optInSwitch;
@property (strong, nonatomic) IBOutlet UIButton *finishButton;

- (IBAction)finishButtonTouched:(id)sender;
- (IBAction)backButtonTouched:(id)sender;

@end
