//
//  SplashViewController.h
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 10/11/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"


@interface SplashViewController : GAITrackedViewController<UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UIButton *reconnectButton;

- (IBAction)reconnectTouched:(id)sender;

@end
