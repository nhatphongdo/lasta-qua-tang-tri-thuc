//
//  LeaderboardHeaderViewController.m
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 4/21/14.
//  Copyright (c) 2014 VietDev Corporation. All rights reserved.
//

#import "LeaderboardHeaderViewController.h"

@interface LeaderboardHeaderViewController ()

@end

@implementation LeaderboardHeaderViewController


- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) dealloc {
}

@end
