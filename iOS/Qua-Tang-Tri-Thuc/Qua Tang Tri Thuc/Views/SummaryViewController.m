//
//  SummaryViewController.m
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 10/11/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import "SummaryViewController.h"
#import "Constants.h"
#import <AFNetworking/AFNetworking.h>
#import "ContestInfoViewController.h"
#import "ResultViewController.h"
#import "QuestionViewController.h"
#import "QuestionInfoViewController.h"
#import "UserRowViewController.h"
#import "TimeoutHTTPRequestSerializer.h"
#import "SoundManager.h"


@interface SummaryViewController ()

@end

@implementation SummaryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        users = [[NSArray alloc] init];
        selectedRow = -1;
        self.showFull = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    long long userID = [[defaults objectForKey:USERID_KEY] longLongValue];
    long long userScore = [[defaults objectForKey:USER_SCORE_KEY] longLongValue];
    long long userDayScore = [[defaults objectForKey:USER_DAY_SCORE_KEY] longLongValue];
    long long position = [[defaults objectForKey:USER_RANK_KEY] longLongValue];
    long long dayPosition = [[defaults objectForKey:USER_DAY_RANK_KEY] longLongValue];
    
    self.userIDLabel.text = [NSString stringWithFormat:@"%lld", userID];
    self.totalScoreLabel.text = [NSString stringWithFormat:@"%lld", userScore];
    self.dayScoreLabel.text = [NSString stringWithFormat:@"%lld", userDayScore];
    self.positionLabel.text = position == 0 ? @"Chờ xử lý" : [NSString stringWithFormat:@"%lld", position];
    self.dayPositionLabel.text = dayPosition == 0 ? @"Chờ xử lý" : [NSString stringWithFormat:@"%lld", dayPosition];
    
    self.usersTableView.delegate = self;
    self.usersTableView.dataSource = self;
    
    NSArray *nib;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        nib = [[NSBundle mainBundle] loadNibNamed:@"LeaderboardHeaderViewController" owner:self options:nil];
    }
    else {
        nib = [[NSBundle mainBundle] loadNibNamed:@"LeaderboardHeaderViewController" owner:self options:nil];
    }
    [self.usersTableViewHeader addSubview:[nib objectAtIndex:0]];
    
    if (self.showFull == YES) {
        [self.backButton setHidden:NO];
        [self.searchTextField setHidden:NO];
        [self.searchButton setHidden:NO];
        
        [defaults removeObjectForKey:QUESTION_NUMBER_KEY];
        [defaults removeObjectForKey:NUMBER_QUESTIONS_KEY];
        [defaults synchronize];
    }
    
    [self getUserInfo:userID];
    
    [self top50DayTouched:self.top50DayButton];

    isFailed = NO;
    isLoading = NO;

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTouched:)];
    [self.view addGestureRecognizer:tap];
    tap = nil;
    
    networkFailedCount = 0;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.screenName = @"Leaderboard Screen";
}

- (void)keyboardWillHide:(NSNotification *)aNotification
{
    // the keyboard is hiding reset the table's height
    NSTimeInterval animationDuration = [[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect frame = self.view.frame;
    frame.origin.y += 160;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];
}

- (void)keyboardWillShow:(NSNotification *)aNotification
{
    // the keyboard is showing so resize the table's height
    NSTimeInterval animationDuration = [[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect frame = self.view.frame;
    frame.origin.y -= 160;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.searchTextField) {
        [self searchButtonTouched:nil];
    }
    return YES;
}

- (void)viewTouched:(id)sender {
    [self.view endEditing:YES];
}

- (void) loadLeaderboard {
    users = nil;
    [self.usersTableView reloadData];
    
    // Load leaderboard
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[TimeoutHTTPRequestSerializer serializer]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:[NSString stringWithFormat:TOP_USER_URL, 50] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
        
        users = (NSArray *)JSON;

        [self.usersTableView reloadData];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // Network problem
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                       message:@"Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại."
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        alert.tag = 1;
        [alert show];
        alert = nil;
    }];
}

- (void) loadDailyLeaderboard {
    users = nil;
    [self.usersTableView reloadData];

    // Load leaderboard
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[TimeoutHTTPRequestSerializer serializer]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:[NSString stringWithFormat:TOP_DAILY_USER_URL, 50] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
        
        users = (NSArray *)JSON;
        
        [self.usersTableView reloadData];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // Network problem
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                       message:@"Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại."
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        alert.tag = 1;
        [alert show];
        alert = nil;
    }];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (self.showFull == NO && updateTimer == nil) {
        updateTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(update:) userInfo:nil repeats:YES];
    }

    if (self.showFull == NO) {
        [[SoundManager getBackgroundAudio] play];
    }
    else {
        [[SoundManager getOpenningAudio] play];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (updateTimer != nil) {
        [updateTimer invalidate];
        updateTimer = nil;
    }
}

- (void) update:(id) sender {
    if (isLoading == NO) {
        [self loadState];
    }
}

- (void) loadState {
    if (self.contestKey != nil && ![self.contestKey isEqualToString:@""]) {
        isLoading = YES;
        
        // Load question
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager setRequestSerializer:[TimeoutHTTPRequestSerializer serializer]];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        [manager GET:[NSString stringWithFormat:STATE_URL, self.contestKey] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {

            self.contestState = [[JSON objectForKey:@"Status"] longValue];
            
            [self processState];
            isLoading = NO;
            
            networkFailedCount = 0;
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            // Network problem
            ++networkFailedCount;
            if (isFailed == NO && networkFailedCount >= 5) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                               message:@"Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công."
                                                              delegate:self
                                                     cancelButtonTitle:nil
                                                     otherButtonTitles:@"OK",nil];
                alert.tag = 2;
                [alert show];
                alert = nil;
                
                isFailed = YES;
                
                networkFailedCount = 0;
            }
            
            isLoading = NO;
        }];
    }
}

- (void) processState {
    if (self.contestState >= 10 && self.contestState < 10000) {
        if (self.contestState % 10 == 3) {
        }
        else if (self.contestState % 10 == 2) {
            // Go to question page
            NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
            [defaults setValue:[NSNumber numberWithInt:self.contestState / 10]  forKey:QUESTION_NUMBER_KEY];
            [defaults synchronize];

            QuestionViewController *viewController = [[QuestionViewController alloc] initWithNibName:@"QuestionViewController" bundle:nil];
            viewController.questionNumber = self.contestState / 10; // Divided by 10 to get the question number
            viewController.contestKey = self.contestKey;
            viewController.contestState = self.contestState;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else {
            // Go to question info page
            NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
            [defaults setValue:[NSNumber numberWithInt:self.contestState / 10]  forKey:QUESTION_NUMBER_KEY];
            [defaults synchronize];

            QuestionInfoViewController *viewController = [[QuestionInfoViewController alloc] initWithNibName:@"QuestionInfoViewController" bundle:nil];
            viewController.questionNumber = self.contestState / 10; // Divided by 10 to get the question number
            viewController.contestKey = self.contestKey;
            viewController.contestState = self.contestState;
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }
    else if (self.contestState == 10400) {
        // Go to question info page
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        
        QuestionInfoViewController *viewController = [[QuestionInfoViewController alloc] initWithNibName:@"QuestionInfoViewController" bundle:nil];
        viewController.questionNumber = [[defaults objectForKey:NUMBER_QUESTIONS_KEY] intValue] + 1; // Divided by 10 to get the question number
        viewController.contestKey = self.contestKey;
        viewController.contestState = self.contestState;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else if (self.contestState == 10401) {
        // Go to question page
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];

        QuestionViewController *viewController = [[QuestionViewController alloc] initWithNibName:@"QuestionViewController" bundle:nil];
        viewController.contestKey = self.contestKey;
        viewController.contestState = self.contestState;
        viewController.questionNumber = [[defaults objectForKey:NUMBER_QUESTIONS_KEY] intValue] + 1;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else if (self.contestState == 10000 || self.contestState == 10100 || self.contestState == 10200) {
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        int quesNum = [[defaults valueForKey:QUESTION_NUMBER_KEY] intValue];
        if (quesNum > 0 && quesNum % 4 == 0) {
        }
        else {
            // Go to info page
            ContestInfoViewController *viewController = [[ContestInfoViewController alloc] initWithNibName:@"ContestInfoViewController" bundle:nil];
            viewController.contestKey = self.contestKey;
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [users count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UserRowViewController *cell = (UserRowViewController *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        NSArray *nib;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            nib = [[NSBundle mainBundle] loadNibNamed:@"UserRowViewController" owner:self options:nil];
        }
        else {
            nib = [[NSBundle mainBundle] loadNibNamed:@"UserRowViewController" owner:self options:nil];
        }
        
        cell = [nib objectAtIndex:0];
        
        cell.frame = CGRectMake(0, 0, self.view.frame.size.width, cell.frame.size.height);

        NSDictionary *user = [users objectAtIndex:indexPath.row];
        [cell setNumber:indexPath.row + 1];
        [cell setID:[user objectForKey:@"ID"] == [NSNull null] ? 0 : [[user objectForKey:@"ID"] longLongValue]];
        [cell setScore:[user objectForKey:@"Score"] == [NSNull null] ? 0 : [[user objectForKey:@"Score"] longLongValue]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedRow = indexPath.row;
    [self.usersTableView reloadData];
}

- (IBAction)backButtonTouched:(id)sender {
    [[SoundManager getButtonAudio] play];
    [self.navigationController popToRootViewControllerAnimated:YES];
//    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)searchButtonTouched:(id)sender {
    if (self.searchTextField.text.length == 0) {
        return;
    }
    
    [self.searchButton setEnabled:NO];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[TimeoutHTTPRequestSerializer serializer]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:[NSString stringWithFormat:SEARCH_USER_URL, self.searchTextField.text] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
        
        users = (NSArray *)JSON;
        
        [self.usersTableView reloadData];
        
        [self.searchButton setEnabled:YES];
        [self.view endEditing:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // Network problem
        NSLog(@"%@", error);
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                       message:@"Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại."
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        alert.tag = 1;
        [alert show];
        alert = nil;

        [self.searchButton setEnabled:YES];
    }];
}

- (IBAction)top50DayTouched:(id)sender {
    [self.top50DayButton setSelected:YES];
    [self.top50TotalButton setSelected:NO];
    
    [self loadDailyLeaderboard];
}

- (IBAction)top50TotalTouched:(id)sender {
    [self.top50TotalButton setSelected:YES];
    [self.top50DayButton setSelected:NO];
    
    [self loadLeaderboard];
}

- (void) getUserInfo:(long long)userID {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[TimeoutHTTPRequestSerializer serializer]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:[NSString stringWithFormat:GET_USER_URL, userID] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
        
        NSLog(@"%@", JSON);
        
        [defaults setObject:([[JSON objectForKey:@"Score"] isKindOfClass:[NSNull class]] ? @"0" : [JSON objectForKey:@"Score"]) forKey:USER_SCORE_KEY];
        [defaults setObject:([[JSON objectForKey:@"DayScore"] isKindOfClass:[NSNull class]] ? @"0" : [JSON objectForKey:@"DayScore"]) forKey:USER_DAY_SCORE_KEY];
        [defaults setObject:([[JSON objectForKey:@"Rank"] isKindOfClass:[NSNull class]] ? @"" : [JSON objectForKey:@"Rank"]) forKey:USER_RANK_KEY];
        [defaults setObject:([[JSON objectForKey:@"DayRank"] isKindOfClass:[NSNull class]] ? @"" : [JSON objectForKey:@"DayRank"]) forKey:USER_DAY_RANK_KEY];
        [defaults synchronize];
        
        self.totalScoreLabel.text = ([[JSON objectForKey:@"Score"] isKindOfClass:[NSNull class]] ? @"0" : [NSString stringWithFormat:@"%lld", [[JSON objectForKey:@"Score"] longLongValue]]);
        self.dayScoreLabel.text = ([[JSON objectForKey:@"DayScore"] isKindOfClass:[NSNull class]] ? @"0" : [NSString stringWithFormat:@"%lld", [[JSON objectForKey:@"DayScore"] longLongValue]]);
        self.positionLabel.text = (([[JSON objectForKey:@"Rank"] isKindOfClass:[NSNull class]] || [[JSON objectForKey:@"Rank"] longLongValue] == 0) ? @"Chờ xử lý" : [NSString stringWithFormat:@"%lld", [[JSON objectForKey:@"Rank"] longLongValue]]);
        self.dayPositionLabel.text = (([[JSON objectForKey:@"DayRank"] isKindOfClass:[NSNull class]] || [[JSON objectForKey:@"DayRank"] longLongValue] == 0) ? @"Chờ xử lý" : [NSString stringWithFormat:@"%lld", [[JSON objectForKey:@"DayRank"] longLongValue]]);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@", error);
        if (operation.response.statusCode == 404) {
        }
        else {
            // Network problem
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            alert.tag = 3;
            [alert show];
            alert = nil;
        }
    }];
}

@end
