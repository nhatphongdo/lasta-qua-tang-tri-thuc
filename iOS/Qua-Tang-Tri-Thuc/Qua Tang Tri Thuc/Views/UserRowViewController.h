//
//  UserRowViewController.h
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 1/9/14.
//  Copyright (c) 2014 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserRowViewController : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *backgroundImage;

@property (strong, nonatomic) IBOutlet UILabel *char01;
@property (strong, nonatomic) IBOutlet UILabel *char02;
@property (strong, nonatomic) IBOutlet UILabel *char03;
@property (strong, nonatomic) IBOutlet UILabel *char04;
@property (strong, nonatomic) IBOutlet UILabel *char05;
@property (strong, nonatomic) IBOutlet UILabel *char06;
@property (strong, nonatomic) IBOutlet UILabel *char07;
@property (strong, nonatomic) IBOutlet UILabel *char08;
@property (strong, nonatomic) IBOutlet UILabel *char09;
@property (strong, nonatomic) IBOutlet UILabel *char10;
@property (strong, nonatomic) IBOutlet UILabel *char11;
@property (strong, nonatomic) IBOutlet UILabel *char12;
@property (strong, nonatomic) IBOutlet UILabel *char13;
@property (strong, nonatomic) IBOutlet UILabel *char14;
@property (strong, nonatomic) IBOutlet UILabel *char15;
@property (strong, nonatomic) IBOutlet UILabel *char16;
@property (strong, nonatomic) IBOutlet UILabel *char17;
@property (strong, nonatomic) IBOutlet UILabel *char18;
@property (strong, nonatomic) IBOutlet UILabel *char19;
@property (strong, nonatomic) IBOutlet UILabel *char20;
@property (strong, nonatomic) IBOutlet UILabel *char21;
@property (strong, nonatomic) IBOutlet UILabel *char22;
@property (strong, nonatomic) IBOutlet UILabel *char23;
@property (strong, nonatomic) IBOutlet UILabel *char24;

- (void) setNumber:(int)number;
- (void) setID:(long long)idNumber;
- (void) setDayScore:(long long)score;
- (void) setScore:(long long)score;

@end
