//
//  QuestionInfoViewController.h
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 12/2/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"


@interface QuestionInfoViewController : GAITrackedViewController {
    NSTimer *updateTimer;
    BOOL isLoading;
    BOOL isFailed;
    BOOL isQuestionLoaded;
    
    BOOL isLoadedTopic;
    BOOL isLoadedScore;
    
    int networkFailedCount;
    
    NSString *answerA;
    NSString *answerB;
    NSString *answerC;
    NSString *answerD;
    int totalTime;
}

@property (nonatomic) long long questionScore;
@property (nonatomic) int questionNumber;
@property (nonatomic) long contestState;
@property (nonatomic, strong) NSString *contestKey;
@property (nonatomic, strong) NSString *topic;

@property (strong, nonatomic) IBOutlet UILabel *userIDLabel;
@property (strong, nonatomic) IBOutlet UILabel *dayScoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *totalScoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *topicTextLabel;
@property (strong, nonatomic) IBOutlet UILabel *scoreLabel;
@property (strong, nonatomic) IBOutlet UIImageView *topicImageView;

@end
