//
//  MenuViewController.h
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 10/11/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"


@interface MenuViewController : GAITrackedViewController

- (IBAction)startButtonTouched:(id)sender;
- (IBAction)leaderboardButtonTouched:(id)sender;
- (IBAction)termButtonTouched:(id)sender;
- (IBAction)typhuKhongDoButtonTouched:(id)sender;
- (IBAction)sendScoreButtonTouched:(id)sender;
- (IBAction)shareButtonTouched:(id)sender;
- (IBAction)settingButtonTouched:(id)sender;

@end
