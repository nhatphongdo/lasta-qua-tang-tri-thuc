//
//  ShareViewController.m
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 4/15/14.
//  Copyright (c) 2014 VietDev Corporation. All rights reserved.
//

#import "ShareViewController.h"
#import <Social/Social.h>
#import "Constants.h"
#import "SoundManager.h"


@interface ShareViewController ()

@end

@implementation ShareViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.screenName = @"Sharing Screen";
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[SoundManager getOpenningAudio] play];
}

- (IBAction)backButtonTouched:(id)sender {
    [[SoundManager getButtonAudio] play];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)shareButtonTouched:(id)sender {
    [[SoundManager getButtonAudio] play];
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];

        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        long long userScore = [[defaults objectForKey:USER_SCORE_KEY] longLongValue];

        [controller setInitialText:[NSString stringWithFormat:@"Mình vừa giành được %lld điểm trong GameShow truyền hình trực tiếp \"Cùng Là Tỷ Phú\" trên kênh truyền hình Let's Việt. Các bạn và mình chúng ta cùng nhau tham gia nhé!", userScore]];
        [controller addURL:[NSURL URLWithString:@"http://cunglatyphu.com"]];
        
        [self presentViewController:controller animated:YES completion:Nil];
    }
}

- (IBAction)facebookButtonTouched:(id)sender {
    [[SoundManager getButtonAudio] play];
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        [controller setInitialText:@"\"Cùng Là Tỷ Phú\" sau 3 tháng, tham gia ngay!"];
        [controller addURL:[NSURL URLWithString:@"http://cunglatyphu.com"]];
        
        [self presentViewController:controller animated:YES completion:Nil];
    }
}

@end
