//
//  MenuViewController.m
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 10/11/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import "MenuViewController.h"
#import "QRViewController.h"
#import "SummaryViewController.h"
#import "ExchangeViewController.h"
#import "TermViewController.h"
#import "SettingsViewController.h"
#import "TransferViewController.h"
#import "ShareViewController.h"
#import "Constants.h"
#import "SoundManager.h"


@interface MenuViewController ()

@end

@implementation MenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[SoundManager getOpenningAudio] play];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.screenName = @"Menu Screen";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)startButtonTouched:(id)sender {
    QRViewController *qrViewController = [[QRViewController alloc] initWithNibName:@"QRViewController" bundle:nil];
    [self.navigationController pushViewController:qrViewController animated:YES];
    [[SoundManager getButtonAudio] play];
}

- (IBAction)leaderboardButtonTouched:(id)sender {
    SummaryViewController *summaryViewController = [[SummaryViewController alloc] initWithNibName:@"SummaryViewController" bundle:nil];
    summaryViewController.showFull = YES;
    [self.navigationController pushViewController:summaryViewController animated:YES];
    [[SoundManager getButtonAudio] play];
}

- (IBAction)termButtonTouched:(id)sender {
    TermViewController *viewController = [[TermViewController alloc] initWithNibName:@"TermViewController" bundle:nil];
    [self.navigationController pushViewController:viewController animated:YES];
    [[SoundManager getButtonAudio] play];
}

- (IBAction)typhuKhongDoButtonTouched:(id)sender {
#if STUDIO_VERSION
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                   message:@"Chức năng này chỉ dành riêng cho phiên bản người chơi ở nhà."
                                                  delegate:self
                                         cancelButtonTitle:nil
                                         otherButtonTitles:@"OK",nil];
    [alert show];
    alert = nil;
#else
    ExchangeViewController *viewController = [[ExchangeViewController alloc] initWithNibName:@"ExchangeViewController" bundle:nil];
    [self.navigationController pushViewController:viewController animated:YES];
#endif
    [[SoundManager getButtonAudio] play];
}

- (IBAction)sendScoreButtonTouched:(id)sender {
#if STUDIO_VERSION
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                   message:@"Chức năng này chỉ dành riêng cho phiên bản người chơi ở nhà."
                                                  delegate:self
                                         cancelButtonTitle:nil
                                         otherButtonTitles:@"OK",nil];
    [alert show];
    alert = nil;
#else
    TransferViewController *viewController = [[TransferViewController alloc] initWithNibName:@"TransferViewController" bundle:nil];
    [self.navigationController pushViewController:viewController animated:YES];
#endif
    [[SoundManager getButtonAudio] play];
}

- (IBAction)shareButtonTouched:(id)sender {
#if STUDIO_VERSION
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                   message:@"Chức năng này chỉ dành riêng cho phiên bản người chơi ở nhà."
                                                  delegate:self
                                         cancelButtonTitle:nil
                                         otherButtonTitles:@"OK",nil];
    [alert show];
    alert = nil;
#else
    ShareViewController *viewController = [[ShareViewController alloc] initWithNibName:@"ShareViewController" bundle:nil];
    [self.navigationController pushViewController:viewController animated:YES];
#endif
    [[SoundManager getButtonAudio] play];
}

- (IBAction)settingButtonTouched:(id)sender {
    SettingsViewController *viewController = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];
    [self.navigationController pushViewController:viewController animated:YES];
    [[SoundManager getButtonAudio] play];
}

@end
