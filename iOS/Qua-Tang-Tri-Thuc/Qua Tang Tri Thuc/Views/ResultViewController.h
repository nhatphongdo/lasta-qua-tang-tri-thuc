//
//  ResultViewController.h
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 10/11/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"


@interface ResultViewController : GAITrackedViewController {
    NSTimer *updateTimer;
    BOOL isFailed;
    BOOL isLoading;
    BOOL isCheckingAnswer;
    
    long long userID;
    int networkFailedCount;
}

@property (nonatomic) long contestState;
@property (nonatomic, strong) NSString *contestKey;
@property (nonatomic, strong) NSString *correctAnswer;

@property (strong, nonatomic) IBOutlet UIImageView *textTitle;
@property (strong, nonatomic) IBOutlet UIImageView *rightImage;
@property (strong, nonatomic) IBOutlet UIImageView *smileIcon;
@property (strong, nonatomic) IBOutlet UILabel *currentScoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *answeredLabel;
@property (strong, nonatomic) IBOutlet UIImageView *textCongras;

@end
