//
//  SplashViewController.m
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 10/11/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import "SplashViewController.h"
#import "Constants.h"
#import "BPXLUUIDHandler.h"
#import <AFNetworking/AFNetworking.h>
#import "MenuViewController.h"
#import "TimeoutHTTPRequestSerializer.h"


@interface SplashViewController ()

@end

@implementation SplashViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:QUESTION_NUMBER_KEY];
    [defaults removeObjectForKey:NUMBER_QUESTIONS_KEY];
    [defaults synchronize];

    // Check version
    [self checkVersion];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.screenName = @"Splash Screen";
}

- (void) checkVersion {
    [self.reconnectButton setHidden:YES];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[TimeoutHTTPRequestSerializer serializer]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:CHECK_VERSION_URL parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
        NSString *currentVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
        
#ifdef STUDIO_VERSION
        [self registerUser];
#else
        if (![[JSON objectForKey:@"Value"] isEqualToString:currentVersion]) {
            // Exist user with the same device
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Bạn đang sử dụng phiên bản cũ của chương trình. Xin vui lòng cập nhật phiên bản mới trên kho ứng dụng để có thể tham gia cuộc thi."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            alert.tag = 3;
            [alert show];
            alert = nil;
        }
        else {
            [self registerUser];
        }
#endif
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // Network problem
        NSLog(@"%@", error);
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                       message:@"Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại."
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        alert.tag = 3;
        [alert show];
        alert = nil;
        
        [self.reconnectButton setHidden:NO];
    }];
}

- (void)registerUser {
    [self.reconnectButton setHidden:YES];
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    
    // Register user
    NSString *deviceID = [BPXLUUIDHandler UUID];
    [defaults setObject:deviceID forKey:DEVICEID_KEY];
    [defaults synchronize];
    
    long long userID = [[defaults objectForKey:USERID_KEY] longLongValue];
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[TimeoutHTTPRequestSerializer serializer]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:[NSString stringWithFormat:REGISTER_USER_URL, deviceID] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
        if ([[JSON objectForKey:@"Error"] intValue] == -1 && [[JSON objectForKey:@"ID"] longLongValue] != userID) {
            // Exist user with the same device
            [defaults setObject:[JSON objectForKey:@"ID"] forKey:USERID_KEY];
            [defaults synchronize];
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:[NSString stringWithFormat:@"Thiết bị đã được đăng ký trước đây với mã số '%@'. Chương trình sẽ tự động kết nối lại với mã số này.", [JSON objectForKey:@"ID"]]
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            alert.tag = 1;
            [alert show];
            alert = nil;
        }
        else if ([[JSON objectForKey:@"Error"] intValue] == -2) {
            // Database error
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Chương trình xảy ra lỗi trong quá trình xử lý. Vui lòng thử lại hoặc thông báo tới BTC để được hướng dẫn xử lý và hỗ trợ."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            alert.tag = 2;
            [alert show];
            alert = nil;
        }
        else {
            [defaults setObject:[JSON objectForKey:@"ID"] forKey:USERID_KEY];
            [defaults synchronize];
            
            [self getUserInfo:[[JSON objectForKey:@"ID"] longLongValue]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // Network problem
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                       message:@"Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại."
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        alert.tag = 3;
        [alert show];
        alert = nil;
        
        [self.reconnectButton setHidden:NO];
    }];
}

- (void) getUserInfo:(long long)userID {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[TimeoutHTTPRequestSerializer serializer]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:[NSString stringWithFormat:GET_USER_URL, userID] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
        
        NSLog(@"%@", JSON);
        
        [defaults setObject:([[JSON objectForKey:@"Fullname"] isKindOfClass:[NSNull class]] ? @"" : [JSON objectForKey:@"Fullname"]) forKey:USER_FULLNAME_KEY];
        [defaults setObject:([[JSON objectForKey:@"Email"] isKindOfClass:[NSNull class]] ? @"" : [JSON objectForKey:@"Email"]) forKey:USER_EMAIL_KEY];
        [defaults setObject:([[JSON objectForKey:@"PhoneNumber"] isKindOfClass:[NSNull class]] ? @"" : [JSON objectForKey:@"PhoneNumber"]) forKey:USER_PHONE_NUMBER_KEY];
        [defaults setObject:([[JSON objectForKey:@"Address"] isKindOfClass:[NSNull class]] ? @"" : [JSON objectForKey:@"Address"]) forKey:USER_ADDRESS_KEY];
        [defaults setObject:([[JSON objectForKey:@"Score"] isKindOfClass:[NSNull class]] ? @"0" : [JSON objectForKey:@"Score"]) forKey:USER_SCORE_KEY];
        [defaults setObject:([[JSON objectForKey:@"DayScore"] isKindOfClass:[NSNull class]] ? @"0" : [JSON objectForKey:@"DayScore"]) forKey:USER_DAY_SCORE_KEY];
        [defaults setObject:([[JSON objectForKey:@"Rank"] isKindOfClass:[NSNull class]] ? @"" : [JSON objectForKey:@"Rank"]) forKey:USER_RANK_KEY];
        [defaults setObject:([[JSON objectForKey:@"DayRank"] isKindOfClass:[NSNull class]] ? @"" : [JSON objectForKey:@"DayRank"]) forKey:USER_DAY_RANK_KEY];
        [defaults setObject:([[JSON objectForKey:@"IsOptIn"] isKindOfClass:[NSNull class]] ? [NSNumber numberWithBool:YES] : [NSNumber numberWithBool:[[JSON objectForKey:@"IsOptIn"] boolValue]]) forKey:USER_OPT_IN_KEY];
        [defaults removeObjectForKey:QUESTION_NUMBER_KEY];
        [defaults removeObjectForKey:NUMBER_QUESTIONS_KEY];
        [defaults synchronize];
        
#ifdef STUDIO_VERSION
#else
        [self registerToken];
#endif
        
        MenuViewController *menuViewController = [[MenuViewController alloc] initWithNibName:@"MenuViewController" bundle:nil];
        [self.navigationController setViewControllers:@[menuViewController] animated:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@", error);
        if (operation.response.statusCode == 404) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Tài khoản này đã bị vô hiệu hóa. Vui lòng đăng ký lại tài khoản mới hoặc liên hệ với BTC để được hỗ trợ. CHÚ Ý: tài khoản mới sẽ không còn lưu trữ các thông tin cá nhân và điểm số hiện tại!"
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            alert.tag = 3;
            [alert show];
            alert = nil;
            
            [defaults removeObjectForKey:USERID_KEY];
            [defaults removeObjectForKey:USER_FULLNAME_KEY];
            [defaults removeObjectForKey:USER_EMAIL_KEY];
            [defaults removeObjectForKey:USER_PHONE_NUMBER_KEY];
            [defaults removeObjectForKey:USER_ADDRESS_KEY];
            [defaults removeObjectForKey:USER_OPT_IN_KEY];
            [defaults removeObjectForKey:USER_SCORE_KEY];
            [defaults removeObjectForKey:USER_DAY_SCORE_KEY];
            [defaults removeObjectForKey:USER_RANK_KEY];
            [defaults removeObjectForKey:USER_DAY_RANK_KEY];
            [defaults removeObjectForKey:QUESTION_NUMBER_KEY];
            [defaults removeObjectForKey:NUMBER_QUESTIONS_KEY];
            
            [defaults synchronize];
        }
        else {
            // Network problem
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            alert.tag = 3;
            [alert show];
            alert = nil;
        }
        
        [self.reconnectButton setHidden:NO];
    }];
}

- (void) registerToken {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *deviceID = [defaults objectForKey:DEVICEID_KEY];
    NSString *token = [defaults objectForKey:TOKEN_KEY];
    long long userID = [[defaults objectForKey:USERID_KEY] longLongValue];
    
    if (deviceID.length == 0 || token.length == 0 || userID == 0) {
        return;
    }
    
    NSLog(@"%@", [NSString stringWithFormat:SET_TOKEN_URL, deviceID, userID, token]);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[TimeoutHTTPRequestSerializer serializer]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:[NSString stringWithFormat:SET_TOKEN_URL, deviceID, userID, token] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // Network problem
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                       message:@"Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại."
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        alert.tag = 3;
        [alert show];
        alert = nil;
    }];
}

- (IBAction)reconnectTouched:(id)sender {
    [self checkVersion];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"OK"]) {
            NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
            [self getUserInfo:[[defaults objectForKey:USERID_KEY] longLongValue]];
        }
    }
}

@end
