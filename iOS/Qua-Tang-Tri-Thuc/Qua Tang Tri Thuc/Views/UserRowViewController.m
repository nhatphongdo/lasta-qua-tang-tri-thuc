//
//  UserRowViewController.m
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 1/9/14.
//  Copyright (c) 2014 VietDev Corporation. All rights reserved.
//

#import "UserRowViewController.h"

@interface UserRowViewController ()

@end

@implementation UserRowViewController

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) dealloc {
}

- (void) setNumber:(int)number {
    NSString *text = [NSString stringWithFormat:@"%02d", number];
    self.char01.text = [text substringWithRange:NSMakeRange(text.length - 2, 1)];
    self.char02.text = [text substringWithRange:NSMakeRange(text.length - 1, 1)];
}

- (void) setID:(long long)idNumber {
    NSString *text = [NSString stringWithFormat:@"%6lld", idNumber];
    self.char04.text = [text substringWithRange:NSMakeRange(text.length - 6, 1)];
    self.char05.text = [text substringWithRange:NSMakeRange(text.length - 5, 1)];
    self.char06.text = [text substringWithRange:NSMakeRange(text.length - 4, 1)];
    self.char07.text = [text substringWithRange:NSMakeRange(text.length - 3, 1)];
    self.char08.text = [text substringWithRange:NSMakeRange(text.length - 2, 1)];
    self.char09.text = [text substringWithRange:NSMakeRange(text.length - 1, 1)];
}

- (void) setDayScore:(long long)score {
    NSString *text = [NSString stringWithFormat:@"%5lld", score];
    self.char11.text = [text substringWithRange:NSMakeRange(text.length - 5, 1)];
    self.char12.text = [text substringWithRange:NSMakeRange(text.length - 4, 1)];
    self.char13.text = [text substringWithRange:NSMakeRange(text.length - 3, 1)];
    self.char14.text = [text substringWithRange:NSMakeRange(text.length - 2, 1)];
    self.char15.text = [text substringWithRange:NSMakeRange(text.length - 1, 1)];
}

- (void) setScore:(long long)score {
    NSString *text = [NSString stringWithFormat:@"%6lld", score];
    self.char17.text = [text substringWithRange:NSMakeRange(text.length - 6, 1)];
    self.char18.text = [text substringWithRange:NSMakeRange(text.length - 5, 1)];
    self.char19.text = [text substringWithRange:NSMakeRange(text.length - 4, 1)];
    self.char20.text = [text substringWithRange:NSMakeRange(text.length - 3, 1)];
    self.char21.text = [text substringWithRange:NSMakeRange(text.length - 2, 1)];
    self.char22.text = [text substringWithRange:NSMakeRange(text.length - 1, 1)];
}

@end
