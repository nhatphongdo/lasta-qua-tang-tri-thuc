//
//  TransferViewController.m
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 4/15/14.
//  Copyright (c) 2014 VietDev Corporation. All rights reserved.
//

#import "TransferViewController.h"
#import <AFNetworking/AFNetworking.h>
#import "Constants.h"
#import "BPXLUUIDHandler.h"
#import "TimeoutHTTPRequestSerializer.h"
#import "SoundManager.h"


@interface TransferViewController ()

@end

@implementation TransferViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    long long userID = [[defaults objectForKey:USERID_KEY] longLongValue];
    long long userScore = [[defaults objectForKey:USER_SCORE_KEY] longLongValue];
    long long position = [[defaults objectForKey:USER_RANK_KEY] longLongValue];
    
    self.userIDLabel.text = [NSString stringWithFormat:@"%lld", userID];
    self.totalScoreLabel.text = [NSString stringWithFormat:@"%lld", userScore];
    self.positionLabel.text = [NSString stringWithFormat:@"%lld", position];
    
    [self.searchIDTextField setDelegate:self];
    [self.transferScoreTextField setDelegate:self];

    NSURLRequest *urlReq = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"tangdiem" ofType:@"html"]]];
    [self.contentWebView loadRequest:urlReq];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTouched:)];
    [self.view addGestureRecognizer:tap];
    tap = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.screenName = @"Transfer Score Screen";
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[SoundManager getOpenningAudio] play];
}

- (void)keyboardWillHide:(NSNotification *)aNotification
{
    // the keyboard is hiding reset the table's height
    NSTimeInterval animationDuration = [[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect frame = self.view.frame;
    frame.origin.y += 160;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];
}

- (void)keyboardWillShow:(NSNotification *)aNotification
{
    // the keyboard is showing so resize the table's height
    NSTimeInterval animationDuration = [[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect frame = self.view.frame;
    frame.origin.y -= 160;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.searchIDTextField) {
        [self searchUser];
    }
    else if (textField == self.transferScoreTextField) {
        [self transferScoreButtonTouched:nil];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == self.searchIDTextField) {
        [self searchUser];
    }
}

- (void)viewTouched:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)transferScoreButtonTouched:(id)sender {
    [[SoundManager getButtonAudio] play];
    
    long long receiverID = [self.searchIDTextField.text longLongValue];
    long long score = [self.transferScoreTextField.text longLongValue];
    
    if (receiverID == 0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                       message:@"Vui lòng nhập mã số ID của người được nhận điểm."
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
        
        [self.searchIDTextField becomeFirstResponder];
        return;
    }
    
    if (score == 0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                       message:@"Vui lòng nhập số điểm muốn chuyển cho người nhận."
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
        
        [self.transferScoreTextField becomeFirstResponder];
        return;
    }
    
    [self.submitButton setEnabled:NO];
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    long long userID = [[defaults objectForKey:USERID_KEY] longLongValue];
    
    NSString *deviceID = [BPXLUUIDHandler UUID];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[TimeoutHTTPRequestSerializer serializer]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:[NSString stringWithFormat:TRANSFER_SCORE_URL, userID, deviceID, receiverID, score] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
        
        NSLog(@"%@", JSON);
        
        if ([[JSON objectForKey:@"Error"] intValue] == -1) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Chưa đến thời gian cho phép tặng điểm. Thời gian tặng điểm diễn ra từ đầu chương trình 23 đến giữa chương trình 24."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            [alert show];
            alert = nil;
        }
        else if ([[JSON objectForKey:@"Error"] intValue] == 1) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Bạn đã tặng điểm cho người khác rồi. Bạn không thể thực hiện tặng điểm quá 1 lần."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            [alert show];
            alert = nil;
        }
        else if ([[JSON objectForKey:@"Error"] intValue] == 2) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Người nhận điểm tặng đã nhận đủ tối đa 5 lần tặng."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            [alert show];
            alert = nil;
        }
        else if ([[JSON objectForKey:@"Error"] intValue] == 3) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Chương trình xảy ra lỗi trong quá trình xử lý. Vui lòng thử lại hoặc thông báo tới BTC để được hướng dẫn xử lý và hỗ trợ."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            [alert show];
            alert = nil;            
        }
        else if ([[JSON objectForKey:@"Error"] intValue] == 4) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Hiện tại bạn không có điểm để tặng cho người nhận."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            [alert show];
            alert = nil;
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Bạn đã thực hiện việc tặng điểm cho người nhận thành công."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            [alert show];
            alert = nil;
            
            [self.view endEditing:YES];
        }
        
        [self.submitButton setEnabled:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@", error);
        // Network problem
        if (operation.response.statusCode == 404) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Không tìm thấy người nhận điểm hoặc thông tin bạn cung cấp không chính xác. Vui lòng kiểm tra thông tin và thử lại."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            [alert show];
            alert = nil;
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            [alert show];
            alert = nil;
        }
        
        [self.submitButton setEnabled:YES];
    }];
}

- (void) searchUser {
    long long userID = [self.searchIDTextField.text longLongValue];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[TimeoutHTTPRequestSerializer serializer]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:[NSString stringWithFormat:GET_USER_URL, userID] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
        
        NSLog(@"%@", JSON);
        
        [self.searchIDLabel setText:[NSString stringWithFormat:@"%lld", ([[JSON objectForKey:@"ID"] isKindOfClass:[NSNull class]] ? 0 : [[JSON objectForKey:@"ID"] longLongValue])]];
        [self.searchFullnameLabel setText:([[JSON objectForKey:@"Fullname"] isKindOfClass:[NSNull class]] ? @"" : [JSON objectForKey:@"Fullname"])];
        [self.searchScoreLabel setText:[NSString stringWithFormat:@"%lld", ([[JSON objectForKey:@"Score"] isKindOfClass:[NSNull class]] ? 0 : [[JSON objectForKey:@"Score"] longLongValue])]];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@", error);
        // Network problem
        if (operation.response.statusCode == 404) {
            [self.searchIDLabel setText:@""];
            [self.searchFullnameLabel setText:@""];
            [self.searchScoreLabel setText:@""];

            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:[NSString stringWithFormat:@"Không tìm thấy người chơi với mã số ID '%lld'.", userID]
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            [alert show];
            alert = nil;
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            [alert show];
            alert = nil;
        }
    }];
}

- (IBAction)joinButtonTouched:(id)sender {
    [[SoundManager getButtonAudio] play];
    [self.contentView setHidden:NO];
    [self.infoView setHidden:YES];
}

- (IBAction)backButtonTouched:(id)sender {
    [[SoundManager getButtonAudio] play];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
