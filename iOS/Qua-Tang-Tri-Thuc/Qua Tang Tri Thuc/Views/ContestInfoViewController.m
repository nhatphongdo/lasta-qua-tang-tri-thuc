//
//  ContestInfoViewController.m
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 10/11/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import "ContestInfoViewController.h"
#import "Constants.h"
#import <AFNetworking/AFNetworking.h>
#import "QuestionViewController.h"
#import "QuestionInfoViewController.h"
#import "ResultViewController.h"
#import "SummaryViewController.h"
#import "TimeoutHTTPRequestSerializer.h"
#import "SoundManager.h"


@interface ContestInfoViewController ()

@end

@implementation ContestInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    long long userID = [[defaults objectForKey:USERID_KEY] longLongValue];
    long long userScore = [[defaults objectForKey:USER_SCORE_KEY] longLongValue];
    long long userDayScore = [[defaults objectForKey:USER_DAY_SCORE_KEY] longLongValue];
    
    self.userIDLabel.text = [NSString stringWithFormat:@"%lld", userID];
    self.dayScoreLabel.text = [NSString stringWithFormat:@"%lld", userDayScore];
    self.totalScoreLabel.text = [NSString stringWithFormat:@"%lld", userScore];
    
    UISwipeGestureRecognizer *oneFingerSwipeLeft = [[UISwipeGestureRecognizer alloc]
                                                    initWithTarget:self
                                                    action:@selector(oneFingerSwipeLeft:)];
    [oneFingerSwipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.prizeContainerView addGestureRecognizer:oneFingerSwipeLeft];
    
    UISwipeGestureRecognizer *oneFingerSwipeRight = [[UISwipeGestureRecognizer alloc]
                                                     initWithTarget:self
                                                     action:@selector(oneFingerSwipeRight:)];
    [oneFingerSwipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.prizeContainerView addGestureRecognizer:oneFingerSwipeRight];
    
    isFailed = NO;
    isLoading = NO;
    direction = 1;
    
    networkFailedCount = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.screenName = @"Contest Info Screen";
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    [[SoundManager getBackgroundAudio] play];

    if (updateTimer == nil) {
        updateTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(update:) userInfo:nil repeats:YES];
    }
    if (slideTimer == nil) {
        slideTimer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(slide:) userInfo:nil repeats:YES];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (updateTimer != nil) {
        [updateTimer invalidate];
        updateTimer = nil;
    }
    
    if (slideTimer != nil) {
        [slideTimer invalidate];
        slideTimer = nil;
    }
}

- (void)oneFingerSwipeLeft:(UITapGestureRecognizer *)recognizer {
    // Insert your own code to handle swipe left
    // Move images to left
    if (self.pageView.currentPage < 6) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            [self.prizeView setFrame:CGRectMake(self.prizeView.frame.origin.x - 480, 0, self.prizeView.frame.size.width, self.prizeView.frame.size.height)];
        }
        else {
            [self.prizeView setFrame:CGRectMake(self.prizeView.frame.origin.x - 240, 0, self.prizeView.frame.size.width, self.prizeView.frame.size.height)];
        }
        
        [UIView commitAnimations];
        
        [self.pageView setCurrentPage:self.pageView.currentPage + 1];
    }
}

- (void)oneFingerSwipeRight:(UITapGestureRecognizer *)recognizer {
    // Insert your own code to handle swipe right
    // Move images to right
    if (self.pageView.currentPage > 0) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];

        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            [self.prizeView setFrame:CGRectMake(self.prizeView.frame.origin.x + 480, 0, self.prizeView.frame.size.width, self.prizeView.frame.size.height)];
        }
        else {
            [self.prizeView setFrame:CGRectMake(self.prizeView.frame.origin.x + 240, 0, self.prizeView.frame.size.width, self.prizeView.frame.size.height)];
        }
        
        [UIView commitAnimations];
        
        [self.pageView setCurrentPage:self.pageView.currentPage - 1];
    }
}

- (void) slide:(id) sender {
    if (self.pageView.currentPage == 6) {
        direction = 0;
    }
    else if (self.pageView.currentPage == 0) {
        direction = 1;
    }
    
    if (direction == 1) {
        // Forward
        [self oneFingerSwipeLeft:nil];
    }
    else {
        [self oneFingerSwipeRight:nil];
    }
}

- (void) update:(id) sender {
    if (isLoading == NO) {
        [self loadState];
    }
}

- (void) loadState {
    if (![self.contestKey isEqualToString:@""]) {
        isLoading = YES;
        
        // Load question
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager setRequestSerializer:[TimeoutHTTPRequestSerializer serializer]];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        [manager GET:[NSString stringWithFormat:STATE_URL, self.contestKey] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
            self.contestState = [[JSON objectForKey:@"Status"] longValue];
            NSLog(@"%ld", self.contestState);
            
            [self processState];
            isLoading = NO;
            
            networkFailedCount = 0;
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            // Network problem
            NSLog(@"%@", error);
            ++networkFailedCount;
            if (isFailed == NO && networkFailedCount >= 5) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                               message:@"Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công."
                                                              delegate:self
                                                     cancelButtonTitle:nil
                                                     otherButtonTitles:@"OK",nil];
                alert.tag = 2;
                [alert show];
                alert = nil;
                
                isFailed = YES;
                networkFailedCount = 0;
            }
            
            isLoading = NO;
        }];
    }
}

- (void) processState {
    if (self.contestState >= 10 && self.contestState < 10000) {
        if (self.contestState % 10 == 3) {
            // Go to summary page
            ResultViewController *viewController = [[ResultViewController alloc] initWithNibName:@"ResultViewController" bundle:nil];
            viewController.contestKey = self.contestKey;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else if (self.contestState % 10 == 2) {
            // Question
            NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
            [defaults setValue:[NSNumber numberWithInt:self.contestState / 10]  forKey:QUESTION_NUMBER_KEY];
            [defaults synchronize];
            
            QuestionViewController *questionView = [[QuestionViewController alloc] initWithNibName:@"QuestionViewController" bundle:nil];
            questionView.contestKey = self.contestKey;
            questionView.questionNumber = self.contestState / 10;
            [self.navigationController pushViewController:questionView animated:YES];
        }
        else {
            // Go to question info page
            NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
            [defaults setValue:[NSNumber numberWithInt:self.contestState / 10]  forKey:QUESTION_NUMBER_KEY];
            [defaults synchronize];
            
            QuestionInfoViewController *viewController = [[QuestionInfoViewController alloc] initWithNibName:@"QuestionInfoViewController" bundle:nil];
            viewController.questionNumber = self.contestState / 10; // Divided by 10 to get the question number
            viewController.contestKey = self.contestKey;
            viewController.contestState = self.contestState;
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }
    else if (self.contestState == 10400) {
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        
        QuestionInfoViewController *viewController = [[QuestionInfoViewController alloc] initWithNibName:@"QuestionInfoViewController" bundle:nil];
        viewController.questionNumber = [[defaults objectForKey:NUMBER_QUESTIONS_KEY] intValue] + 1;
        viewController.contestKey = self.contestKey;
        viewController.contestState = self.contestState;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else if (self.contestState == 10401) {
        // Go to question page
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        
        QuestionViewController *viewController = [[QuestionViewController alloc] initWithNibName:@"QuestionViewController" bundle:nil];
        viewController.contestKey = self.contestKey;
        viewController.questionNumber = [[defaults objectForKey:NUMBER_QUESTIONS_KEY] intValue] + 1;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else if (self.contestState == 10402) {
        // Go to result page
        ResultViewController *viewController = [[ResultViewController alloc] initWithNibName:@"ResultViewController" bundle:nil];
        viewController.contestKey = self.contestKey;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else if (self.contestState == 10300) {
        SummaryViewController *summaryView = [[SummaryViewController alloc] initWithNibName:@"SummaryViewController" bundle:nil];
        summaryView.showFull = YES;
        summaryView.contestKey = self.contestKey;
        [self.navigationController pushViewController:summaryView animated:YES];
    }
}

@end
