//
//  TermViewController.h
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 4/15/14.
//  Copyright (c) 2014 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"


@interface TermViewController : GAITrackedViewController

@property (strong, nonatomic) IBOutlet UIWebView *contentWebView;

- (IBAction)backButtonTouched:(id)sender;

@end
