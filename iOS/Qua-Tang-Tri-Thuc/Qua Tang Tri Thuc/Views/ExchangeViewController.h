//
//  TermViewController.h
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 4/15/14.
//  Copyright (c) 2014 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"


@interface ExchangeViewController : GAITrackedViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIView *infoView;
@property (strong, nonatomic) IBOutlet UIWebView *contentWebView;
@property (strong, nonatomic) IBOutlet UITextField *codeTextView;
@property (strong, nonatomic) IBOutlet UIView *submitView;
@property (strong, nonatomic) IBOutlet UIView *popupView;
@property (strong, nonatomic) IBOutlet UIButton *exchangeButton;
@property (strong, nonatomic) IBOutlet UILabel *scoreLabel;

- (IBAction)joinButtonTouched:(id)sender;
- (IBAction)exchangeButtonTouched:(id)sender;
- (IBAction)backButtonTouched:(id)sender;
- (IBAction)popupBackButtonTouched:(id)sender;

@end
