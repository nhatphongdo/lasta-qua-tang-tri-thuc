//
//  SettingsViewController.m
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 4/15/14.
//  Copyright (c) 2014 VietDev Corporation. All rights reserved.
//

#import "SettingsViewController.h"
#import "Constants.h"
#import <AFNetworking/AFNetworking.h>
#import "BPXLUUIDHandler.h"
#import "TimeoutHTTPRequestSerializer.h"
#import "SoundManager.h"


@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [self.fullnameTextField setDelegate:self];
    [self.phoneTextField setDelegate:self];
    [self.emailTextField setDelegate:self];
    [self.addressTextField setDelegate:self];
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [self.fullnameTextField setText:[defaults objectForKey:USER_FULLNAME_KEY] == nil ? @"" : [defaults objectForKey:USER_FULLNAME_KEY]];
    [self.phoneTextField setText:[defaults objectForKey:USER_PHONE_NUMBER_KEY] == nil ? @"" : [defaults objectForKey:USER_PHONE_NUMBER_KEY]];
    [self.addressTextField setText:[defaults objectForKey:USER_ADDRESS_KEY] == nil ? @"" : [defaults objectForKey:USER_ADDRESS_KEY]];
    [self.emailTextField setText:[defaults objectForKey:USER_EMAIL_KEY] == nil ? @"" : [defaults objectForKey:USER_EMAIL_KEY]];
    [self.optInSwitch setOn:[defaults objectForKey:USER_OPT_IN_KEY] == nil ? YES : [[defaults objectForKey:USER_OPT_IN_KEY] boolValue] animated:YES];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTouched:)];
    [self.view addGestureRecognizer:tap];
    tap = nil;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[SoundManager getOpenningAudio] play];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.screenName = @"Settings Screen";
}

- (void)keyboardWillHide:(NSNotification *)aNotification
{
    // the keyboard is hiding reset the table's height
    NSTimeInterval animationDuration = [[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect frame = self.view.frame;
    frame.origin.y += 160;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];
}

- (void)keyboardWillShow:(NSNotification *)aNotification
{
    // the keyboard is showing so resize the table's height
    NSTimeInterval animationDuration = [[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect frame = self.view.frame;
    frame.origin.y -= 160;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.fullnameTextField) {
        [textField resignFirstResponder];
        [self.phoneTextField becomeFirstResponder];
    }
    else if (textField == self.phoneTextField) {
        [self.addressTextField becomeFirstResponder];
    }
    else if (textField == self.addressTextField) {
        [self.emailTextField becomeFirstResponder];
    }
    else if (textField == self.emailTextField) {
        [self finishButtonTouched:nil];
    }
    return YES;
}

- (void)viewTouched:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)finishButtonTouched:(id)sender {
    [[SoundManager getButtonAudio] play];
    NSString *fullName = self.fullnameTextField.text;
    NSString *phone = self.phoneTextField.text;
    NSString *email = self.emailTextField.text;
    NSString *address = self.addressTextField.text;
    int optIn = [self.optInSwitch isOn] ? 1 : 0;
    
    if (fullName.length == 0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                       message:@"Bạn chưa nhập Tên người chơi."
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;

        [self.fullnameTextField becomeFirstResponder];
        return;
    }
    if (phone.length == 0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                       message:@"Bạn chưa nhập Số điện thoại."
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
        
        [self.phoneTextField becomeFirstResponder];
        return;
    }
    if (address.length == 0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                       message:@"Bạn chưa nhập Địa chỉ."
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
        
        [self.addressTextField becomeFirstResponder];
        return;
    }
    if (email.length == 0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                       message:@"Bạn chưa nhập Địa chỉ email."
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
        
        [self.emailTextField becomeFirstResponder];
        return;
    }

    [self.finishButton setEnabled:NO];

    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    long long userID = [[defaults objectForKey:USERID_KEY] longLongValue];
    
    NSString *deviceID = [BPXLUUIDHandler UUID];
    
    fullName = [[[fullName stringByReplacingOccurrencesOfString:@"/" withString:@"=="] stringByReplacingOccurrencesOfString:@"+" withString:@"==="] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    phone = [[[phone stringByReplacingOccurrencesOfString:@"/" withString:@"=="] stringByReplacingOccurrencesOfString:@"+" withString:@"==="] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    email = [[[email stringByReplacingOccurrencesOfString:@"/" withString:@"=="] stringByReplacingOccurrencesOfString:@"+" withString:@"==="] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    address = [[[address stringByReplacingOccurrencesOfString:@"/" withString:@"=="] stringByReplacingOccurrencesOfString:@"+" withString:@"==="] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"%@", [NSString stringWithFormat:UPDATE_USER_URL, deviceID, userID, fullName, phone, email, address, optIn]);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[TimeoutHTTPRequestSerializer serializer]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:[NSString stringWithFormat:UPDATE_USER_URL, deviceID, userID, fullName, phone, email, address, optIn] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
        
        NSLog(@"%@", JSON);

        if ([[JSON objectForKey:@"Error"] intValue] == 0) {
            [defaults setObject:self.fullnameTextField.text forKey:USER_FULLNAME_KEY];
            [defaults setObject:self.emailTextField.text forKey:USER_EMAIL_KEY];
            [defaults setObject:self.phoneTextField.text forKey:USER_PHONE_NUMBER_KEY];
            [defaults setObject:self.addressTextField.text forKey:USER_ADDRESS_KEY];
            [defaults synchronize];
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Bạn đã cập nhật thông tin cá nhân thành công."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            [alert show];
            alert = nil;
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Chương trình xảy ra lỗi trong quá trình xử lý. Vui lòng thử lại hoặc thông báo tới BTC để được hướng dẫn xử lý và hỗ trợ."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            [alert show];
            alert = nil;
        }
        
        [self.view endEditing:YES];
        [self.finishButton setEnabled:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@", error);
        // Network problem
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                       message:@"Chương trình cần kết nối mạng để hoạt động. Vui lòng kiểm tra kết nối mạng và thử lại."
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
        [self.finishButton setEnabled:YES];
    }];
}

- (IBAction)backButtonTouched:(id)sender {
    [[SoundManager getButtonAudio] play];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
