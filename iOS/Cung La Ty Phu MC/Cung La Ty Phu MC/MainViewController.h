//
//  MainViewController.h
//  Cung La Ty Phu MC
//
//  Created by Do Nhat Phong on 11/5/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController<UITableViewDataSource, UITableViewDelegate> {
    NSArray *playingUsers;
    NSArray *homeUsers;

    NSTimer *updateTimer;
    BOOL isFailed;
    BOOL isLoading;
    
    int currentLoadedQuestion;
    BOOL loadedScore;
    int correctAnswer;
    int currentLoadedResult;
    
    int currentList;
}

@property (nonatomic, strong) NSString *contestKey;
@property (nonatomic) long currentState;
@property (nonatomic) int numberOfQuestions;
@property (nonatomic) long remainTime;

@property (strong, nonatomic) IBOutlet UILabel *topicLabel;
@property (strong, nonatomic) IBOutlet UILabel *timerLabel;
@property (strong, nonatomic) IBOutlet UILabel *scoreLabel;
@property (strong, nonatomic) IBOutlet UITextView *questionTextView;
@property (strong, nonatomic) IBOutlet UILabel *answer1Label;
@property (strong, nonatomic) IBOutlet UIImageView *answer1Background;
@property (strong, nonatomic) IBOutlet UILabel *answer2Label;
@property (strong, nonatomic) IBOutlet UIImageView *answer2Background;
@property (strong, nonatomic) IBOutlet UILabel *answer3Label;
@property (strong, nonatomic) IBOutlet UIImageView *answer3Background;
@property (strong, nonatomic) IBOutlet UILabel *answer4Label;
@property (strong, nonatomic) IBOutlet UIImageView *answer4Background;
@property (strong, nonatomic) IBOutlet UITextView *hintTextView;
@property (strong, nonatomic) IBOutlet UILabel *correctCountLabel;
@property (strong, nonatomic) IBOutlet UILabel *wrongCountLabel;

@property (strong, nonatomic) IBOutlet UILabel *player1FullnameLabel;
@property (strong, nonatomic) IBOutlet UILabel *player1AnswerLabel;
@property (strong, nonatomic) IBOutlet UILabel *player1TimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *player1ScoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *player1TotalLabel;

@property (strong, nonatomic) IBOutlet UILabel *player2FullnameLabel;
@property (strong, nonatomic) IBOutlet UILabel *player2AnswerLabel;
@property (strong, nonatomic) IBOutlet UILabel *player2TimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *player2ScoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *player2TotalLabel;

@property (strong, nonatomic) IBOutlet UILabel *player3FullnameLabel;
@property (strong, nonatomic) IBOutlet UILabel *player3AnswerLabel;
@property (strong, nonatomic) IBOutlet UILabel *player3TimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *player3ScoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *player3TotalLabel;

@property (strong, nonatomic) IBOutlet UILabel *player4FullnameLabel;
@property (strong, nonatomic) IBOutlet UILabel *player4AnswerLabel;
@property (strong, nonatomic) IBOutlet UILabel *player4TimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *player4ScoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *player4TotalLabel;

@property (strong, nonatomic) IBOutlet UIButton *studioButton;
@property (strong, nonatomic) IBOutlet UIButton *homeButton;
@property (strong, nonatomic) IBOutlet UITableView *usersTableView;

@property (strong, nonatomic) IBOutlet UILabel *topic1;
@property (strong, nonatomic) IBOutlet UILabel *topic2;
@property (strong, nonatomic) IBOutlet UILabel *topic3;
@property (strong, nonatomic) IBOutlet UILabel *topic4;
@property (strong, nonatomic) IBOutlet UILabel *topic5;
@property (strong, nonatomic) IBOutlet UILabel *topic6;
@property (strong, nonatomic) IBOutlet UILabel *topic7;
@property (strong, nonatomic) IBOutlet UILabel *topic8;
@property (strong, nonatomic) IBOutlet UILabel *topicCount1;
@property (strong, nonatomic) IBOutlet UILabel *topicCount2;
@property (strong, nonatomic) IBOutlet UILabel *topicCount3;
@property (strong, nonatomic) IBOutlet UILabel *topicCount4;
@property (strong, nonatomic) IBOutlet UILabel *topicCount5;
@property (strong, nonatomic) IBOutlet UILabel *topicCount6;
@property (strong, nonatomic) IBOutlet UILabel *topicCount7;
@property (strong, nonatomic) IBOutlet UILabel *topicCount8;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator1;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator2;

- (IBAction)updateTop4ButtonTouched:(id)sender;
- (IBAction)studioButtonTouched:(id)sender;
- (IBAction)homeButtonTouched:(id)sender;
- (IBAction)updateUsersButtonTouched:(id)sender;

@end
