//
//  MainViewController.m
//  Cung La Ty Phu MC
//
//  Created by Do Nhat Phong on 11/5/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import "MainViewController.h"
#import "Constants.h"
#import <AFNetworking/AFNetworking.h>
#import "UserRowViewController.h"


@interface MainViewController ()

@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        currentLoadedQuestion = -1;
        currentLoadedResult = -1;
        loadedScore = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.usersTableView.delegate = self;
    self.usersTableView.dataSource = self;
    
    [self processState];

    [self loadTopPlaying];
    
    [self studioButtonTouched:self.studioButton];
    
    isFailed = NO;
    isLoading = NO;
    // Refresh in 1s
    updateTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(update:) userInfo:nil repeats:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)updateTop4ButtonTouched:(id)sender {
    [self loadTopPlaying];
}

- (IBAction)studioButtonTouched:(id)sender {
    [self.studioButton setImage:[UIImage imageNamed:@"button_selected_studio.png"] forState:UIControlStateNormal];
    [self.homeButton setImage:[UIImage imageNamed:@"button_home.png"] forState:UIControlStateNormal];
    
    currentList = 1;
    [self loadTopStudio];
}

- (IBAction)homeButtonTouched:(id)sender {
    [self.studioButton setImage:[UIImage imageNamed:@"button_studio.png"] forState:UIControlStateNormal];
    [self.homeButton setImage:[UIImage imageNamed:@"button_selected_home.png"] forState:UIControlStateNormal];
    
    currentList = 2;
    [self loadTopHome];
}

- (IBAction)updateUsersButtonTouched:(id)sender {
    if (currentList == 1) {
        [self loadTopStudio];
    }
    else {
        [self loadTopHome];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (updateTimer != nil) {
        [updateTimer invalidate];
        updateTimer = nil;
    }
}

- (void) update:(id) sender {
    if (isLoading == NO) {
        [self loadState];
    }
}

- (void) loadState {
    isLoading = YES;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:[NSString stringWithFormat:STATE_URL, self.contestKey] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
        self.currentState = [[JSON objectForKey:@"Status"] longValue];
        
        self.remainTime = [[JSON objectForKey:@"RemainTime"] longValue];
        
        [self processState];
        isLoading = NO;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // Network problem
        if (isFailed == NO) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            alert.tag = 2;
            [alert show];
            alert = nil;
            
            isFailed = YES;
        }
        
        isLoading = NO;
    }];
}

- (void) processState {
    if (self.currentState >= 10 && self.currentState < 10000) {
        if (self.currentState / 10 != currentLoadedQuestion || (self.currentState / 10 == currentLoadedQuestion && loadedScore == NO)) {
            [self loadTopic];
            [self loadQuestion:self.currentState / 10];
        }
        
        if (self.currentState / 10 != currentLoadedResult) {
            if (self.currentState % 10 == 3) {
                // Go to summary page
                [self loadTopPlaying];
                [self loadTopStudio];
                [self loadTopHome];
                currentLoadedResult = self.currentState / 10;
            }
        }

        if (self.currentState % 10 == 2) {
            // Set time
            self.timerLabel.text = [NSString stringWithFormat:@"%ld", self.remainTime];
            if (self.remainTime > 0) {
                [self loadTopPlaying];
            }
        }
    }
    else if (self.currentState == 10400 || self.currentState == 10401) {
        // Go to question page
        if (currentLoadedQuestion != self.numberOfQuestions + 1) {
            [self loadQuestion:self.numberOfQuestions + 1];
        }
        self.timerLabel.text = [NSString stringWithFormat:@"%ld", self.remainTime];
    }
    else if (self.currentState == 10402) {
        if (self.numberOfQuestions + 1 != currentLoadedResult) {
            // Go to summary page
            [self loadTopPlaying];
            [self loadTopStudio];
            [self loadTopHome];
            currentLoadedResult = self.numberOfQuestions + 1;
        }
    }
}

- (void) loadQuestion:(int)number {
    isLoading = YES;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:[NSString stringWithFormat:QUESTION_URL, self.contestKey, number] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {

        self.questionTextView.text = [JSON objectForKey:@"Content"];
        self.questionTextView.font = [UIFont fontWithName:@"Helvetica" size:17];
        self.answer1Label.text = [NSString stringWithFormat:@"A. %@", [JSON objectForKey:@"Answer1"]];
        self.answer2Label.text = [NSString stringWithFormat:@"B. %@", [JSON objectForKey:@"Answer2"]];
        self.answer3Label.text = [NSString stringWithFormat:@"C. %@", [JSON objectForKey:@"Answer3"]];
        self.answer4Label.text = [NSString stringWithFormat:@"D. %@", [JSON objectForKey:@"Answer4"]];
        self.timerLabel.text = [NSString stringWithFormat:@"%d", [[JSON objectForKey:@"CountdownTime"] intValue]];
        self.hintTextView.text = [JSON objectForKey:@"Hint"];
        self.hintTextView.font = [UIFont fontWithName:@"Helvetica" size:17];
        self.topicLabel.text = [NSString stringWithFormat:@"Câu %d: %@", [[JSON objectForKey:@"Number"] intValue], [JSON objectForKey:@"Topic"]];
        self.scoreLabel.text = [NSString stringWithFormat:@"Số điểm: %ld", [[JSON objectForKey:@"Score"] longValue]];
        
        self.answer1Label.textColor = [UIColor whiteColor];
        self.answer2Label.textColor = [UIColor whiteColor];
        self.answer3Label.textColor = [UIColor whiteColor];
        self.answer4Label.textColor = [UIColor whiteColor];
        [self.answer1Background setImage:[UIImage imageNamed:@"bg_answer.png"]];
        [self.answer2Background setImage:[UIImage imageNamed:@"bg_answer.png"]];
        [self.answer3Background setImage:[UIImage imageNamed:@"bg_answer.png"]];
        [self.answer4Background setImage:[UIImage imageNamed:@"bg_answer.png"]];
        
        correctAnswer = [[JSON objectForKey:@"CorrectAnswer"] intValue];
        if (correctAnswer == 1) {
            [self.answer1Background setImage:[UIImage imageNamed:@"bg_correct_answer.png"]];
            self.answer1Label.textColor = [UIColor blackColor];
        }
        else if (correctAnswer == 2) {
            [self.answer2Background setImage:[UIImage imageNamed:@"bg_correct_answer.png"]];
            self.answer2Label.textColor = [UIColor blackColor];
        }
        else if (correctAnswer == 3) {
            [self.answer3Background setImage:[UIImage imageNamed:@"bg_correct_answer.png"]];
            self.answer3Label.textColor = [UIColor blackColor];
        }
        else if (correctAnswer == 4) {
            [self.answer4Background setImage:[UIImage imageNamed:@"bg_correct_answer.png"]];
            self.answer4Label.textColor = [UIColor blackColor];
        }
        
        if ([[JSON objectForKey:@"Score"] longValue] > 0) {
            loadedScore = YES;
        }
        else {
            loadedScore = NO;
        }
        
        currentLoadedQuestion = number;
        isLoading = NO;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // Network problem
        if (isFailed == NO) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            alert.tag = 2;
            [alert show];
            alert = nil;
            
            isFailed = YES;
        }
        
        isLoading = NO;
    }];
}

- (void) loadTopic {
    isLoading = YES;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:[NSString stringWithFormat:TOPICS_URL, self.contestKey] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
        
        NSArray *topics = (NSArray *)JSON;
        
        self.topic1.text = [[topics objectAtIndex:0] objectForKey:@"Name"];
        self.topicCount1.text = [NSString stringWithFormat:@"%d", 2 - [[[topics objectAtIndex:0] objectForKey:@"Count"] intValue]];
        self.topic2.text = [[topics objectAtIndex:1] objectForKey:@"Name"];
        self.topicCount2.text = [NSString stringWithFormat:@"%d", 2 - [[[topics objectAtIndex:1] objectForKey:@"Count"] intValue]];
        self.topic3.text = [[topics objectAtIndex:2] objectForKey:@"Name"];
        self.topicCount3.text = [NSString stringWithFormat:@"%d", 2 - [[[topics objectAtIndex:2] objectForKey:@"Count"] intValue]];
        self.topic4.text = [[topics objectAtIndex:3] objectForKey:@"Name"];
        self.topicCount4.text = [NSString stringWithFormat:@"%d", 2 - [[[topics objectAtIndex:3] objectForKey:@"Count"] intValue]];
        self.topic5.text = [[topics objectAtIndex:4] objectForKey:@"Name"];
        self.topicCount5.text = [NSString stringWithFormat:@"%d", 2 - [[[topics objectAtIndex:4] objectForKey:@"Count"] intValue]];
        self.topic6.text = [[topics objectAtIndex:5] objectForKey:@"Name"];
        self.topicCount6.text = [NSString stringWithFormat:@"%d", 2 - [[[topics objectAtIndex:5] objectForKey:@"Count"] intValue]];
        self.topic7.text = [[topics objectAtIndex:6] objectForKey:@"Name"];
        self.topicCount7.text = [NSString stringWithFormat:@"%d", 2 - [[[topics objectAtIndex:6] objectForKey:@"Count"] intValue]];
        self.topic8.text = [[topics objectAtIndex:7] objectForKey:@"Name"];
        self.topicCount8.text = [NSString stringWithFormat:@"%d", 2 - [[[topics objectAtIndex:7] objectForKey:@"Count"] intValue]];

        isLoading = NO;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // Network problem
        if (isFailed == NO) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            alert.tag = 2;
            [alert show];
            alert = nil;
            
            isFailed = YES;
        }
        
        isLoading = NO;
    }];
}

- (void) loadTopPlaying {
    [self.loadingIndicator1 startAnimating];
    isLoading = YES;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:[NSString stringWithFormat:TOP_PLAYERS_URL, 50] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
        
        NSArray *currentPlayers = (NSArray *)JSON;
        
        int correct = 0;
        int wrong = 0;
        int idx = 0;
        
        self.player1AnswerLabel.text = @"";
        self.player1FullnameLabel.text = @"";
        self.player1ScoreLabel.text = @"";
        self.player1TimeLabel.text = @"";
        self.player1TotalLabel.text = @"";
        self.player2AnswerLabel.text = @"";
        self.player2FullnameLabel.text = @"";
        self.player2ScoreLabel.text = @"";
        self.player2TimeLabel.text = @"";
        self.player2TotalLabel.text = @"";
        self.player3AnswerLabel.text = @"";
        self.player3FullnameLabel.text = @"";
        self.player3ScoreLabel.text = @"";
        self.player3TimeLabel.text = @"";
        self.player3TotalLabel.text = @"";
        self.player4AnswerLabel.text = @"";
        self.player4FullnameLabel.text = @"";
        self.player4ScoreLabel.text = @"";
        self.player4TimeLabel.text = @"";
        self.player4TotalLabel.text = @"";
        self.player1FullnameLabel.textColor = [UIColor redColor];
        self.player1AnswerLabel.textColor = [UIColor redColor];
        self.player1ScoreLabel.textColor = [UIColor redColor];
        self.player1TimeLabel.textColor = [UIColor redColor];
        self.player1TotalLabel.textColor = [UIColor redColor];
        self.player2FullnameLabel.textColor = [UIColor redColor];
        self.player2AnswerLabel.textColor = [UIColor redColor];
        self.player2ScoreLabel.textColor = [UIColor redColor];
        self.player2TimeLabel.textColor = [UIColor redColor];
        self.player2TotalLabel.textColor = [UIColor redColor];
        self.player3FullnameLabel.textColor = [UIColor redColor];
        self.player3AnswerLabel.textColor = [UIColor redColor];
        self.player3ScoreLabel.textColor = [UIColor redColor];
        self.player3TimeLabel.textColor = [UIColor redColor];
        self.player3TotalLabel.textColor = [UIColor redColor];
        self.player4FullnameLabel.textColor = [UIColor redColor];
        self.player4AnswerLabel.textColor = [UIColor redColor];
        self.player4ScoreLabel.textColor = [UIColor redColor];
        self.player4TimeLabel.textColor = [UIColor redColor];
        self.player4TotalLabel.textColor = [UIColor redColor];

        for (int i = 0; i < [currentPlayers count]; i++) {
            NSDictionary *item = [currentPlayers objectAtIndex:i];
            
            if ([item objectForKey:@"Answer"] != [NSNull null]) {
                int answer = [[item objectForKey:@"Answer"] intValue];
                if (answer == correctAnswer) {
                    ++correct;
                }
                else {
                    ++wrong;
                }
            }
            else {
                ++wrong;
            }
            
            if (idx < 4 && [[item objectForKey:@"IsPlaying"] boolValue] == YES) {
                if (idx == 0) {
                    self.player1FullnameLabel.text = [NSString stringWithFormat:@"%@ (%lld)", [item objectForKey:@"Fullname"] == [NSNull null] ? @"" : [item objectForKey:@"Fullname"], [item objectForKey:@"ID"] == [NSNull null] ? 0 : [[item objectForKey:@"ID"] longLongValue]];
                    if ([item objectForKey:@"Answer"] != [NSNull null]) {
                        int answer = [[item objectForKey:@"Answer"] intValue];
                        self.player1AnswerLabel.text = (answer == 1 ? @"A" : (answer == 2 ? @"B" : (answer == 3 ? @"C" : (answer == 4 ? @"D" : @""))));
                        
                        if (answer == correctAnswer) {
                            self.player1FullnameLabel.textColor = [UIColor colorWithRed:29/255.0 green:152/255.0 blue:50/255.0 alpha:1.0];
                            self.player1AnswerLabel.textColor = [UIColor colorWithRed:29/255.0 green:152/255.0 blue:50/255.0 alpha:1.0];
                            self.player1ScoreLabel.textColor = [UIColor colorWithRed:29/255.0 green:152/255.0 blue:50/255.0 alpha:1.0];
                            self.player1TimeLabel.textColor = [UIColor colorWithRed:29/255.0 green:152/255.0 blue:50/255.0 alpha:1.0];
                            self.player1TotalLabel.textColor = [UIColor colorWithRed:29/255.0 green:152/255.0 blue:50/255.0 alpha:1.0];
                        }
                    }
                    else {
                        self.player1AnswerLabel.text = @"";
                    }
                    if ([item objectForKey:@"LastScore"] != [NSNull null]) {
                        self.player1ScoreLabel.text = [NSString stringWithFormat:@"%ld", [[item objectForKey:@"LastScore"] longValue]];
                    }
                    else {
                        self.player1ScoreLabel.text = @"";
                    }
                    if ([item objectForKey:@"Time"] != [NSNull null]) {
                        self.player1TimeLabel.text = [NSString stringWithFormat:@"%0.2fs", [[item objectForKey:@"Time"] doubleValue]];
                    }
                    else {
                        self.player1TimeLabel.text = @"";
                    }
                    if ([item objectForKey:@"DayScore"] != [NSNull null]) {
                        self.player1TotalLabel.text = [NSString stringWithFormat:@"%ld", [[item objectForKey:@"DayScore"] longValue]];
                    }
                }
                else if (idx == 1) {
                    self.player2FullnameLabel.text = [NSString stringWithFormat:@"%@ (%lld)", [item objectForKey:@"Fullname"] == [NSNull null] ? @"" : [item objectForKey:@"Fullname"], [item objectForKey:@"ID"] == [NSNull null] ? 0 : [[item objectForKey:@"ID"] longLongValue]];
                    if ([item objectForKey:@"Answer"] != [NSNull null]) {
                        int answer = [[item objectForKey:@"Answer"] intValue];
                        self.player2AnswerLabel.text = (answer == 1 ? @"A" : (answer == 2 ? @"B" : (answer == 3 ? @"C" : (answer == 4 ? @"D" : @""))));
                        
                        if (answer == correctAnswer) {
                            self.player2FullnameLabel.textColor = [UIColor colorWithRed:29/255.0 green:152/255.0 blue:50/255.0 alpha:1.0];
                            self.player2AnswerLabel.textColor = [UIColor colorWithRed:29/255.0 green:152/255.0 blue:50/255.0 alpha:1.0];
                            self.player2ScoreLabel.textColor = [UIColor colorWithRed:29/255.0 green:152/255.0 blue:50/255.0 alpha:1.0];
                            self.player2TimeLabel.textColor = [UIColor colorWithRed:29/255.0 green:152/255.0 blue:50/255.0 alpha:1.0];
                            self.player2TotalLabel.textColor = [UIColor colorWithRed:29/255.0 green:152/255.0 blue:50/255.0 alpha:1.0];
                        }
                    }
                    else {
                        self.player2AnswerLabel.text = @"";
                    }
                    if ([item objectForKey:@"LastScore"] != [NSNull null]) {
                        self.player2ScoreLabel.text = [NSString stringWithFormat:@"%ld", [[item objectForKey:@"LastScore"] longValue]];
                    }
                    else {
                        self.player2ScoreLabel.text = @"";
                    }
                    if ([item objectForKey:@"Time"] != [NSNull null]) {
                        self.player2TimeLabel.text = [NSString stringWithFormat:@"%0.2fs", [[item objectForKey:@"Time"] doubleValue]];
                    }
                    else {
                        self.player2TimeLabel.text = @"";
                    }
                    if ([item objectForKey:@"DayScore"] != [NSNull null]) {
                        self.player2TotalLabel.text = [NSString stringWithFormat:@"%ld", [[item objectForKey:@"DayScore"] longValue]];
                    }
                }
                else if (idx == 2) {
                    self.player3FullnameLabel.text = [NSString stringWithFormat:@"%@ (%lld)", [item objectForKey:@"Fullname"] == [NSNull null] ? @"" : [item objectForKey:@"Fullname"], [item objectForKey:@"ID"] == [NSNull null] ? 0 : [[item objectForKey:@"ID"] longLongValue]];
                    if ([item objectForKey:@"Answer"] != [NSNull null]) {
                        int answer = [[item objectForKey:@"Answer"] intValue];
                        self.player3AnswerLabel.text = (answer == 1 ? @"A" : (answer == 2 ? @"B" : (answer == 3 ? @"C" : (answer == 4 ? @"D" : @""))));
                        
                        if (answer == correctAnswer) {
                            self.player3FullnameLabel.textColor = [UIColor colorWithRed:29/255.0 green:152/255.0 blue:50/255.0 alpha:1.0];
                            self.player3AnswerLabel.textColor = [UIColor colorWithRed:29/255.0 green:152/255.0 blue:50/255.0 alpha:1.0];
                            self.player3ScoreLabel.textColor = [UIColor colorWithRed:29/255.0 green:152/255.0 blue:50/255.0 alpha:1.0];
                            self.player3TimeLabel.textColor = [UIColor colorWithRed:29/255.0 green:152/255.0 blue:50/255.0 alpha:1.0];
                            self.player3TotalLabel.textColor = [UIColor colorWithRed:29/255.0 green:152/255.0 blue:50/255.0 alpha:1.0];
                        }
                    }
                    else {
                        self.player3AnswerLabel.text = @"";
                    }
                    if ([item objectForKey:@"LastScore"] != [NSNull null]) {
                        self.player3ScoreLabel.text = [NSString stringWithFormat:@"%ld", [[item objectForKey:@"LastScore"] longValue]];
                    }
                    else {
                        self.player3ScoreLabel.text = @"";
                    }
                    if ([item objectForKey:@"Time"] != [NSNull null]) {
                        self.player3TimeLabel.text = [NSString stringWithFormat:@"%0.2fs", [[item objectForKey:@"Time"] doubleValue]];
                    }
                    else {
                        self.player3TimeLabel.text = @"";
                    }
                    if ([item objectForKey:@"DayScore"] != [NSNull null]) {
                        self.player3TotalLabel.text = [NSString stringWithFormat:@"%ld", [[item objectForKey:@"DayScore"] longValue]];
                    }
                }
                else if (idx == 3) {
                    self.player4FullnameLabel.text = [NSString stringWithFormat:@"%@ (%lld)", [item objectForKey:@"Fullname"] == [NSNull null] ? @"" : [item objectForKey:@"Fullname"], [item objectForKey:@"ID"] == [NSNull null] ? 0 : [[item objectForKey:@"ID"] longLongValue]];
                    if ([item objectForKey:@"Answer"] != [NSNull null]) {
                        int answer = [[item objectForKey:@"Answer"] intValue];
                        self.player4AnswerLabel.text = (answer == 1 ? @"A" : (answer == 2 ? @"B" : (answer == 3 ? @"C" : (answer == 4 ? @"D" : @""))));
                        
                        if (answer == correctAnswer) {
                            self.player4FullnameLabel.textColor = [UIColor colorWithRed:29/255.0 green:152/255.0 blue:50/255.0 alpha:1.0];
                            self.player4AnswerLabel.textColor = [UIColor colorWithRed:29/255.0 green:152/255.0 blue:50/255.0 alpha:1.0];
                            self.player4ScoreLabel.textColor = [UIColor colorWithRed:29/255.0 green:152/255.0 blue:50/255.0 alpha:1.0];
                            self.player4TimeLabel.textColor = [UIColor colorWithRed:29/255.0 green:152/255.0 blue:50/255.0 alpha:1.0];
                            self.player4TotalLabel.textColor = [UIColor colorWithRed:29/255.0 green:152/255.0 blue:50/255.0 alpha:1.0];
                        }
                    }
                    else {
                        self.player4AnswerLabel.text = @"";
                    }
                    if ([item objectForKey:@"LastScore"] != [NSNull null]) {
                        self.player4ScoreLabel.text = [NSString stringWithFormat:@"%ld", [[item objectForKey:@"LastScore"] longValue]];
                    }
                    else {
                        self.player4ScoreLabel.text = @"";
                    }
                    if ([item objectForKey:@"Time"] != [NSNull null]) {
                        self.player4TimeLabel.text = [NSString stringWithFormat:@"%0.2fs", [[item objectForKey:@"Time"] doubleValue]];
                    }
                    else {
                        self.player4TimeLabel.text = @"";
                    }
                    if ([item objectForKey:@"DayScore"] != [NSNull null]) {
                        self.player4TotalLabel.text = [NSString stringWithFormat:@"%ld", [[item objectForKey:@"DayScore"] longValue]];
                    }
                }
                
                ++idx;
            }
        }
        
        self.correctCountLabel.text = [NSString stringWithFormat:@"%d", correct];
        self.wrongCountLabel.text = [NSString stringWithFormat:@"%d", wrong];
        
        isLoading = NO;
        [self.loadingIndicator1 stopAnimating];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // Network problem
        NSLog(@"%@", error);
        if (isFailed == NO) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            alert.tag = 2;
            [alert show];
            alert = nil;
            
            isFailed = YES;
        }
        
        isLoading = NO;
        [self.loadingIndicator1 stopAnimating];
    }];
}

- (void) loadTopStudio {
    [self.loadingIndicator2 startAnimating];
    isLoading = YES;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:[NSString stringWithFormat:TOP_USERS_URL, 50] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
        
        playingUsers = (NSArray *)JSON;
        [self.usersTableView reloadData];
        
        isLoading = NO;
        [self.loadingIndicator2 stopAnimating];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // Network problem
        if (isFailed == NO) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            alert.tag = 2;
            [alert show];
            alert = nil;
            
            isFailed = YES;
        }
        
        isLoading = NO;
        [self.loadingIndicator2 stopAnimating];
    }];
}

- (void) loadTopHome {
    [self.loadingIndicator2 startAnimating];
    isLoading = YES;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:[NSString stringWithFormat:TOP_HOME_USERS_URL, 50] parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
        
        homeUsers = (NSArray *)JSON;
        [self.usersTableView reloadData];
        
        isLoading = NO;
        [self.loadingIndicator2 stopAnimating];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // Network problem
        if (isFailed == NO) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ApplicationTitle
                                                           message:@"Chương trình cần kết nối mạng để hoạt động. Chương trình sẽ tự động kết nối lại cho đến khi kết nối thành công."
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            alert.tag = 2;
            [alert show];
            alert = nil;
            
            isFailed = YES;
        }
        
        isLoading = NO;
        [self.loadingIndicator2 stopAnimating];
    }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (currentList == 1) {
        return [playingUsers count];
    }
    return [homeUsers count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UserRowViewController *cell = (UserRowViewController *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        NSArray *nib;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            nib = [[NSBundle mainBundle] loadNibNamed:@"UserRowViewController" owner:self options:nil];
        }
        else {
            nib = [[NSBundle mainBundle] loadNibNamed:@"UserRowViewController" owner:self options:nil];
        }
        
        cell = [nib objectAtIndex:0];
        
        cell.frame = CGRectMake(0, 0, self.view.frame.size.width, cell.frame.size.height);
        
        if (indexPath.row % 2 == 1) {
            cell.backgroundColor = [UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:1];
        }
        else {
            cell.backgroundColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1];
        }
        
        NSDictionary *user;
        if (currentList == 1) {
            user = [playingUsers objectAtIndex:indexPath.row];
        }
        else {
            user = [homeUsers objectAtIndex:indexPath.row];
        }
        cell.idLabel.text = [NSString stringWithFormat:@"%lld", [user objectForKey:@"ID"] == [NSNull null] ? 0 : [[user objectForKey:@"ID"] longLongValue]];
        cell.fullnameLabel.text = [user objectForKey:@"Fullname"] == [NSNull null] ? @"" : [user objectForKey:@"Fullname"];
        cell.scoreLabel.text = [NSString stringWithFormat:@"%lld", [user objectForKey:@"Score"] == [NSNull null] ? 0 : [[user objectForKey:@"Score"] longLongValue]];
    }
    
    return cell;
}

@end
