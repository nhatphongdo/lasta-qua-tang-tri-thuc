//
//  Constants.h
//  Cung La Ty Phu MC
//
//  Created by Do Nhat Phong on 1/9/14.
//  Copyright (c) 2014 VietDev Corporation. All rights reserved.
//

#ifndef Cung_La_Ty_Phu_MC_Constants_h
#define Cung_La_Ty_Phu_MC_Constants_h

#define ApplicationTitle @"Cùng Là Tỷ Phú"

//#define ROOT_URL @"http://web.vietdev.vn/lasta/api/"
#define ROOT_URL @"http://192.168.1.2/api/"
#define HOME_ROOT_URL @"http://cunglatyphu.vn/api/"
#define CONTEST_TODAY_URL [ROOT_URL stringByAppendingString:@"contests/today"]
#define CONTEST_DAY_URL [ROOT_URL stringByAppendingString:@"contests/%@"]
#define STATE_URL [ROOT_URL stringByAppendingString:@"states/%@"]
#define QUESTION_URL [ROOT_URL stringByAppendingString:@"questions/%@/%d/mc"]
#define TOPICS_URL [ROOT_URL stringByAppendingString:@"contests/%@/topics"]
#define TOP_USERS_URL [ROOT_URL stringByAppendingString:@"topusers/%d"]
#define TOP_PLAYERS_URL [ROOT_URL stringByAppendingString:@"topusers/playing/%d"]

#define TOP_HOME_USERS_URL [HOME_ROOT_URL stringByAppendingString:@"topusers/dailyhome/%d"]

#endif
