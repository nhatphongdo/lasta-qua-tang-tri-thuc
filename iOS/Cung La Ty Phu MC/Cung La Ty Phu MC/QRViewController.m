//
//  QRViewController.m
//  Cung La Ty Phu MC
//
//  Created by Do Nhat Phong on 1/9/14.
//  Copyright (c) 2014 VietDev Corporation. All rights reserved.
//

#import "QRViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AFNetworking/AFNetworking.h>
#import "MainViewController.h"
#import "Constants.h"


@interface QRViewController ()

@end

@implementation QRViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    
    [reader.scanner setSymbology: 0
                          config: ZBAR_CFG_ENABLE
                              to: 0];
    [reader.scanner setSymbology: ZBAR_QRCODE
                          config: ZBAR_CFG_ENABLE
                              to: 1];
    
    reader.showsCameraControls = NO;  // for UIImagePickerController
    reader.showsZBarControls = NO;
    
    reader.readerView.frame = CGRectMake(0, 0, self.qrScannerView.frame.size.width, self.qrScannerView.frame.size.height);
    reader.readerView.zoom = 1.0;
    reader.cameraFlashMode = UIImagePickerControllerCameraFlashModeOff;
    reader.readerView.torchMode = AVCaptureTorchModeOff;
    reader.view = reader.readerView;
    
    [self addChildViewController:reader];
    [self.qrScannerView addSubview:reader.view];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
//    [self loadContest:[NSString stringWithFormat:CONTEST_DAY_URL, @"a47d0e30-bf44-4ea9-9ec1-e939eadd50b7"]];
    autoFailed = YES;
    isLoading = NO;
    // Refresh in 1s
    updateTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(update:) userInfo:nil repeats:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (updateTimer != nil) {
        [updateTimer invalidate];
        updateTimer = nil;
    }
}

- (void) update:(id) sender {
    if (isLoading == NO && autoFailed == YES) {
        [self loadContest:CONTEST_TODAY_URL];
    }
}



#pragma ZBarReader Delegate

- (void) imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary*) info
{
    id<NSFastEnumeration> results = [info objectForKey:ZBarReaderControllerResults];
    
    for (id item in results) {
        [self loadContest:((ZBarSymbol *)item).data];
    }
}


- (void) loadContest:(NSString *)contestLink {
    [self.loadingIndicator startAnimating];
    [reader.readerView stop];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:contestLink parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {

        long state = [[JSON objectForKey:@"State"] longValue];

        MainViewController *mainViewController = [[MainViewController alloc] initWithNibName:@"MainViewController" bundle:nil];
        mainViewController.currentState = state;
        mainViewController.contestKey = [JSON objectForKey:@"ContestKey"];
        mainViewController.numberOfQuestions = [[JSON objectForKey:@"NumberOfQuestions"] intValue];
        mainViewController.remainTime = [[JSON objectForKey:@"Remain"] longValue];
        [self.navigationController pushViewController:mainViewController animated:YES];
        
        [self.loadingIndicator stopAnimating];
        [reader.readerView start];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (operation.response.statusCode == 404) {
            // Not found
            if ([contestLink isEqualToString:CONTEST_TODAY_URL]) {
                // Load today contest failed then stay here for QR scanning
                autoFailed = NO;
            }
        }
        else {
            if ([contestLink isEqualToString:CONTEST_TODAY_URL]) {
                // Network error and auto-loading
                autoFailed = YES;
            }
        }
        
        [self.loadingIndicator stopAnimating];
        [reader.readerView start];
    }];
}


@end
