//
//  UserRowViewController.h
//  cung-la-ty-phu
//
//  Created by Do Nhat Phong on 1/9/14.
//  Copyright (c) 2014 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserRowViewController : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *idLabel;
@property (strong, nonatomic) IBOutlet UILabel *fullnameLabel;
@property (strong, nonatomic) IBOutlet UILabel *scoreLabel;

@end
