//
//  QRViewController.h
//  Cung La Ty Phu MC
//
//  Created by Do Nhat Phong on 1/9/14.
//  Copyright (c) 2014 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBarSDK.h"


@interface QRViewController : UIViewController<ZBarReaderDelegate> {
    ZBarReaderViewController *reader;
    BOOL autoFailed;
    NSTimer *updateTimer;
    BOOL isLoading;
}

@property (strong, nonatomic) IBOutlet UIView *qrScannerView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;

@end
