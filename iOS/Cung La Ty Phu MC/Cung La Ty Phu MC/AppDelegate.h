//
//  AppDelegate.h
//  Cung La Ty Phu MC
//
//  Created by Do Nhat Phong on 11/5/13.
//  Copyright (c) 2013 VietDev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UINavigationController *rootViewController;
@property (strong, nonatomic) UIWindow *window;

@end
