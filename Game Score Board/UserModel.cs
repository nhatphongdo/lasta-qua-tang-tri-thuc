﻿
namespace Game_Score_Board
{
    public class UserModel
    {
        public long ID
        {
            get;
            set;
        }

        public string Fullname
        {
            get;
            set;
        }

        public bool IsPlaying
        {
            get;
            set;
        }

        public string Email
        {
            get;
            set;
        }

        public long? Score
        {
            get;
            set;
        }

        public long? DayScore
        {
            get;
            set;
        }

        public int? Answer
        {
            get;
            set;
        }

        public double? Time
        {
            get;
            set;
        }

        public int? Duration
        {
            get;
            set;
        }
    }
}
