﻿using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;

namespace Game_Score_Board
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const double VideoOriginalWidth = 720.0;
        private const double VideoOriginalHeight = 576.0;

        private System.Windows.Threading.DispatcherTimer timer;
        private bool alreadyShowed = false;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Get secondary screen
            var screen = System.Windows.Forms.Screen.PrimaryScreen;
            foreach (var s in System.Windows.Forms.Screen.AllScreens)
            {
                if (s.Primary == false)
                {
                    screen = s;
                    break;
                }
            }

            var screenWidth = (double)screen.Bounds.Width;
            var screenHeight = (double)screen.Bounds.Height;
            var ratioWidth = screenWidth / VideoOriginalWidth;
            var ratioHeight = screenHeight / VideoOriginalHeight;
            MainGrid.LayoutTransform = new ScaleTransform(ratioWidth, ratioHeight);
            this.Left = screen.Bounds.X;
            this.Top = screen.Bounds.Y;
            this.Width = screen.Bounds.Width;
            this.Height = screen.Bounds.Height;

            // Timer to update score board
            timer = new System.Windows.Threading.DispatcherTimer();
            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = new TimeSpan(0, 0, 0, 3);
            timer.Start();

            // Add tiles to leaderboards
            for (var i = 0; i < 10; i++)
            {
                var text = new FlipControl();
                Grid.SetRow(text, i + 1);
                Grid.SetColumn(text, 0);
                LeaderboardHome.Children.Add(text);
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            try
            {
                using (var local = new LocalEntities())
                using (var cloud = new CloudDataEntities())
                {
                    // Get current contest
                    var startDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    var endDay = startDay.AddDays(1);

                    var contest = local.ContestDays.FirstOrDefault(c => !c.IsDeleted && c.StartTime >= startDay && c.StartTime < endDay);
                    if (contest == null)
                    {
                        return;
                    }

                    // Check current state
                    if (contest.State % 10 == 3)
                    {
                        var question = local.Questions
                            .FirstOrDefault(q => !q.IsDeleted && q.Number > 0 && q.ContestDayID == contest.ID && q.Number == (contest.State / 10));

                        var questionID = question == null ? 0 : question.ID;

                        PlayGrid.Visibility = Visibility.Visible;
                        LastGrid.Visibility = Visibility.Hidden;

                        if (!alreadyShowed)
                        {
                            //alreadyShowed = true;

                            // Check correct, reload result
                            //var homeList = cloud.HomeUsers
                            //    .Where(u => !u.IsDeleted)
                            //    .Select(u => new UserModel()
                            //    {
                            //        ID = u.ID,
                            //        //Answer = u.HomeAnswers.FirstOrDefault(a => !a.IsDeleted && a.QuestionID == questionID).AnswerNumber,
                            //        Time = SqlFunctions.DateDiff("ms", u.HomeAnswers.FirstOrDefault(a => !a.IsDeleted && a.QuestionID == questionID).StartTime, u.HomeAnswers.FirstOrDefault(a => !a.IsDeleted && a.QuestionID == questionID).EndTime) / 1000.0,
                            //        //Score = u.HomeAnswers.FirstOrDefault(a => !a.IsDeleted && a.Score > 0).Score,
                            //        DayScore = u.HomeAnswers.Where(a => !a.IsDeleted && a.Score > 0 && a.Question.ContestDayID == contest.ID)
                            //            .GroupBy(a => a.QuestionID)
                            //            .Select(a => a.FirstOrDefault().Score).DefaultIfEmpty(0).Sum()
                            //    })
                            //    .OrderByDescending(u => u.DayScore)
                            //    .Take(10).ToList();
                            var homeList = cloud.DayScores
                                .Where(u => u.ContestDayID == contest.ID)
                                .Select(u => new UserModel()
                                {
                                    ID = u.UserID,
                                    Score = u.TotalScore
                                })
                                .OrderByDescending(u => u.Score)
                                .Take(10).ToList();
                            GenerateList(LeaderboardHome, homeList);

                            var playingList = local.Users
                                .Where(u => !u.IsDeleted)
                                .Select(u => new UserModel()
                                {
                                    ID = u.ID,
                                    //Answer = u.Answers.FirstOrDefault(a => !a.IsDeleted && a.QuestionID == questionID).AnswerNumber,
                                    Time = SqlFunctions.DateDiff("ms", u.Answers.FirstOrDefault(a => !a.IsDeleted && a.QuestionID == questionID).StartTime, u.Answers.FirstOrDefault(a => !a.IsDeleted && a.QuestionID == questionID).EndTime) / 1000.0,
                                    Duration = u.Answers.FirstOrDefault(a => !a.IsDeleted && a.QuestionID == questionID).Duration,
                                    //Score = u.Answers.FirstOrDefault(a => !a.IsDeleted && a.Score > 0).Score,
                                    DayScore = u.Answers.Where(a => !a.IsDeleted && a.Score > 0 && a.Question.ContestDayID == contest.ID)
                                        .GroupBy(a => a.QuestionID)
                                        .Select(a => a.FirstOrDefault().Score).DefaultIfEmpty(0).Sum()
                                })
                                .OrderByDescending(u => u.DayScore)
                                .ThenBy(u => u.Duration).ThenBy(u => u.Time)
                                .Take(4).ToList();

                            for (var i = 0; i < playingList.Count; i++)
                            {
                                if (playingList[i].Time.HasValue == false || playingList[i].Duration.HasValue == false)
                                {
                                    continue;
                                }

                                double fraction = 0;
                                if (playingList[i].Time.Value < playingList[i].Duration.Value)
                                {
                                    // Round diff
                                    //fraction = Math.Ceiling(players[i].Duration.Value - players[i].Time.Value);
                                    fraction = playingList[i].Time.Value - Math.Truncate(playingList[i].Time.Value);
                                }
                                else if (playingList[i].Time.Value > playingList[i].Duration.Value)
                                {
                                    fraction = playingList[i].Time.Value - Math.Truncate(playingList[i].Time.Value);
                                }
                                playingList[i].Time = fraction + playingList[i].Duration.Value;
                            }

                            if (playingList.Count > 0)
                            {
                                StandUserID01.Content = playingList[0].ID.ToString();
                                StandTotalScore01.Content = (playingList[0].DayScore == null ? "0" : playingList[0].DayScore.ToString()) + " Đ";
                                StandTime01.Content = (playingList[0].Time == null ? "0" : playingList[0].Time.Value.ToString("0.00")) + " s";
                            }

                            if (playingList.Count > 1)
                            {
                                StandUserID02.Content = playingList[1].ID.ToString();
                                StandTotalScore02.Content = (playingList[1].DayScore == null ? "0" : playingList[1].DayScore.ToString()) + " Đ";
                                StandTime02.Content = (playingList[1].Time == null ? "0" : playingList[1].Time.Value.ToString("0.00")) + " s";
                            }

                            if (playingList.Count > 2)
                            {
                                StandUserID03.Content = playingList[2].ID.ToString();
                                StandTotalScore03.Content = (playingList[2].DayScore == null ? "0" : playingList[2].DayScore.ToString()) + " Đ";
                                StandTime03.Content = (playingList[2].Time == null ? "0" : playingList[2].Time.Value.ToString("0.00")) + " s";
                            }

                            if (playingList.Count > 3)
                            {
                                StandUserID04.Content = playingList[3].ID.ToString();
                                StandTotalScore04.Content = (playingList[3].DayScore == null ? "0" : playingList[3].DayScore.ToString()) + " Đ";
                                StandTime04.Content = (playingList[3].Time == null ? "0" : playingList[3].Time.Value.ToString("0.00")) + " s";
                            }

                            var players = local.Users
                                .Where(u => !u.IsDeleted && u.IsPlaying)
                                .OrderBy(u => u.Seat)
                                .Select(u => new UserModel()
                                {
                                    ID = u.ID,
                                    //Answer = u.Answers.FirstOrDefault(a => !a.IsDeleted && a.QuestionID == questionID).AnswerNumber,
                                    Time = SqlFunctions.DateDiff("ms", u.Answers.FirstOrDefault(a => !a.IsDeleted && a.QuestionID == questionID).StartTime, u.Answers.FirstOrDefault(a => !a.IsDeleted && a.QuestionID == questionID).EndTime) / 1000.0,
                                    Duration = u.Answers.FirstOrDefault(a => !a.IsDeleted && a.QuestionID == questionID).Duration,
                                    //Score = u.Answers.FirstOrDefault(a => !a.IsDeleted && a.QuestionID == questionID).Score,
                                    DayScore = u.Answers.Where(a => !a.IsDeleted && a.Score > 0 && a.Question.ContestDayID == contest.ID)
                                        .GroupBy(a => a.QuestionID)
                                        .Select(a => a.FirstOrDefault().Score).DefaultIfEmpty(0).Sum()
                                })
                                .Take(4).ToList();

                            for (var i = 0; i < players.Count; i++)
                            {
                                if (players[i].Time.HasValue == false || players[i].Duration.HasValue == false)
                                {
                                    continue;
                                }

                                double fraction = 0;
                                if (players[i].Time.Value < players[i].Duration.Value)
                                {
                                    // Round diff
                                    //fraction = Math.Ceiling(players[i].Duration.Value - players[i].Time.Value);
                                    fraction = players[i].Time.Value - Math.Truncate(players[i].Time.Value);
                                }
                                else if (players[i].Time.Value > players[i].Duration.Value)
                                {
                                    fraction = players[i].Time.Value - Math.Truncate(players[i].Time.Value);
                                }
                                players[i].Time = fraction + players[i].Duration.Value;
                            }

                            if (players.Count > 0)
                            {
                                UserID01.Content = players[0].ID.ToString();
                                TotalScore01.Content = (players[0].DayScore == null ? "0" : players[0].DayScore.ToString()) + " Đ";
                                Time01.Content = (players[0].Time == null ? "0" : players[0].Time.Value.ToString("0.00")) + " s";
                            }

                            if (players.Count > 1)
                            {
                                UserID02.Content = players[1].ID.ToString();
                                TotalScore02.Content = (players[1].DayScore == null ? "0" : players[1].DayScore.ToString()) + " Đ";
                                Time02.Content = (players[1].Time == null ? "0" : players[1].Time.Value.ToString("0.00")) + " s";
                            }

                            if (players.Count > 2)
                            {
                                UserID03.Content = players[2].ID.ToString();
                                TotalScore03.Content = (players[2].DayScore == null ? "0" : players[2].DayScore.ToString()) + " Đ";
                                Time03.Content = (players[2].Time == null ? "0" : players[2].Time.Value.ToString("0.00")) + " s";
                            }

                            if (players.Count > 3)
                            {
                                UserID04.Content = players[3].ID.ToString();
                                TotalScore04.Content = (players[3].DayScore == null ? "0" : players[3].DayScore.ToString()) + " Đ";
                                Time04.Content = (players[3].Time == null ? "0" : players[3].Time.Value.ToString("0.00")) + " s";
                            }

                        }
                    }
                    else if (contest.State == 10402)
                    {
                        var question = local.Questions
                            .FirstOrDefault(q => !q.IsDeleted && q.Number > 0 && q.ContestDayID == contest.ID && q.Number == contest.NumberOfQuestions + 1);

                        var questionID = question == null ? 0 : question.ID;

                        PlayGrid.Visibility = Visibility.Hidden;
                        LastGrid.Visibility = Visibility.Visible;

                        if (!alreadyShowed)
                        {
                            alreadyShowed = true;

                            // Check correct, reload result
                            var players = local.Users
                                .Where(u => !u.IsDeleted && u.IsPlaying)
                                .OrderBy(u => u.Seat)
                                .Select(u => new UserModel()
                                {
                                    ID = u.ID,
                                    Answer = u.Answers.FirstOrDefault(a => !a.IsDeleted && a.QuestionID == questionID).AnswerNumber,
                                    Time = SqlFunctions.DateDiff("ms", u.Answers.FirstOrDefault(a => !a.IsDeleted && a.QuestionID == questionID).StartTime, u.Answers.FirstOrDefault(a => !a.IsDeleted && a.QuestionID == questionID).EndTime) / 1000.0,
                                    Duration = u.Answers.FirstOrDefault(a => !a.IsDeleted && a.QuestionID == questionID).Duration,
                                    Score = u.Answers.FirstOrDefault(a => !a.IsDeleted && a.QuestionID == questionID).Score,
                                    DayScore = u.Answers.Where(a => !a.IsDeleted && a.Score > 0 && a.Question.ContestDayID == contest.ID)
                                        .GroupBy(a => a.QuestionID)
                                        .Select(a => a.FirstOrDefault().Score).DefaultIfEmpty(0).Sum()
                                })
                                .Take(4).ToList();

                            for (var i = 0; i < players.Count; i++)
                            {
                                if (players[i].Time.HasValue == false || players[i].Duration.HasValue == false)
                                {
                                    continue;
                                }

                                double fraction = 0;
                                if (players[i].Time.Value < players[i].Duration.Value)
                                {
                                    // Round diff
                                    //fraction = Math.Ceiling(players[i].Duration.Value - players[i].Time.Value);
                                    fraction = players[i].Time.Value - Math.Truncate(players[i].Time.Value);
                                }
                                else if (players[i].Time.Value > players[i].Duration.Value)
                                {
                                    fraction = players[i].Time.Value - Math.Truncate(players[i].Time.Value);
                                }
                                players[i].Time = fraction + players[i].Duration.Value;
                            }

                            if (players.Count > 0)
                            {
                                LastUserID01.Content = players[0].ID.ToString();
                                LastAnswer01.Content = players[0].Answer == null ? "" : " ABCD"[players[0].Answer.Value].ToString();
                                LastTime01.Content = (players[0].Time == null ? "0" : players[0].Time.Value.ToString("0.00")) + " s";
                                LastScore01.Content = (players[0].Score == null ? "0" : players[0].Score.ToString()) + " Đ";
                            }

                            if (players.Count > 1)
                            {
                                LastUserID02.Content = players[1].ID.ToString();
                                LastAnswer02.Content = players[1].Answer == null ? "" : " ABCD"[players[1].Answer.Value].ToString();
                                LastTime02.Content = (players[1].Time == null ? "0" : players[1].Time.Value.ToString("0.00")) + " s";
                                LastScore02.Content = (players[1].Score == null ? "0" : players[1].Score.ToString()) + " Đ";
                            }

                            if (players.Count > 2)
                            {
                                LastUserID03.Content = players[2].ID.ToString();
                                LastAnswer03.Content = players[2].Answer == null ? "" : " ABCD"[players[2].Answer.Value].ToString();
                                LastTime03.Content = (players[2].Time == null ? "0" : players[2].Time.Value.ToString("0.00")) + " s";
                                LastScore03.Content = (players[2].Score == null ? "0" : players[2].Score.ToString()) + " Đ";
                            }

                            if (players.Count > 3)
                            {
                                LastUserID04.Content = players[3].ID.ToString();
                                LastAnswer04.Content = players[3].Answer == null ? "" : " ABCD"[players[3].Answer.Value].ToString();
                                LastTime04.Content = (players[3].Time == null ? "0" : players[3].Time.Value.ToString("0.00")) + " s";
                                LastScore04.Content = (players[3].Score == null ? "0" : players[3].Score.ToString()) + " Đ";
                            }

                        }
                    }
                    else
                    {
                        alreadyShowed = false;
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.ToString());
            }
        }

        private void GenerateList(Grid leaderboard, List<UserModel> top)
        {
            var i = 0;
            var timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(0.1) };
            timer.Start();
            timer.Tick += (s, args) =>
            {
                if (i >= 10 || i >= top.Count)
                {
                    timer.Stop();
                }
                else
                {
                    var content = "";
                    content += (i + 1).ToString("D2").PadLeft(3);
                    content += (i < top.Count ? top[i].ID.ToString() : "").PadLeft(9).PadRight(10);
                    content += (i < top.Count ? (top[i].Score.HasValue ? top[i].Score.Value.ToString() : "0") : "").PadLeft(11);

                    FlipControl flip = leaderboard.Children.Cast<UIElement>().First(e => Grid.GetRow(e) == i + 1 && Grid.GetColumn(e) == 0) as FlipControl;
                    flip.StartFlipping(content);
                    ++i;

                    if (i < 5)
                    {
                        flip.SetColor(new SolidColorBrush(Color.FromRgb(0, 255, 24)));
                    }
                }
            };
        }
    }
}
