﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Game_Controller
{
    public partial class MainForm : Form
    {
        // Constants
        private readonly Color TopicFirstSelected = Color.Yellow;
        private readonly Color TopicSecondSelected = Color.Green;
        private readonly int[] Scores = { 150, 300, 450, 600, 750, 900, 1050, 1200, 1350, 1500, 1650, 1800, 1950, 2100, 2250, 2400 };

        // Variables
        private Random _randomizer = new Random();
        private int _currentQuestionNumber;
        private Question _currentQuestion;
        private ContestDay _currentContest;
        private List<Topic> _currentTopics;
        private int _currentTimer;
        private List<int> _scores = new List<int>();


        public MainForm()
        {
            InitializeComponent();
        }

        private void butTopic_Click(object sender, EventArgs e)
        {
            if (_currentContest == null)
            {
                MessageBox.Show("Không có cuộc thi nào diễn ra vào ngày hôm nay!");
                return;
            }

            if (_currentContest.State != 10002)
            {
                MessageBox.Show("Chưa hiển thị danh sách chủ đề để thí sinh chọn!");
                return;
            }

            if (_currentQuestionNumber >= _currentContest.NumberOfQuestions)
            {
                MessageBox.Show("Đã đủ số câu hỏi cho ngày thi hôm nay!");
                return;
            }

            Button topicButton = (Button)sender;

            // Choose one topic
            if (topicButton.BackColor == TopicSecondSelected)
            {
                // Choose twice
                return;
            }

            using (var entities = new Lasta_QTTTEntities())
            {
                var topicID = (int)topicButton.Tag;
                var questions = entities.Questions.Where(q => !q.IsDeleted && q.ContestDayID.HasValue == false && q.TopicID == topicID).ToList();
                if (questions.Count == 0)
                {
                    MessageBox.Show("Đã hết câu hỏi trong kho câu hỏi!");
                    return;
                }

                var scoreIndex = _randomizer.Next(0, _scores.Count);
                var index = _randomizer.Next(0, questions.Count);
                _currentQuestion = questions[index];

                ++_currentQuestionNumber;
                _currentQuestion.Number = _currentQuestionNumber;
                _currentQuestion.Score = _scores[scoreIndex];
                _scores.RemoveAt(scoreIndex);
                _currentQuestion.ContestDayID = _currentContest.ID;

                var contest = entities.ContestDays.FirstOrDefault(c => c.ID == _currentContest.ID);
                contest.State = _currentQuestionNumber * 10;
                contest.RemainTime = _currentQuestion.CountdownTime;
                entities.SaveChanges();

                _currentContest.State = _currentQuestionNumber * 10;
                _currentContest.RemainTime = _currentQuestion.CountdownTime;

                // Set question info
                lblQuestionNumber.Text = _currentQuestionNumber.ToString();
                lblRemainTime.Text = _currentQuestion.CountdownTime.ToString();
                var status = GetStatus(_currentContest.State);
                lblStatus.Text = status;

                lblTopic.Text = topicButton.Text;
                lblScore.Text = string.Format("{0}/{1}", _currentQuestion.Score, _currentQuestion.Difficulty);
                lblQuestion.Text = _currentQuestion.QuestionContent;
                lblAnswerA.Text = _currentQuestion.Answer1;
                lblAnswerB.Text = _currentQuestion.Answer2;
                lblAnswerC.Text = _currentQuestion.Answer3;
                lblAnswerD.Text = _currentQuestion.Answer4;

                if (topicButton.BackColor == TopicFirstSelected)
                {
                    topicButton.BackColor = TopicSecondSelected;
                }
                else if (topicButton.BackColor == TopicSecondSelected)
                {
                    topicButton.Enabled = false;
                }
                else
                {
                    topicButton.BackColor = TopicFirstSelected;
                }
            }
        }

        private void butShowTopics_Click(object sender, EventArgs e)
        {
            // Show topics for chosing
            if (_currentContest == null)
            {
                MessageBox.Show("Không có cuộc thi nào diễn ra vào ngày hôm nay!");
                return;
            }

            if (_currentContest.State != 10000 && _currentContest.State != 10001)
            {
                MessageBox.Show("Đang diễn ra 1 câu hỏi, không thể thay đổi chủ đề!");
                return;
            }

            using (var entities = new Lasta_QTTTEntities())
            {
                var contest = entities.ContestDays.FirstOrDefault(c => c.ID == _currentContest.ID);
                contest.State = 10002;
                entities.SaveChanges();

                _currentContest.State = 10002;
                var status = GetStatus(_currentContest.State);
                lblStatus.Text = status;
            }
        }

        private void butStartQuestion_Click(object sender, EventArgs e)
        {
            if (_currentContest == null || _currentQuestion == null)
            {
                return;
            }

            if (_currentContest.State % 10 != 0 || _currentContest.State / 10 < 1 || _currentContest.State / 10 >= 1000)
            {
                return;
            }

            using (var entities = new Lasta_QTTTEntities())
            {
                var contest = entities.ContestDays.FirstOrDefault(c => c.ID == _currentContest.ID);
                contest.State = _currentQuestionNumber * 10 + 1;
                entities.SaveChanges();

                _currentContest.State = _currentQuestionNumber * 10 + 1;
                var status = GetStatus(_currentContest.State);
                lblStatus.Text = status;
            }

            _currentTimer = 0;
            timer1.Enabled = true;
        }

        private void butShowCorrect_Click(object sender, EventArgs e)
        {
            if (_currentContest == null)
            {
                MessageBox.Show("Không có cuộc thi nào diễn ra vào ngày hôm nay!");
                return;
            }

            using (var entities = new Lasta_QTTTEntities())
            {
                var contest = entities.ContestDays.FirstOrDefault(c => c.ID == _currentContest.ID);
                contest.State = _currentQuestionNumber * 10 + 2;
                entities.SaveChanges();

                _currentContest.State = _currentQuestionNumber * 10 + 2;
                var status = GetStatus(_currentContest.State);
                lblStatus.Text = status;
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // Load initial data
            using (var entities = new Lasta_QTTTEntities())
            {
                _randomizer = new Random((int)DateTime.Now.Ticks);

                // Today question
                var startDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                var endDay = startDay.AddDays(1);

                _currentContest = entities.ContestDays.FirstOrDefault(c => !c.IsDeleted && c.StartTime >= startDay && c.StartTime < endDay);

                if (_currentContest != null)
                {
                    _scores = new List<int>();
                    _scores.AddRange(Scores);

                    var questions = entities.Questions.Where(q => !q.IsDeleted && q.ContestDayID == _currentContest.ID);
                    foreach (var q in questions)
                    {
                        _scores.Remove(q.Score);
                    }

                    _currentQuestionNumber = 0;
                    lblContestDate.Text = _currentContest.StartTime.ToShortDateString();
                    lblQuestionNumber.Text = (_currentContest.State >= 10 && _currentContest.State < 10000) ? (_currentContest.State / 10).ToString() : "";
                    lblRemainTime.Text = _currentContest.RemainTime.ToString();
                    var status = GetStatus(_currentContest.State);
                    lblStatus.Text = status;

                    if (_currentContest.State >= 10 && _currentContest.State < 10000 && _currentContest.RemainTime > 0)
                    {
                        _currentQuestionNumber = _currentContest.State / 10;
                        _currentQuestion = entities.Questions.FirstOrDefault(q => !q.IsDeleted && q.ContestDayID == _currentContest.ID && q.Number == _currentQuestionNumber);

                        if (_currentQuestion != null)
                        {
                            lblTopic.Text = _currentQuestion.Topic.TopicContent;
                            lblScore.Text = string.Format("{0}/{1}", _currentQuestion.Score, _currentQuestion.Difficulty);
                            lblQuestion.Text = _currentQuestion.QuestionContent;
                            lblAnswerA.Text = _currentQuestion.Answer1;
                            lblAnswerB.Text = _currentQuestion.Answer2;
                            lblAnswerC.Text = _currentQuestion.Answer3;
                            lblAnswerD.Text = _currentQuestion.Answer4;
                        }
                    }
                }


                // Topics
                _currentTopics = entities.Topics.Where(t => !t.IsDeleted).Take(8).ToList();
                if (_currentTopics.Count > 0)
                {
                    butTopic1.Text = _currentTopics[0].TopicContent;
                    butTopic1.Tag = _currentTopics[0].ID;
                }
                if (_currentTopics.Count > 1)
                {
                    butTopic2.Text = _currentTopics[1].TopicContent;
                    butTopic2.Tag = _currentTopics[1].ID;
                }
                if (_currentTopics.Count > 2)
                {
                    butTopic3.Text = _currentTopics[2].TopicContent;
                    butTopic3.Tag = _currentTopics[2].ID;
                }
                if (_currentTopics.Count > 3)
                {
                    butTopic4.Text = _currentTopics[3].TopicContent;
                    butTopic4.Tag = _currentTopics[3].ID;
                }
                if (_currentTopics.Count > 4)
                {
                    butTopic5.Text = _currentTopics[4].TopicContent;
                    butTopic5.Tag = _currentTopics[4].ID;
                }
                if (_currentTopics.Count > 5)
                {
                    butTopic6.Text = _currentTopics[5].TopicContent;
                    butTopic6.Tag = _currentTopics[5].ID;
                }
                if (_currentTopics.Count > 6)
                {
                    butTopic7.Text = _currentTopics[6].TopicContent;
                    butTopic7.Tag = _currentTopics[6].ID;
                }
                if (_currentTopics.Count > 7)
                {
                    butTopic8.Text = _currentTopics[7].TopicContent;
                    butTopic8.Tag = _currentTopics[7].ID;
                }
            }
        }

        private string GetStatus(int state)
        {
            switch (state)
            {
                case 10000:
                    return "Hiển thị thông tin giới thiệu";
                case 10001:
                    return "Hiển thị thông tin tổng kết cho câu hỏi";
                case 10002:
                    return "Chờ chọn chủ đề";
                case 10003:
                    return "Chương trình đã kết thúc";
            }

            var status = state % 10;
            switch (status)
            {
                case 0:
                    return "Hiển thị câu hỏi";
                case 1:
                    return "Bắt đầu trả lời câu hỏi";
                case 2:
                    return "Đang kiểm tra đáp án";
            }

            return string.Empty;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            _currentTimer += timer1.Interval;

            var remain = Math.Ceiling((_currentQuestion.CountdownTime * 1000 - _currentTimer) / 1000.0);
            if (remain <= 0)
            {
                lblRemainTime.Text = "0";
                timer1.Enabled = false;

                using (var entities = new Lasta_QTTTEntities())
                {
                    var contest = entities.ContestDays.FirstOrDefault(c => c.ID == _currentContest.ID);
                    contest.RemainTime = 0;
                    entities.SaveChanges();

                    _currentContest.RemainTime = 0;
                    var status = GetStatus(_currentContest.State);
                    lblStatus.Text = status;
                }
            }
            else
            {
                lblRemainTime.Text = remain.ToString();
                using (var entities = new Lasta_QTTTEntities())
                {
                    var contest = entities.ContestDays.FirstOrDefault(c => c.ID == _currentContest.ID);
                    contest.RemainTime = (int)remain;
                    entities.SaveChanges();

                    _currentContest.RemainTime = (int)remain;
                }
            }
        }

        private void butStopQuestion_Click(object sender, EventArgs e)
        {
            if (_currentContest == null)
            {
                MessageBox.Show("Không có cuộc thi nào diễn ra vào ngày hôm nay!");
                return;
            }

            using (var entities = new Lasta_QTTTEntities())
            {
                var contest = entities.ContestDays.FirstOrDefault(c => c.ID == _currentContest.ID);
                if (_currentQuestionNumber >= contest.NumberOfQuestions)
                {
                    contest.State = 10003;
                }
                else
                {
                    contest.State = 10001;
                }
                entities.SaveChanges();

                if (_currentQuestionNumber >= contest.NumberOfQuestions)
                {
                    _currentContest.State = 10003;
                }
                else
                {
                    _currentContest.State = 10001;
                }
                var status = GetStatus(_currentContest.State);
                lblStatus.Text = status;
            }
        }
    }
}
