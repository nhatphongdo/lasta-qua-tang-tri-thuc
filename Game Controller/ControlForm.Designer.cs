﻿namespace Game_Controller
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.lblContestDate = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblQuestionNumber = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.butShowCorrect = new System.Windows.Forms.Button();
            this.butStartQuestion = new System.Windows.Forms.Button();
            this.lblRemainTime = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.butShowTopics = new System.Windows.Forms.Button();
            this.butTopic8 = new System.Windows.Forms.Button();
            this.butTopic7 = new System.Windows.Forms.Button();
            this.butTopic6 = new System.Windows.Forms.Button();
            this.butTopic5 = new System.Windows.Forms.Button();
            this.butTopic4 = new System.Windows.Forms.Button();
            this.butTopic3 = new System.Windows.Forms.Button();
            this.butTopic2 = new System.Windows.Forms.Button();
            this.butTopic1 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.butUser45 = new System.Windows.Forms.Button();
            this.butUser25 = new System.Windows.Forms.Button();
            this.butUser35 = new System.Windows.Forms.Button();
            this.butUser15 = new System.Windows.Forms.Button();
            this.butUser05 = new System.Windows.Forms.Button();
            this.butUser49 = new System.Windows.Forms.Button();
            this.butUser29 = new System.Windows.Forms.Button();
            this.butUser39 = new System.Windows.Forms.Button();
            this.butUser19 = new System.Windows.Forms.Button();
            this.butUser50 = new System.Windows.Forms.Button();
            this.butUser30 = new System.Windows.Forms.Button();
            this.butUser40 = new System.Windows.Forms.Button();
            this.butUser20 = new System.Windows.Forms.Button();
            this.butUser10 = new System.Windows.Forms.Button();
            this.butUser09 = new System.Windows.Forms.Button();
            this.butUser44 = new System.Windows.Forms.Button();
            this.butUser24 = new System.Windows.Forms.Button();
            this.butUser34 = new System.Windows.Forms.Button();
            this.butUser14 = new System.Windows.Forms.Button();
            this.butUser04 = new System.Windows.Forms.Button();
            this.butUser48 = new System.Windows.Forms.Button();
            this.butUser28 = new System.Windows.Forms.Button();
            this.butUser38 = new System.Windows.Forms.Button();
            this.butUser18 = new System.Windows.Forms.Button();
            this.butUser08 = new System.Windows.Forms.Button();
            this.butUser43 = new System.Windows.Forms.Button();
            this.butUser23 = new System.Windows.Forms.Button();
            this.butUser33 = new System.Windows.Forms.Button();
            this.butUser13 = new System.Windows.Forms.Button();
            this.butUser03 = new System.Windows.Forms.Button();
            this.butUser47 = new System.Windows.Forms.Button();
            this.butUser27 = new System.Windows.Forms.Button();
            this.butUser37 = new System.Windows.Forms.Button();
            this.butUser17 = new System.Windows.Forms.Button();
            this.butUser07 = new System.Windows.Forms.Button();
            this.butUser42 = new System.Windows.Forms.Button();
            this.butUser22 = new System.Windows.Forms.Button();
            this.butUser32 = new System.Windows.Forms.Button();
            this.butUser12 = new System.Windows.Forms.Button();
            this.butUser02 = new System.Windows.Forms.Button();
            this.butUser46 = new System.Windows.Forms.Button();
            this.butUser26 = new System.Windows.Forms.Button();
            this.butUser36 = new System.Windows.Forms.Button();
            this.butUser16 = new System.Windows.Forms.Button();
            this.butUser06 = new System.Windows.Forms.Button();
            this.butUser41 = new System.Windows.Forms.Button();
            this.butUser21 = new System.Windows.Forms.Button();
            this.butUser31 = new System.Windows.Forms.Button();
            this.butUser11 = new System.Windows.Forms.Button();
            this.butUser01 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblScore = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblTopic = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblAnswerD = new System.Windows.Forms.Label();
            this.lblAnswerB = new System.Windows.Forms.Label();
            this.lblAnswerC = new System.Windows.Forms.Label();
            this.lblAnswerA = new System.Windows.Forms.Label();
            this.lblQuestion = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.butStopQuestion = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Chương trình ngày:";
            // 
            // lblContestDate
            // 
            this.lblContestDate.AutoSize = true;
            this.lblContestDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContestDate.Location = new System.Drawing.Point(163, 9);
            this.lblContestDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblContestDate.Name = "lblContestDate";
            this.lblContestDate.Size = new System.Drawing.Size(0, 20);
            this.lblContestDate.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(304, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Câu hỏi số:";
            // 
            // lblQuestionNumber
            // 
            this.lblQuestionNumber.AutoSize = true;
            this.lblQuestionNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuestionNumber.Location = new System.Drawing.Point(398, 9);
            this.lblQuestionNumber.Name = "lblQuestionNumber";
            this.lblQuestionNumber.Size = new System.Drawing.Size(0, 20);
            this.lblQuestionNumber.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(671, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Trạng thái:";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(761, 9);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 20);
            this.lblStatus.TabIndex = 5;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.butStopQuestion);
            this.groupBox1.Controls.Add(this.butShowCorrect);
            this.groupBox1.Controls.Add(this.butStartQuestion);
            this.groupBox1.Location = new System.Drawing.Point(3, 219);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(975, 93);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Điều khiển câu hỏi";
            // 
            // butShowCorrect
            // 
            this.butShowCorrect.Location = new System.Drawing.Point(238, 29);
            this.butShowCorrect.Name = "butShowCorrect";
            this.butShowCorrect.Size = new System.Drawing.Size(226, 54);
            this.butShowCorrect.TabIndex = 0;
            this.butShowCorrect.Text = "Hiển thị đáp án đúng";
            this.butShowCorrect.UseVisualStyleBackColor = true;
            this.butShowCorrect.Click += new System.EventHandler(this.butShowCorrect_Click);
            // 
            // butStartQuestion
            // 
            this.butStartQuestion.Location = new System.Drawing.Point(6, 29);
            this.butStartQuestion.Name = "butStartQuestion";
            this.butStartQuestion.Size = new System.Drawing.Size(226, 54);
            this.butStartQuestion.TabIndex = 0;
            this.butStartQuestion.Text = "Bắt đầu đếm giờ";
            this.butStartQuestion.UseVisualStyleBackColor = true;
            this.butStartQuestion.Click += new System.EventHandler(this.butStartQuestion_Click);
            // 
            // lblRemainTime
            // 
            this.lblRemainTime.AutoSize = true;
            this.lblRemainTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRemainTime.Location = new System.Drawing.Point(591, 9);
            this.lblRemainTime.Name = "lblRemainTime";
            this.lblRemainTime.Size = new System.Drawing.Size(0, 20);
            this.lblRemainTime.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(459, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 20);
            this.label6.TabIndex = 7;
            this.label6.Text = "Thời gian còn lại:";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.groupBox3);
            this.flowLayoutPanel1.Controls.Add(this.groupBox1);
            this.flowLayoutPanel1.Controls.Add(this.groupBox4);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(12, 255);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(973, 648);
            this.flowLayoutPanel1.TabIndex = 9;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.butShowTopics);
            this.groupBox3.Controls.Add(this.butTopic8);
            this.groupBox3.Controls.Add(this.butTopic7);
            this.groupBox3.Controls.Add(this.butTopic6);
            this.groupBox3.Controls.Add(this.butTopic5);
            this.groupBox3.Controls.Add(this.butTopic4);
            this.groupBox3.Controls.Add(this.butTopic3);
            this.groupBox3.Controls.Add(this.butTopic2);
            this.groupBox3.Controls.Add(this.butTopic1);
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(975, 210);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Điều khiển chủ đề";
            // 
            // butShowTopics
            // 
            this.butShowTopics.Location = new System.Drawing.Point(6, 25);
            this.butShowTopics.Name = "butShowTopics";
            this.butShowTopics.Size = new System.Drawing.Size(226, 54);
            this.butShowTopics.TabIndex = 0;
            this.butShowTopics.Text = "Hiển thị danh sách chủ đề";
            this.butShowTopics.UseVisualStyleBackColor = true;
            this.butShowTopics.Click += new System.EventHandler(this.butShowTopics_Click);
            // 
            // butTopic8
            // 
            this.butTopic8.Location = new System.Drawing.Point(702, 145);
            this.butTopic8.Name = "butTopic8";
            this.butTopic8.Size = new System.Drawing.Size(226, 54);
            this.butTopic8.TabIndex = 7;
            this.butTopic8.UseVisualStyleBackColor = true;
            this.butTopic8.Click += new System.EventHandler(this.butTopic_Click);
            // 
            // butTopic7
            // 
            this.butTopic7.Location = new System.Drawing.Point(470, 145);
            this.butTopic7.Name = "butTopic7";
            this.butTopic7.Size = new System.Drawing.Size(226, 54);
            this.butTopic7.TabIndex = 6;
            this.butTopic7.UseVisualStyleBackColor = true;
            this.butTopic7.Click += new System.EventHandler(this.butTopic_Click);
            // 
            // butTopic6
            // 
            this.butTopic6.Location = new System.Drawing.Point(238, 145);
            this.butTopic6.Name = "butTopic6";
            this.butTopic6.Size = new System.Drawing.Size(226, 54);
            this.butTopic6.TabIndex = 5;
            this.butTopic6.UseVisualStyleBackColor = true;
            this.butTopic6.Click += new System.EventHandler(this.butTopic_Click);
            // 
            // butTopic5
            // 
            this.butTopic5.Location = new System.Drawing.Point(6, 145);
            this.butTopic5.Name = "butTopic5";
            this.butTopic5.Size = new System.Drawing.Size(226, 54);
            this.butTopic5.TabIndex = 4;
            this.butTopic5.UseVisualStyleBackColor = true;
            this.butTopic5.Click += new System.EventHandler(this.butTopic_Click);
            // 
            // butTopic4
            // 
            this.butTopic4.Location = new System.Drawing.Point(702, 85);
            this.butTopic4.Name = "butTopic4";
            this.butTopic4.Size = new System.Drawing.Size(226, 54);
            this.butTopic4.TabIndex = 3;
            this.butTopic4.UseVisualStyleBackColor = true;
            this.butTopic4.Click += new System.EventHandler(this.butTopic_Click);
            // 
            // butTopic3
            // 
            this.butTopic3.Location = new System.Drawing.Point(470, 85);
            this.butTopic3.Name = "butTopic3";
            this.butTopic3.Size = new System.Drawing.Size(226, 54);
            this.butTopic3.TabIndex = 2;
            this.butTopic3.UseVisualStyleBackColor = true;
            this.butTopic3.Click += new System.EventHandler(this.butTopic_Click);
            // 
            // butTopic2
            // 
            this.butTopic2.Location = new System.Drawing.Point(238, 85);
            this.butTopic2.Name = "butTopic2";
            this.butTopic2.Size = new System.Drawing.Size(226, 54);
            this.butTopic2.TabIndex = 1;
            this.butTopic2.UseVisualStyleBackColor = true;
            this.butTopic2.Click += new System.EventHandler(this.butTopic_Click);
            // 
            // butTopic1
            // 
            this.butTopic1.Location = new System.Drawing.Point(6, 85);
            this.butTopic1.Name = "butTopic1";
            this.butTopic1.Size = new System.Drawing.Size(226, 54);
            this.butTopic1.TabIndex = 0;
            this.butTopic1.UseVisualStyleBackColor = true;
            this.butTopic1.Click += new System.EventHandler(this.butTopic_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.butUser45);
            this.groupBox4.Controls.Add(this.butUser25);
            this.groupBox4.Controls.Add(this.butUser35);
            this.groupBox4.Controls.Add(this.butUser15);
            this.groupBox4.Controls.Add(this.butUser05);
            this.groupBox4.Controls.Add(this.butUser49);
            this.groupBox4.Controls.Add(this.butUser29);
            this.groupBox4.Controls.Add(this.butUser39);
            this.groupBox4.Controls.Add(this.butUser19);
            this.groupBox4.Controls.Add(this.butUser50);
            this.groupBox4.Controls.Add(this.butUser30);
            this.groupBox4.Controls.Add(this.butUser40);
            this.groupBox4.Controls.Add(this.butUser20);
            this.groupBox4.Controls.Add(this.butUser10);
            this.groupBox4.Controls.Add(this.butUser09);
            this.groupBox4.Controls.Add(this.butUser44);
            this.groupBox4.Controls.Add(this.butUser24);
            this.groupBox4.Controls.Add(this.butUser34);
            this.groupBox4.Controls.Add(this.butUser14);
            this.groupBox4.Controls.Add(this.butUser04);
            this.groupBox4.Controls.Add(this.butUser48);
            this.groupBox4.Controls.Add(this.butUser28);
            this.groupBox4.Controls.Add(this.butUser38);
            this.groupBox4.Controls.Add(this.butUser18);
            this.groupBox4.Controls.Add(this.butUser08);
            this.groupBox4.Controls.Add(this.butUser43);
            this.groupBox4.Controls.Add(this.butUser23);
            this.groupBox4.Controls.Add(this.butUser33);
            this.groupBox4.Controls.Add(this.butUser13);
            this.groupBox4.Controls.Add(this.butUser03);
            this.groupBox4.Controls.Add(this.butUser47);
            this.groupBox4.Controls.Add(this.butUser27);
            this.groupBox4.Controls.Add(this.butUser37);
            this.groupBox4.Controls.Add(this.butUser17);
            this.groupBox4.Controls.Add(this.butUser07);
            this.groupBox4.Controls.Add(this.butUser42);
            this.groupBox4.Controls.Add(this.butUser22);
            this.groupBox4.Controls.Add(this.butUser32);
            this.groupBox4.Controls.Add(this.butUser12);
            this.groupBox4.Controls.Add(this.butUser02);
            this.groupBox4.Controls.Add(this.butUser46);
            this.groupBox4.Controls.Add(this.butUser26);
            this.groupBox4.Controls.Add(this.butUser36);
            this.groupBox4.Controls.Add(this.butUser16);
            this.groupBox4.Controls.Add(this.butUser06);
            this.groupBox4.Controls.Add(this.butUser41);
            this.groupBox4.Controls.Add(this.butUser21);
            this.groupBox4.Controls.Add(this.butUser31);
            this.groupBox4.Controls.Add(this.butUser11);
            this.groupBox4.Controls.Add(this.butUser01);
            this.groupBox4.Location = new System.Drawing.Point(3, 318);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(975, 330);
            this.groupBox4.TabIndex = 8;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Điều khiển người chơi";
            // 
            // butUser45
            // 
            this.butUser45.Location = new System.Drawing.Point(389, 267);
            this.butUser45.Name = "butUser45";
            this.butUser45.Size = new System.Drawing.Size(90, 54);
            this.butUser45.TabIndex = 1;
            this.butUser45.UseVisualStyleBackColor = true;
            // 
            // butUser25
            // 
            this.butUser25.Location = new System.Drawing.Point(390, 147);
            this.butUser25.Name = "butUser25";
            this.butUser25.Size = new System.Drawing.Size(90, 54);
            this.butUser25.TabIndex = 1;
            this.butUser25.UseVisualStyleBackColor = true;
            // 
            // butUser35
            // 
            this.butUser35.Location = new System.Drawing.Point(390, 207);
            this.butUser35.Name = "butUser35";
            this.butUser35.Size = new System.Drawing.Size(90, 54);
            this.butUser35.TabIndex = 1;
            this.butUser35.UseVisualStyleBackColor = true;
            // 
            // butUser15
            // 
            this.butUser15.Location = new System.Drawing.Point(391, 87);
            this.butUser15.Name = "butUser15";
            this.butUser15.Size = new System.Drawing.Size(90, 54);
            this.butUser15.TabIndex = 1;
            this.butUser15.UseVisualStyleBackColor = true;
            // 
            // butUser05
            // 
            this.butUser05.Location = new System.Drawing.Point(390, 27);
            this.butUser05.Name = "butUser05";
            this.butUser05.Size = new System.Drawing.Size(90, 54);
            this.butUser05.TabIndex = 1;
            this.butUser05.UseVisualStyleBackColor = true;
            // 
            // butUser49
            // 
            this.butUser49.Location = new System.Drawing.Point(773, 267);
            this.butUser49.Name = "butUser49";
            this.butUser49.Size = new System.Drawing.Size(90, 54);
            this.butUser49.TabIndex = 1;
            this.butUser49.UseVisualStyleBackColor = true;
            // 
            // butUser29
            // 
            this.butUser29.Location = new System.Drawing.Point(774, 147);
            this.butUser29.Name = "butUser29";
            this.butUser29.Size = new System.Drawing.Size(90, 54);
            this.butUser29.TabIndex = 1;
            this.butUser29.UseVisualStyleBackColor = true;
            // 
            // butUser39
            // 
            this.butUser39.Location = new System.Drawing.Point(774, 207);
            this.butUser39.Name = "butUser39";
            this.butUser39.Size = new System.Drawing.Size(90, 54);
            this.butUser39.TabIndex = 1;
            this.butUser39.UseVisualStyleBackColor = true;
            // 
            // butUser19
            // 
            this.butUser19.Location = new System.Drawing.Point(775, 87);
            this.butUser19.Name = "butUser19";
            this.butUser19.Size = new System.Drawing.Size(90, 54);
            this.butUser19.TabIndex = 1;
            this.butUser19.UseVisualStyleBackColor = true;
            // 
            // butUser50
            // 
            this.butUser50.Location = new System.Drawing.Point(869, 267);
            this.butUser50.Name = "butUser50";
            this.butUser50.Size = new System.Drawing.Size(90, 54);
            this.butUser50.TabIndex = 1;
            this.butUser50.UseVisualStyleBackColor = true;
            // 
            // butUser30
            // 
            this.butUser30.Location = new System.Drawing.Point(870, 147);
            this.butUser30.Name = "butUser30";
            this.butUser30.Size = new System.Drawing.Size(90, 54);
            this.butUser30.TabIndex = 1;
            this.butUser30.UseVisualStyleBackColor = true;
            // 
            // butUser40
            // 
            this.butUser40.Location = new System.Drawing.Point(870, 207);
            this.butUser40.Name = "butUser40";
            this.butUser40.Size = new System.Drawing.Size(90, 54);
            this.butUser40.TabIndex = 1;
            this.butUser40.UseVisualStyleBackColor = true;
            // 
            // butUser20
            // 
            this.butUser20.Location = new System.Drawing.Point(871, 87);
            this.butUser20.Name = "butUser20";
            this.butUser20.Size = new System.Drawing.Size(90, 54);
            this.butUser20.TabIndex = 1;
            this.butUser20.UseVisualStyleBackColor = true;
            // 
            // butUser10
            // 
            this.butUser10.Location = new System.Drawing.Point(871, 27);
            this.butUser10.Name = "butUser10";
            this.butUser10.Size = new System.Drawing.Size(90, 54);
            this.butUser10.TabIndex = 1;
            this.butUser10.UseVisualStyleBackColor = true;
            // 
            // butUser09
            // 
            this.butUser09.Location = new System.Drawing.Point(774, 27);
            this.butUser09.Name = "butUser09";
            this.butUser09.Size = new System.Drawing.Size(90, 54);
            this.butUser09.TabIndex = 1;
            this.butUser09.UseVisualStyleBackColor = true;
            // 
            // butUser44
            // 
            this.butUser44.Location = new System.Drawing.Point(293, 267);
            this.butUser44.Name = "butUser44";
            this.butUser44.Size = new System.Drawing.Size(90, 54);
            this.butUser44.TabIndex = 1;
            this.butUser44.UseVisualStyleBackColor = true;
            // 
            // butUser24
            // 
            this.butUser24.Location = new System.Drawing.Point(294, 147);
            this.butUser24.Name = "butUser24";
            this.butUser24.Size = new System.Drawing.Size(90, 54);
            this.butUser24.TabIndex = 1;
            this.butUser24.UseVisualStyleBackColor = true;
            // 
            // butUser34
            // 
            this.butUser34.Location = new System.Drawing.Point(294, 207);
            this.butUser34.Name = "butUser34";
            this.butUser34.Size = new System.Drawing.Size(90, 54);
            this.butUser34.TabIndex = 1;
            this.butUser34.UseVisualStyleBackColor = true;
            // 
            // butUser14
            // 
            this.butUser14.Location = new System.Drawing.Point(295, 87);
            this.butUser14.Name = "butUser14";
            this.butUser14.Size = new System.Drawing.Size(90, 54);
            this.butUser14.TabIndex = 1;
            this.butUser14.UseVisualStyleBackColor = true;
            // 
            // butUser04
            // 
            this.butUser04.Location = new System.Drawing.Point(294, 27);
            this.butUser04.Name = "butUser04";
            this.butUser04.Size = new System.Drawing.Size(90, 54);
            this.butUser04.TabIndex = 1;
            this.butUser04.UseVisualStyleBackColor = true;
            // 
            // butUser48
            // 
            this.butUser48.Location = new System.Drawing.Point(677, 267);
            this.butUser48.Name = "butUser48";
            this.butUser48.Size = new System.Drawing.Size(90, 54);
            this.butUser48.TabIndex = 1;
            this.butUser48.UseVisualStyleBackColor = true;
            // 
            // butUser28
            // 
            this.butUser28.Location = new System.Drawing.Point(678, 147);
            this.butUser28.Name = "butUser28";
            this.butUser28.Size = new System.Drawing.Size(90, 54);
            this.butUser28.TabIndex = 1;
            this.butUser28.UseVisualStyleBackColor = true;
            // 
            // butUser38
            // 
            this.butUser38.Location = new System.Drawing.Point(678, 207);
            this.butUser38.Name = "butUser38";
            this.butUser38.Size = new System.Drawing.Size(90, 54);
            this.butUser38.TabIndex = 1;
            this.butUser38.UseVisualStyleBackColor = true;
            // 
            // butUser18
            // 
            this.butUser18.Location = new System.Drawing.Point(679, 87);
            this.butUser18.Name = "butUser18";
            this.butUser18.Size = new System.Drawing.Size(90, 54);
            this.butUser18.TabIndex = 1;
            this.butUser18.UseVisualStyleBackColor = true;
            // 
            // butUser08
            // 
            this.butUser08.Location = new System.Drawing.Point(678, 27);
            this.butUser08.Name = "butUser08";
            this.butUser08.Size = new System.Drawing.Size(90, 54);
            this.butUser08.TabIndex = 1;
            this.butUser08.UseVisualStyleBackColor = true;
            // 
            // butUser43
            // 
            this.butUser43.Location = new System.Drawing.Point(197, 267);
            this.butUser43.Name = "butUser43";
            this.butUser43.Size = new System.Drawing.Size(90, 54);
            this.butUser43.TabIndex = 1;
            this.butUser43.UseVisualStyleBackColor = true;
            // 
            // butUser23
            // 
            this.butUser23.Location = new System.Drawing.Point(198, 147);
            this.butUser23.Name = "butUser23";
            this.butUser23.Size = new System.Drawing.Size(90, 54);
            this.butUser23.TabIndex = 1;
            this.butUser23.UseVisualStyleBackColor = true;
            // 
            // butUser33
            // 
            this.butUser33.Location = new System.Drawing.Point(198, 207);
            this.butUser33.Name = "butUser33";
            this.butUser33.Size = new System.Drawing.Size(90, 54);
            this.butUser33.TabIndex = 1;
            this.butUser33.UseVisualStyleBackColor = true;
            // 
            // butUser13
            // 
            this.butUser13.Location = new System.Drawing.Point(199, 87);
            this.butUser13.Name = "butUser13";
            this.butUser13.Size = new System.Drawing.Size(90, 54);
            this.butUser13.TabIndex = 1;
            this.butUser13.UseVisualStyleBackColor = true;
            // 
            // butUser03
            // 
            this.butUser03.Location = new System.Drawing.Point(198, 27);
            this.butUser03.Name = "butUser03";
            this.butUser03.Size = new System.Drawing.Size(90, 54);
            this.butUser03.TabIndex = 1;
            this.butUser03.UseVisualStyleBackColor = true;
            // 
            // butUser47
            // 
            this.butUser47.Location = new System.Drawing.Point(581, 267);
            this.butUser47.Name = "butUser47";
            this.butUser47.Size = new System.Drawing.Size(90, 54);
            this.butUser47.TabIndex = 1;
            this.butUser47.UseVisualStyleBackColor = true;
            // 
            // butUser27
            // 
            this.butUser27.Location = new System.Drawing.Point(582, 147);
            this.butUser27.Name = "butUser27";
            this.butUser27.Size = new System.Drawing.Size(90, 54);
            this.butUser27.TabIndex = 1;
            this.butUser27.UseVisualStyleBackColor = true;
            // 
            // butUser37
            // 
            this.butUser37.Location = new System.Drawing.Point(582, 207);
            this.butUser37.Name = "butUser37";
            this.butUser37.Size = new System.Drawing.Size(90, 54);
            this.butUser37.TabIndex = 1;
            this.butUser37.UseVisualStyleBackColor = true;
            // 
            // butUser17
            // 
            this.butUser17.Location = new System.Drawing.Point(583, 87);
            this.butUser17.Name = "butUser17";
            this.butUser17.Size = new System.Drawing.Size(90, 54);
            this.butUser17.TabIndex = 1;
            this.butUser17.UseVisualStyleBackColor = true;
            // 
            // butUser07
            // 
            this.butUser07.Location = new System.Drawing.Point(582, 27);
            this.butUser07.Name = "butUser07";
            this.butUser07.Size = new System.Drawing.Size(90, 54);
            this.butUser07.TabIndex = 1;
            this.butUser07.UseVisualStyleBackColor = true;
            // 
            // butUser42
            // 
            this.butUser42.Location = new System.Drawing.Point(101, 267);
            this.butUser42.Name = "butUser42";
            this.butUser42.Size = new System.Drawing.Size(90, 54);
            this.butUser42.TabIndex = 1;
            this.butUser42.UseVisualStyleBackColor = true;
            // 
            // butUser22
            // 
            this.butUser22.Location = new System.Drawing.Point(102, 147);
            this.butUser22.Name = "butUser22";
            this.butUser22.Size = new System.Drawing.Size(90, 54);
            this.butUser22.TabIndex = 1;
            this.butUser22.UseVisualStyleBackColor = true;
            // 
            // butUser32
            // 
            this.butUser32.Location = new System.Drawing.Point(102, 207);
            this.butUser32.Name = "butUser32";
            this.butUser32.Size = new System.Drawing.Size(90, 54);
            this.butUser32.TabIndex = 1;
            this.butUser32.UseVisualStyleBackColor = true;
            // 
            // butUser12
            // 
            this.butUser12.Location = new System.Drawing.Point(103, 87);
            this.butUser12.Name = "butUser12";
            this.butUser12.Size = new System.Drawing.Size(90, 54);
            this.butUser12.TabIndex = 1;
            this.butUser12.UseVisualStyleBackColor = true;
            // 
            // butUser02
            // 
            this.butUser02.Location = new System.Drawing.Point(102, 27);
            this.butUser02.Name = "butUser02";
            this.butUser02.Size = new System.Drawing.Size(90, 54);
            this.butUser02.TabIndex = 1;
            this.butUser02.UseVisualStyleBackColor = true;
            // 
            // butUser46
            // 
            this.butUser46.Location = new System.Drawing.Point(485, 267);
            this.butUser46.Name = "butUser46";
            this.butUser46.Size = new System.Drawing.Size(90, 54);
            this.butUser46.TabIndex = 1;
            this.butUser46.UseVisualStyleBackColor = true;
            // 
            // butUser26
            // 
            this.butUser26.Location = new System.Drawing.Point(486, 147);
            this.butUser26.Name = "butUser26";
            this.butUser26.Size = new System.Drawing.Size(90, 54);
            this.butUser26.TabIndex = 1;
            this.butUser26.UseVisualStyleBackColor = true;
            // 
            // butUser36
            // 
            this.butUser36.Location = new System.Drawing.Point(486, 207);
            this.butUser36.Name = "butUser36";
            this.butUser36.Size = new System.Drawing.Size(90, 54);
            this.butUser36.TabIndex = 1;
            this.butUser36.UseVisualStyleBackColor = true;
            // 
            // butUser16
            // 
            this.butUser16.Location = new System.Drawing.Point(487, 87);
            this.butUser16.Name = "butUser16";
            this.butUser16.Size = new System.Drawing.Size(90, 54);
            this.butUser16.TabIndex = 1;
            this.butUser16.UseVisualStyleBackColor = true;
            // 
            // butUser06
            // 
            this.butUser06.Location = new System.Drawing.Point(486, 27);
            this.butUser06.Name = "butUser06";
            this.butUser06.Size = new System.Drawing.Size(90, 54);
            this.butUser06.TabIndex = 1;
            this.butUser06.UseVisualStyleBackColor = true;
            // 
            // butUser41
            // 
            this.butUser41.Location = new System.Drawing.Point(5, 267);
            this.butUser41.Name = "butUser41";
            this.butUser41.Size = new System.Drawing.Size(90, 54);
            this.butUser41.TabIndex = 1;
            this.butUser41.UseVisualStyleBackColor = true;
            // 
            // butUser21
            // 
            this.butUser21.Location = new System.Drawing.Point(6, 147);
            this.butUser21.Name = "butUser21";
            this.butUser21.Size = new System.Drawing.Size(90, 54);
            this.butUser21.TabIndex = 1;
            this.butUser21.UseVisualStyleBackColor = true;
            // 
            // butUser31
            // 
            this.butUser31.Location = new System.Drawing.Point(6, 207);
            this.butUser31.Name = "butUser31";
            this.butUser31.Size = new System.Drawing.Size(90, 54);
            this.butUser31.TabIndex = 1;
            this.butUser31.UseVisualStyleBackColor = true;
            // 
            // butUser11
            // 
            this.butUser11.Location = new System.Drawing.Point(7, 87);
            this.butUser11.Name = "butUser11";
            this.butUser11.Size = new System.Drawing.Size(90, 54);
            this.butUser11.TabIndex = 1;
            this.butUser11.UseVisualStyleBackColor = true;
            // 
            // butUser01
            // 
            this.butUser01.Location = new System.Drawing.Point(6, 27);
            this.butUser01.Name = "butUser01";
            this.butUser01.Size = new System.Drawing.Size(90, 54);
            this.butUser01.TabIndex = 1;
            this.butUser01.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.lblScore);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.lblTopic);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.lblAnswerD);
            this.groupBox2.Controls.Add(this.lblAnswerB);
            this.groupBox2.Controls.Add(this.lblAnswerC);
            this.groupBox2.Controls.Add(this.lblAnswerA);
            this.groupBox2.Controls.Add(this.lblQuestion);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Location = new System.Drawing.Point(12, 41);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(961, 208);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Thông tin hiện tại";
            // 
            // lblScore
            // 
            this.lblScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScore.Location = new System.Drawing.Point(697, 29);
            this.lblScore.Name = "lblScore";
            this.lblScore.Size = new System.Drawing.Size(261, 20);
            this.lblScore.TabIndex = 9;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(557, 29);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(134, 20);
            this.label11.TabIndex = 8;
            this.label11.Text = "Điểm số / Độ khó:";
            // 
            // lblTopic
            // 
            this.lblTopic.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTopic.Location = new System.Drawing.Point(146, 29);
            this.lblTopic.Name = "lblTopic";
            this.lblTopic.Size = new System.Drawing.Size(387, 20);
            this.lblTopic.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 29);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 20);
            this.label9.TabIndex = 6;
            this.label9.Text = "Chủ đề:";
            // 
            // lblAnswerD
            // 
            this.lblAnswerD.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAnswerD.Location = new System.Drawing.Point(553, 166);
            this.lblAnswerD.Name = "lblAnswerD";
            this.lblAnswerD.Size = new System.Drawing.Size(405, 32);
            this.lblAnswerD.TabIndex = 5;
            // 
            // lblAnswerB
            // 
            this.lblAnswerB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAnswerB.Location = new System.Drawing.Point(553, 132);
            this.lblAnswerB.Name = "lblAnswerB";
            this.lblAnswerB.Size = new System.Drawing.Size(405, 32);
            this.lblAnswerB.TabIndex = 4;
            // 
            // lblAnswerC
            // 
            this.lblAnswerC.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAnswerC.Location = new System.Drawing.Point(146, 166);
            this.lblAnswerC.Name = "lblAnswerC";
            this.lblAnswerC.Size = new System.Drawing.Size(405, 32);
            this.lblAnswerC.TabIndex = 3;
            // 
            // lblAnswerA
            // 
            this.lblAnswerA.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAnswerA.Location = new System.Drawing.Point(146, 132);
            this.lblAnswerA.Name = "lblAnswerA";
            this.lblAnswerA.Size = new System.Drawing.Size(405, 32);
            this.lblAnswerA.TabIndex = 2;
            // 
            // lblQuestion
            // 
            this.lblQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQuestion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuestion.Location = new System.Drawing.Point(146, 61);
            this.lblQuestion.Name = "lblQuestion";
            this.lblQuestion.Size = new System.Drawing.Size(809, 62);
            this.lblQuestion.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 132);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 20);
            this.label8.TabIndex = 0;
            this.label8.Text = "Đáp án:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 61);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(131, 20);
            this.label7.TabIndex = 0;
            this.label7.Text = "Nội dung câu hỏi:";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // butStopQuestion
            // 
            this.butStopQuestion.Location = new System.Drawing.Point(470, 29);
            this.butStopQuestion.Name = "butStopQuestion";
            this.butStopQuestion.Size = new System.Drawing.Size(226, 54);
            this.butStopQuestion.TabIndex = 1;
            this.butStopQuestion.Text = "Kết thúc câu hỏi";
            this.butStopQuestion.UseVisualStyleBackColor = true;
            this.butStopQuestion.Click += new System.EventHandler(this.butStopQuestion_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1081, 741);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.lblRemainTime);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblQuestionNumber);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblContestDate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chương trình điều khiển";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblContestDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblQuestionNumber;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblRemainTime;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button butStartQuestion;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button butTopic8;
        private System.Windows.Forms.Button butTopic7;
        private System.Windows.Forms.Button butTopic6;
        private System.Windows.Forms.Button butTopic5;
        private System.Windows.Forms.Button butTopic4;
        private System.Windows.Forms.Button butTopic3;
        private System.Windows.Forms.Button butTopic2;
        private System.Windows.Forms.Button butTopic1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblQuestion;
        private System.Windows.Forms.Label lblAnswerD;
        private System.Windows.Forms.Label lblAnswerB;
        private System.Windows.Forms.Label lblAnswerC;
        private System.Windows.Forms.Label lblAnswerA;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblScore;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblTopic;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button butShowTopics;
        private System.Windows.Forms.Button butShowCorrect;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button butUser01;
        private System.Windows.Forms.Button butUser45;
        private System.Windows.Forms.Button butUser25;
        private System.Windows.Forms.Button butUser35;
        private System.Windows.Forms.Button butUser15;
        private System.Windows.Forms.Button butUser05;
        private System.Windows.Forms.Button butUser49;
        private System.Windows.Forms.Button butUser29;
        private System.Windows.Forms.Button butUser39;
        private System.Windows.Forms.Button butUser19;
        private System.Windows.Forms.Button butUser50;
        private System.Windows.Forms.Button butUser30;
        private System.Windows.Forms.Button butUser40;
        private System.Windows.Forms.Button butUser20;
        private System.Windows.Forms.Button butUser10;
        private System.Windows.Forms.Button butUser09;
        private System.Windows.Forms.Button butUser44;
        private System.Windows.Forms.Button butUser24;
        private System.Windows.Forms.Button butUser34;
        private System.Windows.Forms.Button butUser14;
        private System.Windows.Forms.Button butUser04;
        private System.Windows.Forms.Button butUser48;
        private System.Windows.Forms.Button butUser28;
        private System.Windows.Forms.Button butUser38;
        private System.Windows.Forms.Button butUser18;
        private System.Windows.Forms.Button butUser08;
        private System.Windows.Forms.Button butUser43;
        private System.Windows.Forms.Button butUser23;
        private System.Windows.Forms.Button butUser33;
        private System.Windows.Forms.Button butUser13;
        private System.Windows.Forms.Button butUser03;
        private System.Windows.Forms.Button butUser47;
        private System.Windows.Forms.Button butUser27;
        private System.Windows.Forms.Button butUser37;
        private System.Windows.Forms.Button butUser17;
        private System.Windows.Forms.Button butUser07;
        private System.Windows.Forms.Button butUser42;
        private System.Windows.Forms.Button butUser22;
        private System.Windows.Forms.Button butUser32;
        private System.Windows.Forms.Button butUser12;
        private System.Windows.Forms.Button butUser02;
        private System.Windows.Forms.Button butUser46;
        private System.Windows.Forms.Button butUser26;
        private System.Windows.Forms.Button butUser36;
        private System.Windows.Forms.Button butUser16;
        private System.Windows.Forms.Button butUser06;
        private System.Windows.Forms.Button butUser41;
        private System.Windows.Forms.Button butUser21;
        private System.Windows.Forms.Button butUser31;
        private System.Windows.Forms.Button butUser11;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button butStopQuestion;
    }
}

