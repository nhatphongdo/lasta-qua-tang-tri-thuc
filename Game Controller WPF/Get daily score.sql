/****** Script for SelectTopNRows command from SSMS  ******/
SELECT UserID, Sum(Score) AS Score, [CungLaTyPhu].[dbo].[HomeUser].ID, Fullname, PhoneNumber, [Address], Email
  FROM [CungLaTyPhu].[dbo].[HomeAnswer]
  LEFT JOIN [CungLaTyPhu].[dbo].[HomeUser] ON [CungLaTyPhu].[dbo].[HomeAnswer].UserID = [CungLaTyPhu].[dbo].[HomeUser].ID
  WHERE ContestDayID = 20
  GROUP BY UserID, [CungLaTyPhu].[dbo].[HomeUser].ID, Fullname, PhoneNumber, [Address], Email
  ORDER BY Score DESC