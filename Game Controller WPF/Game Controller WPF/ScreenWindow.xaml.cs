﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Game_Controller_WPF
{
    /// <summary>
    /// Interaction logic for ScreenWindow.xaml
    /// </summary>
    public partial class ScreenWindow : Window
    {
        private const double VideoOriginalWidth = 720.0;
        private const double VideoOriginalHeight = 576.0;

        private WPFMediaKit.DirectShow.Controls.MediaUriElement bufferMediaPlayer;

        private Dictionary<string, bool> _topics;

        public ScreenWindow()
        {
            InitializeComponent();
        }

        #region Scrolling Score
        private Random _random = new Random((int)DateTime.Now.Ticks);
        private const int MaxScrollingTime = 3000;
        private int _timer;
        private string _stopNumber;
        private List<int> _speeds;
        private List<bool> _stopped;
        private System.Windows.Threading.DispatcherTimer _scoreTimer;

        private Action _completedFunc = null;

        private void ScoreAnimation(object sender, EventArgs e)
        {
            _timer += _scoreTimer.Interval.Milliseconds;
            if (_timer >= 1000)
            {
                if (_stopped[0] == false)
                {
                    _stopped[0] = MoveTextBlock(((TextBlock)ScoreCanvas.Children[0]), ((TextBlock)ScoreCanvas.Children[1]), _speeds[0], _stopNumber[0].ToString());
                }
                if (_stopped[1] == false)
                {
                    _stopped[1] = MoveTextBlock(((TextBlock)ScoreCanvas.Children[2]), ((TextBlock)ScoreCanvas.Children[3]), _speeds[1], _stopNumber[1].ToString());
                }
                if (_stopped[2] == false)
                {
                    _stopped[2] = MoveTextBlock(((TextBlock)ScoreCanvas.Children[4]), ((TextBlock)ScoreCanvas.Children[5]), _speeds[2], _stopNumber[2].ToString());
                }
            }

            if (_stopped[0] == true && _stopped[1] == true && _stopped[2] == true)
            {
                _scoreTimer.Stop();
                if (_completedFunc != null)
                {
                    _completedFunc();
                }
            }
        }

        private bool MoveTextBlock(TextBlock block1, TextBlock block2, int speed, string comparedNumber)
        {
            var stop = false;

            var top1 = Canvas.GetTop(block1);
            var top2 = Canvas.GetTop(block2);

            var curSpeed = Math.Max(5, Math.Min(speed, speed - (_timer - MaxScrollingTime) * (float)(speed - 5) / 2000));
            top1 -= curSpeed;
            top2 -= curSpeed;

            if (top2 <= 10)
            {
                // Below reached top
                block1.Text = block2.Text;
                var number = int.Parse(block2.Text);
                number = number == 0 ? 9 : number - 1;
                block2.Text = number.ToString();

                if (curSpeed <= 10 && block1.Text == comparedNumber)
                {
                    top2 = 10;
                    stop = true;
                }

                top1 = top2;
                top2 = top1 + block1.Height + 10;
            }

            Canvas.SetTop(block1, top1);
            Canvas.SetTop(block2, top2);

            return stop;
        }

        private void StartScrollingNumber(int number)
        {
            _stopped = new List<bool>();
            _stopped.Add(false);
            _stopped.Add(false);
            _stopped.Add(false);

            // Generate speeds
            _speeds = new List<int>();
            _speeds.Add(10 * _random.Next(6, 11));
            _speeds.Add(10 * _random.Next(6, 11));
            _speeds.Add(10 * _random.Next(6, 11));

            _timer = 0;
            _stopNumber = number.ToString("D3");

            _scoreTimer = new System.Windows.Threading.DispatcherTimer();
            _scoreTimer.Tick += new EventHandler(ScoreAnimation);
            _scoreTimer.Interval = new TimeSpan(0, 0, 0, 0, 20);
            _scoreTimer.Start();
        }
        #endregion

        public void ChangeToFullScreen()
        {
            // Get secondary screen
            var screen = System.Windows.Forms.Screen.PrimaryScreen;
            foreach (var s in System.Windows.Forms.Screen.AllScreens)
            {
                if (s.Primary == false)
                {
                    screen = s;
                    break;
                }
            }

            var screenWidth = (double)screen.Bounds.Width;
            var screenHeight = (double)screen.Bounds.Height;
            var ratioWidth = screenWidth / VideoOriginalWidth;
            var ratioHeight = screenHeight / VideoOriginalHeight;
            MainGrid.LayoutTransform = new ScaleTransform(ratioWidth, ratioHeight);
            this.Left = screen.Bounds.X;
            this.Top = screen.Bounds.Y;
            this.Width = screen.Bounds.Width;
            this.Height = screen.Bounds.Height;
        }

        public void ShowTopics(Dictionary<string, bool> topics)
        {
            bufferMediaPlayer.Tag = 0;
            var videoWidth = BackgroundVideo.ActualWidth;
            var videoHeight = BackgroundVideo.ActualHeight;
            var ratioWidth = videoWidth / VideoOriginalWidth;
            var ratioHeight = videoHeight / VideoOriginalHeight;
            var ratio = Math.Min(ratioWidth, ratioHeight);
            var distanceX = (videoWidth - VideoOriginalWidth * ratio) / 2;

            Topic1.Visibility = System.Windows.Visibility.Hidden;
            Topic1.Margin = new Thickness(distanceX + 79 * ratio, 280 * ratio, 0, 0);
            Topic1.Width = 97 * ratio;
            Topic1.Height = 127 * ratio;
            Topic2.Visibility = System.Windows.Visibility.Hidden;
            Topic2.Margin = new Thickness(distanceX + 236 * ratio, 280 * ratio, 0, 0);
            Topic2.Width = 97 * ratio;
            Topic2.Height = 127 * ratio;
            Topic3.Visibility = System.Windows.Visibility.Hidden;
            Topic3.Margin = new Thickness(distanceX + 391 * ratio, 280 * ratio, 0, 0);
            Topic3.Width = 97 * ratio;
            Topic3.Height = 127 * ratio;
            Topic4.Visibility = System.Windows.Visibility.Hidden;
            Topic4.Margin = new Thickness(distanceX + 546 * ratio, 280 * ratio, 0, 0);
            Topic4.Width = 97 * ratio;
            Topic4.Height = 127 * ratio;
            Topic5.Visibility = System.Windows.Visibility.Hidden;
            Topic5.Margin = new Thickness(distanceX + 79 * ratio, 400 * ratio, 0, 0);
            Topic5.Width = 97 * ratio;
            Topic5.Height = 127 * ratio;
            Topic6.Visibility = System.Windows.Visibility.Hidden;
            Topic6.Margin = new Thickness(distanceX + 236 * ratio, 400 * ratio, 0, 0);
            Topic6.Width = 97 * ratio;
            Topic6.Height = 127 * ratio;
            Topic7.Visibility = System.Windows.Visibility.Hidden;
            Topic7.Margin = new Thickness(distanceX + 391 * ratio, 400 * ratio, 0, 0);
            Topic7.Width = 97 * ratio;
            Topic7.Height = 127 * ratio;
            Topic8.Visibility = System.Windows.Visibility.Hidden;
            Topic8.Margin = new Thickness(distanceX + 546 * ratio, 400 * ratio, 0, 0);
            Topic8.Width = 97 * ratio;
            Topic8.Height = 127 * ratio;

            _topics = topics;

            // Restart videos
            bufferMediaPlayer.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Topics.mpg");
            bufferMediaPlayer.MediaPosition = 0;
        }

        public void ShowQuestionTopic(string topic)
        {
            Topic1.Visibility = System.Windows.Visibility.Hidden;
            Topic2.Visibility = System.Windows.Visibility.Hidden;
            Topic3.Visibility = System.Windows.Visibility.Hidden;
            Topic4.Visibility = System.Windows.Visibility.Hidden;
            Topic5.Visibility = System.Windows.Visibility.Hidden;
            Topic6.Visibility = System.Windows.Visibility.Hidden;
            Topic7.Visibility = System.Windows.Visibility.Hidden;
            Topic8.Visibility = System.Windows.Visibility.Hidden;

            // Show topics screen
            bufferMediaPlayer.Tag = 1;
            if (topic == "Văn học")
            {
                bufferMediaPlayer.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Van Hoc Zoom.mpg");
            }
            else if (topic == "Điện ảnh")
            {
                bufferMediaPlayer.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Dien Anh Zoom.mpg");
            }
            else if (topic == "Âm nhạc")
            {
                bufferMediaPlayer.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Am Nhac Zoom.mpg");
            }
            else if (topic == "Địa lý")
            {
                bufferMediaPlayer.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Dia Ly Zoom.mpg");
            }
            else if (topic == "Lịch sử")
            {
                bufferMediaPlayer.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Lich Su Zoom.mpg");
            }
            else if (topic == "Văn hóa - Xã hội")
            {
                bufferMediaPlayer.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Van Hoa Zoom.mpg");
            }
            else if (topic == "Thể thao")
            {
                bufferMediaPlayer.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/The Thao Zoom.mpg");
            }
            else if (topic == "Khoa học - Công nghệ")
            {
                bufferMediaPlayer.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Cong Nghe Zoom.mpg");
            }

            // Show topics
            bufferMediaPlayer.MediaPosition = 0;
        }

        public void ShowScore(int number)
        {
            ScoreTitle.Visibility = System.Windows.Visibility.Hidden;
            ScoreCanvas.Visibility = System.Windows.Visibility.Hidden;

            var videoWidth = BackgroundVideo.ActualWidth;
            var videoHeight = BackgroundVideo.ActualHeight;
            var ratioWidth = videoWidth / VideoOriginalWidth;
            var ratioHeight = videoHeight / VideoOriginalHeight;
            var ratio = Math.Min(ratioWidth, ratioHeight);
            var distanceX = (videoWidth - VideoOriginalWidth * ratio) / 2;

            ScoreTitle.LayoutTransform = new ScaleTransform(ratio, ratio);
            ScoreCanvas.LayoutTransform = new ScaleTransform(ratio, ratio);

            ScoreTitle.Text = "Số điểm của câu " + number.ToString();
            ((TextBlock)ScoreCanvas.Children[0]).Text = "9";
            Canvas.SetTop(ScoreCanvas.Children[0], 10);
            ((TextBlock)ScoreCanvas.Children[1]).Text = "8";
            Canvas.SetTop(ScoreCanvas.Children[1], 159);
            ((TextBlock)ScoreCanvas.Children[2]).Text = "9";
            Canvas.SetTop(ScoreCanvas.Children[2], 10);
            ((TextBlock)ScoreCanvas.Children[3]).Text = "8";
            Canvas.SetTop(ScoreCanvas.Children[3], 159);
            ((TextBlock)ScoreCanvas.Children[4]).Text = "9";
            Canvas.SetTop(ScoreCanvas.Children[4], 10);
            ((TextBlock)ScoreCanvas.Children[5]).Text = "8";
            Canvas.SetTop(ScoreCanvas.Children[5], 159);

            // Restart videos
            bufferMediaPlayer.Tag = 2;
            bufferMediaPlayer.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Score.mpg");
            bufferMediaPlayer.MediaPosition = 0;
        }

        public void StartScrollScore(int score, Action completed)
        {
            _completedFunc = completed;
            StartScrollingNumber(score);
        }

        public void ShowQuestionForm(string topic, Question question)
        {
            var videoWidth = BackgroundVideo.ActualWidth;
            var videoHeight = BackgroundVideo.ActualHeight;
            var ratioWidth = videoWidth / VideoOriginalWidth;
            var ratioHeight = videoHeight / VideoOriginalHeight;
            var ratio = Math.Min(ratioWidth, ratioHeight);
            var distanceX = (videoWidth - VideoOriginalWidth * ratio) / 2;

            Question.LayoutTransform = new ScaleTransform(ratio, ratio);
            Question.Margin = new Thickness(distanceX + 172 * ratio, 0, 0, 250 * ratio);
            Answer1.LayoutTransform = new ScaleTransform(ratio, ratio);
            Answer1.Margin = new Thickness(distanceX + 98 * ratio, 0, 0, 135 * ratio);
            Answer2.LayoutTransform = new ScaleTransform(ratio, ratio);
            Answer2.Margin = new Thickness(distanceX + 462 * ratio, 0, 0, 135 * ratio);
            Answer3.LayoutTransform = new ScaleTransform(ratio, ratio);
            Answer3.Margin = new Thickness(distanceX + 98 * ratio, 0, 0, 42 * ratio);
            Answer4.LayoutTransform = new ScaleTransform(ratio, ratio);
            Answer4.Margin = new Thickness(distanceX + 462 * ratio, 0, 0, 42 * ratio);
            Clock.LayoutTransform = new ScaleTransform(ratio, ratio);
            Clock.Margin = new Thickness(distanceX + 118 * ratio, 0, 0, 194 * ratio);
            Timer.LayoutTransform = new ScaleTransform(ratio, ratio);
            Timer.Margin = new Thickness(distanceX + 587 * ratio, 0, 0, 194 * ratio);
            Progress.LayoutTransform = new ScaleTransform(ratio, ratio);
            Progress.Margin = new Thickness(distanceX + 155 * ratio, 0, 0, 200 * ratio);
            ProgressThumb.LayoutTransform = new ScaleTransform(ratio, ratio);
            ProgressThumb.Margin = new Thickness(distanceX + 157 * ratio, 0, 0, 201 * ratio);

            // Restart videos
            bufferMediaPlayer.Tag = 3;
            if (topic == "Văn học")
            {
                bufferMediaPlayer.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Question Van Hoc.mpg");
            }
            else if (topic == "Điện ảnh")
            {
                bufferMediaPlayer.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Question Dien Anh.mpg");
            }
            else if (topic == "Âm nhạc")
            {
                bufferMediaPlayer.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Question Am Nhac.mpg");
            }
            else if (topic == "Địa lý")
            {
                bufferMediaPlayer.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Question Dia Ly.mpg");
            }
            else if (topic == "Lịch sử")
            {
                bufferMediaPlayer.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Question Lich Su.mpg");
            }
            else if (topic == "Văn hóa - Xã hội")
            {
                bufferMediaPlayer.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Question Van Hoa.mpg");
            }
            else if (topic == "Thể thao")
            {
                bufferMediaPlayer.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Question The Thao.mpg");
            }
            else if (topic == "Khoa học - Công nghệ")
            {
                bufferMediaPlayer.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Question Cong Nghe.mpg");
            }
            else
            {
                bufferMediaPlayer.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Question.mpg");
            }
            bufferMediaPlayer.MediaPosition = 0;

            CorrectAnswer.Visibility = Visibility.Hidden;
            Timer.Text = string.Format("{0,3:000}", question.Score);
            Question.Text = "";
            Answer1.Text = "";
            Answer2.Text = "";
            Answer3.Text = "";
            Answer4.Text = "";

            ProgressThumb.Width = 425;
        }

        public void ShowQuestion(Question question)
        {
            var formattedText = new FormattedText(
                                question.QuestionContent,
                                CultureInfo.CurrentUICulture,
                                FlowDirection.LeftToRight,
                                new Typeface(this.Question.FontFamily, this.Question.FontStyle, this.Question.FontWeight, this.Question.FontStretch),
                                this.Question.FontSize,
                                Question.Foreground);
            Question.Height = formattedText.Height * Math.Ceiling(formattedText.Width / Question.Width);
            Question.Text = question.QuestionContent;
        }

        public void StartQuestion(Question question)
        {
            var formattedText = new FormattedText(
                                "A. " + question.Answer1,
                                CultureInfo.CurrentUICulture,
                                FlowDirection.LeftToRight,
                                new Typeface(this.Answer1.FontFamily, this.Answer1.FontStyle, this.Answer1.FontWeight, this.Answer1.FontStretch),
                                this.Answer1.FontSize,
                                Answer1.Foreground);
            Answer1.Height = formattedText.Height * Math.Ceiling(formattedText.Width / Answer1.Width);
            Answer1.Text = "A. " + question.Answer1;

            formattedText = new FormattedText(
                                "B. " + question.Answer2,
                                CultureInfo.CurrentUICulture,
                                FlowDirection.LeftToRight,
                                new Typeface(this.Answer2.FontFamily, this.Answer2.FontStyle, this.Answer2.FontWeight, this.Answer2.FontStretch),
                                this.Answer2.FontSize,
                                Answer2.Foreground);
            Answer2.Height = formattedText.Height * Math.Ceiling(formattedText.Width / Answer2.Width);
            Answer2.Text = "B. " + question.Answer2;

            formattedText = new FormattedText(
                                "C. " + question.Answer3,
                                CultureInfo.CurrentUICulture,
                                FlowDirection.LeftToRight,
                                new Typeface(this.Answer3.FontFamily, this.Answer3.FontStyle, this.Answer3.FontWeight, this.Answer3.FontStretch),
                                this.Answer3.FontSize,
                                Answer3.Foreground);
            Answer3.Height = formattedText.Height * Math.Ceiling(formattedText.Width / Answer3.Width);
            Answer3.Text = "C. " + question.Answer3;

            formattedText = new FormattedText(
                                "D. " + question.Answer4,
                                CultureInfo.CurrentUICulture,
                                FlowDirection.LeftToRight,
                                new Typeface(this.Answer4.FontFamily, this.Answer4.FontStyle, this.Answer4.FontWeight, this.Answer4.FontStretch),
                                this.Answer4.FontSize,
                                Answer4.Foreground);
            Answer4.Height = formattedText.Height * Math.Ceiling(formattedText.Width / Answer4.Width);
            Answer4.Text = "D. " + question.Answer4;
        }

        public void SetTime(int time, int totalTime, int score)
        {
            ProgressThumb.Width = (double)time / totalTime * 425;
            Timer.Text = string.Format("{0,3:000}", score * time / totalTime);
        }

        public void ShowCorrectAnswer(int correct)
        {
            var videoWidth = BackgroundVideo.ActualWidth;
            var videoHeight = BackgroundVideo.ActualHeight;
            var ratioWidth = videoWidth / VideoOriginalWidth;
            var ratioHeight = videoHeight / VideoOriginalHeight;
            var ratio = Math.Min(ratioWidth, ratioHeight);
            var distanceX = (videoWidth - VideoOriginalWidth * ratio) / 2;

            bufferMediaPlayer.Tag = 4;
            bufferMediaPlayer.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Firework.mpg");
            bufferMediaPlayer.MediaPosition = 0;

            if (correct == 1)
            {
                CorrectAnswer.Margin = new Thickness(distanceX + -19 * ratio, 317 * ratio, 0, 0);
            }
            else if (correct == 2)
            {
                CorrectAnswer.Margin = new Thickness(distanceX + 338 * ratio, 317 * ratio, 0, 0);
            }
            else if (correct == 3)
            {
                CorrectAnswer.Margin = new Thickness(distanceX + -19 * ratio, 410 * ratio, 0, 0);
            }
            else if (correct == 4)
            {
                CorrectAnswer.Margin = new Thickness(distanceX + 338 * ratio, 410 * ratio, 0, 0);
            }
        }

        public void ShowLeaderboard(int type)
        {
            Leaderboard.Visibility = System.Windows.Visibility.Hidden;
            LeaderboardHeader.Visibility = System.Windows.Visibility.Hidden;

            bufferMediaPlayer.Tag = 5;
            if (type == 1)
            {
                bufferMediaPlayer.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Top 10.mpg");
            }
            else
            {
                bufferMediaPlayer.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Top 50.mpg");
            }
            bufferMediaPlayer.MediaPosition = 0;
        }

        public void ShowTop(List<UserModel> top)
        {
            GenerateList(top);
        }

        private void GenerateList(List<UserModel> top)
        {
            var i = 0;
            var timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(0.5) };
            timer.Start();
            timer.Tick += (s, args) =>
            {
                if (i >= Math.Min(10, top.Count))
                {
                    timer.Stop();
                }
                else
                {
                    var content = "";
                    content += (i + 1).ToString("D2").PadRight(3);
                    content += top[i].ID.ToString("D6").PadRight(7);
                    content += (top[i].Fullname == null ? "" : top[i].Fullname).PadRight(20);
                    content += top[i].Score.HasValue ? top[i].Score.Value.ToString("D6") : "000000";

                    FlipControl flip = Leaderboard.Children.Cast<UIElement>().First(e => Grid.GetRow(e) == i + 1 && Grid.GetColumn(e) == 0) as FlipControl;
                    flip.StartFlipping(content);
                    ++i;
                }
            };
        }

        public void HideAll()
        {
            BackgroundVideo.Visibility = System.Windows.Visibility.Hidden;
            BackgroundVideoBuffer.Visibility = System.Windows.Visibility.Hidden;
            ScorePanel.Visibility = System.Windows.Visibility.Hidden;
            TopicsPanel.Visibility = System.Windows.Visibility.Hidden;
            QuestionPanel.Visibility = System.Windows.Visibility.Hidden;
            LeaderboardPanel.Visibility = System.Windows.Visibility.Hidden;
        }

        private void Background_MediaOpened(object sender, RoutedEventArgs e)
        {
            if (bufferMediaPlayer.Tag == null)
            {
                return;
            }

            if ((int)bufferMediaPlayer.Tag == 0)
            {
                HideAll();

                // Show topics screen
                bufferMediaPlayer.Visibility = System.Windows.Visibility.Visible;
                TopicsPanel.Visibility = Visibility.Visible;

                bufferMediaPlayer.Play();

                var player = new MediaPlayer();
                player.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/Logo Appear.wav"));
                player.Play();

                // Hide empty topics
                if (_topics.ContainsKey("Văn học") && _topics["Văn học"] == false)
                {
                    Topic1.Visibility = System.Windows.Visibility.Visible;
                }
                if (_topics.ContainsKey("Văn hóa - Xã hội") && _topics["Văn hóa - Xã hội"] == false)
                {
                    Topic2.Visibility = System.Windows.Visibility.Visible;
                }
                if (_topics.ContainsKey("Lịch sử") && _topics["Lịch sử"] == false)
                {
                    Topic3.Visibility = System.Windows.Visibility.Visible;
                }
                if (_topics.ContainsKey("Khoa học - Công nghệ") && _topics["Khoa học - Công nghệ"] == false)
                {
                    Topic4.Visibility = System.Windows.Visibility.Visible;
                }
                if (_topics.ContainsKey("Địa lý") && _topics["Địa lý"] == false)
                {
                    Topic5.Visibility = System.Windows.Visibility.Visible;
                }
                if (_topics.ContainsKey("Âm nhạc") && _topics["Âm nhạc"] == false)
                {
                    Topic6.Visibility = System.Windows.Visibility.Visible;
                }
                if (_topics.ContainsKey("Điện ảnh") && _topics["Điện ảnh"] == false)
                {
                    Topic7.Visibility = System.Windows.Visibility.Visible;
                }
                if (_topics.ContainsKey("Thể thao") && _topics["Thể thao"] == false)
                {
                    Topic8.Visibility = System.Windows.Visibility.Visible;
                }
            }
            else if ((int)bufferMediaPlayer.Tag == 1)
            {
                bufferMediaPlayer.Play();

                BackgroundVideo.Visibility = System.Windows.Visibility.Hidden;
                BackgroundVideoBuffer.Visibility = System.Windows.Visibility.Hidden;
                bufferMediaPlayer.Visibility = System.Windows.Visibility.Visible;

                var timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(1) };
                timer.Start();
                timer.Tick += (s, args) =>
                {
                    timer.Stop();
                    var player = new MediaPlayer();
                    player.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/Topic Zoom.wav"));
                    player.Play();
                };
            }
            else if ((int)bufferMediaPlayer.Tag == 2)
            {
                HideAll();

                ScorePanel.Visibility = System.Windows.Visibility.Visible;
                bufferMediaPlayer.Visibility = System.Windows.Visibility.Visible;

                bufferMediaPlayer.Play();

                ScoreBackground_MediaOpened(sender, e);
            }
            else if ((int)bufferMediaPlayer.Tag == 3)
            {
                HideAll();

                // Show question screen
                QuestionPanel.Visibility = System.Windows.Visibility.Visible;
                bufferMediaPlayer.Visibility = System.Windows.Visibility.Visible;

                bufferMediaPlayer.Play();
            }
            else if ((int)bufferMediaPlayer.Tag == 4)
            {
                bufferMediaPlayer.Play();

                BackgroundVideo.Visibility = System.Windows.Visibility.Hidden;
                BackgroundVideoBuffer.Visibility = System.Windows.Visibility.Hidden;
                bufferMediaPlayer.Visibility = System.Windows.Visibility.Visible;

                BackgroundAnswers_MediaOpened(sender, e);
            }
            else if ((int)bufferMediaPlayer.Tag == 5)
            {
                HideAll();

                bufferMediaPlayer.Play();

                LeaderboardPanel.Visibility = System.Windows.Visibility.Visible;
                bufferMediaPlayer.Visibility = System.Windows.Visibility.Visible;

                BackgroundLeaderboard_MediaOpened(sender, e);
            }

            // Switch current and buffer
            if (bufferMediaPlayer == BackgroundVideo)
            {
                bufferMediaPlayer = BackgroundVideoBuffer;
            }
            else
            {
                bufferMediaPlayer = BackgroundVideo;
            }
        }

        private void ScoreBackground_MediaOpened(object sender, RoutedEventArgs e)
        {
            var timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(4) };
            timer.Start();
            timer.Tick += (s, args) =>
            {
                timer.Stop();
                ScoreTitle.Visibility = System.Windows.Visibility.Visible;
                ScoreCanvas.Visibility = System.Windows.Visibility.Visible;
            };
        }

        private void BackgroundAnswers_MediaOpened(object sender, RoutedEventArgs e)
        {
            if ((int)bufferMediaPlayer.Tag == 4)
            {
                var player = new MediaPlayer();
                player.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/Correct Answer.wav"));
                player.Play();

                CorrectAnswer.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Correct Answer.mpg");
                CorrectAnswer.Visibility = System.Windows.Visibility.Visible;
                CorrectAnswer.MediaPosition = 0;
                CorrectAnswer.Play();

                CorrectAnswer.Tag = 2;
                var timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(7) };
                timer.Start();
                timer.Tick += (s, args) =>
                {
                    CorrectAnswer.Stop();
                    CorrectAnswer.MediaPosition = 0;
                    CorrectAnswer.Play();
                    CorrectAnswer.Tag = (int)CorrectAnswer.Tag - 1;
                    if ((int)CorrectAnswer.Tag == 0)
                    {
                        timer.Stop();
                    }
                };
            }
        }

        private void BackgroundLeaderboard_MediaOpened(object sender, RoutedEventArgs e)
        {
            Leaderboard.Children.RemoveRange(1, Leaderboard.Children.Count - 1);

            for (var i = 0; i < 10; i++)
            {
                var text = new FlipControl();
                Grid.SetRow(text, i + 1);
                Grid.SetColumn(text, 0);
                Leaderboard.Children.Add(text);
            }

            Leaderboard.Visibility = System.Windows.Visibility.Visible;
            LeaderboardHeader.Visibility = System.Windows.Visibility.Visible;

            var player = new MediaPlayer();
            player.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/Connected.mp3"));
            player.Play();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            bufferMediaPlayer = BackgroundVideoBuffer;
        }
    }
}
