﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Media;

namespace Game_Controller_WPF
{
    /// <summary>
    /// Interaction logic for ScreenWindow.xaml
    /// </summary>
    public partial class WaitingScreenWindow : Window
    {
        private const double VideoOriginalWidth = 720.0;
        private const double VideoOriginalHeight = 576.0;

        public WaitingScreenWindow()
        {
            InitializeComponent();
        }

        public void ChangeToFullScreen()
        {
            // Get secondary screen
            var screen = System.Windows.Forms.Screen.PrimaryScreen;
            foreach (var s in System.Windows.Forms.Screen.AllScreens)
            {
                if (s.Primary == false)
                {
                    screen = s;
                    break;
                }
            }

            var screenWidth = (double)screen.Bounds.Width;
            var screenHeight = (double)screen.Bounds.Height;
            var ratioWidth = screenWidth / VideoOriginalWidth;
            var ratioHeight = screenHeight / VideoOriginalHeight;
            MainGrid.LayoutTransform = new ScaleTransform(ratioWidth, ratioHeight);
            this.Left = screen.Bounds.X;
            this.Top = screen.Bounds.Y;
            this.Width = screen.Bounds.Width;
            this.Height = screen.Bounds.Height;

            Restart();
        }

        public void Restart()
        {
            WaitingBackground.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Waiting Background.mpg");
            WaitingBackground.Play();
        }
    }
}
