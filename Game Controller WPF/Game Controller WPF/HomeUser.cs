//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Game_Controller_WPF
{
    using System;
    using System.Collections.Generic;
    
    public partial class HomeUser
    {
        public HomeUser()
        {
            this.ExchangeScores = new HashSet<ExchangeScore>();
            this.HomeAnswers = new HashSet<HomeAnswer>();
            this.PushMessages = new HashSet<PushMessage>();
        }
    
        public long ID { get; set; }
        public string Fullname { get; set; }
        public Nullable<System.DateTime> Birthday { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string UniqueIdentifier { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public bool IsDeleted { get; set; }
        public string Token { get; set; }
        public string OS { get; set; }
        public Nullable<bool> OptIn { get; set; }
    
        public virtual ICollection<ExchangeScore> ExchangeScores { get; set; }
        public virtual ICollection<HomeAnswer> HomeAnswers { get; set; }
        public virtual ICollection<PushMessage> PushMessages { get; set; }
    }
}
