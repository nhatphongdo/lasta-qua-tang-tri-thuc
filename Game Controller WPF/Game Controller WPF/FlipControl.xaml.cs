﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;

namespace Game_Controller_WPF
{
    /// <summary>
    /// Interaction logic for FlipControl.xaml
    /// </summary>
    public partial class FlipControl : UserControl
    {
        private const double deltaAngle = 30;
        public FlipControl()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty BoundFlipContentProperty = DependencyProperty.Register("FlipContent", typeof(string), typeof(FlipControl), new FrameworkPropertyMetadata(new PropertyChangedCallback(AdjustControl)));

        public static readonly DependencyProperty BoundNoFlipProperty = DependencyProperty.Register("NoFlip", typeof(bool), typeof(FlipControl), new FrameworkPropertyMetadata(new PropertyChangedCallback(AdjustControl)));

        public string FlipContent
        {
            get
            {
                return (string)GetValue(BoundFlipContentProperty);
            }
            set
            {
                SetValue(BoundFlipContentProperty, value);
            }
        }

        public bool NoFlip
        {
            get
            {
                return (bool)GetValue(BoundNoFlipProperty);
            }
            set
            {
                SetValue(BoundNoFlipProperty, value);
            }
        }

        private static void AdjustControl(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            if (e.Property == BoundNoFlipProperty)
            {
                if ((bool)e.NewValue == true)
                {
                    (source as FlipControl).Rotation.Angle = 0;
                }
            }
            else if (e.Property == BoundFlipContentProperty)
            {
                (source as FlipControl).SetContent((string)e.NewValue);
            }
        }

        public void SetColor(Brush color)
        {
            for (var i = 0; i < 24; i++)
            {
                var upperText = UpperList.FindName("UpperText" + (i + 1).ToString("00")) as TextBlock;
                var downerText = DownerList.FindName("DownerText" + (i + 1).ToString("00")) as TextBlock;

                upperText.Foreground = color;
                downerText.Foreground = color;
            }
        }

        public void SetContent(string content)
        {
            for (var i = 0; i < Math.Min(content.Length, 24); i++)
            {
                var upperImage = UpperList.FindName("Upper" + (i + 1).ToString("00")) as Image;
                var upperText = UpperList.FindName("UpperText" + (i + 1).ToString("00")) as TextBlock;
                var downerImage = DownerList.FindName("Downer" + (i + 1).ToString("00")) as Image;
                var downerText = DownerList.FindName("DownerText" + (i + 1).ToString("00")) as TextBlock;

                if (content[i] == ' ')
                {
                    // Hide
                    upperImage.Visibility = System.Windows.Visibility.Hidden;
                    downerImage.Visibility = System.Windows.Visibility.Hidden;
                    upperText.Text = "";
                    downerText.Text = "";
                }
                else
                {
                    upperImage.Visibility = System.Windows.Visibility.Visible;
                    downerImage.Visibility = System.Windows.Visibility.Visible;
                    upperText.Text = content[i].ToString();
                    downerText.Text = content[i].ToString();
                }
            }
        }

        public void StartFlipping(string content)
        {
            this.FlipContent = content;
            if (this.NoFlip == true)
            {
                Rotation.Angle = 0;
                return;
            }

            Rotation.Angle = -90;

            var timer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(20) };
            timer.Start();
            timer.Tick += (s, args) =>
            {
                if (Rotation.Angle + deltaAngle >= 0)
                {
                    Rotation.Angle = 0;
                    timer.Stop();
                }
                else
                {
                    Rotation.Angle += deltaAngle;
                }
            };
        }
    }
}
