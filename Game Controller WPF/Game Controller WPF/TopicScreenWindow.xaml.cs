﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace Game_Controller_WPF
{
    /// <summary>
    /// Interaction logic for ScreenWindow.xaml
    /// </summary>
    public partial class TopicScreenWindow : Window
    {
        private const double VideoOriginalWidth = 720.0;
        private const double VideoOriginalHeight = 576.0;

        private Dictionary<string, int> _topics;

        //private MediaPlayer _logoAppearSound;
        //private MediaPlayer _logoZoomSound;

        public TopicScreenWindow()
        {
            InitializeComponent();
        }

        public void ChangeToFullScreen()
        {
            // Get secondary screen
            var screen = System.Windows.Forms.Screen.PrimaryScreen;
            foreach (var s in System.Windows.Forms.Screen.AllScreens)
            {
                if (s.Primary == false)
                {
                    screen = s;
                    break;
                }
            }

            var screenWidth = (double)screen.Bounds.Width;
            var screenHeight = (double)screen.Bounds.Height;
            var ratioWidth = screenWidth / VideoOriginalWidth;
            var ratioHeight = screenHeight / VideoOriginalHeight;
            MainGrid.LayoutTransform = new ScaleTransform(ratioWidth, ratioHeight);
            this.Left = screen.Bounds.X;
            this.Top = screen.Bounds.Y;
            this.Width = screen.Bounds.Width;
            this.Height = screen.Bounds.Height;
        }

        public void ShowTopics(Dictionary<string, int> topics)
        {
            var videoWidth = TopicList.ActualWidth;
            var videoHeight = TopicList.ActualHeight;
            var ratioWidth = videoWidth / VideoOriginalWidth;
            var ratioHeight = videoHeight / VideoOriginalHeight;
            var ratio = Math.Min(ratioWidth, ratioHeight);
            var distanceX = (videoWidth - VideoOriginalWidth * ratio) / 2 + 70;

            TopicZoom.Visibility = System.Windows.Visibility.Hidden;
            TopicList.Visibility = System.Windows.Visibility.Visible;

            var size = 30;

            Topic1.Visibility = System.Windows.Visibility.Hidden;
            Topic2.Visibility = System.Windows.Visibility.Hidden;
            Topic3.Visibility = System.Windows.Visibility.Hidden;
            Topic4.Visibility = System.Windows.Visibility.Hidden;
            Topic5.Visibility = System.Windows.Visibility.Hidden;
            Topic6.Visibility = System.Windows.Visibility.Hidden;
            Topic7.Visibility = System.Windows.Visibility.Hidden;
            Topic8.Visibility = System.Windows.Visibility.Hidden;
            
            Topic1.Opacity = 0;
            Topic1.Margin = new Thickness(distanceX + 79 * ratio, 270 * ratio, 0, 0);
            Topic1.Width = size * ratio;
            Topic1.Height = size * ratio;
            Topic2.Opacity = 0;
            Topic2.Margin = new Thickness(distanceX + 232 * ratio, 270 * ratio, 0, 0);
            Topic2.Width = size * ratio;
            Topic2.Height = size * ratio;
            Topic3.Opacity = 0;
            Topic3.Margin = new Thickness(distanceX + 388 * ratio, 270 * ratio, 0, 0);
            Topic3.Width = size * ratio;
            Topic3.Height = size * ratio;
            Topic4.Opacity = 0;
            Topic4.Margin = new Thickness(distanceX + 542 * ratio, 270 * ratio, 0, 0);
            Topic4.Width = size * ratio;
            Topic4.Height = size * ratio;
            Topic5.Opacity = 0;
            Topic5.Margin = new Thickness(distanceX + 79 * ratio, 410 * ratio, 0, 0);
            Topic5.Width = size * ratio;
            Topic5.Height = size * ratio;
            Topic6.Opacity = 0;
            Topic6.Margin = new Thickness(distanceX + 232 * ratio, 410 * ratio, 0, 0);
            Topic6.Width = size * ratio;
            Topic6.Height = size * ratio;
            Topic7.Opacity = 0;
            Topic7.Margin = new Thickness(distanceX + 388 * ratio, 410 * ratio, 0, 0);
            Topic7.Width = size * ratio;
            Topic7.Height = size * ratio;
            Topic8.Opacity = 0;
            Topic8.Margin = new Thickness(distanceX + 542 * ratio, 410 * ratio, 0, 0);
            Topic8.Width = size * ratio;
            Topic8.Height = size * ratio;

            _topics = topics;

            // Restart videos
            TopicList.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Topics.mpg");
        }

        public void ShowQuestionTopic(string topic)
        {
            Topic1.Visibility = System.Windows.Visibility.Hidden;
            Topic2.Visibility = System.Windows.Visibility.Hidden;
            Topic3.Visibility = System.Windows.Visibility.Hidden;
            Topic4.Visibility = System.Windows.Visibility.Hidden;
            Topic5.Visibility = System.Windows.Visibility.Hidden;
            Topic6.Visibility = System.Windows.Visibility.Hidden;
            Topic7.Visibility = System.Windows.Visibility.Hidden;
            Topic8.Visibility = System.Windows.Visibility.Hidden;

            // Show topics screen
            if (topic == "Văn học")
            {
                TopicZoom.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Van Hoc Zoom.mpg");
            }
            else if (topic == "Giải trí")
            {
                TopicZoom.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Giai Tri Zoom.mpg");
            }
            else if (topic == "Sức khỏe")
            {
                TopicZoom.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Suc Khoe Zoom.mpg");
            }
            else if (topic == "Địa lý")
            {
                TopicZoom.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Dia Ly Zoom.mpg");
            }
            else if (topic == "Lịch sử")
            {
                TopicZoom.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Lich Su Zoom.mpg");
            }
            else if (topic == "Văn hóa - Xã hội")
            {
                TopicZoom.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Van Hoa Zoom.mpg");
            }
            else if (topic == "Thể thao")
            {
                TopicZoom.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/The Thao Zoom.mpg");
            }
            else if (topic == "Khoa học - Công nghệ")
            {
                TopicZoom.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Cong Nghe Zoom.mpg");
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //_logoAppearSound = new MediaPlayer();
            //_logoAppearSound.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/Logo Appear.wav"));

            //_logoZoomSound = new MediaPlayer();
            //_logoZoomSound.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/Topic Zoom.wav"));
        }

        protected internal void TopicZoom_MediaOpened(object sender, RoutedEventArgs e)
        {
            // Show topics
            TopicZoom.Visibility = System.Windows.Visibility.Visible;
            TopicZoom.Play();
        }

        protected internal void TopicList_MediaOpened(object sender, RoutedEventArgs e)
        {
            // Show topics
            TopicList.Visibility = System.Windows.Visibility.Visible;
            TopicList.Play();

            var fadingAnimation = new DoubleAnimation();
            fadingAnimation.From = 0;
            fadingAnimation.To = 1;
            fadingAnimation.Duration = new Duration(TimeSpan.FromSeconds(2));
            fadingAnimation.AutoReverse = false;

            var timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(6) };
            timer.Start();
            timer.Tick += (s, args) =>
            {
                timer.Stop();

                Topic1.Visibility = System.Windows.Visibility.Visible;
                Topic2.Visibility = System.Windows.Visibility.Visible;
                Topic3.Visibility = System.Windows.Visibility.Visible;
                Topic4.Visibility = System.Windows.Visibility.Visible;
                Topic5.Visibility = System.Windows.Visibility.Visible;
                Topic6.Visibility = System.Windows.Visibility.Visible;
                Topic7.Visibility = System.Windows.Visibility.Visible;
                Topic8.Visibility = System.Windows.Visibility.Visible;


                Topic1.BeginAnimation(System.Windows.Controls.Image.OpacityProperty, fadingAnimation);
                Topic2.BeginAnimation(System.Windows.Controls.Image.OpacityProperty, fadingAnimation);
                Topic3.BeginAnimation(System.Windows.Controls.Image.OpacityProperty, fadingAnimation);
                Topic4.BeginAnimation(System.Windows.Controls.Image.OpacityProperty, fadingAnimation);
                Topic5.BeginAnimation(System.Windows.Controls.Image.OpacityProperty, fadingAnimation);
                Topic6.BeginAnimation(System.Windows.Controls.Image.OpacityProperty, fadingAnimation);
                Topic7.BeginAnimation(System.Windows.Controls.Image.OpacityProperty, fadingAnimation);
                Topic8.BeginAnimation(System.Windows.Controls.Image.OpacityProperty, fadingAnimation);
            };

            //_logoAppearSound.Position = TimeSpan.FromSeconds(0);
            //_logoAppearSound.Play();

            // Hide empty topics
            if (_topics.ContainsKey("Văn học") && _topics["Văn học"] == 1)
            {
                BitmapImage topicImage = new BitmapImage();
                topicImage.BeginInit();
                topicImage.UriSource = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/one.png");
                topicImage.EndInit();
                Topic1.Source = topicImage;
            }
            else if (_topics.ContainsKey("Văn học") && _topics["Văn học"] == 0)
            {
                BitmapImage topicImage = new BitmapImage();
                topicImage.BeginInit();
                topicImage.UriSource = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/zero.png");
                topicImage.EndInit();
                Topic1.Source = topicImage;
            }

            if (_topics.ContainsKey("Văn hóa - Xã hội") && _topics["Văn hóa - Xã hội"] == 1)
            {
                BitmapImage topicImage = new BitmapImage();
                topicImage.BeginInit();
                topicImage.UriSource = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/one.png");
                topicImage.EndInit();
                Topic2.Source = topicImage;
            }
            else if (_topics.ContainsKey("Văn hóa - Xã hội") && _topics["Văn hóa - Xã hội"] == 0)
            {
                BitmapImage topicImage = new BitmapImage();
                topicImage.BeginInit();
                topicImage.UriSource = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/zero.png");
                topicImage.EndInit();
                Topic2.Source = topicImage;
            }

            if (_topics.ContainsKey("Lịch sử") && _topics["Lịch sử"] == 1)
            {
                BitmapImage topicImage = new BitmapImage();
                topicImage.BeginInit();
                topicImage.UriSource = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/one.png");
                topicImage.EndInit();
                Topic3.Source = topicImage;
            }
            else if (_topics.ContainsKey("Lịch sử") && _topics["Lịch sử"] == 0)
            {
                BitmapImage topicImage = new BitmapImage();
                topicImage.BeginInit();
                topicImage.UriSource = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/zero.png");
                topicImage.EndInit();
                Topic3.Source = topicImage;
            }

            if (_topics.ContainsKey("Địa lý") && _topics["Địa lý"] == 1)
            {
                BitmapImage topicImage = new BitmapImage();
                topicImage.BeginInit();
                topicImage.UriSource = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/one.png");
                topicImage.EndInit();
                Topic4.Source = topicImage;
            }
            else if (_topics.ContainsKey("Địa lý") && _topics["Địa lý"] == 0)
            {
                BitmapImage topicImage = new BitmapImage();
                topicImage.BeginInit();
                topicImage.UriSource = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/zero.png");
                topicImage.EndInit();
                Topic4.Source = topicImage;
            }

            if (_topics.ContainsKey("Khoa học - Công nghệ") && _topics["Khoa học - Công nghệ"] == 1)
            {
                BitmapImage topicImage = new BitmapImage();
                topicImage.BeginInit();
                topicImage.UriSource = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/one.png");
                topicImage.EndInit();
                Topic5.Source = topicImage;
            }
            else if (_topics.ContainsKey("Khoa học - Công nghệ") && _topics["Khoa học - Công nghệ"] == 0)
            {
                BitmapImage topicImage = new BitmapImage();
                topicImage.BeginInit();
                topicImage.UriSource = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/zero.png");
                topicImage.EndInit();
                Topic5.Source = topicImage;
            }

            if (_topics.ContainsKey("Sức khỏe") && _topics["Sức khỏe"] == 1)
            {
                BitmapImage topicImage = new BitmapImage();
                topicImage.BeginInit();
                topicImage.UriSource = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/one.png");
                topicImage.EndInit();
                Topic6.Source = topicImage;
            }
            else if (_topics.ContainsKey("Sức khỏe") && _topics["Sức khỏe"] == 0)
            {
                BitmapImage topicImage = new BitmapImage();
                topicImage.BeginInit();
                topicImage.UriSource = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/zero.png");
                topicImage.EndInit();
                Topic6.Source = topicImage;
            }

            if (_topics.ContainsKey("Giải trí") && _topics["Giải trí"] == 1)
            {
                BitmapImage topicImage = new BitmapImage();
                topicImage.BeginInit();
                topicImage.UriSource = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/one.png");
                topicImage.EndInit();
                Topic7.Source = topicImage;
            }
            else if (_topics.ContainsKey("Giải trí") && _topics["Giải trí"] == 0)
            {
                BitmapImage topicImage = new BitmapImage();
                topicImage.BeginInit();
                topicImage.UriSource = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/zero.png");
                topicImage.EndInit();
                Topic7.Source = topicImage;
            }

            if (_topics.ContainsKey("Thể thao") && _topics["Thể thao"] == 1)
            {
                BitmapImage topicImage = new BitmapImage();
                topicImage.BeginInit();
                topicImage.UriSource = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/one.png");
                topicImage.EndInit();
                Topic8.Source = topicImage;
            }
            else if (_topics.ContainsKey("Thể thao") && _topics["Thể thao"] == 0)
            {
                BitmapImage topicImage = new BitmapImage();
                topicImage.BeginInit();
                topicImage.UriSource = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/zero.png");
                topicImage.EndInit();
                Topic8.Source = topicImage;
            }
        }
    }
}
