﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Game_Controller_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Constants
        private readonly Brush TopicFirstSelected = Brushes.Yellow;
        private readonly Brush TopicSecondSelected = Brushes.Green;
        private readonly int[] Scores = { 30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330, 360, 390, 420, 450, 480 };

        // Variables
        private Random _randomizer = new Random((int)DateTime.Now.Ticks);
        private int _currentQuestionNumber;
        private Question _currentQuestion;
        private ContestDay _currentContest;
        private List<Topic> _currentTopics;
        private int _currentTimer;
        private List<int> _scores = new List<int>();

        private Dictionary<int, List<int>> _specialTopics;

        private int _delayTime;
        private int _delayQuestionTime;
        private bool _delayedQuestion;
        private bool _delayedHomeQuestion;
        private int _currentHomeTimer;
        private int _savedQuestionID;
        private int _savedContestState;
        private int _savedCurrentQuestionNumber;

        private Dictionary<string, int> _empties = new Dictionary<string, int>();

        private System.Windows.Threading.DispatcherTimer timer1;
        private System.Windows.Threading.DispatcherTimer timer2;
        private Timer timer3;
        private Timer timer4;
        private WaitingScreenWindow _waitingScreenWindow;
        private TopicScreenWindow _topicScreenWindow;
        private ScoreScreenWindow _scoreScreenWindow;
        private QuestionScreenWindow _questionScreenWindow;
        private LeaderboardScreenWindow _leaderboardScreenWindow;

        #region Cloud server

        private async void UpdateContest(bool updateTimer)
        {
            try
            {
                using (var entities = new CungLaTyPhuEntities())
                {
                    var contest = await entities.ContestDays.FirstOrDefaultAsync(c => c.ID == _currentContest.ID);
                    contest.State = _currentContest.State;
                    if (updateTimer)
                    {
                        contest.RemainTime = _currentContest.RemainTime;
                    }
                    var result = await entities.SaveChangesAsync();
                }
            }
            catch (Exception exc)
            {
            }
        }

        private async void UpdateContest(int state, int remainTime)
        {
            try
            {
                using (var entities = new CungLaTyPhuEntities())
                {
                    var contest = await entities.ContestDays.FirstOrDefaultAsync(c => c.ID == _currentContest.ID);
                    if (state >= 0)
                    {
                        contest.State = state;
                    }
                    if (remainTime >= 0)
                    {
                        contest.RemainTime = remainTime;
                    }
                    var result = await entities.SaveChangesAsync();
                }
            }
            catch (Exception exc)
            {
            }
        }

        private async void UpdateQuestion(int questionID, int number, int contestDayID)
        {
            try
            {
                using (var entities = new CungLaTyPhuEntities())
                {
                    var question = await entities.Questions.FirstOrDefaultAsync(q => q.ID == questionID);
                    question.Number = number;
                    question.ContestDayID = contestDayID;
                    var result = await entities.SaveChangesAsync();
                }
            }
            catch (Exception exc)
            {
                textErrorLog.Text += exc.ToString();
            }
        }

        private async void UpdateQuestionEndTime(int questionID, DateTime endTime)
        {
            try
            {
                using (var entities = new CungLaTyPhuEntities())
                {
                    var question = await entities.Questions.FirstOrDefaultAsync(q => q.ID == questionID);
                    question.EndTime = endTime;
                    var result = await entities.SaveChangesAsync();
                }
            }
            catch (Exception exc)
            {
            }
        }

        private async void UpdateQuestionStartTime(int questionID, DateTime startTime)
        {
            try
            {
                using (var entities = new CungLaTyPhuEntities())
                {
                    var question = await entities.Questions.FirstOrDefaultAsync(q => q.ID == questionID);
                    question.StartTime = startTime;
                    var result = await entities.SaveChangesAsync();
                }
            }
            catch (Exception exc)
            {
            }
        }

        private async void UpdateQuestionScore(int questionID, int score)
        {
            try
            {
                using (var entities = new CungLaTyPhuEntities())
                {
                    var question = await entities.Questions.FirstOrDefaultAsync(q => q.ID == questionID);
                    question.Score = score;
                    var result = await entities.SaveChangesAsync();
                }
            }
            catch (Exception exc)
            {
                textErrorLog.Text += exc.ToString();
            }
        }

        #endregion

        public MainWindow()
        {
            InitializeComponent();
            timer1 = new System.Windows.Threading.DispatcherTimer();
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Interval = new TimeSpan(0, 0, 0, 0, 100);

            timer2 = new System.Windows.Threading.DispatcherTimer();
            timer2.Tick += new EventHandler(timer2_Tick);
            timer2.Interval = new TimeSpan(0, 0, 0, 10);

            timer3 = new Timer(1000);
            timer3.Elapsed += timer3_Tick;

            timer4 = new Timer(1000);
            timer4.Elapsed += timer4_Tick;
        }

        private void butTopic_Click(object sender, EventArgs e)
        {
            if (_currentContest == null)
            {
                MessageBox.Show("Không có cuộc thi nào diễn ra vào ngày hôm nay!");
                return;
            }

            if (_currentContest.State != 10200)
            {
                MessageBox.Show("Chưa hiển thị danh sách chủ đề để thí sinh chọn!");
                return;
            }

            if (_currentQuestionNumber >= _currentContest.NumberOfQuestions)
            {
                MessageBox.Show("Đã đủ số câu hỏi cho ngày thi hôm nay!");
                return;
            }

            ResetButtons();

            Button topicButton = (Button)sender;

            // Choose one topic
            if (topicButton.Background == TopicSecondSelected)
            {
                // Choose twice
                return;
            }

            try
            {
                using (var entities = new Entities())
                {
                    var topicID = (int)topicButton.Tag;
                    var questions = entities.Questions.Where(q => !q.IsDeleted && q.ContestDayID.HasValue == false && q.TopicID == topicID).ToList();
                    if (questions.Count == 0)
                    {
                        MessageBox.Show("Đã hết câu hỏi trong kho câu hỏi!");
                        return;
                    }

                    if (questions.Count(q => q.Difficulty < 10) > 0)
                    {
                        questions = questions.Where(q => q.Difficulty < 10).ToList();
                        var index = _randomizer.Next(0, questions.Count);
                        _currentQuestion = questions[index];
                    }
                    else
                    {
                        var index = _randomizer.Next(0, questions.Count);
                        _currentQuestion = questions[index];
                    }

                    // Select special question
                    //if (_currentQuestionNumber <= 16 && _specialTopics.ContainsKey(topicID))
                    //{
                    //    //double rate = _currentQuestionNumber == 16 ? 1 : (16 - _currentQuestionNumber);
                    //    //rate = (2 - _specialQuestionCount) / rate;
                    //    if (_specialTopics[topicID].Count > 0)
                    //    {
                    //        var count = entities.Questions.Where(q => !q.IsDeleted && q.ContestDayID == _currentContest.ID && q.TopicID == topicID).Count();
                    //        if (_specialTopics[topicID].Contains(count + 1))
                    //        {
                    //            var specialQuestions = entities.Questions.Where(q => !q.IsDeleted && q.ContestDayID.HasValue == false && q.TopicID == topicID && q.Difficulty == 10).ToList();
                    //            var specialIndex = _randomizer.Next(0, specialQuestions.Count);

                    //            _currentQuestion = specialQuestions[specialIndex];

                    //            _specialTopics[topicID].Remove(count + 1);
                    //        }
                    //    }
                    //}
                    if (_currentQuestionNumber <= 16 && (topicID == 9 || topicID == 4))
                    {
                        var specialQuestions = entities.Questions.Where(q => !q.IsDeleted && q.ContestDayID.HasValue == false && q.TopicID == topicID && q.Difficulty == 10).ToList();
                        if (specialQuestions.Count > 0)
                        {
                            var specialIndex = _randomizer.Next(0, specialQuestions.Count);

                            _currentQuestion = specialQuestions[specialIndex];
                        }
                    }

                    ++_currentQuestionNumber;
                    _currentQuestion.Number = _currentQuestionNumber;
                    _currentQuestion.ContestDayID = _currentContest.ID;

                    var contest = entities.ContestDays.FirstOrDefault(c => c.ID == _currentContest.ID);
                    contest.State = _currentQuestionNumber * 10;
                    contest.RemainTime = _currentQuestion.CountdownTime;
                    entities.SaveChanges();

                    _currentContest.State = contest.State;
                    _currentContest.RemainTime = _currentQuestion.CountdownTime;

                    _savedQuestionID = _currentQuestion.ID;
                    _savedCurrentQuestionNumber = _currentQuestionNumber;
                    _savedContestState = _currentContest.State;
                    var timer = new System.Windows.Threading.DispatcherTimer { Interval = TimeSpan.FromSeconds(_delayTime) };
                    timer.Start();
                    timer.Tick += (s, args) =>
                    {
                        timer.Stop();

                        UpdateQuestion(_savedQuestionID, _savedCurrentQuestionNumber, _currentContest.ID);
                        UpdateContest(_savedContestState, _currentQuestion.CountdownTime);
                    };

                    // Set question info
                    lblQuestionNumber.Content = _currentQuestionNumber.ToString();
                    lblRemainTime.Content = _currentQuestion.CountdownTime.ToString();
                    var status = GetStatus(_currentContest.State);
                    lblStatus.Content = status;

                    lblTopic.Content = topicButton.Content;
                    lblQuestion.Content = _currentQuestion.QuestionContent;
                    lblAnswerA.Content = "A. " + _currentQuestion.Answer1;
                    lblAnswerA.Foreground = Brushes.Black;
                    lblAnswerB.Content = "B. " + _currentQuestion.Answer2;
                    lblAnswerB.Foreground = Brushes.Black;
                    lblAnswerC.Content = "C. " + _currentQuestion.Answer3;
                    lblAnswerC.Foreground = Brushes.Black;
                    lblAnswerD.Content = "D. " + _currentQuestion.Answer4;
                    lblAnswerD.Foreground = Brushes.Black;

                    //if (_currentQuestion.CorrectAnswer == 1)
                    //{
                    //    lblAnswerA.Foreground = Brushes.Blue;
                    //}
                    //else if (_currentQuestion.CorrectAnswer == 2)
                    //{
                    //    lblAnswerB.Foreground = Brushes.Blue;
                    //}
                    //else if (_currentQuestion.CorrectAnswer == 3)
                    //{
                    //    lblAnswerC.Foreground = Brushes.Blue;
                    //}
                    //else if (_currentQuestion.CorrectAnswer == 4)
                    //{
                    //    lblAnswerD.Foreground = Brushes.Blue;
                    //}

                    if (topicButton.Background == TopicFirstSelected)
                    {
                        topicButton.Background = TopicSecondSelected;
                        topicButton.IsEnabled = false;
                        _empties[topicButton.Content.ToString()] = 0;
                    }
                    else if (topicButton.Background != TopicSecondSelected)
                    {
                        topicButton.Background = TopicFirstSelected;
                        _empties[topicButton.Content.ToString()] = 1;
                    }
                }

                // Ignore show topic
                //_topicScreenWindow.ShowQuestionTopic(topicButton.Content.ToString());
            }
            catch (Exception exc)
            {
                // Error
            }
        }

        private void butShowTopics_Click(object sender, EventArgs e)
        {
            // Show topics for chosing
            if (_currentContest == null)
            {
                MessageBox.Show("Không có cuộc thi nào diễn ra vào ngày hôm nay!");
                return;
            }

            if (_currentContest.State < 10000 || _currentContest.State > 10100)
            {
                MessageBox.Show("Đang diễn ra 1 câu hỏi, không thể thay đổi chủ đề!");
                return;
            }

            ResetButtons();
            butShowTopics.Background = Brushes.Crimson;

            try
            {
                using (var entities = new Entities())
                {
                    var contest = entities.ContestDays.FirstOrDefault(c => c.ID == _currentContest.ID);
                    contest.State = 10200;
                    entities.SaveChanges();

                    _currentContest.State = contest.State;
                    var status = GetStatus(_currentContest.State);
                    lblStatus.Content = status;

                    _savedContestState = _currentContest.State;
                    var timer = new System.Windows.Threading.DispatcherTimer { Interval = TimeSpan.FromSeconds(_delayTime) };
                    timer.Start();
                    timer.Tick += (s, args) =>
                    {
                        timer.Stop();

                        UpdateContest(_savedContestState, -1);
                    };
                }

                // Show topic list in Screen
                //_waitingScreenWindow.Hide();
                _questionScreenWindow.Hide();
                _scoreScreenWindow.Hide();
                _leaderboardScreenWindow.Hide();
                _topicScreenWindow.Show();

                _topicScreenWindow.ShowTopics(_empties);
            }
            catch (Exception exc)
            {

            }
        }

        private void butStartQuestion_Click(object sender, EventArgs e)
        {
            if (_currentContest == null || _currentQuestion == null)
            {
                return;
            }

            if (_currentContest.State != 10400 && (_currentContest.State % 10 != 1 || _currentContest.State / 10 < 1 || _currentContest.State / 10 >= 1000))
            {
                return;
            }

            ResetButtons();
            butStartQuestion.Background = Brushes.Crimson;

            try
            {
                using (var entities = new Entities())
                {
                    var question = entities.Questions.FirstOrDefault(q => q.ID == _currentQuestion.ID);
                    question.StartTime = DateTime.Now;
                    _currentQuestion.StartTime = question.StartTime;

                    var contest = entities.ContestDays.FirstOrDefault(c => c.ID == _currentContest.ID);
                    if (contest.State == 10400)
                    {
                        contest.State = 10401;
                    }
                    else
                    {
                        contest.State = _currentQuestionNumber * 10 + 2;
                    }
                    entities.SaveChanges();

                    _currentContest.State = contest.State;
                    var status = GetStatus(_currentContest.State);
                    lblStatus.Content = status;
                }

                _questionScreenWindow.StartQuestion(_currentQuestion);

                _currentTimer = 0;
                _delayedQuestion = false;
                timer2.Stop();
                timer1.Start();
                timer4.Start();

                _savedQuestionID = _currentQuestion.ID;
                _savedContestState = _currentContest.State;
                var timer = new Timer(_delayTime * 1000);
                timer.Start();
                timer.Elapsed += (s, args) =>
                {
                    timer.Stop();

                    UpdateContest(false);
                    UpdateQuestionStartTime(_savedQuestionID, DateTime.Now);

                    _currentHomeTimer = 0;
                    _delayedHomeQuestion = false;
                    timer3.Start();
                };
            }
            catch (Exception exc)
            {

            }
        }

        private void butShowCorrect_Click(object sender, EventArgs e)
        {
            if (_currentContest == null)
            {
                MessageBox.Show("Không có cuộc thi nào diễn ra vào ngày hôm nay!");
                return;
            }

            ResetButtons();
            butShowCorrectAnswer.Background = Brushes.Crimson;

            try
            {
                using (var entities = new Entities())
                {
                    var contest = entities.ContestDays.FirstOrDefault(c => c.ID == _currentContest.ID);
                    if (contest.State == 10401)
                    {
                        contest.State = 10402;
                    }
                    else
                    {
                        contest.State = _currentQuestionNumber * 10 + 3;
                    }
                    entities.SaveChanges();

                    _currentContest.State = contest.State;
                    var status = GetStatus(_currentContest.State);
                    lblStatus.Content = status;

                    _savedContestState = _currentContest.State;
                    var timer = new System.Windows.Threading.DispatcherTimer { Interval = TimeSpan.FromSeconds(_delayTime) };
                    timer.Start();
                    timer.Tick += (s, args) =>
                    {
                        timer.Stop();

                        try
                        {
                            using (var cloud = new CungLaTyPhuEntities())
                            {
                                cloud.CopyDailyLeaderboard(_currentContest.ID);
                            }
                        }
                        catch (Exception exc)
                        {
                            textErrorLog.Text += exc.ToString();
                        }

                        UpdateContest(_savedContestState, -1);
                    };
                }

                _questionScreenWindow.ShowCorrectAnswer(_currentQuestion.CorrectAnswer);
            }
            catch (Exception exc)
            {

            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // Start screen window
            _waitingScreenWindow = new WaitingScreenWindow();
            _topicScreenWindow = new TopicScreenWindow();
            _scoreScreenWindow = new ScoreScreenWindow();
            _questionScreenWindow = new QuestionScreenWindow();
            _leaderboardScreenWindow = new LeaderboardScreenWindow();

            timer2.Start();

            try
            {
                // Load initial data
                using (var entities = new Entities())
                {
                    var delaySetting = entities.Settings.FirstOrDefault(s => s.Name.ToLower() == "home_delay");
                    if (delaySetting == null || !int.TryParse(delaySetting.Value, out _delayTime))
                    {
                        _delayTime = 8;
                    }

                    var delayQuestionSetting = entities.Settings.FirstOrDefault(s => s.Name.ToLower() == "question_delay");
                    if (delayQuestionSetting == null || !int.TryParse(delayQuestionSetting.Value, out _delayQuestionTime))
                    {
                        _delayQuestionTime = 3;
                    }

                    var specialQuestionsSetting = entities.Settings.FirstOrDefault(s => s.Name.ToLower() == "special_questions");
                    if (specialQuestionsSetting == null)
                    {
                        specialQuestionsSetting = new Setting()
                        {
                            Value = ""
                        };
                    }

                    _randomizer = new Random((int)DateTime.Now.Ticks);

                    // Today question
                    var startDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    var endDay = startDay.AddDays(1);

                    _currentContest = entities.ContestDays.FirstOrDefault(c => !c.IsDeleted && c.StartTime >= startDay && c.StartTime < endDay);

                    if (_currentContest != null)
                    {
                        _specialTopics = new Dictionary<int, List<int>>();

                        foreach (var item in specialQuestionsSetting.Value.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            var parts = item.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                            if (parts.Length < 2)
                            {
                                continue;
                            }

                            var key = 0;
                            var tempNumber = 0;
                            if (int.TryParse(parts[0], out key))
                            {
                                if (!_specialTopics.ContainsKey(key))
                                {
                                    _specialTopics.Add(key, new List<int>());
                                }
                            }
                            else
                            {
                                continue;
                            }

                            if (int.TryParse(parts[1], out tempNumber))
                            {
                                _specialTopics[key].Add(tempNumber);
                            }
                            else
                            {
                                _specialTopics[key].Add(1);
                            }
                        }

                        _scores = new List<int>();
                        _scores.AddRange(Scores);

                        var questions = entities.Questions.Where(q => !q.IsDeleted && q.ContestDayID == _currentContest.ID && q.Score > 0).ToList();
                        foreach (var q in questions)
                        {
                            if (q.Difficulty == 10)
                            {
                                if (_specialTopics.ContainsKey(q.TopicID))
                                {
                                    _specialTopics[q.TopicID].Remove(questions.Where(ques => ques.TopicID == q.TopicID).OrderBy(ques => ques.Number).Select(ques => ques.Number).ToList().IndexOf(q.Number) + 1);
                                }
                            }
                            _scores.Remove(q.Score);
                        }

                        _currentQuestionNumber = 0;
                        lblContestDate.Content = _currentContest.StartTime.ToShortDateString();
                        lblQuestionNumber.Content = (_currentContest.State >= 10 && _currentContest.State < 10000) ? (_currentContest.State / 10).ToString() : "";
                        lblRemainTime.Content = _currentContest.RemainTime.ToString();
                        var status = GetStatus(_currentContest.State);
                        lblStatus.Content = status;

                        if (_currentContest.State >= 10 && _currentContest.State < 10000 && _currentContest.RemainTime > 0)
                        {
                            _currentQuestionNumber = _currentContest.State / 10;
                            _currentQuestion = entities.Questions.FirstOrDefault(q => !q.IsDeleted && q.ContestDayID == _currentContest.ID && q.Number == _currentQuestionNumber);

                            if (_currentQuestion != null)
                            {
                                lblTopic.Content = _currentQuestion.Topic.TopicContent;
                                lblScore.Content = string.Format("{0}/{1}", _currentQuestion.Score, _currentQuestion.Difficulty);
                                lblQuestion.Content = _currentQuestion.QuestionContent;
                                lblAnswerA.Content = "A. " + _currentQuestion.Answer1;
                                lblAnswerB.Content = "B. " + _currentQuestion.Answer2;
                                lblAnswerC.Content = "C. " + _currentQuestion.Answer3;
                                lblAnswerD.Content = "D. " + _currentQuestion.Answer4;
                            }
                        }
                        else if (_currentContest.State >= 10400)
                        {
                            _currentQuestionNumber = _currentContest.NumberOfQuestions + 1;
                            _currentQuestion = entities.Questions.FirstOrDefault(q => !q.IsDeleted && q.ContestDayID == _currentContest.ID && q.Number == _currentQuestionNumber);

                            if (_currentQuestion != null)
                            {
                                lblTopic.Content = _currentQuestion.Topic.TopicContent;
                                lblScore.Content = string.Format("{0}/{1}", _currentQuestion.Score, _currentQuestion.Difficulty);
                                lblQuestion.Content = _currentQuestion.QuestionContent;
                                lblAnswerA.Content = "A. " + _currentQuestion.Answer1;
                                lblAnswerB.Content = "B. " + _currentQuestion.Answer2;
                                lblAnswerC.Content = "C. " + _currentQuestion.Answer3;
                                lblAnswerD.Content = "D. " + _currentQuestion.Answer4;
                            }
                        }
                    }


                    // Topics
                    _currentTopics = entities.Topics.Where(t => !t.IsDeleted).Take(8).ToList();

                    // Get used topics
                    var usedTopics = entities.Questions.Where(q => q.ContestDayID == _currentContest.ID && q.Number > 0).Select(q => q.Topic.TopicContent);

                    _empties = new Dictionary<string, int>();
                    foreach (var topic in _currentTopics)
                    {
                        _empties.Add(topic.TopicContent, 2 - usedTopics.Count(t => t == topic.TopicContent));
                    }

                    if (_currentTopics.Count > 0)
                    {
                        butTopic1.Content = _currentTopics[0].TopicContent;
                        butTopic1.Tag = _currentTopics[0].ID;
                        if (_empties[_currentTopics[0].TopicContent] == 1)
                        {
                            butTopic1.Background = TopicFirstSelected;
                        }
                        else if (_empties[_currentTopics[0].TopicContent] == 0)
                        {
                            butTopic1.Background = TopicSecondSelected;
                            butTopic1.IsEnabled = false;
                        }
                    }
                    if (_currentTopics.Count > 1)
                    {
                        butTopic2.Content = _currentTopics[1].TopicContent;
                        butTopic2.Tag = _currentTopics[1].ID;
                        if (_empties[_currentTopics[1].TopicContent] == 1)
                        {
                            butTopic2.Background = TopicFirstSelected;
                        }
                        else if (_empties[_currentTopics[1].TopicContent] == 0)
                        {
                            butTopic2.Background = TopicSecondSelected;
                            butTopic2.IsEnabled = false;
                        }
                    }
                    if (_currentTopics.Count > 2)
                    {
                        butTopic3.Content = _currentTopics[2].TopicContent;
                        butTopic3.Tag = _currentTopics[2].ID;
                        if (_empties[_currentTopics[2].TopicContent] == 1)
                        {
                            butTopic3.Background = TopicFirstSelected;
                        }
                        else if (_empties[_currentTopics[2].TopicContent] == 0)
                        {
                            butTopic3.Background = TopicSecondSelected;
                            butTopic3.IsEnabled = false;
                        }
                    }
                    if (_currentTopics.Count > 3)
                    {
                        butTopic4.Content = _currentTopics[3].TopicContent;
                        butTopic4.Tag = _currentTopics[3].ID;
                        if (_empties[_currentTopics[3].TopicContent] == 1)
                        {
                            butTopic4.Background = TopicFirstSelected;
                        }
                        else if (_empties[_currentTopics[3].TopicContent] == 0)
                        {
                            butTopic4.Background = TopicSecondSelected;
                            butTopic4.IsEnabled = false;
                        }
                    }
                    if (_currentTopics.Count > 4)
                    {
                        butTopic5.Content = _currentTopics[4].TopicContent;
                        butTopic5.Tag = _currentTopics[4].ID;
                        if (_empties[_currentTopics[4].TopicContent] == 1)
                        {
                            butTopic5.Background = TopicFirstSelected;
                        }
                        else if (_empties[_currentTopics[4].TopicContent] == 0)
                        {
                            butTopic5.Background = TopicSecondSelected;
                            butTopic5.IsEnabled = false;
                        }
                    }
                    if (_currentTopics.Count > 5)
                    {
                        butTopic6.Content = _currentTopics[5].TopicContent;
                        butTopic6.Tag = _currentTopics[5].ID;
                        if (_empties[_currentTopics[5].TopicContent] == 1)
                        {
                            butTopic6.Background = TopicFirstSelected;
                        }
                        else if (_empties[_currentTopics[5].TopicContent] == 0)
                        {
                            butTopic6.Background = TopicSecondSelected;
                            butTopic6.IsEnabled = false;
                        }
                    }
                    if (_currentTopics.Count > 6)
                    {
                        butTopic7.Content = _currentTopics[6].TopicContent;
                        butTopic7.Tag = _currentTopics[6].ID;
                        if (_empties[_currentTopics[6].TopicContent] == 1)
                        {
                            butTopic7.Background = TopicFirstSelected;
                        }
                        else if (_empties[_currentTopics[6].TopicContent] == 0)
                        {
                            butTopic7.Background = TopicSecondSelected;
                            butTopic7.IsEnabled = false;
                        }
                    }
                    if (_currentTopics.Count > 7)
                    {
                        butTopic8.Content = _currentTopics[7].TopicContent;
                        butTopic8.Tag = _currentTopics[7].ID;
                        if (_empties[_currentTopics[7].TopicContent] == 1)
                        {
                            butTopic8.Background = TopicFirstSelected;
                        }
                        else if (_empties[_currentTopics[7].TopicContent] == 0)
                        {
                            butTopic8.Background = TopicSecondSelected;
                            butTopic8.IsEnabled = false;
                        }
                    }
                }
            }
            catch (Exception exc)
            {

            }
        }

        private string GetStatus(int state)
        {
            if (state >= 10000 && state <= 10099)
            {
                return "Hiển thị thông tin giải thưởng";
            }

            switch (state)
            {
                case 10100:
                    return "Hiển thị thông tin tổng kết cho câu hỏi";
                case 10200:
                    return "Chờ chọn chủ đề";
                case 10300:
                    return "Chương trình đã kết thúc";
                case 10400:
                    return "Bắt đầu vòng 2";
                case 10401:
                    return "Đang chơi vòng 2";
                case 10402:
                    return "Đang kiểm tra đáp án vòng 2";
            }

            var status = state % 10;
            switch (status)
            {
                case 0:
                    return "Hiển thị chủ đề của câu hỏi";
                case 1:
                    return "Hiển thị điểm số của câu hỏi";
                case 2:
                    return "Bắt đầu trả lời câu hỏi";
                case 3:
                    return "Đang kiểm tra đáp án";
            }

            return string.Empty;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            _currentTimer += timer1.Interval.Milliseconds;

            if (_delayedQuestion == false && _currentTimer < _delayQuestionTime * 1000)
            {
                return;
            }
            else if (_delayedQuestion == false)
            {
                _delayedQuestion = true;
                _currentTimer = 0;
            }

            var remain = _currentQuestion.CountdownTime * 1000 - _currentTimer;
            if (remain <= 0)
            {
                lblRemainTime.Content = "0";
                timer1.Stop();
                timer2.Start();

                _questionScreenWindow.SetTime(0, _currentQuestion.CountdownTime * 1000, _currentQuestion.Score);

                //try
                //{
                //    using (var entities = new Entities())
                //    {
                //        var question = entities.Questions.FirstOrDefault(q => q.ID == _currentQuestion.ID);
                //        question.EndTime = DateTime.Now;
                //        _currentQuestion.EndTime = question.EndTime;

                //        var contest = entities.ContestDays.FirstOrDefault(c => c.ID == _currentContest.ID);
                //        contest.RemainTime = 0;
                //        entities.SaveChanges();

                //        _currentContest.RemainTime = 0;
                //        var status = GetStatus(_currentContest.State);
                //        lblStatus.Content = status;
                //    }
                //}
                //catch (Exception exc)
                //{

                //}
            }
            else
            {
                _questionScreenWindow.SetTime(remain, _currentQuestion.CountdownTime * 1000, _currentQuestion.Score);
                lblRemainTime.Content = ((int)Math.Ceiling(remain / 1000.0)).ToString();

                //if (remain % 1000 >= 900)
                //{
                //    try
                //    {
                //        using (var entities = new Entities())
                //        {
                //            var contest = entities.ContestDays.FirstOrDefault(c => c.ID == _currentContest.ID);
                //            contest.RemainTime = ((int)Math.Ceiling(remain / 1000.0));
                //            entities.SaveChanges();

                //            _currentContest.RemainTime = ((int)Math.Ceiling(remain / 1000.0));
                //        }
                //    }
                //    catch (Exception exc)
                //    {

                //    }
                //}
            }
        }

        private void timer3_Tick(object sender, ElapsedEventArgs e)
        {
            _currentHomeTimer += 1; // 1s

            if (_delayedHomeQuestion == false && _currentHomeTimer < _delayQuestionTime)
            {
                return;
            }
            else if (_delayedHomeQuestion == false)
            {
                _delayedHomeQuestion = true;
                _currentHomeTimer = 0;
            }

            var remain = _currentQuestion.CountdownTime - _currentHomeTimer;
            if (remain <= 0)
            {
                timer3.Stop();

                UpdateQuestionEndTime(_savedQuestionID, DateTime.Now);
                UpdateContest(-1, 0);
            }
            else
            {
                UpdateContest(-1, remain);
            }
        }

        private void timer4_Tick(object sender, ElapsedEventArgs e)
        {
            if (_delayedQuestion == false && _currentTimer < _delayQuestionTime * 1000)
            {
                return;
            }

            var remain = _currentQuestion.CountdownTime * 1000 - _currentTimer;
            if (remain <= 0)
            {
                try
                {
                    timer4.Stop();

                    using (var entities = new Entities())
                    {
                        var question = entities.Questions.FirstOrDefault(q => q.ID == _currentQuestion.ID);
                        question.EndTime = DateTime.Now;
                        _currentQuestion.EndTime = question.EndTime;

                        var contest = entities.ContestDays.FirstOrDefault(c => c.ID == _currentContest.ID);
                        contest.RemainTime = 0;
                        entities.SaveChanges();

                        _currentContest.RemainTime = 0;
                        var status = GetStatus(_currentContest.State);
                        lblStatus.Content = status;
                    }
                }
                catch (Exception exc)
                {

                }
            }
            else
            {
                try
                {
                    using (var entities = new Entities())
                    {
                        var contest = entities.ContestDays.FirstOrDefault(c => c.ID == _currentContest.ID);
                        contest.RemainTime = ((int)Math.Ceiling(remain / 1000.0));
                        entities.SaveChanges();

                        _currentContest.RemainTime = ((int)Math.Ceiling(remain / 1000.0));
                    }
                }
                catch (Exception exc)
                {

                }
            }
        }

        private void butStopQuestion_Click(object sender, EventArgs e)
        {
            if (_currentContest == null)
            {
                MessageBox.Show("Không có cuộc thi nào diễn ra vào ngày hôm nay!");
                return;
            }

            ResetButtons();
            butStopQuestion.Background = Brushes.Crimson;

            try
            {
                using (var entities = new Entities())
                {
                    var contest = entities.ContestDays.FirstOrDefault(c => c.ID == _currentContest.ID);
                    if (_currentQuestionNumber > contest.NumberOfQuestions)
                    {
                        contest.State = 10300;
                    }
                    else
                    {
                        contest.State = 10100;
                    }
                    entities.SaveChanges();

                    _currentContest.State = contest.State;
                    var status = GetStatus(_currentContest.State);
                    lblStatus.Content = status;

                    _savedContestState = _currentContest.State;
                    var timer = new System.Windows.Threading.DispatcherTimer { Interval = TimeSpan.FromSeconds(_delayTime) };
                    timer.Start();
                    timer.Tick += (s, args) =>
                    {
                        timer.Stop();

                        UpdateContest(_savedContestState, -1);
                    };
                }

                _topicScreenWindow.Hide();
                _leaderboardScreenWindow.Hide();
                _scoreScreenWindow.Hide();
                _questionScreenWindow.Hide();
                _waitingScreenWindow.Show();
                _waitingScreenWindow.Restart();
            }
            catch (Exception exc)
            {

            }
        }

        private void butStartRound2_Click(object sender, EventArgs e)
        {
            if (_currentContest == null)
            {
                MessageBox.Show("Không có cuộc thi nào diễn ra vào ngày hôm nay!");
                return;
            }

            ResetButtons();
            butStartRound2.Background = Brushes.Crimson;

            try
            {
                using (var entities = new Entities())
                {

                    var topicID = 12;
                    var questions = entities.Questions.Where(q => !q.IsDeleted && q.ContestDayID.HasValue == false && q.TopicID == topicID).ToList();
                    if (questions.Count == 0)
                    {
                        MessageBox.Show("Đã hết câu hỏi trong kho câu hỏi!");
                        return;
                    }

                    var index = _randomizer.Next(0, questions.Count);
                    _currentQuestion = questions[index];

                    ++_currentQuestionNumber;
                    _currentQuestion.Number = _currentQuestionNumber;
                    _currentQuestion.ContestDayID = _currentContest.ID;
                    _currentQuestion.Score = 720;

                    var contest = entities.ContestDays.FirstOrDefault(c => c.ID == _currentContest.ID);
                    contest.State = 10400;
                    contest.RemainTime = _currentQuestion.CountdownTime;
                    entities.SaveChanges();

                    _currentContest.State = contest.State;
                    _currentContest.RemainTime = _currentQuestion.CountdownTime;
                    var status = GetStatus(_currentContest.State);
                    lblStatus.Content = status;

                    _savedQuestionID = _currentQuestion.ID;
                    _savedCurrentQuestionNumber = _currentQuestionNumber;
                    _savedContestState = _currentContest.State;
                    var timer = new System.Windows.Threading.DispatcherTimer { Interval = TimeSpan.FromSeconds(_delayTime) };
                    timer.Start();
                    timer.Tick += (s, args) =>
                    {
                        timer.Stop();

                        UpdateQuestion(_savedQuestionID, _savedCurrentQuestionNumber, _currentContest.ID);
                        UpdateQuestionScore(_savedQuestionID, 720);
                        UpdateContest(_savedContestState, _currentQuestion.CountdownTime);
                    };

                    // Set question info
                    lblQuestionNumber.Content = _currentQuestionNumber.ToString();
                    lblRemainTime.Content = _currentQuestion.CountdownTime.ToString();

                    lblTopic.Content = "Vòng 2";
                    lblQuestion.Content = _currentQuestion.QuestionContent;
                    lblAnswerA.Content = "A. " + _currentQuestion.Answer1;
                    lblAnswerA.Foreground = Brushes.Black;
                    lblAnswerB.Content = "B. " + _currentQuestion.Answer2;
                    lblAnswerB.Foreground = Brushes.Black;
                    lblAnswerC.Content = "C. " + _currentQuestion.Answer3;
                    lblAnswerC.Foreground = Brushes.Black;
                    lblAnswerD.Content = "D. " + _currentQuestion.Answer4;
                    lblAnswerD.Foreground = Brushes.Black;

                    lblScore.Content = string.Format("{0}/{1}", _currentQuestion.Score, _currentQuestion.Difficulty);
                }

                //_waitingScreenWindow.Hide();
                _topicScreenWindow.Hide();
                _leaderboardScreenWindow.Hide();
                _scoreScreenWindow.Hide();
                _questionScreenWindow.Show();

                _questionScreenWindow.ShowQuestionForm("", _currentQuestion);
            }
            catch (Exception exc)
            {

            }
        }

        private void butShowScore_Click(object sender, RoutedEventArgs e)
        {
            if (_currentQuestion == null)
            {
                return;
            }

            ResetButtons();
            butShowScore.Background = Brushes.Crimson;

            //_waitingScreenWindow.Hide();
            _topicScreenWindow.Hide();
            _leaderboardScreenWindow.Hide();
            _questionScreenWindow.Hide();
            _scoreScreenWindow.Show();

            _scoreScreenWindow.ShowScore(_currentQuestion.Number);
        }

        private void butSelectScore_Click(object sender, RoutedEventArgs e)
        {
            if (_currentContest == null)
            {
                MessageBox.Show("Không có cuộc thi nào diễn ra vào ngày hôm nay!");
                return;
            }

            if (_currentContest.State < 10 || _currentContest.State >= 10000 || _currentQuestion == null)
            {
                MessageBox.Show("Chưa chọn chủ đề cho câu hỏi hiện tại hoặc chưa chọn câu hỏi!");
                return;
            }

            if (_currentContest.State % 10 != 0)
            {
                return;
            }

            ResetButtons();
            butSelectScore.Background = Brushes.Crimson;

            var scoreIndex = _randomizer.Next(0, _scores.Count);

            _scoreScreenWindow.StartScrollScore(_scores[scoreIndex], () =>
            {
                try
                {
                    using (var entities = new Entities())
                    {
                        var question = entities.Questions.FirstOrDefault(q => q.ID == _currentQuestion.ID);
                        question.Score = _scores[scoreIndex];

                        var score = _scores[scoreIndex];
                        _currentQuestion.Score = _scores[scoreIndex];
                        _scores.RemoveAt(scoreIndex);

                        var contest = entities.ContestDays.FirstOrDefault(c => c.ID == _currentContest.ID);
                        contest.State = _currentQuestionNumber * 10 + 1;

                        entities.SaveChanges();

                        _currentContest.State = contest.State;

                        _savedContestState = _currentContest.State;
                        _savedQuestionID = _currentQuestion.ID;
                        var timer = new System.Windows.Threading.DispatcherTimer { Interval = TimeSpan.FromSeconds(_delayTime) };
                        timer.Start();
                        timer.Tick += (s, args) =>
                        {
                            timer.Stop();

                            UpdateQuestionScore(_savedQuestionID, score);
                            UpdateContest(_savedContestState, -1);
                        };

                        // Set question info
                        var status = GetStatus(_currentContest.State);
                        lblStatus.Content = status;

                        lblScore.Content = string.Format("{0}/{1}", _currentQuestion.Score, _currentQuestion.Difficulty);
                    }
                }
                catch (Exception exc)
                {

                }
            });
        }

        private void butShowLeaderboard_Click(object sender, RoutedEventArgs e)
        {
            //_waitingScreenWindow.Hide();
            _questionScreenWindow.Hide();
            _scoreScreenWindow.Hide();
            _topicScreenWindow.Hide();
            _leaderboardScreenWindow.Show();

            if (sender == LeaderboardTop10)
            {
                _leaderboardScreenWindow.ShowLeaderboard(1);
            }
            else
            {
                _leaderboardScreenWindow.ShowLeaderboard(2);
            }
        }

        private void butShowTop10_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var entities = new Entities())
                {
                    var startDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    var endDay = startDay.AddDays(1);

                    var contest = entities.ContestDays.FirstOrDefault(c => !c.IsDeleted && c.StartTime >= startDay && c.StartTime < endDay);
                    if (contest == null)
                    {
                        return;
                    }

                    var users = entities.Users
                        .Where(u => !u.IsDeleted)
                        .Select(u => new UserModel()
                                {
                                    ID = u.ID,
                                    Score = u.Answers.Where(a => !a.IsDeleted && a.Score > 0 && a.ContestDayID == contest.ID).GroupBy(a => a.QuestionID).Select(a => a.FirstOrDefault().Score).DefaultIfEmpty(0).Sum()
                                })
                        .OrderByDescending(u => u.Score)
                        .Take(10).ToList();

                    _leaderboardScreenWindow.ShowTop(users);
                }
            }
            catch (Exception exc)
            {

            }
        }

        private void butShowTop50_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var entities = new Entities())
                {
                    var startDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    var endDay = startDay.AddDays(1);

                    var contest = entities.ContestDays.FirstOrDefault(c => !c.IsDeleted && c.StartTime >= startDay && c.StartTime < endDay);
                    if (contest == null)
                    {
                        return;
                    }

                    var users = entities.Users
                        .Where(u => !u.IsDeleted)
                        .Select(u => new UserModel()
                        {
                            ID = u.ID,
                            Score = u.Answers.Where(a => !a.IsDeleted && a.Score > 0).GroupBy(a => a.QuestionID).Select(a => a.FirstOrDefault().Score).DefaultIfEmpty(0).Sum()
                        })
                        .OrderByDescending(u => u.Score)
                        .Take(50).ToList();

                    _leaderboardScreenWindow.ShowTop(users);
                }
            }
            catch (Exception exc)
            {

            }
        }

        private void butShowTop10Home_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var entities = new CungLaTyPhuEntities())
                {
                    var startDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    var endDay = startDay.AddDays(1);

                    var contest = entities.ContestDays.FirstOrDefault(c => !c.IsDeleted && c.StartTime >= startDay && c.StartTime < endDay);
                    if (contest == null)
                    {
                        return;
                    }

                    var users = entities.DayScores
                        .Where(u => u.ContestDayID == contest.ID)
                        .Select(u => new UserModel()
                        {
                            ID = u.UserID,
                            Score = u.TotalScore
                        })
                        .OrderByDescending(u => u.Score)
                        .Take(10).ToList();

                    //var users = entities.HomeUsers
                    //    .Where(u => !u.IsDeleted)
                    //    .Select(u => new UserModel()
                    //    {
                    //        ID = u.ID,
                    //        Score = u.HomeAnswers.Where(a => !a.IsDeleted && a.Score > 0 && a.ContestDayID == contest.ID)
                    //                .GroupBy(a => a.QuestionID).Select(a => a.FirstOrDefault().Score).DefaultIfEmpty(0).Sum()
                    //    })
                    //    .OrderByDescending(u => u.Score)
                    //    .Take(10).ToList();

                    _leaderboardScreenWindow.ShowTop(users);
                }
            }
            catch (Exception exc)
            {

            }
        }

        private void butShowTop50Home_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var entities = new CungLaTyPhuEntities())
                {
                    var startDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    var endDay = startDay.AddDays(1);

                    var contest = entities.ContestDays.FirstOrDefault(c => !c.IsDeleted && c.StartTime >= startDay && c.StartTime < endDay);
                    if (contest == null)
                    {
                        return;
                    }

                    var users = entities.TotalScores
                        .Select(u => new UserModel()
                        {
                            ID = u.UserID,
                            Score = u.TotalScore1
                        })
                        .OrderByDescending(u => u.Score)
                        .Take(50).ToList();

                    //var users = entities.HomeUsers
                    //    .Where(u => !u.IsDeleted)
                    //    .Select(u => new UserModel()
                    //    {
                    //        ID = u.ID,
                    //        Score = u.HomeAnswers.Where(a => !a.IsDeleted && !a.Question.ContestDay.IsDeleted && a.Score > 0).GroupBy(a => a.QuestionID).Select(a => a.FirstOrDefault().Score).DefaultIfEmpty(0).Sum() +
                    //                u.ExchangeScores.Where(s => s.IsValid).Select(s => s.ExchangedScore).DefaultIfEmpty(0).Sum() +
                    //                entities.TransferScores.Where(s => s.ReceiverID == u.ID).Select(s => s.SentScore).DefaultIfEmpty(0).Sum() -
                    //                entities.TransferScores.Where(s => s.SenderID == u.ID).Select(s => s.SentScore).DefaultIfEmpty(0).Sum()
                    //    })
                    //    .OrderByDescending(u => u.Score)
                    //    .Take(50).ToList();

                    _leaderboardScreenWindow.ShowTop(users);
                }
            }
            catch (Exception exc)
            {

            }
        }

        private void butShowQuestionForm_Click(object sender, RoutedEventArgs e)
        {
            if (_currentContest == null || _currentQuestion == null)
            {
                return;
            }

            if (_currentContest.State != 10400 && (_currentContest.State % 10 != 1 || _currentContest.State / 10 < 1 || _currentContest.State / 10 >= 1000))
            {
                return;
            }

            ResetButtons();
            butShowQuestionScreen.Background = Brushes.Crimson;

            //_waitingScreenWindow.Hide();
            _scoreScreenWindow.Hide();
            _topicScreenWindow.Hide();
            _leaderboardScreenWindow.Hide();
            _questionScreenWindow.Show();

            _questionScreenWindow.ShowQuestionForm(lblTopic.Content.ToString(), _currentQuestion);
        }

        private void butShowQuestion_Click(object sender, RoutedEventArgs e)
        {
            if (_currentContest == null || _currentQuestion == null)
            {
                return;
            }

            if (_currentContest.State != 10400 && (_currentContest.State % 10 != 1 || _currentContest.State / 10 < 1 || _currentContest.State / 10 >= 1000))
            {
                return;
            }

            ResetButtons();
            butShowQuestion.Background = Brushes.Crimson;

            _questionScreenWindow.ShowQuestion(_currentQuestion);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            _waitingScreenWindow.Close();
            _topicScreenWindow.Close();
            _scoreScreenWindow.Close();
            _leaderboardScreenWindow.Close();
            _questionScreenWindow.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _topicScreenWindow.Show();
            _scoreScreenWindow.Show();
            _questionScreenWindow.Show();
            _leaderboardScreenWindow.Show();
            _waitingScreenWindow.Show();

            _waitingScreenWindow.ChangeToFullScreen();

            _topicScreenWindow.ChangeToFullScreen();
            _topicScreenWindow.Hide();

            _scoreScreenWindow.ChangeToFullScreen();
            _scoreScreenWindow.Hide();

            _leaderboardScreenWindow.ChangeToFullScreen();
            _leaderboardScreenWindow.Hide();

            _questionScreenWindow.ChangeToFullScreen();
            _questionScreenWindow.Hide();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            try
            {
                using (var entities = new Entities())
                {
                    var users = entities.Users
                        .Where(u => !u.IsDeleted)
                        .Select(u => new UserModel()
                        {
                            ID = u.ID,
                            Fullname = u.Fullname,
                            IsPlaying = u.IsPlaying,
                            Score = u.Answers.Where(a => !a.IsDeleted && a.Score > 0 && a.Question.ContestDay.StartTime.Day == DateTime.Now.Day && a.Question.ContestDay.StartTime.Month == DateTime.Now.Month && a.Question.ContestDay.StartTime.Year == DateTime.Now.Year).Sum(a => (long)a.Score),
                            DayScore = u.Answers.Where(a => !a.IsDeleted && a.Score > 0 && a.Question.ContestDay.StartTime.Day == DateTime.Now.Day && a.Question.ContestDay.StartTime.Month == DateTime.Now.Month && a.Question.ContestDay.StartTime.Year == DateTime.Now.Year).Sum(a => (long)a.Score)
                        })
                        .OrderByDescending(u => u.DayScore)
                        .ToList();

                    // Update list
                    for (var i = 0; i < 50; i++)
                    {
                        var button = UserList.FindName("User" + (i + 1).ToString("00")) as Button;
                        if (button != null)
                        {
                            button.Tag = -1;
                            button.Visibility = System.Windows.Visibility.Hidden;
                        }
                    }

                    for (var i = 0; i < users.Count; i++)
                    {
                        var button = UserList.FindName("User" + (i + 1).ToString("00")) as Button;
                        if (button != null)
                        {
                            button.Tag = users[i].ID;
                            button.Visibility = System.Windows.Visibility.Visible;
                            button.ToolTip = users[i].Fullname + " (" + users[i].Score.GetValueOrDefault(0).ToString() + ")";
                            button.Content = users[i].ID.ToString() + " (" + users[i].DayScore.GetValueOrDefault(0).ToString() + ")";
                            if (users[i].IsPlaying)
                            {
                                button.Background = Brushes.Green;
                            }
                            else
                            {
                                button.Background = Brushes.LightGray;
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {

            }
        }

        private void UserButton_Click(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;
            if (button.Tag == null)
            {
                return;
            }

            try
            {
                using (var entities = new Entities())
                {
                    var id = (long)button.Tag;
                    var user = entities.Users.FirstOrDefault(u => u.ID == id);
                    if (user != null)
                    {
                        user.IsPlaying = !user.IsPlaying;
                        if (user.IsPlaying)
                        {
                            user.Seat = (entities.Users.Count(u => u.Seat == "1" || u.Seat == "2" || u.Seat == "3" || u.Seat == "4") + 1).ToString();
                            button.Background = Brushes.Green;
                        }
                        else
                        {
                            user.Seat = "9";
                            button.Background = Brushes.LightGray;
                        }

                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception exc)
            {

            }
        }

        private void ResetButtons()
        {
            butShowTopics.Background = Brushes.LightGray;
            butShowScore.Background = Brushes.LightGray;
            butSelectScore.Background = Brushes.LightGray;
            butShowQuestionScreen.Background = Brushes.LightGray;
            butShowQuestion.Background = Brushes.LightGray;
            butStartRound2.Background = Brushes.LightGray;
            butStartQuestion.Background = Brushes.LightGray;
            butShowCorrectAnswer.Background = Brushes.LightGray;
            butStopQuestion.Background = Brushes.LightGray;
        }
    }
}
