﻿using System;
using System.Globalization;
using System.Timers;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;

namespace Game_Controller_WPF
{
    /// <summary>
    /// Interaction logic for ScreenWindow.xaml
    /// </summary>
    public partial class QuestionScreenWindow : Window
    {
        private const double VideoOriginalWidth = 788.0;
        private const double VideoOriginalHeight = 576.0;

        private MediaPlayer _showQuestionAudio;
        private MediaPlayer _countdownAudio;
        //private MediaPlayer _correctAnswerAudio;

        private string _topic;

        public QuestionScreenWindow()
        {
            InitializeComponent();
        }

        public void ChangeToFullScreen()
        {
            // Get secondary screen
            var screen = System.Windows.Forms.Screen.PrimaryScreen;
            foreach (var s in System.Windows.Forms.Screen.AllScreens)
            {
                if (s.Primary == false)
                {
                    screen = s;
                    break;
                }
            }

            var screenWidth = (double)screen.Bounds.Width;
            var screenHeight = (double)screen.Bounds.Height;
            var ratioWidth = screenWidth / VideoOriginalWidth;
            var ratioHeight = screenHeight / VideoOriginalHeight;
            MainGrid.LayoutTransform = new ScaleTransform(ratioWidth, ratioHeight);
            this.Left = screen.Bounds.X;
            this.Top = screen.Bounds.Y;
            this.Width = screen.Bounds.Width;
            this.Height = screen.Bounds.Height;
        }

        public void ShowQuestionForm(string topic, Question question)
        {
            var videoWidth = QuestionBackground.ActualWidth;
            var videoHeight = QuestionBackground.ActualHeight;
            var ratioWidth = videoWidth / VideoOriginalWidth;
            var ratioHeight = videoHeight / VideoOriginalHeight;
            var ratio = Math.Min(ratioWidth, ratioHeight);
            var distanceX = (videoWidth - VideoOriginalWidth * ratio) / 2;

            CorrectAnswer.Visibility = Visibility.Hidden;

            Clock.Visibility = Visibility.Hidden;
            Progress.Visibility = Visibility.Hidden;
            ProgressThumb.Visibility = Visibility.Hidden;
            Timer.Visibility = Visibility.Hidden;
            //Countdown.Visibility = Visibility.Hidden;

            //Topic.LayoutTransform = new ScaleTransform(ratio, ratio);
            //Topic.Margin = new Thickness(distanceX + -70 * ratio, 15 * ratio, 0, 0);
            Question.LayoutTransform = new ScaleTransform(ratio, ratio);
            Question.Margin = new Thickness(distanceX + 172 * ratio, 0, 0, 225 * ratio);
            Answer1.LayoutTransform = new ScaleTransform(ratio, ratio);
            Answer1Block.LayoutTransform = new ScaleTransform(ratio, ratio);
            Answer1Block.Margin = new Thickness(distanceX + 190 * ratio, 0, 0, 130 * ratio);
            Answer2.LayoutTransform = new ScaleTransform(ratio, ratio);
            Answer2Block.LayoutTransform = new ScaleTransform(ratio, ratio);
            Answer2Block.Margin = new Thickness(distanceX + 530 * ratio, 0, 0, 130 * ratio);
            Answer3.LayoutTransform = new ScaleTransform(ratio, ratio);
            Answer3Block.LayoutTransform = new ScaleTransform(ratio, ratio);
            Answer3Block.Margin = new Thickness(distanceX + 190 * ratio, 0, 0, 60 * ratio);
            Answer4.LayoutTransform = new ScaleTransform(ratio, ratio);
            Answer4Block.LayoutTransform = new ScaleTransform(ratio, ratio);
            Answer4Block.Margin = new Thickness(distanceX + 530 * ratio, 0, 0, 60 * ratio);
            Clock.LayoutTransform = new ScaleTransform(ratio, ratio);
            Clock.Margin = new Thickness(distanceX + 118 * ratio, 0, 0, 194 * ratio);
            Timer.LayoutTransform = new ScaleTransform(ratio, ratio);
            Timer.Margin = new Thickness(distanceX + 587 * ratio, 0, 0, 194 * ratio);
            Progress.LayoutTransform = new ScaleTransform(ratio, ratio);
            Progress.Margin = new Thickness(distanceX + 155 * ratio, 0, 0, 200 * ratio);
            ProgressThumb.LayoutTransform = new ScaleTransform(ratio, ratio);
            ProgressThumb.Margin = new Thickness(distanceX + 157 * ratio, 0, 0, 201 * ratio);
            //Countdown.LayoutTransform = new ScaleTransform(ratio, ratio);
            //Countdown.Margin = new Thickness(distanceX + 110 * ratio, 0, 0, 194 * ratio);

            // Restart videos
            _topic = topic;

            //Topic.Visibility = System.Windows.Visibility.Visible;
            if (topic == "Văn học")
            {
                //Topic.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Van Hoc.mpg");
                QuestionBackground.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Question Van Hoc.mpg");
            }
            else if (topic == "Giải trí")
            {
                //Topic.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Giai Tri.mpg");
                QuestionBackground.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Question Giai Tri.mpg");
            }
            else if (topic == "Sức khỏe")
            {
                //Topic.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Suc Khoe.mpg");
                QuestionBackground.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Question Suc Khoe.mpg");
            }
            else if (topic == "Địa lý")
            {
                //Topic.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Dia Ly.mpg");
                QuestionBackground.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Question Dia Ly.mpg");
            }
            else if (topic == "Lịch sử")
            {
                //Topic.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Lich Su.mpg");
                QuestionBackground.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Question Lich Su.mpg");
            }
            else if (topic == "Văn hóa - Xã hội")
            {
                //Topic.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Van Hoa.mpg");
                QuestionBackground.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Question Van Hoa.mpg");
            }
            else if (topic == "Thể thao")
            {
                //Topic.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/The Thao.mpg");
                QuestionBackground.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Question The Thao.mpg");
            }
            else if (topic == "Khoa học - Công nghệ")
            {
                //Topic.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Cong Nghe.mpg");
                QuestionBackground.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Question Cong Nghe.mpg");
            }
            else
            {
                //Topic.Visibility = System.Windows.Visibility.Hidden;
                QuestionBackground.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Question.mpg");
            }
            //Topic.Play();

            //QuestionBackground.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Question.mpg");
            QuestionBackground.Play();

            //Countdown.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/thanh diem_25s.avi");

            var timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(4) };
            timer.Start();
            timer.Tick += (s, args) =>
            {
                timer.Stop();
                Clock.Visibility = Visibility.Visible;
                Progress.Visibility = Visibility.Visible;
                ProgressThumb.Visibility = Visibility.Visible;
                Timer.Visibility = Visibility.Visible;
                //Countdown.Visibility = Visibility.Visible;
            };

            Timer.Text = string.Format("{0,3:000}", question.Score);
            Question.Text = "";
            Answer1.Text = "";
            Answer2.Text = "";
            Answer3.Text = "";
            Answer4.Text = "";

            ProgressThumb.Width = 425;
        }

        public void ShowQuestion(Question question)
        {
            var formattedText = new FormattedText(
                                question.QuestionContent.Trim(new char[] { ' ', '\n', '\r', '\t' }),
                                CultureInfo.CurrentUICulture,
                                FlowDirection.LeftToRight,
                                new Typeface(this.Question.FontFamily, this.Question.FontStyle, this.Question.FontWeight, this.Question.FontStretch),
                                this.Question.FontSize,
                                Question.Foreground);
            Question.Height = formattedText.Height * Math.Ceiling(formattedText.Width / (Question.Width - 50));
            Question.Text = question.QuestionContent;

            if (_showQuestionAudio != null)
            {
                _showQuestionAudio.Position = TimeSpan.FromSeconds(0);
                _showQuestionAudio.Play();
            }
        }

        public void StartQuestion(Question question)
        {
            //var formattedText = new FormattedText(
            //                    "A. " + question.Answer1,
            //                    CultureInfo.CurrentUICulture,
            //                    FlowDirection.LeftToRight,
            //                    new Typeface(this.Answer1.FontFamily, this.Answer1.FontStyle, this.Answer1.FontWeight, this.Answer1.FontStretch),
            //                    this.Answer1.FontSize,
            //                    Answer1.Foreground);
            //Answer1.Height = formattedText.Height * Math.Ceiling(formattedText.Width / Answer1.Width);
            Answer1.Text = "A. " + question.Answer1;

            //formattedText = new FormattedText(
            //                    "B. " + question.Answer2,
            //                    CultureInfo.CurrentUICulture,
            //                    FlowDirection.LeftToRight,
            //                    new Typeface(this.Answer2.FontFamily, this.Answer2.FontStyle, this.Answer2.FontWeight, this.Answer2.FontStretch),
            //                    this.Answer2.FontSize,
            //                    Answer2.Foreground);
            //Answer2.Height = formattedText.Height * Math.Ceiling(formattedText.Width / Answer2.Width);
            Answer2.Text = "B. " + question.Answer2;

            //formattedText = new FormattedText(
            //                    "C. " + question.Answer3,
            //                    CultureInfo.CurrentUICulture,
            //                    FlowDirection.LeftToRight,
            //                    new Typeface(this.Answer3.FontFamily, this.Answer3.FontStyle, this.Answer3.FontWeight, this.Answer3.FontStretch),
            //                    this.Answer3.FontSize,
            //                    Answer3.Foreground);
            //Answer3.Height = formattedText.Height * Math.Ceiling(formattedText.Width / Answer3.Width);
            Answer3.Text = "C. " + question.Answer3;

            //formattedText = new FormattedText(
            //                    "D. " + question.Answer4,
            //                    CultureInfo.CurrentUICulture,
            //                    FlowDirection.LeftToRight,
            //                    new Typeface(this.Answer4.FontFamily, this.Answer4.FontStyle, this.Answer4.FontWeight, this.Answer4.FontStretch),
            //                    this.Answer4.FontSize,
            //                    Answer4.Foreground);
            //Answer4.Height = formattedText.Height * Math.Ceiling(formattedText.Width / Answer4.Width);
            Answer4.Text = "D. " + question.Answer4;

            //var timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(2) };
            //timer.Start();
            //timer.Tick += (s, args) =>
            //{
            //    timer.Stop();
            //    if (_countdownAudio != null)
            //    {
            //        _countdownAudio.Position = TimeSpan.FromSeconds(0);
            //        _countdownAudio.Play();
            //    }
            //};

            //Countdown.Play();
        }

        public void SetTime(int time, int totalTime, int score)
        {
            ProgressThumb.Width = (double)time / totalTime * 425;
            Timer.Text = string.Format("{0,3:000}", score * time / totalTime);
        }

        public void ShowCorrectAnswer(int correct)
        {
            var videoWidth = CorrectAnswer.ActualWidth;
            var videoHeight = CorrectAnswer.ActualHeight;
            var ratioWidth = 788.0 / 720;
            var ratioHeight = videoHeight / VideoOriginalHeight;
            var ratio = Math.Min(ratioWidth, ratioHeight);
            var distanceX = (videoWidth - VideoOriginalWidth * ratio) / 2;

            CorrectAnswer.LayoutTransform = new ScaleTransform(ratioWidth, ratioHeight);

            var fileName = "";

            if (_topic == "Văn học")
            {
                fileName = System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Correct Answer Van Hoc";
            }
            else if (_topic == "Giải trí")
            {
                fileName = System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Correct Answer Giai Tri";
            }
            else if (_topic == "Sức khỏe")
            {
                fileName = System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Correct Answer Suc Khoe";
            }
            else if (_topic == "Địa lý")
            {
                fileName = System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Correct Answer Dia Ly";
            }
            else if (_topic == "Lịch sử")
            {
                fileName = System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Correct Answer Lich Su";
            }
            else if (_topic == "Văn hóa - Xã hội")
            {
                fileName = System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Correct Answer Van Hoa";
            }
            else if (_topic == "Thể thao")
            {
                fileName = System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Correct Answer The Thao";
            }
            else if (_topic == "Khoa học - Công nghệ")
            {
                fileName = System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Correct Answer Khoa Hoc";
            }
            else
            {
                fileName = System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Correct Answer";
            }

            if (correct == 1)
            {
                CorrectAnswer.Source = new Uri(fileName + " A.mpg");
            }
            else if (correct == 2)
            {
                CorrectAnswer.Source = new Uri(fileName + " B.mpg");
            }
            else if (correct == 3)
            {
                CorrectAnswer.Source = new Uri(fileName + " C.mpg");
            }
            else if (correct == 4)
            {
                CorrectAnswer.Source = new Uri(fileName + " D.mpg");
            }

            //if (_correctAnswerAudio != null)
            //{
            //    _correctAnswerAudio.Position = TimeSpan.FromSeconds(0);
            //    _correctAnswerAudio.Play();
            //}
        }

        private void CorrectAnswer_MediaOpened(object sender, RoutedEventArgs e)
        {
            CorrectAnswer.Visibility = System.Windows.Visibility.Visible;
            CorrectAnswer.Play();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _showQuestionAudio = new MediaPlayer();
            _showQuestionAudio.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/Show Question.wav"));

            _countdownAudio = new MediaPlayer();
            _countdownAudio.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/Countdown.mp3"));

            //_correctAnswerAudio = new MediaPlayer();
            //_correctAnswerAudio.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/Correct Answer.wav"));
        }
    }
}
