﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game_Controller_WPF
{
    public class UserModel
    {
        public long ID
        {
            get;
            set;
        }

        public string Fullname
        {
            get;
            set;
        }

        public bool IsPlaying
        {
            get;
            set;
        }

        public string Email
        {
            get;
            set;
        }

        public long? Score
        {
            get;
            set;
        }

        public long? DayScore
        {
            get;
            set;
        }
    }
}
