﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Threading;

namespace Game_Controller_WPF
{
    /// <summary>
    /// Interaction logic for ScreenWindow.xaml
    /// </summary>
    public partial class ScoreScreenWindow : Window
    {
        private const double VideoOriginalWidth = 788.0;
        private const double VideoOriginalHeight = 576.0;

        private MediaPlayer _scoreAudio;

        private MediaPlayer _score030Audio;
        private MediaPlayer _score060Audio;
        private MediaPlayer _score090Audio;
        private MediaPlayer _score120Audio;
        private MediaPlayer _score150Audio;
        private MediaPlayer _score180Audio;
        private MediaPlayer _score210Audio;
        private MediaPlayer _score240Audio;
        private MediaPlayer _score270Audio;
        private MediaPlayer _score300Audio;
        private MediaPlayer _score330Audio;
        private MediaPlayer _score360Audio;
        private MediaPlayer _score390Audio;
        private MediaPlayer _score420Audio;
        private MediaPlayer _score450Audio;
        private MediaPlayer _score480Audio;


        public ScoreScreenWindow()
        {
            InitializeComponent();
        }

        #region Scrolling Score
        private Random _random = new Random((int)DateTime.Now.Ticks);
        private const int MaxScrollingTime = 3000;
        private int _timer;
        private int _scoreNumber;
        private string _stopNumber;
        private List<int> _speeds;
        private List<bool> _stopped;
        private System.Windows.Threading.DispatcherTimer _scoreTimer;

        private Action _completedFunc = null;

        private void ScoreAnimation(object sender, EventArgs e)
        {
            _timer += _scoreTimer.Interval.Milliseconds;
            if (_timer >= 1000)
            {
                if (_stopped[0] == false)
                {
                    _stopped[0] = MoveTextBlock(((TextBlock)ScoreCanvas.Children[0]), ((TextBlock)ScoreCanvas.Children[1]), _speeds[0], _stopNumber[0].ToString());
                }
                if (_stopped[1] == false)
                {
                    _stopped[1] = MoveTextBlock(((TextBlock)ScoreCanvas.Children[2]), ((TextBlock)ScoreCanvas.Children[3]), _speeds[1], _stopNumber[1].ToString());
                }
                if (_stopped[2] == false)
                {
                    _stopped[2] = MoveTextBlock(((TextBlock)ScoreCanvas.Children[4]), ((TextBlock)ScoreCanvas.Children[5]), _speeds[2], _stopNumber[2].ToString());
                }
            }

            if (_stopped[0] == true && _stopped[1] == true && _stopped[2] == true)
            {
                _scoreTimer.Stop();
                if (_scoreAudio != null)
                {
                    _scoreAudio.Stop();
                }

                if (_completedFunc != null)
                {
                    _completedFunc();
                }

                //if (_scoreNumber == 30 && _score030Audio != null)
                //{
                //    _score030Audio.Position = TimeSpan.FromSeconds(0);
                //    _score030Audio.Play();
                //}
                //else if (_scoreNumber == 60 && _score060Audio != null)
                //{
                //    _score060Audio.Position = TimeSpan.FromSeconds(0);
                //    _score060Audio.Play();
                //}
                //else if (_scoreNumber == 90 && _score090Audio != null)
                //{
                //    _score090Audio.Position = TimeSpan.FromSeconds(0);
                //    _score090Audio.Play();
                //}
                //else if (_scoreNumber == 120 && _score120Audio != null)
                //{
                //    _score120Audio.Position = TimeSpan.FromSeconds(0);
                //    _score120Audio.Play();
                //}
                //else if (_scoreNumber == 150 && _score150Audio != null)
                //{
                //    _score150Audio.Position = TimeSpan.FromSeconds(0);
                //    _score150Audio.Play();
                //}
                //else if (_scoreNumber == 180 && _score180Audio != null)
                //{
                //    _score180Audio.Position = TimeSpan.FromSeconds(0);
                //    _score180Audio.Play();
                //}
                //else if (_scoreNumber == 210 && _score210Audio != null)
                //{
                //    _score210Audio.Position = TimeSpan.FromSeconds(0);
                //    _score210Audio.Play();
                //}
                //else if (_scoreNumber == 240 && _score240Audio != null)
                //{
                //    _score240Audio.Position = TimeSpan.FromSeconds(0);
                //    _score240Audio.Play();
                //}
                //else if (_scoreNumber == 270 && _score270Audio != null)
                //{
                //    _score270Audio.Position = TimeSpan.FromSeconds(0);
                //    _score270Audio.Play();
                //}
                //else if (_scoreNumber == 300 && _score300Audio != null)
                //{
                //    _score300Audio.Position = TimeSpan.FromSeconds(0);
                //    _score300Audio.Play();
                //}
                //else if (_scoreNumber == 330 && _score330Audio != null)
                //{
                //    _score330Audio.Position = TimeSpan.FromSeconds(0);
                //    _score330Audio.Play();
                //}
                //else if (_scoreNumber == 360 && _score360Audio != null)
                //{
                //    _score360Audio.Position = TimeSpan.FromSeconds(0);
                //    _score360Audio.Play();
                //}
                //else if (_scoreNumber == 390 && _score390Audio != null)
                //{
                //    _score390Audio.Position = TimeSpan.FromSeconds(0);
                //    _score390Audio.Play();
                //}
                //else if (_scoreNumber == 420 && _score420Audio != null)
                //{
                //    _score420Audio.Position = TimeSpan.FromSeconds(0);
                //    _score420Audio.Play();
                //}
                //else if (_scoreNumber == 450 && _score450Audio != null)
                //{
                //    _score450Audio.Position = TimeSpan.FromSeconds(0);
                //    _score450Audio.Play();
                //}
                //else if (_scoreNumber == 480 && _score480Audio != null)
                //{
                //    _score480Audio.Position = TimeSpan.FromSeconds(0);
                //    _score480Audio.Play();
                //}
            }
        }

        private bool MoveTextBlock(TextBlock block1, TextBlock block2, int speed, string comparedNumber)
        {
            var stop = false;

            var top1 = Canvas.GetTop(block1);
            var top2 = Canvas.GetTop(block2);

            var curSpeed = Math.Max(10, Math.Min(speed, speed - (_timer - MaxScrollingTime) * (float)(speed - 10) / 2000));
            top1 -= curSpeed;
            top2 -= curSpeed;

            if (top2 <= 10)
            {
                // Below reached top
                block1.Text = block2.Text;
                var number = int.Parse(block2.Text);
                number = number == 0 ? 9 : number - 1;
                block2.Text = number.ToString();

                if (curSpeed <= 10 && block1.Text == comparedNumber)
                {
                    top2 = 10;
                    stop = true;
                }

                top1 = top2;
                top2 = top1 + block1.Height + 10;
            }

            Canvas.SetTop(block1, top1);
            Canvas.SetTop(block2, top2);

            return stop;
        }

        private void StartScrollingNumber(int number)
        {
            _stopped = new List<bool>();
            _stopped.Add(false);
            _stopped.Add(false);
            _stopped.Add(false);

            // Generate speeds
            _speeds = new List<int>();
            _speeds.Add(10 * _random.Next(6, 11));
            _speeds.Add(10 * _random.Next(6, 11));
            _speeds.Add(10 * _random.Next(6, 11));

            _timer = 0;
            _stopNumber = number.ToString("D3");

            _scoreTimer = new System.Windows.Threading.DispatcherTimer();
            _scoreTimer.Tick += new EventHandler(ScoreAnimation);
            _scoreTimer.Interval = new TimeSpan(0, 0, 0, 0, 20);
            _scoreTimer.Start();
        }
        #endregion

        public void ChangeToFullScreen()
        {
            // Get secondary screen
            var screen = System.Windows.Forms.Screen.PrimaryScreen;
            foreach (var s in System.Windows.Forms.Screen.AllScreens)
            {
                if (s.Primary == false)
                {
                    screen = s;
                    break;
                }
            }

            var screenWidth = (double)screen.Bounds.Width;
            var screenHeight = (double)screen.Bounds.Height;
            var ratioWidth = screenWidth / VideoOriginalWidth;
            var ratioHeight = screenHeight / VideoOriginalHeight;
            MainGrid.LayoutTransform = new ScaleTransform(ratioWidth, ratioHeight);
            this.Left = screen.Bounds.X;
            this.Top = screen.Bounds.Y;
            this.Width = screen.Bounds.Width;
            this.Height = screen.Bounds.Height;
        }

        public void ShowScore(int number)
        {
            ScoreTitle.Visibility = System.Windows.Visibility.Hidden;
            ScoreCanvas.Visibility = System.Windows.Visibility.Hidden;

            var videoWidth = ScoreBackground.ActualWidth;
            var videoHeight = ScoreBackground.ActualHeight;
            var ratioWidth = videoWidth / VideoOriginalWidth;
            var ratioHeight = videoHeight / VideoOriginalHeight;
            var ratio = Math.Min(ratioWidth, ratioHeight);
            var distanceX = (videoWidth - VideoOriginalWidth * ratio) / 2;

            ScoreTitle.Opacity = 0;
            ScoreCanvas.Opacity = 0;

            ScoreTitle.LayoutTransform = new ScaleTransform(ratio, ratio);
            ScoreCanvas.LayoutTransform = new ScaleTransform(ratio, ratio);

            ScoreTitle.Text = "Số điểm của câu " + number.ToString();
            ((TextBlock)ScoreCanvas.Children[0]).Text = "9";
            Canvas.SetTop(ScoreCanvas.Children[0], 10);
            ((TextBlock)ScoreCanvas.Children[1]).Text = "8";
            Canvas.SetTop(ScoreCanvas.Children[1], 159);
            ((TextBlock)ScoreCanvas.Children[2]).Text = "9";
            Canvas.SetTop(ScoreCanvas.Children[2], 10);
            ((TextBlock)ScoreCanvas.Children[3]).Text = "8";
            Canvas.SetTop(ScoreCanvas.Children[3], 159);
            ((TextBlock)ScoreCanvas.Children[4]).Text = "9";
            Canvas.SetTop(ScoreCanvas.Children[4], 10);
            ((TextBlock)ScoreCanvas.Children[5]).Text = "8";
            Canvas.SetTop(ScoreCanvas.Children[5], 159);

            var story = new Storyboard();

            var fadingAnimation = new DoubleAnimation();
            fadingAnimation.From = 0;
            fadingAnimation.To = 1;
            fadingAnimation.Duration = new Duration(TimeSpan.FromSeconds(2));
            fadingAnimation.AutoReverse = false;

            //var zoomXAnimation = new DoubleAnimation();
            //zoomXAnimation.From = 0;
            //zoomXAnimation.To = ratio;
            //zoomXAnimation.Duration = new Duration(TimeSpan.FromSeconds(2));
            //zoomXAnimation.AutoReverse = false;

            //var zoomYAnimation = new DoubleAnimation();
            //zoomYAnimation.From = 0;
            //zoomYAnimation.To = ratio;
            //zoomYAnimation.Duration = new Duration(TimeSpan.FromSeconds(2));
            //zoomYAnimation.AutoReverse = false;

            //story.Children.Add(fadingAnimation);
            //story.Children.Add(zoomXAnimation);
            //story.Children.Add(zoomYAnimation);

            //Storyboard.SetTarget(fadingAnimation, ScoreTitle);
            //Storyboard.SetTarget(zoomXAnimation, ScoreTitle.LayoutTransform as ScaleTransform);
            //Storyboard.SetTarget(zoomYAnimation, ScoreTitle.LayoutTransform as ScaleTransform);

            //Storyboard.SetTarget(fadingAnimation, ScoreCanvas);
            //Storyboard.SetTarget(zoomXAnimation, ScoreCanvas.LayoutTransform as ScaleTransform);
            //Storyboard.SetTarget(zoomYAnimation, ScoreCanvas.LayoutTransform as ScaleTransform);

            //Storyboard.SetTargetProperty(fadingAnimation, new PropertyPath(UIElement.OpacityProperty));
            //Storyboard.SetTargetProperty(zoomXAnimation, new PropertyPath(ScaleTransform.ScaleXProperty));
            //Storyboard.SetTargetProperty(zoomYAnimation, new PropertyPath(ScaleTransform.ScaleYProperty));

            // Restart videos
            ScoreBackground.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Score.mpg");
            ScoreBackground.Play();
            var timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(2) };
            timer.Start();
            timer.Tick += (s, args) =>
            {
                timer.Stop();
                ScoreTitle.Visibility = System.Windows.Visibility.Visible;
                ScoreCanvas.Visibility = System.Windows.Visibility.Visible;

                ScoreTitle.BeginAnimation(TextBlock.OpacityProperty, fadingAnimation);
                ScoreCanvas.BeginAnimation(Canvas.OpacityProperty, fadingAnimation);

                //story.Begin();
            };
        }

        public void StartScrollScore(int score, Action completed)
        {
            _completedFunc = completed;
            StartScrollingNumber(score);

            //if (_scoreAudio != null)
            //{
            //    _scoreAudio.Position = TimeSpan.FromSeconds(0);
            //    _scoreAudio.Play();
            //}

            _scoreNumber = score;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _scoreAudio = new MediaPlayer();
            _scoreAudio.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/Score.wav"));

            _score030Audio = new MediaPlayer();
            _score030Audio.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/30.wav"));

            _score060Audio = new MediaPlayer();
            _score060Audio.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/60.wav"));

            _score090Audio = new MediaPlayer();
            _score090Audio.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/90.wav"));

            _score120Audio = new MediaPlayer();
            _score120Audio.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/120.wav"));

            _score150Audio = new MediaPlayer();
            _score150Audio.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/150.wav"));

            _score180Audio = new MediaPlayer();
            _score180Audio.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/180.wav"));

            _score210Audio = new MediaPlayer();
            _score210Audio.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/210.wav"));

            _score240Audio = new MediaPlayer();
            _score240Audio.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/240.wav"));

            _score270Audio = new MediaPlayer();
            _score270Audio.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/270.wav"));

            _score300Audio = new MediaPlayer();
            _score300Audio.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/300.wav"));

            _score330Audio = new MediaPlayer();
            _score330Audio.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/330.wav"));

            _score360Audio = new MediaPlayer();
            _score360Audio.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/360.wav"));

            _score390Audio = new MediaPlayer();
            _score390Audio.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/390.wav"));

            _score420Audio = new MediaPlayer();
            _score420Audio.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/420.wav"));

            _score450Audio = new MediaPlayer();
            _score450Audio.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/450.wav"));

            _score480Audio = new MediaPlayer();
            _score480Audio.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/480.wav"));
        }
    }
}
