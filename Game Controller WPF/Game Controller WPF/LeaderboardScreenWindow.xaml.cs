﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;

namespace Game_Controller_WPF
{
    /// <summary>
    /// Interaction logic for ScreenWindow.xaml
    /// </summary>
    public partial class LeaderboardScreenWindow : Window
    {
        private const double VideoOriginalWidth = 788.0;
        private const double VideoOriginalHeight = 576.0;

        private int _type;

        private List<UserModel> _list;
        private int _page;

        private MediaPlayer _leaderboardAudio;
        private bool _isShowing;

        public LeaderboardScreenWindow()
        {
            InitializeComponent();
            _isShowing = false;
        }

        public void ChangeToFullScreen()
        {
            // Get secondary screen
            var screen = System.Windows.Forms.Screen.PrimaryScreen;
            foreach (var s in System.Windows.Forms.Screen.AllScreens)
            {
                if (s.Primary == false)
                {
                    screen = s;
                    break;
                }
            }

            var screenWidth = (double)screen.Bounds.Width;
            var screenHeight = (double)screen.Bounds.Height;
            var ratioWidth = screenWidth / VideoOriginalWidth;
            var ratioHeight = screenHeight / VideoOriginalHeight;
            MainGrid.LayoutTransform = new ScaleTransform(ratioWidth, ratioHeight);
            this.Left = screen.Bounds.X;
            this.Top = screen.Bounds.Y;
            this.Width = screen.Bounds.Width;
            this.Height = screen.Bounds.Height;
        }

        public void ShowLeaderboard(int type)
        {
            Leaderboard.Visibility = System.Windows.Visibility.Hidden;

            _isShowing = true;
            _type = type;
            if (type == 1)
            {
                LeaderboardBackground.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Top 10.mpg");
            }
            else
            {
                LeaderboardBackground.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Videos/Background Top 50.mpg");
            }
            LeaderboardBackground.Play();
        }

        public void ShowTop(List<UserModel> top)
        {
            _list = top;
            _page = 1;
            GenerateList(_list.Take(10).ToList());
        }

        private void GenerateList(List<UserModel> top)
        {
            var i = 0;

            var timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(0.5) };
            timer.Start();
            timer.Tick += (s, args) =>
            {
                if (i >= 10)
                {
                    timer.Stop();
                    if (_leaderboardAudio != null)
                    {
                        _leaderboardAudio.Stop();
                    }

                    if (_type == 2)
                    {
                        ++_page;
                        if ((_page - 1) * 10 < _list.Count)
                        {
                            // Next page
                            var timer2 = new DispatcherTimer { Interval = TimeSpan.FromSeconds(10) }; // Delay 10s to read
                            timer2.Start();
                            timer2.Tick += (s2, args2) =>
                            {
                                timer2.Stop();
                                GenerateList(_list.Skip((_page - 1) * 10).Take(10).ToList());
                            };
                        }
                    }
                }
                else
                {
                    var content = "";
                    content += ((_page - 1) * 10 + i + 1).ToString("D2").PadLeft(3);
                    content += (i < top.Count ? top[i].ID.ToString("D2") : "").PadLeft(9);
                    content += (i < top.Count ? (top[i].Score.HasValue ? top[i].Score.Value.ToString() : "0") : "").PadLeft(12);

                    FlipControl flip = Leaderboard.Children.Cast<UIElement>().First(e => Grid.GetRow(e) == i + 1 && Grid.GetColumn(e) == 0) as FlipControl;
                    flip.StartFlipping(content);
                    ++i;

                    if (_leaderboardAudio != null)
                    {
                        _leaderboardAudio.Stop();
                        _leaderboardAudio.Play();
                    }
                }
            };
        }

        private void BackgroundLeaderboard_MediaOpened(object sender, RoutedEventArgs e)
        {
            if (_isShowing == false)
            {
                return;
            }

            _isShowing = false;

            Leaderboard.Children.RemoveRange(1, Leaderboard.Children.Count);

            var timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(1) };
            timer.Start();
            timer.Tick += (s, args) =>
            {
                timer.Stop();
                for (var i = 0; i < 10; i++)
                {
                    var text = new FlipControl();
                    Grid.SetRow(text, i + 1);
                    Grid.SetColumn(text, 0);
                    Leaderboard.Children.Add(text);
                }

                Leaderboard.Visibility = System.Windows.Visibility.Visible;
            };
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _leaderboardAudio = new MediaPlayer();
            _leaderboardAudio.Open(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Images/Connected.wav"));
        }
    }
}
